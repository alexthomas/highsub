package com.alexk8s.highsub.ffmpeg.operation;

import com.alexk8s.highsub.ffmpeg.FfmpegTools;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class ResizeTest {

    @Test
    void getArguments() {
        Resize operation = new Resize("source.mp4", 100, "mp4");
        List<Argument> arguments = operation.getArguments();
        assertEquals(3, arguments.size());
        assertEquals(Argument.of("-i", "source.mp4"), arguments.get(0));
        assertEquals(Argument.of("-vf", "scale=-2:100"), arguments.get(1));
        assertTrue(arguments.get(2).getArguments().get(0).endsWith("mp4"));


    }

    @Test
    void execute() {
        Resize operation = spy(new Resize("source.mp4", 100, "mp4"));
        doReturn(null).when(operation).getArguments();
        String output = operation.execute(mock(FfmpegTools.class));
        assertTrue(output.endsWith("mp4"));
    }
}