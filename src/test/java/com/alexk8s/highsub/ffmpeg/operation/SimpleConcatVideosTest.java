package com.alexk8s.highsub.ffmpeg.operation;

import com.alexk8s.highsub.ffmpeg.FfmpegTools;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;

class SimpleConcatVideosTest {

    @Test
    void getArguments() {
        SimpleConcatVideos operation = new SimpleConcatVideos(null, "mp4");
        List<Argument> arguments = operation.getArguments();
        assertEquals(5, arguments.size());
        assertEquals(Argument.of("-safe", "0"), arguments.get(0));
        assertEquals(Argument.of("-f", "concat"), arguments.get(1));
        assertEquals("-i", arguments.get(2).getArguments().get(0));
        assertTrue(arguments.get(2).getArguments().get(1).endsWith("text"));
        assertEquals(Argument.of("-c", "copy"), arguments.get(3));
        assertTrue(arguments.get(4).getArguments().get(0).endsWith("mp4"));
    }

    @Test
    void execute() {
        SimpleConcatVideos operation = new SimpleConcatVideos(List.of("source1.mp4", "source2.mp4"), "mp4");
        String output = operation.execute(mock(FfmpegTools.class));
        assertTrue(output.endsWith("mp4"));
    }
}