package com.alexk8s.highsub.ffmpeg.operation;

import com.alexk8s.highsub.ffmpeg.FfmpegTools;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;

class ComplexConcatVideosTest {

    @Test
    void getFilterComplex() {
        ComplexConcatVideos operation = new ComplexConcatVideos(List.of(""), "mp4");
        Argument filterComplex = operation.getFilterComplex();
        assertEquals(2, filterComplex.getArguments().size());
        assertEquals("-filter_complex", filterComplex.getArguments().get(0));
        //maybe I'll try and verify that the output is correct later....
    }

    @Test
    void getArguments() {
        ComplexConcatVideos operation = spy(new ComplexConcatVideos(List.of("test.mp4", "test2.mp4"), "mp4"));
        doReturn(Argument.of("-filter_complex", "test")).when(operation).getFilterComplex();
        List<Argument> arguments = operation.getArguments();
        assertEquals(7, arguments.size());
        assertEquals(Argument.of("-i", "test.mp4"), arguments.get(0));
        assertEquals(Argument.of("-i", "test2.mp4"), arguments.get(1));
        assertEquals(Argument.of("-filter_complex", "test"), arguments.get(2));
        assertEquals(Argument.of("-map", "[v]"), arguments.get(3));
        assertEquals(Argument.of("-map", "[a]"), arguments.get(4));
        assertEquals(Argument.of("-vsync", "2"), arguments.get(5));
        assertTrue(arguments.get(6).getArguments().get(0).endsWith(".mp4"));
    }

    @Test
    void execute() {
        ComplexConcatVideos operation = spy(new ComplexConcatVideos(List.of("test.mp4", "test2.mp4"), "mp4"));
        doReturn(null).when(operation).getArguments();
        String result = operation.execute(mock(FfmpegTools.class));
        assertTrue(result.endsWith(".mp4"));
    }
}