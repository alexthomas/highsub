package com.alexk8s.highsub.ffmpeg.operation;

import com.alexk8s.highsub.ffmpeg.FfmpegTools;
import org.junit.jupiter.api.Test;

import java.nio.file.NoSuchFileException;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

class ExtractVideoTest {

    @Test()
    void constructor_withInvalidEncoding() {
        ExtractVideo.ExtractVideoBuilder builder = ExtractVideo.builder().videoEncoding("libx264");
        assertThrows(IllegalArgumentException.class, builder::build);
    }

    @Test
    void getFilter_empty() {
        assertTrue(ExtractVideo.builder().build().getFilter().isEmpty());
    }

    @Test
    void getFilter_withHeight() {
        ExtractVideo operation = ExtractVideo.builder().height(720).build();
        Optional<Argument> filter = operation.getFilter();
        assertTrue(filter.isPresent());
        assertEquals(Argument.of("-vf", "scale=-2:720"), filter.get());
    }

    @Test
    void getFilter_withSubtitles() {
        ExtractVideo operation = ExtractVideo.builder().subtitlePath("subtitles.srt").build();
        Optional<Argument> filter = operation.getFilter();
        assertTrue(filter.isPresent());
        assertEquals(Argument.of("-vf", "subtitles=subtitles.srt"), filter.get());
    }

    @Test
    void getFilter_withFullEncode() {
        ExtractVideo operation = ExtractVideo.builder().fullEncode(true).build();
        Optional<Argument> filter = operation.getFilter();
        assertTrue(filter.isPresent());
        assertEquals(Argument.of("-vf", "format=yuv420p"), filter.get());
    }

    @Test
    void getFilter_withAll() {
        ExtractVideo operation = ExtractVideo.builder().height(720).subtitlePath("subtitles.srt").fullEncode(true).build();
        Optional<Argument> filter = operation.getFilter();
        assertTrue(filter.isPresent());
        assertEquals(Argument.of("-vf", "scale=-2:720,subtitles=subtitles.srt,format=yuv420p"), filter.get());
    }


    @Test
    void getArguments() {
        ExtractVideo operation = spy(ExtractVideo.builder().videoPath("test.mp4").format("mp4").build());
        doReturn(Optional.empty()).when(operation).getFilter();
        List<Argument> arguments = operation.getArguments();
        assertEquals(7, arguments.size());
        assertEquals(Argument.of("-nostats"), arguments.get(0));
        assertEquals(Argument.of("-i", "test.mp4"), arguments.get(1));
        assertEquals(Argument.of("-copyts"), arguments.get(2));
        assertEquals(Argument.of("-c:v", "libx264"), arguments.get(3));
        assertEquals(Argument.of("-preset", "ultrafast"), arguments.get(4));
        assertEquals(Argument.of("-crf", "22"), arguments.get(5));
        assertTrue(arguments.get(6).getArguments().get(0).endsWith(".mp4"));
    }

    @Test
    void getArguments_withStartTime() {
        ExtractVideo operation = spy(ExtractVideo.builder().videoPath("test.mp4").format("mp4").startTime(1000L).build());
        doReturn(Optional.empty()).when(operation).getFilter();
        List<Argument> arguments = operation.getArguments();
        assertEquals(9, arguments.size());
        assertEquals(Argument.of("-nostats"), arguments.get(0));
        assertEquals(Argument.of("-ss", "00:00:01.000"), arguments.get(1));
        assertEquals(Argument.of("-i", "test.mp4"), arguments.get(2));
        assertEquals(Argument.of("-copyts"), arguments.get(3));
        assertEquals(Argument.of("-ss", "00:00:01.000"), arguments.get(4));
        assertEquals(Argument.of("-c:v", "libx264"), arguments.get(5));
        assertEquals(Argument.of("-preset", "ultrafast"), arguments.get(6));
        assertEquals(Argument.of("-crf", "22"), arguments.get(7));
        assertTrue(arguments.get(8).getArguments().get(0).endsWith(".mp4"));
    }

    @Test
    void getArguments_withEndTime() {
        ExtractVideo operation = spy(ExtractVideo.builder().videoPath("test.mp4").format("mp4").startTime(1000L).endTime(4000L).build());
        doReturn(Optional.empty()).when(operation).getFilter();
        List<Argument> arguments = operation.getArguments();
        assertEquals(11, arguments.size());
        assertEquals(Argument.of("-nostats"), arguments.get(0));
        assertEquals(Argument.of("-ss", "00:00:01.000"), arguments.get(1));
        assertEquals(Argument.of("-t", "00:00:03.000"), arguments.get(2));
        assertEquals(Argument.of("-i", "test.mp4"), arguments.get(3));
        assertEquals(Argument.of("-copyts"), arguments.get(4));
        assertEquals(Argument.of("-ss", "00:00:01.000"), arguments.get(5));
        assertEquals(Argument.of("-t", "00:00:03.000"), arguments.get(6));
        assertEquals(Argument.of("-c:v", "libx264"), arguments.get(7));
        assertEquals(Argument.of("-preset", "ultrafast"), arguments.get(8));
        assertEquals(Argument.of("-crf", "22"), arguments.get(9));
        assertTrue(arguments.get(10).getArguments().get(0).endsWith(".mp4"));
    }

    @Test
    void getArguments_withCustomEncoding() {
        ExtractVideo operation = spy(ExtractVideo.builder().videoPath("test.mp4").format("mp4").videoEncoding("fictionalx9000").encodingPreset("ludicrousfast").crf("1000").build());
        doReturn(Optional.empty()).when(operation).getFilter();
        List<Argument> arguments = operation.getArguments();
        assertEquals(7, arguments.size());
        int cursor=0;
        assertEquals(Argument.of("-nostats"), arguments.get(cursor++));
        assertEquals(Argument.of("-i", "test.mp4"), arguments.get(cursor++));
        assertEquals(Argument.of("-copyts"), arguments.get(cursor++));
        assertEquals(Argument.of("-c:v", "fictionalx9000"), arguments.get(cursor++));
        assertEquals(Argument.of("-preset", "ludicrousfast"), arguments.get(cursor++));
        assertEquals(Argument.of("-crf", "1000"), arguments.get(cursor++));
        assertTrue(arguments.get(cursor++).getArguments().get(0).endsWith(".mp4"));

    }

    @Test
    void getArguments_withAudioIndex() {
        ExtractVideo operation = spy(ExtractVideo.builder().videoPath("test.mp4").format("mp4").audioIndex(4).build());
        doReturn(Optional.empty()).when(operation).getFilter();
        List<Argument> arguments = operation.getArguments();
        assertEquals(8, arguments.size());
        assertEquals(Argument.of("-nostats"), arguments.get(0));
        assertEquals(Argument.of("-i", "test.mp4"), arguments.get(1));
        assertEquals(Argument.of("-copyts"), arguments.get(2));
        assertEquals(Argument.of("-c:v", "libx264"), arguments.get(3));
        assertEquals(Argument.of("-preset", "ultrafast"), arguments.get(4));
        assertEquals(Argument.of("-crf", "22"), arguments.get(5));
        assertEquals(Argument.of("-map", "0:4"), arguments.get(6));
        assertTrue(arguments.get(7).getArguments().get(0).endsWith(".mp4"));
    }

    @Test
    void getArguments_withVideoIndex() {
        ExtractVideo operation = spy(ExtractVideo.builder().videoPath("test.mp4").format("mp4").videoIndex(4).build());
        doReturn(Optional.empty()).when(operation).getFilter();
        List<Argument> arguments = operation.getArguments();
        assertEquals(8, arguments.size());
        int cursor = 0;
        assertEquals(Argument.of("-nostats"), arguments.get(cursor++));
        assertEquals(Argument.of("-i", "test.mp4"), arguments.get(cursor++));
        assertEquals(Argument.of("-copyts"), arguments.get(cursor++));
        assertEquals(Argument.of("-c:v", "libx264"), arguments.get( cursor++));
        assertEquals(Argument.of("-preset", "ultrafast"), arguments.get(cursor++));
        assertEquals(Argument.of("-crf", "22"), arguments.get(cursor++));
        assertEquals(Argument.of("-map", "0:4"), arguments.get( cursor++));
        assertTrue(arguments.get(cursor++).getArguments().get(0).endsWith(".mp4"));
    }

    @Test
    void getArguments_withFullEncode() {
        ExtractVideo operation = spy(ExtractVideo.builder().videoPath("test.mp4").format("mp4").fullEncode(true).build());
        doReturn(Optional.empty()).when(operation).getFilter();
        List<Argument> arguments = operation.getArguments();
        assertEquals(6, arguments.size());
        int cursor = 0;
        assertEquals(Argument.of("-nostats"), arguments.get(cursor++));
        assertEquals(Argument.of("-i", "test.mp4"), arguments.get(cursor++));
        assertEquals(Argument.of("-copyts"), arguments.get(cursor++));
        assertEquals(Argument.of("-dn"), arguments.get(cursor++));
        assertEquals(Argument.of("-map_metadata:c", "-1"), arguments.get(   cursor++));
        assertTrue(arguments.get(cursor++).getArguments().get(0).endsWith(".mp4"));
    }

    @Test
    void getArguments_withFilter() {
        ExtractVideo operation = spy(ExtractVideo.builder().videoPath("test.mp4").format("mp4").height(720).subtitlePath("subtitles.srt").fullEncode(true).build());
        List<Argument> arguments = operation.getArguments();
        assertEquals(7, arguments.size());
        int cursor = 0;
        assertEquals(Argument.of("-nostats"), arguments.get(cursor++));
        assertEquals(Argument.of("-i", "test.mp4"), arguments.get(cursor++));
        assertEquals(Argument.of("-copyts"), arguments.get(cursor++));
        assertEquals(Argument.of("-dn"), arguments.get(cursor++));
        assertEquals(Argument.of("-map_metadata:c", "-1"), arguments.get(cursor++));
        assertEquals(Argument.of("-vf", "scale=-2:720,subtitles=subtitles.srt,format=yuv420p"), arguments.get(cursor++));
        assertTrue(arguments.get(cursor++).getArguments().get(0).endsWith(".mp4"));
    }

    @Test
    void getRemoveChaptersArguments() {
        List<Argument> removeChaptersArguments = ExtractVideo.builder().build().getRemoveChaptersArguments("source.mp4", "output.mp4");
        assertEquals(5, removeChaptersArguments.size());
        int cursor = 0;
        assertEquals(Argument.of("-nostats"), removeChaptersArguments.get(cursor++));
        assertEquals(Argument.of("-ignore_chapters", "1"), removeChaptersArguments.get(cursor++));
        assertEquals(Argument.of("-i", "source.mp4"), removeChaptersArguments.get(cursor++));
        assertEquals(Argument.of("-c","copy"), removeChaptersArguments.get(cursor++));
        assertEquals(Argument.of("output.mp4"), removeChaptersArguments.get(cursor));
    }

    @Test
    void execute() {
        ExtractVideo operation = spy(ExtractVideo.builder().videoPath("test.mp4").format("mp4").build());
        doReturn(null).when(operation).getArguments();
        FfmpegTools ffmpegTools = mock(FfmpegTools.class);
        assertTrue(operation.execute(ffmpegTools).endsWith(".mp4"));
        verify(ffmpegTools, times(1)).execute(any(), any());
    }

    @Test
    void execute_withFullEncode() {
        ExtractVideo operation = spy(ExtractVideo.builder().videoPath("test.mp4").format("mp4").fullEncode(true).build());
        doReturn(null).when(operation).getArguments();
        FfmpegTools ffmpegTools = mock(FfmpegTools.class);
        assertThrows(NoSuchFileException.class, () -> operation.execute(ffmpegTools)); //the file isn't actually created so when it gets deleted it should throw an error
        verify(ffmpegTools, times(2)).execute(any(), any());
    }
}