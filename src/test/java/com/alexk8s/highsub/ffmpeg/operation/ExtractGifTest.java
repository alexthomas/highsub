package com.alexk8s.highsub.ffmpeg.operation;

import com.alexk8s.highsub.ffmpeg.FfmpegTools;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;

class ExtractGifTest {

    @Test
    void getFilter() {
        ExtractGif operation = new ExtractGif("test.mp4", 1, null, 0L, 0L, null);
        Argument actual = operation.getFilter();
        Argument expected = Argument.of("-vf", "fps=10,split[s0][s1];[s0]palettegen[p];[s1][p]paletteuse");
        assertEquals(expected, actual);
    }


    @Test
    void getFilter_withHeight() {
        ExtractGif operation = new ExtractGif("test.mp4", 1, null, 0L, 0L, 100);
        Argument actual = operation.getFilter();
        Argument expected = Argument.of("-vf", "fps=10,split[s0][s1];[s0]palettegen[p];[s1][p]paletteuse,scale=-2:100:flags=lanczos");
        assertEquals(expected, actual);
    }

    @Test
    void getFilter_withSubtitle() {
        ExtractGif operation = new ExtractGif("test.mp4", 1, "subs.srt", 0L, 0L, null);
        Argument actual = operation.getFilter();
        Argument expected = Argument.of("-vf", "fps=10,split[s0][s1];[s0]palettegen[p];[s1][p]paletteuse,subtitles=subs.srt");
        assertEquals(expected, actual);
    }

    @Test
    void getArguments() {
        ExtractGif operation = spy(new ExtractGif("test.mp4", null, "subs.srt", 0L, 1000L, null));
        doReturn(null).when(operation).getFilter();
        List<Argument> arguments = operation.getArguments();
        assertEquals(5, arguments.size());
        assertEquals(Argument.of("-ss", "00:00:00.000"), arguments.get(0));
        assertEquals(Argument.of("-t", "00:00:01.000"), arguments.get(1));
        assertEquals(Argument.of("-i", "test.mp4"), arguments.get(2));
        assertNull(arguments.get(3));
        assertTrue(arguments.get(4).getArguments().get(0).endsWith(".gif"));
    }

    @Test
    void getArguments_withVideoIndex() {
        ExtractGif operation = spy(new ExtractGif("test.mp4", 4, "subs.srt", 0L, 1000L, null));
        doReturn(null).when(operation).getFilter();
        List<Argument> arguments = operation.getArguments();
        assertEquals(6, arguments.size());
        assertEquals(Argument.of("-ss", "00:00:00.000"), arguments.get(0));
        assertEquals(Argument.of("-t", "00:00:01.000"), arguments.get(1));
        assertEquals(Argument.of("-i", "test.mp4"), arguments.get(2));
        assertEquals(Argument.of("-map", "0:4"), arguments.get(3));
        assertNull(arguments.get(4));
        assertTrue(arguments.get(5).getArguments().get(0).endsWith(".gif"));
    }

    @Test
    void execute() {
        ExtractGif operation = spy(new ExtractGif("test.mp4", null, "subs.srt", 0L, 1000L, null));
        doReturn(null).when(operation).getArguments();
        String result = operation.execute(mock(FfmpegTools.class));
        assertTrue(result.endsWith(".gif"));
    }
}