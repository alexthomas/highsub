package com.alexk8s.highsub.ffmpeg.operation;

import com.alexk8s.highsub.ffmpeg.FfmpegTools;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;

class AddTextToGifTest {

    @Test
    void getArguments() {
        AddTextToGif operation = spy(new AddTextToGif("test.gif", "test", 1000));
        doReturn("test").when(operation).buildFiltersForText();
        List<Argument> arguments = operation.getArguments();
        assertEquals(3, arguments.size());
        assertEquals(Argument.of("-i", "test.gif"), arguments.get(0));
        assertEquals(Argument.of("-vf", "test"), arguments.get(1));
        assertTrue(arguments.get(2).getArguments().get(0).endsWith(".gif"));

    }

    @Test
    void execute() {
        AddTextToGif operation = spy(new AddTextToGif("test.gif", "test", 1000));
        doReturn(null).when(operation).getArguments();
        String result = operation.execute(mock(FfmpegTools.class));
        assertTrue(result.endsWith(".gif"));
    }
}