package com.alexk8s.highsub.ffmpeg.operation;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ArgumentTest {

    @Test
    void getArguments() {
        Argument flag = Argument.of("-f");
        assertEquals(1, flag.getArguments().size());
        assertEquals("-f", flag.getArguments().get(0));

        Argument option = Argument.of("-f", "mp4");
        assertEquals(2, option.getArguments().size());
        assertEquals("-f", option.getArguments().get(0));
        assertEquals("mp4", option.getArguments().get(1));
    }

    @Test
    void getSpanTag() {
        Argument flag = Argument.of("-f");
        assertEquals("-f", flag.getSpanTag());
        Argument option = Argument.of("-f", "mp4");
        assertEquals("-f mp4", option.getSpanTag());
    }
}