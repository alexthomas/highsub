package com.alexk8s.highsub.ffmpeg.operation;

import com.alexk8s.highsub.ffmpeg.FfmpegTools;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;

class ExtractFrameTest {

    @Test
    void getArguments_withSubtitles() {
        ExtractFrame operation = new ExtractFrame("test.mp4", 1000L, "png", "80", "test.srt");
        List<Argument> arguments = operation.getArguments();
        assertEquals(7, arguments.size());
        assertEquals(Argument.of("-ss", "00:00:01.000"), arguments.get(0));
        assertEquals(Argument.of("-i", "test.mp4"), arguments.get(1));
        assertEquals(Argument.of("-vf", "subtitles=test.srt,scale=iw*sar:ih"), arguments.get(2));
        assertEquals(Argument.of("-vframes", "1"), arguments.get(3));
        assertEquals(Argument.of("-q:v", "80"), arguments.get(4));
        assertEquals(Argument.of("-copyts"), arguments.get(5));
        assertTrue(arguments.get(6).getArguments().get(0).endsWith(".png"));
    }

    @Test
    void getArguments_withoutSubtitles() {
        ExtractFrame operation = new ExtractFrame("test.mp4", 1000L, "png", "80", null);
        List<Argument> arguments = operation.getArguments();
        assertEquals(7, arguments.size());
        assertEquals(Argument.of("-ss", "00:00:01.000"), arguments.get(0));
        assertEquals(Argument.of("-i", "test.mp4"), arguments.get(1));
        assertEquals(Argument.of("-vf", "scale=iw*sar:ih"), arguments.get(2));
        assertEquals(Argument.of("-vframes", "1"), arguments.get(3));
        assertEquals(Argument.of("-q:v", "80"), arguments.get(4));
        assertEquals(Argument.of("-copyts"), arguments.get(5));
        assertTrue(arguments.get(6).getArguments().get(0).endsWith(".png"));

    }

    @Test
    void execute() {
        ExtractFrame operation = spy(new ExtractFrame("test.mp4", 1000L, "png", "80", "test.srt"));
        doReturn(null).when(operation).getArguments();
        String result = operation.execute(mock(FfmpegTools.class));
        assertTrue(result.endsWith(".png"));
    }
}