package com.alexk8s.highsub.ffmpeg.operation;

import com.alexk8s.highsub.ffmpeg.FfmpegTools;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

class GetDurationTest {

    @Test
    void getArguments() {
        GetDuration operation = new GetDuration("test.mp4");
        List<Argument> arguments = operation.getArguments();
        assertEquals(4, arguments.size());
        assertEquals(Argument.of("-v", "error"), arguments.get(0));
        assertEquals(Argument.of("-show_entries", "format=duration"), arguments.get(1));
        assertEquals(Argument.of("-of", "default=noprint_wrappers=1:nokey=1"), arguments.get(2));
        assertEquals(Argument.of("test.mp4"), arguments.get(3));
    }

    @Test
    void execute() {
        GetDuration operation = spy(new GetDuration("test.mp4"));
        doReturn(null).when(operation).getArguments();
        FfmpegTools ffmpegTools = mock(FfmpegTools.class);
        when(ffmpegTools.execute(operation.getFunction(), operation.getArguments())).thenReturn("1.3");
        Long duration = operation.execute(ffmpegTools);
        assertEquals(1300, duration);
    }
}