package com.alexk8s.highsub.ffmpeg.operation;

import com.alexk8s.highsub.ffmpeg.FfmpegTools;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;

class ExtractBitmapTest {

    @Test
    void of() {
        ExtractBitmap operation = ExtractBitmap.of("test.mp4", 1000L);
        List<Argument> arguments = operation.getArguments();
        assertEquals(7, arguments.size());
        assertEquals(Argument.of("-ss", "00:00:01.000"), arguments.get(0));
        assertEquals(Argument.of("-i", "test.mp4"), arguments.get(1));
        assertEquals(Argument.of("-vframes", "1"), arguments.get(2));
        assertEquals(Argument.of("-vsync", "0"), arguments.get(3));
        assertEquals(Argument.of("-update", "1"), arguments.get(4));
        assertEquals(Argument.of("-copyts"), arguments.get(5));
        assertTrue(arguments.get(6).getArguments().get(0).endsWith(".bmp"));

    }

    @Test
    void ofLastFrame() {
        ExtractBitmap operation = ExtractBitmap.ofLastFrame("test.mp4");
        List<Argument> arguments = operation.getArguments();
        assertEquals(7, arguments.size());
        assertEquals(Argument.of("-sseof", "-1"), arguments.get(0));
        assertEquals(Argument.of("-i", "test.mp4"), arguments.get(1));
        assertEquals(Argument.of("-vframes", "1"), arguments.get(2));
        assertEquals(Argument.of("-vsync", "0"), arguments.get(3));
        assertEquals(Argument.of("-update", "1"), arguments.get(4));
        assertEquals(Argument.of("-copyts"), arguments.get(5));
        assertTrue(arguments.get(6).getArguments().get(0).endsWith(".bmp"));
    }


    @Test
    void execute() {
        ExtractBitmap operation = spy(ExtractBitmap.of("test.mp4", 1000L));
        String result = operation.execute(mock(FfmpegTools.class));
        assertTrue(result.endsWith(".bmp"));
    }
}