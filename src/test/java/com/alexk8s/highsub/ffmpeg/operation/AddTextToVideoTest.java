package com.alexk8s.highsub.ffmpeg.operation;

import com.alexk8s.highsub.ffmpeg.FfmpegTools;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;

class AddTextToVideoTest {

    @Test
    void getArguments() {
        AddTextToVideo operation = spy(new AddTextToVideo("test.mp4", "mp4", "test", 1000));
        doReturn("test").when(operation).buildFiltersForText();
        List<Argument> arguments = operation.getArguments();
        assertEquals(4, arguments.size());
        assertEquals(Argument.of("-i", "test.mp4"), arguments.get(0));
        assertEquals(Argument.of("-vf", "test"), arguments.get(1));
        assertEquals(Argument.of("-c:a", "copy"), arguments.get(2));
        assertTrue(arguments.get(3).getArguments().get(0).endsWith(".mp4"));
    }

    @Test
    void execute() {
        AddTextToVideo operation = spy(new AddTextToVideo("test.mp4", "mp4", "test", 1000));
        doReturn(null).when(operation).getArguments();
        String result = operation.execute(mock(FfmpegTools.class));
        assertTrue(result.endsWith(".mp4"));
    }
}