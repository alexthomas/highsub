package com.alexk8s.highsub.ffmpeg.operation;

import com.alexk8s.highsub.ffmpeg.FfmpegTools;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AddTextOperationTest {

    @Test
    void buildFiltersForText() {
        String text = "This is a test";
        String expected = "pad=width=iw" //width doesn't need to be padded
                + ":height=ih+55:y=55" //height should be padded by 20 + 35 per line
                + ":color=white"
                + ","
                + "drawtext=font='sans-serif'" //configure the font to be sans-serif
                + ":text='This is a test" //the text to be added
                + "':fontcolor=black" //the color of the text
                + ":fontsize=30" //the size of the text
                + ":line_spacing=5" //the spacing between lines
                + ":x=(w-text_w)/2" //the x position of the text
                + ":y=20"; //the y position of the text

        String actual = new AddTextOperation(text, 1000) {
            @Override
            public List<Argument> getArguments() {
                return null;
            }

            @Override
            public String execute(FfmpegTools ffmpegTools) {
                return null;
            }
        }.buildFiltersForText();
        assertEquals(expected, actual);
    }
}