package com.alexk8s.highsub.ffmpeg.operation;

import com.alexk8s.highsub.ffmpeg.FfmpegTools;
import com.alexk8s.highsub.model.SubtitleFormat;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.function.Consumer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

class ExtractSubtitlesTest {

    @Test
    void getArguments() {
        ExtractSubtitles operation = new ExtractSubtitles("test.mp4", SubtitleFormat.SRT, 1, null);
        List<Argument> arguments = operation.getArguments();
        assertEquals(4, arguments.size());
        assertEquals(Argument.of("-i", "test.mp4"), arguments.get(0));
        assertEquals(Argument.of("-map", "0:1"), arguments.get(1));
        assertEquals(Argument.of("-progress", "-"), arguments.get(2));
        assertTrue(arguments.get(3).getArguments().get(0).endsWith(".srt"));

    }

    @Test
    void onProgressUpdate() {
        ExtractSubtitles operation = new ExtractSubtitles("test.mp4", SubtitleFormat.SRT, 1, null);
        operation.onProgressUpdate("any");
        Consumer<Long> progressConsumer = mock(Consumer.class);
        operation = spy(new ExtractSubtitles("test.mp4", SubtitleFormat.SRT, 1, progressConsumer));
        doReturn(19L).when(operation).parseProgress(anyString());
        operation.onProgressUpdate("any");
        verify(progressConsumer).accept(19L);
        verify(operation).parseProgress("any");

    }

    @Test
    void parseProgress() {
        ExtractSubtitles operation = new ExtractSubtitles("test.mp4", SubtitleFormat.SRT, 1, null);
        assertEquals(-1L, operation.parseProgress("progress=end"));
        assertEquals(-1L, operation.parseProgress("progress=frame=1"));
        assertEquals(-1L, operation.parseProgress("out_time="));
        assertEquals(-1L, operation.parseProgress("out_time=-2"));
        assertEquals(1000L, operation.parseProgress("out_time=00:00:01.000"));

    }

    @Test
    void execute() {
        ExtractSubtitles operation = spy(new ExtractSubtitles("test.mp4", SubtitleFormat.SRT, 1, null));
        doReturn(null).when(operation).getArguments();
        String result = operation.execute(mock(FfmpegTools.class));
        assertTrue(result.endsWith(".srt"));
    }
}