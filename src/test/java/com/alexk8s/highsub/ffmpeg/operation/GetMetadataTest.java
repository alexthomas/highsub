package com.alexk8s.highsub.ffmpeg.operation;

import com.alexk8s.highsub.ffmpeg.FfmpegTools;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;

class GetMetadataTest {

    @Test
    void getCacheKey() {
        GetMetadata operation = new GetMetadata("test.mp4", "video");
        assertEquals("test.mp4|video", operation.getCacheKey());
    }

    @Test
    void getArguments() {
        GetMetadata operation = new GetMetadata("test.mp4", "video");
        List<Argument> arguments = operation.getArguments();
        assertEquals(6, arguments.size());
        assertEquals(Argument.of("-v", "error"), arguments.get(0));
        assertEquals(Argument.of("-of", "json"), arguments.get(1));
        assertEquals(Argument.of("test.mp4"), arguments.get(2));
        assertEquals(Argument.of("-show_chapters"), arguments.get(3));
        assertEquals(Argument.of("-show_entries", "stream=index,width,height,codec_name,r_frame_rate,codec_type:disposition=forced:stream_tags=language,title"), arguments.get(4));
        assertEquals(Argument.of("-select_streams", "v"), arguments.get(5));
    }

    @Test
    void execute() {
        GetMetadata operation = spy(new GetMetadata("test.mp4", "video"));
        FfmpegTools ffmpegTools = mock(FfmpegTools.class);
        doReturn(null).when(operation).getArguments();
        doReturn("{}").when(ffmpegTools).execute(any(), any());
        operation.execute(ffmpegTools);
        operation.execute(ffmpegTools);

    }
}