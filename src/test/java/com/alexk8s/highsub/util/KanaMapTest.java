package com.alexk8s.highsub.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class KanaMapTest {

    @Test
    void loadKana() {
        new KanaMap().loadKana();
    }

    @Test
    void getHiragana() {
        KanaMap kanaMap = new KanaMap();
        kanaMap.loadKana();
        assertEquals("あ", kanaMap.getHiragana("ア"));
    }

    @Test
    void getRomaji() {
        KanaMap kanaMap = new KanaMap();
        kanaMap.loadKana();
        assertEquals("a", kanaMap.getRomaji("ア"));
        assertEquals("tsu", kanaMap.getRomaji("ツ"));
    }

    @Test
    void getRomaji_littleVowel(){
        KanaMap kanaMap = new KanaMap();
        kanaMap.loadKana();
        assertEquals("fi", kanaMap.getRomaji("フィ"));
        assertEquals("sheru", kanaMap.getRomaji("シェル"));
    }
}