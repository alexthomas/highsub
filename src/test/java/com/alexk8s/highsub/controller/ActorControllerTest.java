package com.alexk8s.highsub.controller;

import com.alexk8s.highsub.scrape.ActorRole;
import com.alexk8s.highsub.scrape.ActorRoleRepository;
import com.alexk8s.highsub.scrape.ActorRoleRule;
import com.alexk8s.highsub.service.ActorService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Answers;
import org.neo4j.driver.internal.value.FloatValue;
import org.neo4j.driver.internal.value.ListValue;
import org.neo4j.driver.internal.value.StringValue;
import org.springframework.data.neo4j.core.Neo4jClient;
import org.springframework.data.neo4j.core.ReactiveNeo4jClient;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class ActorControllerTest {

    private ReactiveNeo4jClient neo4jClient;
    private ActorService actorService;
    private ActorRoleRepository actorRoleRepository;
    private ActorController actorController;

    @BeforeEach
    void setUp() {
        neo4jClient = mock(ReactiveNeo4jClient.class, Answers.RETURNS_MOCKS);
        actorService = mock(ActorService.class);
        actorRoleRepository = mock(ActorRoleRepository.class);
        actorController = spy( new ActorController(neo4jClient, actorService, actorRoleRepository));
    }

    @Test
    void getSameActorV2() {
        ReactiveNeo4jClient.UnboundRunnableSpec unboundRunnableSpec = mock(ReactiveNeo4jClient.UnboundRunnableSpec.class);
        Neo4jClient.OngoingBindSpec<String, ReactiveNeo4jClient.RunnableSpec> ongoingBindSpec = mock(Neo4jClient.OngoingBindSpec.class);
        ReactiveNeo4jClient.RunnableSpec runnableSpec = mock(ReactiveNeo4jClient.RunnableSpec.class);
        ReactiveNeo4jClient.MappingSpec<ActorController.SameActor2> mappingSpec = mock(ReactiveNeo4jClient.MappingSpec.class);
        ReactiveNeo4jClient.RecordFetchSpec<ActorController.SameActor2> recordFetchSpec = mock(ReactiveNeo4jClient.RecordFetchSpec.class);

        when(neo4jClient.query(anyString())).thenReturn(unboundRunnableSpec);
        when(unboundRunnableSpec.bind(anyString())).thenReturn(ongoingBindSpec);
        when(ongoingBindSpec.to(anyString())).thenReturn(runnableSpec);
        when(runnableSpec.fetchAs(eq(ActorController.SameActor2.class))).thenReturn(mappingSpec);
        when(mappingSpec.mappedBy(any())).thenReturn(recordFetchSpec);
        when(recordFetchSpec.all()).thenReturn(Flux.empty());

        Flux<ActorController.SameActor2> sameActorV2 = actorController.getSameActorV2("show");
        assertNotNull(sameActorV2);

    }

    @Test
    void mapRecord() {
        org.neo4j.driver.Record record = mock(org.neo4j.driver.Record.class);
        when(record.get("actorName")).thenReturn(new StringValue("actor"));
        when(record.get("originalShow")).thenReturn(new StringValue("originalShow"));
        when(record.get("originalCharacters")).thenReturn(new ListValue());
        when(record.get("otherCharacters")).thenReturn(new ListValue());
        when(record.get("score")).thenReturn(new FloatValue(2.5));

        ActorController.SameActor2 sameActor2 = actorController.mapRecord(record);
        assertNotNull(sameActor2);
        assertEquals("actor", sameActor2.actorName());
        assertEquals("originalShow", sameActor2.originalShow());
        assertEquals(0, sameActor2.originalShowCharacters().size());
        assertEquals(0, sameActor2.otherShowCharacters().size());
        assertEquals(2.5, sameActor2.score());

    }

    @Test
    void getRolesByActor() {
        List<ActorRole> actorRoles = List.of(
                ActorRole.builder().actor("Small time actor").show("Show A").characterRole("small").build(),
                ActorRole.builder().actor("Small time actor").show("Show B").characterRole("small").build(),
                ActorRole.builder().actor("Medium time actor").characterRole("medium").build(),
                ActorRole.builder().actor("Big time actor").characterRole("big").build()
        );

        when(actorRoleRepository.findByActor(anyString())).thenReturn(Flux.fromIterable(actorRoles));
        doReturn(1).when(actorController).getRoleValue("small");
        doReturn(2).when(actorController).getRoleValue("medium");
        doReturn(3).when(actorController).getRoleValue("big");

        Flux<ActorRole> rolesByActor = actorController.getRolesByActor("actor");

        StepVerifier.create(rolesByActor)
                .expectNextMatches(actorRole -> actorRole.getActor().equals("Big time actor"))
                .expectNextMatches(actorRole -> actorRole.getActor().equals("Medium time actor"))
                .expectNextMatches(actorRole -> actorRole.getActor().equals("Small time actor") && actorRole.getShow().equals("Show B"))
                .expectNextMatches(actorRole -> actorRole.getActor().equals("Small time actor") && actorRole.getShow().equals("Show A"))
                .verifyComplete();

    }

    @Test
    void getRolesByShow() {
        List<ActorRole> actorRoles = List.of(
                ActorRole.builder().actor("Small time actor").characterRole("small").build(),
                ActorRole.builder().actor("A Small time actor").characterRole("small").build(),
                ActorRole.builder().actor("Medium time actor").characterRole("medium").build(),
                ActorRole.builder().actor("Big time actor").characterRole("big").build()
        );

        when(actorRoleRepository.findByShow(anyString())).thenReturn(Flux.fromIterable(actorRoles));
        doReturn(1).when(actorController).getRoleValue("small");
        doReturn(2).when(actorController).getRoleValue("medium");
        doReturn(3).when(actorController).getRoleValue("big");

        Flux<ActorRole> rolesByActor = actorController.getRolesByShow("show");

        StepVerifier.create(rolesByActor)
                .expectNextMatches(actorRole -> actorRole.getActor().equals("Big time actor"))
                .expectNextMatches(actorRole -> actorRole.getActor().equals("Medium time actor"))
                .expectNextMatches(actorRole -> actorRole.getActor().equals("Small time actor") )
                .expectNextMatches(actorRole -> actorRole.getActor().equals("A Small time actor") )
                .verifyComplete();

    }

    @Test
    void getRoleValue() {
        assertEquals(0, actorController.getRoleValue(null));
        assertEquals(100, actorController.getRoleValue("main"));
        assertEquals(50, actorController.getRoleValue("major"));
        assertEquals(10, actorController.getRoleValue("supporting"));
        assertEquals(1, actorController.getRoleValue("anything else"));
    }

    @Test
    void saveRule() {
        ActorRoleRule actorRoleRule = new ActorRoleRule();
        actorController.saveRule(actorRoleRule);
        verify(actorService).saveActorRoleRule(actorRoleRule);
    }

    @Test
    void rename() {
        ActorController.ActorRename actorRename = new ActorController.ActorRename("old", "new");
        actorController.rename(actorRename);
        verify(actorService).mapActorName("old","new");
    }
}