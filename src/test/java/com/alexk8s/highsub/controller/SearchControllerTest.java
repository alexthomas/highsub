package com.alexk8s.highsub.controller;

import com.alexk8s.highsub.elasticsearch.SearchCriteria;
import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

class SearchControllerTest {

    @Test
    void parseQuery() {
        SearchCriteria searchCriteria = SearchController.parseQuery("I will find the one piece",0,10);
        SearchCriteria expected = new SearchCriteria();
        expected.setQ("I will find the one piece");
        expected.setTags(Collections.emptyList());
        expected.setBlacklistTags(Collections.emptyList());
        assertEquals(expected, searchCriteria);
    }

    @Test
    void parseQuery_requiredWord() {
        SearchCriteria searchCriteria = SearchController.parseQuery("I will find the \"one piece\" ",0,10  );
        SearchCriteria expected = new SearchCriteria();
        expected.setQ("I will find the one piece");
        expected.setText("one piece");
        expected.setTags(Collections.emptyList());
        expected.setBlacklistTags(Collections.emptyList());
        assertEquals(expected, searchCriteria);

        searchCriteria = SearchController.parseQuery("I will +find the \"one piece\" ",0,10  );
        expected.setText("find one piece");
        assertEquals(expected, searchCriteria);
    }

    @Test
    void parseQuery_filter() {
        SearchCriteria searchCriteria = SearchController.parseQuery("I will find the one piece name:luffy ",0,10);
        SearchCriteria expected = new SearchCriteria();
        expected.setQ("I will find the one piece");
        expected.setName("luffy");
        expected.setTags(Collections.emptyList());
        expected.setBlacklistTags(Collections.emptyList());
        assertEquals(expected, searchCriteria);

        searchCriteria = SearchController.parseQuery("I will find the one piece name:luffy show:\"one piece\" season:\"Season 1\" episode:4 romaji:\"wan pisu\" kanji:\"ワンピース\" ",0,10);
        expected.setShow("one piece");
        expected.setSeason("Season 1");
        expected.setEpisode("4");
        expected.setRomaji("wan pisu");
        expected.setKanji("ワンピース");
        assertEquals(expected, searchCriteria);
    }

    @Test
    void parseQuery_otherQuotes() {
        SearchCriteria searchCriteria = SearchController.parseQuery("romaji:”kono sekai”",0,10);
        SearchCriteria expected = new SearchCriteria();
        expected.setRomaji("kono sekai");
        expected.setTags(Collections.emptyList());
        expected.setBlacklistTags(Collections.emptyList());
        assertEquals(expected, searchCriteria);
    }

    @Test
    void parseQuery_tag() {
        SearchCriteria searchCriteria = SearchController.parseQuery("I will find the one piece #action",0,10);
        SearchCriteria expected = new SearchCriteria();
        expected.setQ("I will find the one piece");
        expected.setTags(Collections.singletonList("action"));
        expected.setBlacklistTags(Collections.emptyList());
        assertEquals(expected, searchCriteria);
    }

    @Test
    void parseQuery_complex() {
        SearchCriteria searchCriteria = SearchController.parseQuery("It's for +world +peace name:Anya show:\"SPY x FAMILY\" #anime",0,10);
        SearchCriteria expected = new SearchCriteria();
        expected.setQ("It's for world peace");
        expected.setText("world peace");
        expected.setName("Anya");
        expected.setShow("SPY x FAMILY");
        expected.setTags(Collections.singletonList("anime"));
        expected.setBlacklistTags(Collections.emptyList());
        assertEquals(expected, searchCriteria);
    }
}