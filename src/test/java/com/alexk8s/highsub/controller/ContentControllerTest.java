package com.alexk8s.highsub.controller;

import com.alexk8s.highsub.configuration.HighsubConfig;
import com.alexk8s.highsub.controller.content.PngContentStrategy;
import com.alexk8s.highsub.ffmpeg.FfmpegTools;
import com.alexk8s.highsub.model.Episode;
import com.alexk8s.highsub.repository.EpisodeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Answers;
import org.springframework.core.io.FileSystemResource;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;
import software.amazon.awssdk.core.ResponseBytes;
import software.amazon.awssdk.core.async.AsyncResponseTransformer;
import software.amazon.awssdk.services.s3.S3AsyncClient;
import software.amazon.awssdk.services.s3.model.GetObjectRequest;
import software.amazon.awssdk.services.s3.model.GetObjectResponse;
import software.amazon.awssdk.services.s3.model.NoSuchKeyException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class ContentControllerTest {

    private FfmpegTools ffmpegTools;
    private HighsubConfig highsubConfig;
    private EpisodeRepository episodeRepository;
    private S3AsyncClient s3Client;
    private ContentController contentController;
    private PngContentStrategy pngContentStrategy;

    @BeforeEach
    void setUp() throws IOException {
        cleanUpOldFiles();
        ffmpegTools = mock(FfmpegTools.class);
        highsubConfig = mock(HighsubConfig.class, Answers.RETURNS_MOCKS);
        episodeRepository = mock(EpisodeRepository.class);
        s3Client = mock(S3AsyncClient.class);
        pngContentStrategy = mock(PngContentStrategy.class);

        contentController = spy(new ContentController(ffmpegTools, highsubConfig,  episodeRepository, s3Client,Collections.emptyList(),pngContentStrategy));
        when(highsubConfig.getCacheRoot()).thenReturn("/tmp");
    }

    void cleanUpOldFiles() throws IOException {
        List<String> files = List.of("generated-file.txt", "test-file.txt");
        for (String file : files) {
            Path path = Path.of(file);
            if (Files.exists(path))
                Files.delete(path);
        }
    }

    @Test
    void getOp() {
        doReturn(Path.of("")).when(contentController).getOpening(any());
        doReturn(Mono.just(new Episode())).when(episodeRepository).findByShowAndSeasonAndEpisode(any(), any(), any());
        Mono<FileSystemResource> op = contentController.getOp("test-show", "test-season", "test-episode");
        StepVerifier.create(op)
                .expectNextCount(1)
                .verifyComplete();
    }



    @Test
    void getPoster_withHappyPath() {
        ResponseBytes<GetObjectResponse> value =  ResponseBytes.fromByteArray(GetObjectResponse.builder().build(), new byte[0]);
        doReturn(CompletableFuture.completedFuture(value)).when(s3Client).getObject(any(GetObjectRequest.class), any(AsyncResponseTransformer.class));
        HighsubConfig.Minio minio = mock(HighsubConfig.Minio.class);
        doReturn(minio).when(highsubConfig).getMinio();
        doReturn("thumbnails").when(minio).getThumbnailBucket();
        contentController.getPoster("test-show",0);
        contentController.getPoster("test-show",100);
    }

    @Test
    void getPoster_withResize(){
        doReturn(CompletableFuture.failedFuture(NoSuchKeyException.builder().build())).when(s3Client).getObject(any(GetObjectRequest.class), any(AsyncResponseTransformer.class));
        HighsubConfig.Minio minio = mock(HighsubConfig.Minio.class);
        doReturn(minio).when(highsubConfig).getMinio();
        doReturn("thumbnails").when(minio).getThumbnailBucket();
        doReturn(Flux.empty()).when(contentController).resizePoster(any(), anyInt());
        StepVerifier.create(contentController.getPoster("test-show",100).getBody())
                .verifyComplete();
        verify(contentController, times(1)).resizePoster(any(), anyInt());
    }
}