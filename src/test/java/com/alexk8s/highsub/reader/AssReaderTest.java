package com.alexk8s.highsub.reader;

import com.alexk8s.highsub.subtitle.reader.AssReader;
import com.alexk8s.highsub.subtitle.reader.ReadSubtitle;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class AssReaderTest {

    @Test
    void readSubtitles() {
        AssReader assReader = new AssReader("subtitles/akira.en.ass", true);
        List<ReadSubtitle> readSubtitles = assReader.readSubtitles();
        assertFalse(readSubtitles.isEmpty());
    }
}