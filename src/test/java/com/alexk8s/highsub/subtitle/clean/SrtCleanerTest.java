package com.alexk8s.highsub.subtitle.clean;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SrtCleanerTest {

    @Test
    void clean() {
        String newLine = new SrtCleaner().clean("abc\ndef");
        assertEquals("abc def", newLine);
    }
}