package com.alexk8s.highsub.subtitle.clean;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BaseCleanerTest {

    private final BaseCleaner cleaner = new BaseCleaner(){
        @Override
        public String clean(String text) {
            return super.clean(text);
        }
    };

    @Test
    void clean_newline() {
        String newLine = cleaner.clean("abc\ndef");
        assertEquals("abc def", newLine);
    }

    @Test
    void clean_doubleWhitespace(){
        String doubleWhitespace = cleaner.clean("abc  def");
        assertEquals("abc def", doubleWhitespace);
    }

    @Test
    void clean_controlCharacters(){
        String controlCharacters = cleaner.clean("abc\0def");
        assertEquals("abcdef", controlCharacters);
    }

    @Test
    void clean_parentheses(){
        String parentheses = cleaner.clean("abc(def&)ghi");
        assertEquals("abcghi", parentheses);
    }

    @Test
    void clean_an(){
        String an = cleaner.clean("{\\an8}abc");
        assertEquals("abc", an);
    }



    @Test
    void verifyParentheses(){
        String parentheses = cleaner.clean("MATT:(Laughing) I'm sorry, I'm sorry.");
        assertEquals("MATT:(Laughing) I'm sorry, I'm sorry.", parentheses);
        parentheses = cleaner.clean("MATT: (Sighs)");
        assertEquals("MATT: (Sighs)", parentheses);
        parentheses = cleaner.clean("(2nin) WHAT?!");
        assertEquals("(2nin) WHAT?!", parentheses);
    }
}