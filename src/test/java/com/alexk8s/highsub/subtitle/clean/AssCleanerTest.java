package com.alexk8s.highsub.subtitle.clean;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AssCleanerTest {

    private final AssCleaner cleaner = new AssCleaner();


    @Test
    void clean_newline(){
        String newLine = cleaner.clean("abc\\Ndef");
        assertEquals("abc def", newLine);
    }

    @Test
    void clean_brackets(){
        String brackets = cleaner.clean("abc{def}ghi");
        assertEquals("abcghi", brackets);
    }

    @Test
    void clean_spaces(){
        String spaces = cleaner.clean("  abc  ");
        assertEquals("abc", spaces);
    }

    @Test
    void clean_length(){
        String length = cleaner.clean("a".repeat(201));
        assertEquals("", length);
    }

    @Test
    void clean_map(){
        String map = cleaner.clean("m 1");
        assertEquals("", map);
    }
}