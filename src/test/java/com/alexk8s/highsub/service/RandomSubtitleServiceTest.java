package com.alexk8s.highsub.service;

import com.alexk8s.highsub.configuration.HighsubConfig;
import com.alexk8s.highsub.kafka.model.SubtitleType;
import com.alexk8s.highsub.model.FlatSubtitle;
import com.alexk8s.highsub.model.HashedSubtitle;
import com.alexk8s.highsub.model.Show;
import com.alexk8s.highsub.repository.FlatSubtitleRepository;
import com.alexk8s.highsub.repository.HashedSubtitleRepository;
import com.alexk8s.highsub.repository.ShowRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class RandomSubtitleServiceTest {


    private static final Show SHOW1 = new Show().withShow("show1").withTags(Collections.emptyList());
    private static final Show SHOW2 = new Show().withShow("show2").withTags(Collections.emptyList());
    private static final Show SHOW3 = new Show().withShow("show3").withTags(Collections.emptyList());
    private static final HashedSubtitle HASHED_SUBTITLE1 = new HashedSubtitle("show1", "hash1","","",2);
    private static final HashedSubtitle HASHED_SUBTITLE2 = new HashedSubtitle("show1", "hash2","","",2);
    private static final HashedSubtitle HASHED_SUBTITLE3 = new HashedSubtitle("show2", "hash3","","",2);
    private static final HashedSubtitle HASHED_SUBTITLE4 = new HashedSubtitle("show2", "hash4","","",2);
    private static final HashedSubtitle HASHED_SUBTITLE5 = new HashedSubtitle("show3", "hash5","","",2);

    private ShowRepository showRepository;
    private FlatSubtitleRepository flatSubtitleRepository;
    private HashedSubtitleRepository hashedSubtitleRepository;
    private RandomSubtitleService randomSubtitleService;
    private HighsubConfig highsubConfig;

    @BeforeEach
    void setUp() {
        showRepository = mock(ShowRepository.class);
        flatSubtitleRepository = mock(FlatSubtitleRepository.class);
        hashedSubtitleRepository = mock(HashedSubtitleRepository.class);
        highsubConfig = mock(HighsubConfig.class);
        randomSubtitleService = new RandomSubtitleService(flatSubtitleRepository, hashedSubtitleRepository, showRepository,highsubConfig);

        FlatSubtitle subtitleMock = mock(FlatSubtitle.class);
        when(subtitleMock.getType()).thenReturn(SubtitleType.DIALOGUE);
        when(subtitleMock.getShouldIndex()).thenReturn(Boolean.TRUE);
        when(flatSubtitleRepository.findByShowAndSeasonAndEpisodeAndSubtitleNumber(any(),any(),any(),anyInt()))
                .thenReturn(Mono.fromCallable(()-> subtitleMock));
        when(hashedSubtitleRepository.findByShowAndHashGreaterThanOrderByHashDescLimit(any(),any(),anyInt()))
                .thenReturn(Flux.just(HASHED_SUBTITLE1,HASHED_SUBTITLE2,HASHED_SUBTITLE3,HASHED_SUBTITLE4,HASHED_SUBTITLE5).repeat(1));
        when(showRepository.findAll()).thenReturn(Flux.just(SHOW1,SHOW2,SHOW3));
        when(highsubConfig.getBlacklistTags()).thenReturn(Collections.emptyList());
        randomSubtitleService.init();
    }

    @Test
    void getRandomSubtitle() {
//        StepVerifier
//                .create(randomSubtitleService.getRandomSubtitle().repeat(4))
//                .expectNextCount(5)
//                .expectComplete()
//                .verify();
        for (int i = 0; i <10 ; i++) {
            StepVerifier
                    .create(randomSubtitleService.getRandomSubtitle(true))
                    .consumeNextWith(flatSubtitle -> System.out.println(flatSubtitle.toString()))
                    .expectComplete()
                    .verify();
        }
    }

    @Test
    void getRandomHash() {
        StepVerifier.create(randomSubtitleService.getRandomHash())
                .expectNextCount(1)
                .expectComplete()
                .verify();
    }
}