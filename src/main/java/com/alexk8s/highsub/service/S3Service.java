package com.alexk8s.highsub.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import software.amazon.awssdk.services.s3.S3AsyncClient;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;

import java.nio.file.Path;

@Service
@RequiredArgsConstructor
@Slf4j
public class S3Service {
    private final S3AsyncClient s3Client;

    public Mono<Void> saveScrapedImage(String path, String key) {
        log.info("saving {} from {}", key, path);
        PutObjectRequest request = PutObjectRequest.builder().bucket("scraped")
                .key(key).build();
        return Mono.fromFuture(s3Client.putObject(request, Path.of(path))).then();
    }
}
