package com.alexk8s.highsub.service;

import com.alexk8s.highsub.ffmpeg.FfmpegTools;
import com.alexk8s.highsub.ffmpeg.operation.ExtractBitmap;
import com.alexk8s.highsub.ffmpeg.operation.FfmpegOperation;
import com.alexk8s.highsub.util.HashBuilder;
import datadog.trace.api.Trace;
import jakarta.annotation.PreDestroy;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.jtransforms.dct.FloatDCT_2D;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.function.Consumer;

@Service
@AllArgsConstructor
@Slf4j
public class FlickerService {
    private static final int SCALED_IMAGE_SIZE = 64;
    private static final FloatDCT_2D DCT = new FloatDCT_2D(SCALED_IMAGE_SIZE, SCALED_IMAGE_SIZE);
    private static final int THRESHOLD = 20;
    public static final long FLICKER_OFFSET = 250L;//how far the reference frame is from the edge of the duration

    private final FfmpegTools ffmpegTools;
    private final ExecutorService executorService = Executors.newFixedThreadPool(10);


    BigInteger getPerceptualHash(final Image image) {
        try {
            // As a foreword, this is an adaptation of a high-level explanation of the pHash algorithm. Many implementation
            // details are probably a bit off the mark. For the rough explanation, see
            // http://www.hackerfactor.com/blog/index.php?/archives/432-Looks-Like-It.html.

            // Start by rescaling the image to a known size. This may involve some squishing
            // (or, in rare cases, stretching), but that's fine for our purposes. We also want to go to greyscale so we only
            // have a single channel to worry about.
            final BufferedImage scaledImage = new BufferedImage(SCALED_IMAGE_SIZE, SCALED_IMAGE_SIZE, BufferedImage.TYPE_BYTE_GRAY);
            {
                final Graphics2D graphics = scaledImage.createGraphics();
                graphics.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);

                graphics.drawImage(image, 0, 0, SCALED_IMAGE_SIZE, SCALED_IMAGE_SIZE, null);

                graphics.dispose();
            }

            // Next, we want to calculate the 2D discrete cosine transform for our small image.
            final float[] dct = new float[SCALED_IMAGE_SIZE * SCALED_IMAGE_SIZE];
            scaledImage.getData().getPixels(0, 0, SCALED_IMAGE_SIZE, SCALED_IMAGE_SIZE, dct);
            DCT.forward(dct, false);

            // We're interested in the lowest-frequency parts of the DCT, so we take the lowest 8x8 square of frequency
            // components. The lowest-frequency bits tend to carry the most "structural" information about a signal (in
            // some ways, they're analogous to the most significant bits of an integer), and so they're also the least
            // sensitive to smaller perturbations in an image. For quantization purposes, we want to calculate the average
            // of the low-frequency components excluding the zero-frequency ("DC") component, which is often an outlier.
            float lowFrequencyDctAverage = -dct[0];

            for (int x = 0; x < 8; x++) {
                for (int y = 0; y < 8; y++) {
                    lowFrequencyDctAverage += dct[x + (y * SCALED_IMAGE_SIZE)];
                }
            }

            lowFrequencyDctAverage /= 64;

            // Now that we have an average value for the lowest-frequency components, we can quantize each of the components
            // in that 8x8 square into a 1 or 0 depending on whether the component is above or below the average value for
            // that square. Each of those components turns into a bit in our finished hash.
            HashBuilder hashBuilder = new HashBuilder(16);

            for (int x = 0; x < 8; x++) {
                for (int y = 0; y < 8; y++) {

                    if (dct[x + (y * SCALED_IMAGE_SIZE)] > lowFrequencyDctAverage) {
                        hashBuilder.prependOne();
                    } else {
                        hashBuilder.prependZero();
                    }
                }
            }

            return hashBuilder.toBigInteger();
        } catch (RuntimeException e) {
            log.error("Error while calculating perceptual hash", e);
            throw e;
        }
    }

    public enum FlickerType {
        FIRST_FRAME,
        LAST_FRAME,
        BOTH,
        NONE
    }

    @PreDestroy
    public void shutdown() {
        executorService.shutdown();
    }

    @Trace
    public FlickerType hasFlicker(String videoPath, long duration, Consumer<FfmpegOperation<?>> registerCommand) {
        Future<Boolean> lastFrameHasFlickerFuture = executorService.submit(() -> hasFlicker(new LastFramesProvider(videoPath, duration), ffmpegTools, registerCommand));
        Future<Boolean> firstFrameHasFlickerFuture = executorService.submit(() -> hasFlicker(new FirstFramesProvider(videoPath), ffmpegTools, registerCommand));
        try {
            boolean firstFrameHasFlicker = firstFrameHasFlickerFuture.get();
            boolean lastFrameHasFlicker = lastFrameHasFlickerFuture.get();
            if (firstFrameHasFlicker && lastFrameHasFlicker) {
                return FlickerType.BOTH;
            } else if (firstFrameHasFlicker) {
                return FlickerType.FIRST_FRAME;
            } else if (lastFrameHasFlicker) {
                return FlickerType.LAST_FRAME;
            } else {
                return FlickerType.NONE;
            }
        } catch (ExecutionException e) {
            log.error("Error while checking for flicker", e);
            throw new RuntimeException(e);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new RuntimeException(e);
        }
    }

    interface FrameProvider {
        FfmpegOperation<String> getFrame();

        FfmpegOperation<String> getReferenceFrame();
    }

    private record FirstFramesProvider(String videoPath) implements FrameProvider {
        @Override
        public FfmpegOperation<String> getFrame() {
            return ExtractBitmap.of(videoPath, 0L);
        }

        @Override
        public FfmpegOperation<String> getReferenceFrame() {
            return ExtractBitmap.of(videoPath, FLICKER_OFFSET);
        }
    }

    private record LastFramesProvider(String videoPath,
                                      Long duration) implements FrameProvider {
        @Override
        public FfmpegOperation<String> getFrame() {
            return ExtractBitmap.ofLastFrame(videoPath);
        }

        @Override
        public FfmpegOperation<String> getReferenceFrame() {
            return ExtractBitmap.of(videoPath, duration - FLICKER_OFFSET);
        }
    }

    @SneakyThrows(IOException.class)
    private boolean hasFlicker(FrameProvider frameProvider, FfmpegTools ffmpegTools, Consumer<FfmpegOperation<?>> registerCommand) {
        registerCommand.accept(frameProvider.getFrame());
        Path framePath = Path.of(frameProvider.getFrame().execute(ffmpegTools));
        registerCommand.accept(frameProvider.getReferenceFrame());
        Path referenceFramePath = Path.of(frameProvider.getReferenceFrame().execute(ffmpegTools));
        if (Files.notExists(framePath)) {
            log.warn("Frame path does not exist: {}", framePath);
            return false;
        }
        if (Files.notExists(referenceFramePath)) {
            log.warn("Reference frame path does not exist: {}", referenceFramePath);
            return false;
        }

        boolean hasFlicker = hasFlicker(framePath.toString(), referenceFramePath.toString());
        Files.delete(framePath);
        Files.delete(referenceFramePath);
        return hasFlicker;
    }


    @SneakyThrows
    private boolean hasFlicker(String frame, String referenceFrame) {
        BigInteger frameHash = getPerceptualHash(ImageIO.read(new File(frame)));
        BigInteger referenceFrameHash = getPerceptualHash(ImageIO.read(new File(referenceFrame)));
        int distance = frameHash.xor(referenceFrameHash).bitCount();
        log.debug("distance: {}", distance);
        return distance > THRESHOLD;
    }

}
