package com.alexk8s.highsub.service;

import com.alexk8s.highsub.configuration.HighsubConfig;
import com.alexk8s.highsub.kafka.model.SubtitleType;
import com.alexk8s.highsub.model.FlatSubtitle;
import com.alexk8s.highsub.model.Show;
import com.alexk8s.highsub.repository.FlatSubtitleRepository;
import com.alexk8s.highsub.repository.HashedSubtitleRepository;
import com.alexk8s.highsub.repository.ShowRepository;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.SynchronousSink;
import reactor.util.retry.Retry;

import java.time.Duration;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import static com.alexk8s.highsub.util.HashUtil.hashStrings;

@Service
@RequiredArgsConstructor
@Slf4j
public class RandomSubtitleService {

    private final FlatSubtitleRepository flatSubtitleRepository;
    private final HashedSubtitleRepository hashedSubtitleRepository;
    private final ShowRepository showRepository;
    private final Flux<String> hashProvider = Flux.generate(RandomSubtitleService::generateHash);
    private Flux<List<Show>> showCache;
    private final Map<Show, Flux<FlatSubtitle>> flatSubtitleCache = new ConcurrentHashMap<>();
    private final HighsubConfig highsubConfig;
    private final Set<String> tagWarnings = new HashSet<>();


    static void generateHash(SynchronousSink<String> sink) {
        String hash = hashStrings(String.valueOf(Math.random()));
        sink.next(hash);
    }

    @PostConstruct
    public void init() {
        showCache = Flux.interval(Duration.ZERO,Duration.ofMinutes(10)).flatMap(x -> getShows()).cache();
    }

    Mono<List<Show>> getShows() {
        return showRepository.findAll()
                .filterWhen(show -> hashedSubtitleRepository.findByShowAndHashGreaterThanOrderByHashDescLimit(show.getShow(), "0", 1)
                        .doOnError(e -> log.error("Error checking if show had hashed subtitles {}", show.getShow(), e)).hasElements()
                        .doOnNext(hasSubtitles -> log.info("Show {} has subtitles {}", show.getShow(), hasSubtitles)))
                .collectList()
                .retryWhen(Retry.backoff(10, Duration.ofMillis(100)));
    }

    private boolean filterShow(Show show) {
        if (show.getTags() != null)
            return show.getTags().stream().noneMatch(highsubConfig.getBlacklistTags()::contains);
        if(tagWarnings.add(show.getShow()))
            log.warn("Show {} has no tags", show.getShow());
        return false;
    }

    public Mono<FlatSubtitle> getRandomSubtitle(boolean clean) {
        return getRandomShow(clean)
                .flatMap(this::getRandomSubtitle);
    }

    public Mono<FlatSubtitle> getRandomSubtitle(Show show) {
        return showCache.next()
                .filter(shows -> shows.contains(show))
                .flatMap(s -> flatSubtitleCache.computeIfAbsent(show, this::getRandomSubtitles).next());
    }

    Mono<Show> getRandomShow(boolean clean) {
        return showCache.next().map(s->getRandomShow(s,clean));
    }

    Show getRandomShow(List<Show> shows,boolean clean){
        List<Show> s = clean?shows.stream().filter(this::filterShow).toList():shows;
        return s.get((int) (Math.random() * s.size()));
    }

    Mono<String> getRandomHash() {
        return hashProvider.next();
    }

    Flux<FlatSubtitle> getRandomSubtitles(Show show) {
        return hashProvider
                .flatMap(h -> {
                    log.info("Getting subtitles for show {} with hash {}", show.getShow(), h);
                    return hashedSubtitleRepository.findByShowAndHashGreaterThanOrderByHashDescLimit(show.getShow(), h, 100);
                }, 1)
                .flatMap(h -> flatSubtitleRepository.findByShowAndSeasonAndEpisodeAndSubtitleNumber(h.getShow(), h.getSeason(), h.getEpisode(), h.getSubtitleNumber()), 5)
                .filter(f -> f.getType() == SubtitleType.DIALOGUE && f.getShouldIndex()==Boolean.TRUE)
                .limitRate(100, 10)
                .publish(10).refCount(1, Duration.ofHours(1))
                ;
    }
}
