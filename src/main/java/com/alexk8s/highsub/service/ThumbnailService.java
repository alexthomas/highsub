package com.alexk8s.highsub.service;

import com.alexk8s.highsub.configuration.HighsubConfig;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import software.amazon.awssdk.core.internal.async.InputStreamResponseTransformer;
import software.amazon.awssdk.services.s3.S3AsyncClient;
import software.amazon.awssdk.services.s3.model.GetObjectRequest;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;

import java.nio.file.Path;

@AllArgsConstructor
@Service
public class ThumbnailService {
    private final HighsubConfig highsubConfig;
    private final S3AsyncClient s3client;
    private final PlexService plexService;

    public Mono<Void> ensureThumbnail(String key, String thumb) {
        GetObjectRequest getObjectRequest = GetObjectRequest.builder()
                .bucket(highsubConfig.getMinio().getThumbnailBucket())
                .key(key)
                .build();

        return Mono.fromFuture(s3client.getObject(getObjectRequest, new InputStreamResponseTransformer<>())).then()
                .onErrorResume((e) -> updateThumbnail(key, thumb))
                .then();
    }

    public Mono<Void> updateThumbnail(String key, String thumb) {
        PutObjectRequest putObjectRequest = PutObjectRequest.builder()
                .bucket(highsubConfig.getMinio().getThumbnailBucket())
                .key(key)
                .build();
        return plexService.downloadThumbnail(thumb)
                .flatMap(p -> Mono.fromFuture(s3client.putObject(putObjectRequest, Path.of(p))))
                .then(Mono.empty());
    }
}
