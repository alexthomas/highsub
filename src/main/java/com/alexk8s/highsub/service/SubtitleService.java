package com.alexk8s.highsub.service;

import com.alexk8s.highsub.configuration.HighsubConfig;
import com.alexk8s.highsub.ffmpeg.FfmpegOutput;
import com.alexk8s.highsub.ffmpeg.FfmpegTools;
import com.alexk8s.highsub.ffmpeg.Stream;
import com.alexk8s.highsub.ffmpeg.operation.ExtractSubtitles;
import com.alexk8s.highsub.ffmpeg.operation.GetMetadata;
import com.alexk8s.highsub.model.Episode;
import com.alexk8s.highsub.model.Language;
import com.alexk8s.highsub.model.Subtitle;
import com.alexk8s.highsub.model.SubtitleFormat;
import com.alexk8s.highsub.service.storage.StorageService;
import com.alexk8s.highsub.subtitle.reader.AssReader;
import com.alexk8s.highsub.subtitle.reader.ReadSubtitle;
import com.alexk8s.highsub.subtitle.reader.SrtReader;
import com.alexk8s.highsub.subtitle.reader.SubtitleReader;
import com.alexk8s.highsub.subtitle.reader.VttReader;
import com.alexk8s.highsub.util.CommandUtils;
import datadog.trace.api.Trace;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.ReactiveHashOperations;
import org.springframework.data.redis.core.ReactiveRedisOperations;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;
import software.amazon.awssdk.services.s3.model.ListObjectVersionsResponse;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.regex.Pattern;

import static com.alexk8s.highsub.util.PathUtils.getTempFile;


@Service
@Slf4j
@AllArgsConstructor
public class SubtitleService {
    private final FfmpegTools ffmpegTools;
    private final ReactiveRedisOperations<String, String> redisOperations;
    private final HighsubConfig highsubConfig;
    private final CommandUtils commandUtils;
    private final StorageService<?, ?> storageService;

    private static final Pattern SIGNS_AND_SONGS_PATTERN = Pattern.compile("((signs|songs)\\s?[\\\\/&-]?\\s?(songs)?)|(s[\\\\/&-]s)", Pattern.CASE_INSENSITIVE);
    private static final List<String> EXTERNAL_EXTENSIONS = List.of(".srt", ".ass", ".vtt");

    private boolean isTargetSub(Stream s, Language target) {
        Language language = Language.findByCode(s.getTags().getLanguage());
        return target.equals(language);

    }

    private boolean isUnknownLanguageSub(Stream s) {
        Language language = Language.findByCode(s.getTags().getLanguage());
        return Language.UNKNOWN.equals(language);
    }

    private boolean isReadable(Stream s, Language targetLanguage, boolean fallbackUnknown) {
        String codecName = s.getCodecName();
        if (codecName != null && (codecName.contains("pgs") || codecName.contains("dvd_subtitle")))
            return false;
        if (!(isTargetSub(s, targetLanguage) || (fallbackUnknown && isUnknownLanguageSub(s))))
            return false;
        String title = s.getTags().getTitle();
        if (title != null) {
            String formattedTitle = title.toLowerCase();
            if (formattedTitle.contains("dialog") || formattedTitle.contains("dialogue") || formattedTitle.contains("full"))
                return true;
            if (formattedTitle.contains("translation only") || formattedTitle.contains("karaoke only") || SIGNS_AND_SONGS_PATTERN.matcher(title).find())
                return false;
            if (formattedTitle.contains("vobsub"))
                return false;
        }
        return true;
    }


    @Trace
    public List<Subtitle> readSubtitles(SubtitleFormat format, String path, Language language) {

        SubtitleReader subtitleReader;
        switch (format) {
            case ASS -> subtitleReader = new AssReader(path, language != Language.JAPANESE);
            case SRT -> subtitleReader = new SrtReader(path);
            case VTT -> subtitleReader = new VttReader(path);
            default -> throw new RuntimeException("Unknown subtitle format");
        }
        List<ReadSubtitle> readSubtitles = subtitleReader.readSubtitles();
        List<Subtitle> subtitles = readSubtitles.stream().map(Subtitle::fromReadSubtitle)
                .filter(s -> s.getEndTime() - s.getStartTime() > 500)
                .toList();
        log.info("Read {} subtitles",readSubtitles.size());
        Set<Subtitle> removeThroughConcat = new HashSet<>();
        for (int i = 0; i < subtitles.size(); i++) {
            Subtitle current = subtitles.get(i);
            while (i < subtitles.size()-1 && Objects.equals(current.getStartTime(), subtitles.get(i + 1).getStartTime()) && Objects.equals(current.getEndTime(), subtitles.get(i + 1).getEndTime())) {
                i++;
                Subtitle next = subtitles.get(i);
                current.setText(current.getText().trim() + " " + next.getText().trim());
                removeThroughConcat.add(next);
            }
            int cursor = i;
            List<String> context = new ArrayList<>();
            while (context.size() < 5 && cursor + 1 < subtitles.size() && subtitles.get(cursor + 1).getStartTime() - subtitles.get(cursor).getEndTime() < 5000) {
                cursor++;
                context.add(subtitles.get(cursor).getText());
            }
            current.setContext(context);
        }
        log.info("Removed {} subtitles through concat", removeThroughConcat.size());
        return subtitles.stream().filter(s -> !removeThroughConcat.contains(s)).toList();
    }

    @SneakyThrows
    public Optional<String> resolveSubtitlesFile(String videoPath, Language targetLanguage, boolean fallbackAny, Consumer<Long> progressConsumer) {
        FfmpegOutput subtitlesStreams = new GetMetadata(videoPath, "subtitle").execute(ffmpegTools);

        List<Stream> streams = subtitlesStreams.getStreams();
        Optional<Stream> subtitlesOptional = resolveSubtitlesStream(streams, targetLanguage, fallbackAny);
        Optional<Path> subtitlesFileOptional = subtitlesOptional.map(s -> Path.of(extractSubtitles(s, videoPath, progressConsumer)))
                .or(() -> findExternalSubtitlesFile(Path.of(videoPath), targetLanguage));
        return subtitlesFileOptional.map(Path::toString);
    }

    public Optional<Stream> resolveSubtitlesStream(List<Stream> streams, Language targetLanguage, boolean fallbackAny) {
        return streams.stream()
                .filter(s -> isReadable(s, targetLanguage, fallbackAny))
                .max(Comparator.comparing(scoreStream(targetLanguage)));
    }

    public Mono<String> putCacheFile(Episode episode, Language language, Path sourceFile) {
        SubtitleFormat subtitleFormat = SubtitleFormat.getFormat(sourceFile.toString());
        String fileName = "subtitles." + language.getSubtitleCode() + "." + subtitleFormat.getExtension();
        String key = String.join("/", List.of(episode.getShow(), episode.getSeason(), episode.getEpisode(), fileName));
        ReactiveHashOperations<String, String, String> redisHOps = redisOperations.opsForHash();
        String destination = getTempFile(fileName.substring(fileName.lastIndexOf('.') + 1));
        log.debug("Putting file {} to cache with key {}", sourceFile, key);
        return storageService.putFile(sourceFile, highsubConfig.getMinio().getSubtitleBucket(), key)
                .doOnNext(r -> copy(sourceFile, destination))
                .flatMap(r -> redisHOps.put("cache-file", key, destination))
                .thenReturn(key);
    }

    public Mono<String> getCacheFile(String key) {
        ReactiveHashOperations<String, String, String> redisHOps = redisOperations.opsForHash();
        log.debug("Checking for key {}", key);
        return redisHOps.get("cache-file", key)
                .switchIfEmpty(Mono.defer(() -> {
                    String destination = getTempFile(key.substring(key.lastIndexOf('.') + 1));
                    return redisHOps.put("cache-file", key, destination).thenReturn(destination);
                }))
                .publishOn(Schedulers.boundedElastic())
                .flatMap(v -> ensureLocalFile(key, v));
    }

    @SneakyThrows
    private void copy(Path source, String destination) {
        Files.copy(source, Path.of(destination), StandardCopyOption.REPLACE_EXISTING);
    }

    public Mono<ListObjectVersionsResponse> getVersions(String key) {
        return storageService.getVersions(highsubConfig.getMinio().getSubtitleBucket(), key);
    }


    private Mono<String> ensureLocalFile(String key, String destination) {
        Path path = Path.of(destination);
        if (Files.exists(path))
            return Mono.just(destination);
        return Mono.just(path)
                .delayUntil(p -> storageService.getFile(highsubConfig.getMinio().getSubtitleBucket(), key, p))
                .map(Path::toString);
    }

    private Function<Stream, Integer> scoreStream(Language targetLanguage) {
        return stream -> {
            int score = 0;
            if (!isTargetSub(stream, targetLanguage))
                score -= 100;
            if (stream.getTags().getTitle() != null) {
                String title = stream.getTags().getTitle().toLowerCase();
                if (title.contains("dialog") || title.contains("dialogue") || title.contains("full"))
                    score += 10;
                if (title.contains("forced"))
                    score -= 11;
            }
            return score;
        };

    }


    public Optional<Path> findExternalSubtitlesFile(Path videoPath, Language language) {
        String fileName = videoPath.getFileName().toString();
        String fileNameWithoutExt = fileName.substring(0, fileName.lastIndexOf(".")) + "." + language.getSubtitleCode();
        for (String externalExtension : EXTERNAL_EXTENSIONS) {
            Path externalPath = videoPath.resolveSibling(fileNameWithoutExt + externalExtension);
            log.debug("Looking for external subtitle file {}", externalPath);
            if (Files.exists(externalPath))
                return Optional.of(externalPath);
        }
        return Optional.empty();
    }

    public String extractSubtitles(Stream subtitlesStream, String videoFile, Consumer<Long> progressConsumer) {
        log.debug("Extracting subtitle stream {}", subtitlesStream);
        SubtitleFormat format = SubtitleFormat.findByCodec(subtitlesStream.getCodecName());
        if (format == SubtitleFormat.UNKNOWN)
            throw new RuntimeException("Unknown codec for subtitles stream " + subtitlesStream);
        return new ExtractSubtitles(videoFile, format, subtitlesStream.getIndex(), progressConsumer).execute(ffmpegTools);
    }

    public enum SyncMode {
        SPLIT_10, NO_SPLIT
    }

    public String alassSync(String referenceFile, String sourceFile, String destinationFile, SyncMode syncMode) {
        return switch (syncMode) {
            case SPLIT_10 -> commandUtils.executeCommand(highsubConfig.getTools().getAlass(),
                    referenceFile, sourceFile, destinationFile, "--split-penalty", "10");
            case NO_SPLIT -> commandUtils.executeCommand(highsubConfig.getTools().getAlass(),
                    referenceFile, sourceFile, destinationFile, "--no-split");
        };

    }
}
