package com.alexk8s.highsub.service;

import com.alexk8s.highsub.configuration.HighsubConfig;
import com.alexk8s.highsub.model.CachedVector;
import com.alexk8s.highsub.repository.CachedVectorRepository;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.cassandra.core.InsertOptions;
import org.springframework.data.cassandra.core.ReactiveCassandraOperations;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.time.Duration;
import java.util.List;
import java.util.Map;

@Component
@Slf4j
@AllArgsConstructor
public class EmbeddingService {
    private final HighsubConfig highsubConfig;

    private static final String VERSION = "text-embedding-3-small";
    private final WebClient webClient = WebClient.builder().build();
    private final CachedVectorRepository cachedVectorRepository;
    private final ReactiveCassandraOperations cassandraOperations;

    public Mono<Double[]> getEmbedding(String text){
        return  getCachedVector(text)
                .switchIfEmpty(loadEmbedding(text).delayUntil(d->saveCachedVector(text,d)))
                .publishOn(Schedulers.boundedElastic());
    }

    Mono<Double[]> getCachedVector(String text){
        return cachedVectorRepository.findByVersionAndInput(VERSION,text)
                .map(CachedVector::getVector)
                .map(v -> v.toArray(new Double[0]));
    }
    
    Mono<?> saveCachedVector(String text, Double[] vector){
        CachedVector cachedVector = new CachedVector(VERSION, text, List.of(vector), System.currentTimeMillis());
        InsertOptions insertOptions = InsertOptions.builder().ttl(Duration.ofDays(7)).build();
        return cassandraOperations.insert(cachedVector, insertOptions);
    }

    private record OpenAIResponse(String object,List<Embedding> data,String model,TokenUsage usage){}
    private record Embedding(String object,Double[] embedding,Integer index){}
    private record TokenUsage(@JsonProperty("prompt_tokens")Integer promptTokens, @JsonProperty("total_tokens")Integer totalTokens){}

    Mono<Double[]> loadEmbedding(String text){
        return webClient.post()
                .uri(highsubConfig.getEmbedding().getUrl())
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer " + highsubConfig.getEmbedding().getApiKey())
                .bodyValue(Map.of("input", text,"model",VERSION,"dimensions",384))
                .retrieve()
                .onStatus(s -> s.is4xxClientError() || s.is5xxServerError(), r -> r.bodyToMono(String.class).doOnSuccess(s->log.error("Error getting embedding for {} {}",text,s)).flatMap(e -> Mono.error(new RuntimeException(e))))
                .bodyToMono(OpenAIResponse.class)
                .map(r -> r.data.get(0).embedding)
                .timeout(Duration.ofSeconds(3))
                .retry(2);
    }

}
