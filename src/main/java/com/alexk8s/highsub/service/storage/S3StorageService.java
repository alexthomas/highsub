package com.alexk8s.highsub.service.storage;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;
import software.amazon.awssdk.services.s3.S3AsyncClient;
import software.amazon.awssdk.services.s3.model.GetObjectRequest;
import software.amazon.awssdk.services.s3.model.GetObjectResponse;
import software.amazon.awssdk.services.s3.model.ListObjectVersionsRequest;
import software.amazon.awssdk.services.s3.model.ListObjectVersionsResponse;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;
import software.amazon.awssdk.services.s3.model.PutObjectResponse;

import java.nio.file.Path;

@Service
@AllArgsConstructor
@ConditionalOnProperty(name = "highsub.storage.type", havingValue = "s3")
@Slf4j
public class S3StorageService implements StorageService<GetObjectResponse, PutObjectResponse> {

    private final S3AsyncClient s3Client;

    @Override
    public Mono<PutObjectResponse> putFile(Path sourceFile, String bucket, String key) {
        PutObjectRequest request = PutObjectRequest.builder()
                .bucket(bucket)
                .key(key)
                .build();
        log.info("Putting {} to {}:{}", sourceFile, bucket, key);
        return Mono.fromFuture(s3Client.putObject(request, sourceFile)).publishOn(Schedulers.boundedElastic());
    }

    @Override
    public Mono<GetObjectResponse> getFile(String bucket, String key, Path destinationFile) {
        GetObjectRequest request = GetObjectRequest.builder()
                .bucket(bucket)
                .key(key)
                .build();
        log.info("Getting {} from {}:{}", destinationFile, bucket, key);
        return Mono.fromFuture(s3Client.getObject(request, destinationFile)).publishOn(Schedulers.boundedElastic());
    }

    @Override
    public Mono<ListObjectVersionsResponse> getVersions(String bucket, String key) {
        ListObjectVersionsRequest request = ListObjectVersionsRequest.builder()
                .bucket(bucket)
                .prefix(key)
                .build();
        log.info("Getting versions for {}:{}", bucket, key);
        return Mono.fromFuture(s3Client.listObjectVersions(request)).publishOn(Schedulers.boundedElastic());
    }
}
