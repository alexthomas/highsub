package com.alexk8s.highsub.service.storage;

import com.alexk8s.highsub.configuration.HighsubConfig;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import software.amazon.awssdk.services.s3.model.ListObjectVersionsResponse;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

@Service
@ConditionalOnProperty(name = "highsub.storage.type", havingValue = "local")
@AllArgsConstructor
@Slf4j
public class LocalStorageService implements StorageService<String, String> {
    private final HighsubConfig highsubConfig;

    @Override
    public Mono<String> putFile(Path sourceFile, String bucket, String key) {
        Path destination = Path.of(highsubConfig.getCacheRoot(), bucket, key);
        log.info("Putting {} to {}", sourceFile, destination);
        return Mono.fromCallable(() -> copyFile(sourceFile, destination));
    }


    @SneakyThrows(IOException.class)
    private String copyFile(Path source, Path destination) {
        Files.createDirectories(destination.getParent());
        Files.copy(source, destination, StandardCopyOption.REPLACE_EXISTING);
        return destination.toString();
    }

    @Override
    public Mono<String> getFile(String bucket, String key, Path destinationFile) {
        Path source = Path.of(highsubConfig.getCacheRoot(), bucket, key);
        log.info("Getting {} and copying to {}", source, destinationFile);
        if (!Files.exists(source)) {
            return Mono.error(new IOException("Source - "+source+" - File not found"));
        }
        return Mono.fromCallable(() -> copyFile(source, destinationFile));
    }

    @Override
    public Mono<ListObjectVersionsResponse> getVersions(String bucket, String key) {
        return Mono.empty();
    }
}
