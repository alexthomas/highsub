package com.alexk8s.highsub.service.storage;

import reactor.core.publisher.Mono;
import software.amazon.awssdk.services.s3.model.ListObjectVersionsResponse;

import java.nio.file.Path;

public interface StorageService<G,P>{

    Mono<P> putFile(Path sourceFile, String bucket, String key);
    Mono<G> getFile(String bucket, String key, Path destinationFile);
    Mono<ListObjectVersionsResponse> getVersions(String bucket, String key);
}
