package com.alexk8s.highsub.service;

import com.alexk8s.highsub.configuration.HighsubConfig;
import com.alexk8s.highsub.ffmpeg.FfmpegOutput;
import com.alexk8s.highsub.ffmpeg.FfmpegTools;
import com.alexk8s.highsub.ffmpeg.operation.ExtractVideo;
import com.alexk8s.highsub.ffmpeg.operation.GetMetadata;
import com.alexk8s.highsub.model.Episode;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

import static com.alexk8s.highsub.util.HashUtil.hashFile;

@Service
@AllArgsConstructor
@Slf4j
public class EpisodeService {
    private final HighsubConfig config;
    private final FfmpegTools ffmpegTools;
    private final SubtitleService subtitleService;


    @SneakyThrows
    public Mono<Episode> createSizes(Episode episode) {
        log.debug("Creating sizes for {}", episode);
        if (!Files.exists(Path.of(episode.getSourceVideo())))
            return Mono.just(episode);
        Path scaledPath = Path.of(config.getCacheRoot(), "scaled", episode.getShow().replaceAll("/",""), episode.getSeason(), episode.getEpisode());
        return subtitleService.getCacheFile(episode.getEnglishSubtitles())
                .publishOn(Schedulers.boundedElastic())
                .map(p -> {
                    try {
                        Files.createDirectories(scaledPath);
                        episode.setScaled480pVideo(createSize(episode, p, 480, scaledPath));
                        episode.setScaled480pVideoBytes(Files.size(Path.of(episode.getScaled480pVideo())));
                        episode.setScaled480pHash(hashFile(Path.of(episode.getScaled480pVideo())));
                        episode.setScaled150pVideo(createSize(episode, p, 150, scaledPath));
                        episode.setScaled150pVideoBytes(Files.size(Path.of(episode.getScaled150pVideo())));
                        episode.setScaled150pHash(hashFile(Path.of(episode.getScaled150pVideo())));
                        return episode;
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                });
    }

    @SneakyThrows
    private String createSize(Episode episode, String subtitlePath, int height, Path scaledPath) {
        FfmpegOutput sourceVideoMetadata = new GetMetadata(episode.getSourceVideo(), "video").execute(ffmpegTools);
        FfmpegOutput sourceAudioMetadata = new GetMetadata(episode.getSourceVideo(), "audio").execute(ffmpegTools);
        String output = ExtractVideo.builder()
                .videoPath(episode.getSourceVideo())
                .subtitlePath(subtitlePath)
                .format("mp4")
                .height(height)
                .videoIndex(ffmpegTools.getVideoStream(sourceVideoMetadata).getIndex())
                .audioIndex(ffmpegTools.getAudioIndex(sourceAudioMetadata))
                .fullEncode(true)
                .build()
                .execute(ffmpegTools);

        Path destination = scaledPath.resolve(height + ".mp4");
        Files.move(Path.of(output), destination, StandardCopyOption.REPLACE_EXISTING);
        return destination.toString();
    }



}
