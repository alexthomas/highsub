package com.alexk8s.highsub.service;

import com.alexk8s.highsub.configuration.HighsubConfig;
import com.alexk8s.highsub.kafka.model.SubtitleV4;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.retry.Retry;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

@Service
@Slf4j
@RequiredArgsConstructor
public class IndexerService {

    private WebClient webClient;
    private final HighsubConfig config;
    private final HighsubConfig highsubConfig;

    @PostConstruct
    public void init() {
        webClient = WebClient.builder()
                .defaultHeader("Authorization", "Bearer " + config.getIndex().getToken())
                .build();
    }

    public record IndexedSubtitle(String show, String season, String episode,
                                  @JsonProperty("start_time") Long startTime, @JsonProperty("end_time") Long endTime,
                                  String text, Integer number, String style,String name, String domain, String japaneseText,
                                  String romaji,
                                  @JsonProperty("japanese_start_time") Long japaneseStartTime,
                                  @JsonProperty("japanese_end_time") Long japaneseEndTime,
                                  @JsonProperty("match_rate") String matchRate,
                                  List<String> tags, List<String> context
    ) {


        public String getId() {
            return String.join("-", show, season, episode, number + "");
        }

        @JsonProperty("video_url")
        public String getVideoUrl() {
            return domain + "/v1/content/subtitle/" + getId() + ".mp4";
        }

        @JsonProperty("gif_url")
        public String getGifUrl() {
            return domain + "/v1/content/subtitle/" + getId() + ".gif";
        }

        @JsonProperty("png_url")
        public String getPngUrl() {
            return domain + "/v1/content/subtitle/" + getId() + ".png";
        }

        @JsonProperty("subtitle_url")
        public String getSubtitleUrl() {
            return domain + "/v1/subtitle/" + getId();
        }

        @JsonProperty("previous_subtitle_url")
        public String getPreviousSubtitleUrl() {
            return getSubtitleUrl() + "/previous";
        }

        @JsonProperty("next_subtitle_url")
        public String getNextSubtitleUrl() {
            return getSubtitleUrl() + "/next";
        }
    }


    public Flux<IndexedSubtitle> indexSubtitles(List<SubtitleV4> subtitles) {
        List<IndexedSubtitle> indexedSubtitles = subtitles.stream().map(this::toIndexedSubtitle).toList();

        return webClient.post()
                .uri(highsubConfig.getIndex().getIndexUrl())
                .bodyValue(indexedSubtitles).retrieve().toBodilessEntity().checkpoint()
                .retryWhen(Retry.backoff(5, Duration.ofSeconds(2))).checkpoint()
                .thenReturn(indexedSubtitles)
                .flatMapIterable(Function.identity());
    }

    private IndexedSubtitle toIndexedSubtitle(SubtitleV4 subtitle) {
        List<String> tags = new ArrayList<>();
        if (subtitle.getTags() != null)
            tags.addAll(subtitle.getTags());
        if (subtitle.getParentTags() != null)
            tags.addAll(subtitle.getParentTags());
        return new IndexedSubtitle(subtitle.getShow(), subtitle.getSeason(), subtitle.getEpisode(), subtitle.getStartTime(), subtitle.getEndTime(),
                subtitle.getText(), subtitle.getSubtitleNumber(), subtitle.getStyle(),subtitle.getName(), highsubConfig.getServerUrl(),
                subtitle.getJapaneseText(), subtitle.getRomaji(), subtitle.getJapaneseStartTime(), subtitle.getJapaneseEndTime(), subtitle.getMatchRate(),
                tags, subtitle.getContext());

    }

    public Mono<Void> deleteSubtitles(List<String> ids) {
        log.info("Deleting indexed subtitles {}", ids);
        return webClient.method(HttpMethod.DELETE)
                .uri(highsubConfig.getIndex().getIndexUrl())
                .bodyValue(ids)
                .retrieve().toEntity(String.class)
                .doOnNext(e -> {
                    log.info("Completed delete with {}\n{}", e.getStatusCode(), e.getBody());
                }).then();

    }

}
