package com.alexk8s.highsub.service;

import com.alexk8s.highsub.credits.Actor;
import com.alexk8s.highsub.credits.ActorRepository;
import com.alexk8s.highsub.credits.Neo4jShow;
import com.alexk8s.highsub.credits.ShowRepository;
import com.alexk8s.highsub.scrape.ActorRole;
import com.alexk8s.highsub.scrape.ActorRoleRepository;
import com.alexk8s.highsub.scrape.ActorRoleRule;
import com.alexk8s.highsub.scrape.ActorRoleRuleRepository;
import com.alexk8s.highsub.util.PathUtils;
import com.alexk8s.highsub.util.ThumbnailUtility;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.neo4j.driver.summary.ResultSummary;
import org.springframework.data.neo4j.core.ReactiveNeo4jClient;
import org.springframework.data.redis.core.ReactiveHashOperations;
import org.springframework.data.redis.core.ReactiveRedisOperations;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;
import reactor.util.function.Tuples;
import software.amazon.awssdk.services.s3.S3AsyncClient;
import software.amazon.awssdk.services.s3.model.GetObjectRequest;
import software.amazon.awssdk.services.s3.model.GetObjectResponse;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;

import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@RequiredArgsConstructor
@Service
@Slf4j
public class ActorService {
    private final ActorRepository actorRepository;
    private final ShowRepository showRepository;
    private final ReactiveNeo4jClient neo4jClient;

    private final ActorRoleRuleRepository actorRoleRuleRepository;
    private final ActorRoleRepository actorRoleRepository;

    private final ReactiveRedisOperations<String, String> redisOperations;
    private final S3AsyncClient s3Client;



    private Mono<Neo4jShow> getNewShow(String show) {
        return showRepository.findById(show)
                .flatMap(showRepository::delete)
                .then(showRepository.save(new Neo4jShow(show)));
    }


    public Mono<Void> saveActorRoles(String show, List<ActorRole> actorRoles) {
        log.debug("Saving show actors {}", show);
        return getNewShow(show).checkpoint("save show")
                .thenMany(Flux.fromIterable(actorRoles))
                .flatMap(this::mapActorName)
                .flatMap(a -> actorRoleRuleRepository.findByShowAndActorAndCharacter(a.getShow(), a.getActor(), a.getCharacter()).switchIfEmpty(Mono.just(ActorRoleRule.NONE_RULE)).map(r -> Tuples.of(a, r)))
                .flatMap(t -> processActorRoleRule(t.getT1(), t.getT2()))
                .flatMap(this::saveActorRole, 1)
                .then();
    }

    public Mono<Void> saveActorRoleRule(ActorRoleRule rule) {
        return actorRoleRuleRepository.save(rule)
                .flatMap(this::processActorRoleRule);
    }

    public Mono<Void> processActorRoleRule(ActorRoleRule rule) {
        if (rule.getType() == ActorRoleRule.Type.IS_DUPLICATE) {
            return deleteHasRole(rule.getCharacter(), rule.getActor(), rule.getShow());
        } else if (rule.getType() == ActorRoleRule.Type.RENAME_CHARACTER) {
            return deleteHasRole(rule.getCharacter(), rule.getActor(), rule.getShow())
                    .then(deleteHasRole(rule.getCharacter(), rule.getOption(), rule.getShow()))
                    .then(actorRoleRepository.findByShowAndActorAndCharacter(rule.getShow(), rule.getActor(), rule.getCharacter()))
                    .map(ar -> ar.withCharacter(rule.getOption()))
                    .flatMap(actorRoleRepository::save)
                    .flatMap(this::saveActorRole)
                    .then(copyImage(ThumbnailUtility.getCharacterKey(rule.getShow(), rule.getCharacter()), ThumbnailUtility.getCharacterKey(rule.getShow(), rule.getOption())))
                    ;
        } else if (rule.getType() == ActorRoleRule.Type.SET_CHARACTER_ROLE) {
            return neo4JDeleteHasRole(rule.getCharacter(), rule.getActor(), rule.getShow())
                    .then(actorRoleRepository.findByShowAndActorAndCharacter(rule.getShow(), rule.getActor(), rule.getCharacter()))
                    .map(ar -> ar.withCharacterRole(rule.getOption()))
                    .flatMap(actorRoleRepository::save)
                    .flatMap(this::saveActorRole);
        }
        return Mono.empty();
    }

    private Mono<Void> deleteHasRole(String character, String actor, String show) {
        Mono<Void> cassandraDelete = actorRoleRepository.findByShowAndActorAndCharacter(show, actor, character)
                .flatMap(actorRoleRepository::delete).then()
                .then();
        return Flux.concat(cassandraDelete, neo4JDeleteHasRole(character, actor, show).then()).then();
    }

    private Mono<ResultSummary> neo4JDeleteHasRole(String character, String actor, String show) {

        return neo4jClient.query("match ()-[hr:HAS_ROLE {character:$character,actor:$actor,show:$show}]->() delete hr").bindAll(Map.of("character", character, "show", show, "actor", actor))
                .run()
                .doOnNext(r -> log.debug("Delete role ({},{},{}) results {} ", show, actor, character, r.counters()));
    }

    private Mono<Void> copyImage(String oldKey, String newKey) {
        GetObjectRequest getRequest = GetObjectRequest.builder().bucket("scraped")
                .key(oldKey)
                .build();
        PutObjectRequest.Builder putRequestBuilder = PutObjectRequest.builder()
                .bucket("scraped")
                .key(newKey);
        String tempFile = PathUtils.getTempFile("tmp");
        Mono<GetObjectResponse> getResponse = Mono.fromFuture(s3Client.getObject(getRequest, Path.of(tempFile)));
        return getResponse.map(r -> putRequestBuilder.contentType(r.contentType()).build()).flatMap(r -> Mono.fromFuture(s3Client.putObject(r, Path.of(tempFile))))
                .then();
    }

    public Mono<ActorRole> processActorRoleRule(ActorRole actorRole, ActorRoleRule rule) {
        return switch (rule.getType()) {
            case IS_DUPLICATE -> Mono.empty();
            case RENAME_CHARACTER -> Mono.just(actorRole.withCharacter(rule.getOption()));
            case SET_CHARACTER_ROLE -> Mono.just(actorRole.withCharacterRole(rule.getOption()));
            default -> Mono.just(actorRole);
        };
    }

    public Mono<Void> mapActorName(String originalName, String correctName) {
        ReactiveHashOperations<String, String, String> ops = redisOperations.opsForHash();
        return ops.put("actor-name-map", originalName, correctName)
                .publishOn(Schedulers.boundedElastic())
                .thenMany(actorRoleRepository.findByActor(originalName))
                .flatMap(ar -> actorRoleRepository.delete(ar).thenReturn(ar))
                .map(ar -> ar.withActor(correctName))
                .flatMap(actorRoleRepository::save)
                .flatMap(this::saveActorRole, 1)
                .then(actorRepository.deleteById(originalName));
    }


    private int getScore(ActorRole actorRole) {
        if (actorRole.getCharacterRole() == null)
            return 0;
        return switch (actorRole.getCharacterRole().toLowerCase()) {
            case "main" -> 100;
            case "major" -> 50;
            case "supporting" -> 10;
            default -> 1;
        };
    }

    private Mono<ActorRole> mapActorName(ActorRole actorRole) {
        ReactiveHashOperations<String, String, String> ops = redisOperations.opsForHash();
        return ops.get("actor-name-map", actorRole.getActor())
                .map(actorRole::withActor)
                .switchIfEmpty(Mono.just(actorRole));
    }

    private Mono<Void> saveActorRole(ActorRole actorRole) {
        log.info("saving {}", actorRole);
        int score = getScore(actorRole);
        String characterRole = Objects.requireNonNullElse(actorRole.getCharacterRole(), "none");
        Mono<Void> save = neo4jClient.query("""
                        MATCH (a:ACTOR {name: $name})
                        OPTIONAL MATCH (e:SHOW {show: $show})
                        CREATE (a)-[hr:HAS_ROLE {character: $character,show: $show, actor: $name,role: $role,score: $score}]->(e)
                        """).bindAll(Map.of("name", actorRole.getActor(), "show", actorRole.getShow(), "character", actorRole.getCharacter(),
                        "role", characterRole, "score", score))
                .run().checkpoint("save has role").then();
        return actorRepository.findById(actorRole.getActor())
                .switchIfEmpty(actorRepository.save(new Actor(actorRole.getActor(), null))).checkpoint("Get Actor")
                .then(save);
    }

}
