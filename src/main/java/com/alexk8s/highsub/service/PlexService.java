package com.alexk8s.highsub.service;

import com.alexk8s.highsub.plex.Directory;
import com.alexk8s.highsub.plex.Leaf;
import com.alexk8s.highsub.plex.LeafsMediaContainer;
import com.alexk8s.highsub.plex.LibrariesMediaContainer;
import com.alexk8s.highsub.plex.LibraryMediaContainer;
import com.alexk8s.highsub.plex.LibraryMetadata;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.data.redis.core.ReactiveHashOperations;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.codec.json.Jackson2JsonDecoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.nio.file.Path;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.alexk8s.highsub.util.PathUtils.getTempFile;

@Service
@Slf4j
@RequiredArgsConstructor
public class PlexService {
    private static final String PLEX_LIBRARIES_PATH = "/library/sections";
    private static final String PLEX_LIBRARY_PATH = "/library/sections/{libraryKey}/all";
    private static final String PLEX_LEAVES_PATH = "/library/metadata/{showKey}/allLeaves";
    private static final Pattern SHOW_KEY_REGEX = Pattern.compile("/library/metadata/(\\d+)/children");
    @Value("${PLEX_URL}")
    private String plexUrl;
    @Value("${PLEX_TOKEN}")
    private String plexToken;
    private WebClient webClient;

    private final ReactiveRedisTemplate<String, Integer> redisTemplate;
    private final Map<String, String> altLibraryUUIDs = new ConcurrentHashMap<>();

    @PostConstruct
    public void init() {
        ObjectMapper objectMapper = new ObjectMapper().configure(DeserializationFeature.UNWRAP_ROOT_VALUE, true);
        ExchangeStrategies exchangeStrategies = ExchangeStrategies.builder()
                .codecs(configurer ->
                {
                    configurer.defaultCodecs().jackson2JsonDecoder(new Jackson2JsonDecoder(objectMapper));
                    configurer.defaultCodecs().maxInMemorySize(10 * 1024 * 1024);
                }).build();
        webClient = WebClient.builder().baseUrl(plexUrl)
                .defaultHeader("X-Plex-Token", plexToken)
                .defaultHeader("accept", "application/json")
                .exchangeStrategies(exchangeStrategies).build();
        getLibraries().filter(d -> !"show".equals(d.type()))
                .subscribe(d -> altLibraryUUIDs.put(d.uuid(), d.key()));
    }


    public Flux<Directory> getLibraries() {
        return webClient.get().uri(PLEX_LIBRARIES_PATH)
                .retrieve()
                .bodyToMono(LibrariesMediaContainer.class)
                .flatMapIterable(LibrariesMediaContainer::directories);
    }


    public Mono<LibraryMediaContainer> getLibraryMetadata(Directory directory) {
        return getLibraryMetadata(directory.key());
    }

    public Mono<LibraryMediaContainer> getLibraryMetadata(String libraryKey) {
        return webClient.get().uri(PLEX_LIBRARY_PATH, Map.of("libraryKey", libraryKey))
                .retrieve()
                .bodyToMono(LibraryMediaContainer.class);
    }

    public Flux<Leaf> getLibraryEpisodes(String key) {
        return webClient.get().uri(PLEX_LEAVES_PATH, Map.of("showKey", key))
                .retrieve()
                .bodyToMono(LeafsMediaContainer.class)
                .flatMapIterable(LeafsMediaContainer::leaves);
    }

    private Flux<PlexEpisode> getAltLibraryEpisodes(String libraryKey) {
        return getLibraryMetadata(libraryKey).flatMapIterable(this::parseAltLibraryEpisodes);

    }

    private List<PlexEpisode> parseAltLibraryEpisodes(LibraryMediaContainer libraryMediaContainer) {
        List<PlexEpisode> episodes = new ArrayList<>(libraryMediaContainer.metadata().size());
        for (LibraryMetadata metadatum : libraryMediaContainer.metadata()) {
            String videoFile = metadatum.media().get(0).parts().get(0).file();
            episodes.add(new PlexEpisode(libraryMediaContainer.title1(), metadatum.title(), "1", metadatum.title(), metadatum.key(),
                    videoFile, metadatum.summary(), metadatum.studio(), metadatum.thumb(), metadatum.thumb(), metadatum.originallyAvailableAt(), metadatum.year()));
        }
        return episodes;
    }

    private static class ThumbnailDownloadException extends RuntimeException {
    }

    public Mono<String> downloadThumbnail(String thumb) {
        String png = getTempFile("png");
        return webClient.get().uri(thumb)
                .retrieve()
                .onStatus(HttpStatusCode::isError, r -> Mono.error(new ThumbnailDownloadException()))
                .bodyToMono(DataBuffer.class)
                .transform(m -> DataBufferUtils.write(m, Path.of(png)).share())
                .thenReturn(png)
                .onErrorResume(ThumbnailDownloadException.class, (t) -> Mono.empty())
                ;
    }

    public record PlexShow(String title, String key, String summary, Integer year,String thumb) {
    }

    @GetMapping("shows")
    public Flux<PlexShow> getShows() {
        ReactiveHashOperations<String, String, Integer> redisOps = redisTemplate.opsForHash();
        Flux<Directory> libraries = getLibraries().cache();
        Flux<PlexShow> shows = libraries
                .filter(d -> "show".equals(d.type()))
                .flatMap(this::getLibraryMetadata)
                .flatMapIterable(LibraryMediaContainer::metadata)
                .map(s -> new PlexShow(s.title(), parseKey(s.key()), s.summary(), s.year(),s.thumb()));
        Flux<PlexShow> otherLibraries = libraries
                .filter(d -> !"show".equals(d.type()))
                .map(d -> new PlexShow(d.title(), d.uuid(), d.title(), LocalDate.now().getYear(),d.thumb()));

        return Flux.concat(shows, otherLibraries)
                .delayUntil(p -> redisOps.put("show-year", p.key, p.year));
    }

    private String parseKey(String key) {
        Matcher matcher = SHOW_KEY_REGEX.matcher(key);
        if (!matcher.find()) {
            throw new RuntimeException("Unable to parse key: " + key);
        }
        return matcher.group(1);
    }

    public record PlexEpisode(String show, String season, String episode, String title, String key, String videoFile,
                              String summary, String studio, String thumb, String showThumb,
                              String originallyAvailableAt,
                              Integer showYear) {
    }

    public Flux<PlexEpisode> getEpisodes(String showKey) {
        ReactiveHashOperations<String, String, Integer> redisOps = redisTemplate.opsForHash();
        if (altLibraryUUIDs.containsKey(showKey)) {
            return getAltLibraryEpisodes(altLibraryUUIDs.get(showKey));
        }
        return getLibraryEpisodes(showKey).zipWith(redisOps.get("show-year", showKey).switchIfEmpty(Mono.just(-1)).cache().repeat())
                .map(t -> {
                    Leaf l = t.getT1();
                    String videoFile = l.media().get(0).parts().get(0).file();
                    return new PlexEpisode(l.grandparentTitle(), l.parentTitle(), l.index() + "", l.title(), l.key(),
                            videoFile, l.summary(), l.studio(), l.thumb(), l.grandparentThumb(), l.originallyAvailableAt(), t.getT2());
                })
                ;
    }


}
