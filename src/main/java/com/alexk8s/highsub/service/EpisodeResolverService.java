package com.alexk8s.highsub.service;

import com.alexk8s.highsub.configuration.HighsubConfig;
import com.alexk8s.highsub.ffmpeg.Chapter;
import com.alexk8s.highsub.ffmpeg.FfmpegOutput;
import com.alexk8s.highsub.ffmpeg.FfmpegTools;
import com.alexk8s.highsub.ffmpeg.MetadataParseException;
import com.alexk8s.highsub.ffmpeg.Stream;
import com.alexk8s.highsub.ffmpeg.operation.GetDuration;
import com.alexk8s.highsub.ffmpeg.operation.GetMetadata;
import com.alexk8s.highsub.kafka.IndexingStatusInteractions;
import com.alexk8s.highsub.kafka.model.IndexingStatusType;
import com.alexk8s.highsub.kafka.model.IndexingStatusV1;
import com.alexk8s.highsub.model.Episode;
import com.alexk8s.highsub.model.Language;
import com.alexk8s.highsub.model.PlexOpening;
import com.alexk8s.highsub.model.Show;
import com.alexk8s.highsub.repository.PlexOpeningRepository;
import com.alexk8s.highsub.repository.ShowRepository;
import com.alexk8s.highsub.util.CommandUtils;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;
import java.util.function.Predicate;

import static com.alexk8s.highsub.util.EpisodeUtils.getSlug;
import static com.alexk8s.highsub.util.HashUtil.hashFile;

@AllArgsConstructor
@Service
@Slf4j
public class EpisodeResolverService {

    private final PlexOpeningRepository plexOpeningRepository;
    private final FfmpegTools ffmpegTools;
    private final CommandUtils commandUtils;
    private final ShowRepository showRepository;
    private final IndexingStatusInteractions indexingStatusInteractions;
    private final SubtitleService subtitleService;
    private final HighsubConfig highsubConfig;

    @SneakyThrows
    public Mono<Episode> resolveEpisode(PlexService.PlexEpisode plexEpisode) {
        String show = plexEpisode.show();
        String season = plexEpisode.season();
        String episode = plexEpisode.episode();
        StatusUpdater statusUpdater = new StatusUpdater(show, season, episode, indexingStatusInteractions);
        statusUpdater.updateStatus(IndexingStatusType.STARTING, "Starting", null, null);
        try {

            Episode e = buildEpisode(show, season, episode, plexEpisode, statusUpdater);
            statusUpdater.updateStatus(IndexingStatusType.EXTRACTING_SUBTITLES, "Extracting english subtitles", e.getDuration(), 0L);
            String englishSubtitlePath = subtitleService.resolveSubtitlesFile(plexEpisode.videoFile(), Language.ENGLISH, true,
                    (l) -> statusUpdater.updateStatus(IndexingStatusType.EXTRACTING_SUBTITLES, "Extracting English subtitles", e.getDuration(), l)).orElseThrow(SubtitleNotFoundException::new);
            withSourceHash(e, statusUpdater);
            withChapters(e);
            Mono<Episode> englishMono = subtitleService.putCacheFile(e, Language.ENGLISH, Path.of(englishSubtitlePath)).map(s -> {
                e.setEnglishSubtitles(s);
                return e;
            });
            statusUpdater.updateStatus(IndexingStatusType.EXTRACTING_SUBTITLES, "Extracting Japanese subtitles", e.getDuration(), 0L);
            Optional<String> japaneseSubtitles = subtitleService.resolveSubtitlesFile(plexEpisode.videoFile(), Language.JAPANESE, false,
                    (l) -> statusUpdater.updateStatus(IndexingStatusType.EXTRACTING_SUBTITLES, "Extracting Japanese subtitles", e.getDuration(), l));
            Mono<Episode> subtitleMono;
            if (japaneseSubtitles.isPresent()) {
                Mono<Episode> japaneseMono = subtitleService.putCacheFile(e, Language.JAPANESE, Path.of(japaneseSubtitles.get()))
                        .map(s -> {
                            e.setJapaneseSubtitles(s);
                            return e;
                        });
                subtitleMono = Flux.concat(japaneseMono, englishMono)
                        .doOnNext(s -> log.info("With subtitles {}", s))
                        .last()
                        .flatMap(ep -> {
                            String output = commandUtils.executeCommand(highsubConfig.getTools().getAlass(),
                                    englishSubtitlePath, japaneseSubtitles.get(), japaneseSubtitles.get());
                            log.debug("Alass Sync output\n{}", output);
                            return subtitleService.putCacheFile(ep, Language.JAPANESE, Path.of(japaneseSubtitles.get())).map(s -> {
                                ep.setJapaneseSubtitles(s);
                                return ep;
                            });
                        })
                ;
            } else {
                subtitleMono = englishMono;
            }
            Mono<Episode> episodeMono;
            if (e.getOpeningStart() == null || e.getEndingStart() == null) {
                episodeMono = plexOpeningRepository.findByShowAndSeasonAndEpisode(e.getShow(), e.getSeason(), e.getEpisode())
                        .doOnNext(o -> applyOpening(e, o))
                        .then(subtitleMono);
            } else {
                episodeMono = subtitleMono;
            }
            return Mono.zip(showRepository.findById(show), episodeMono)
                    .map(t -> {
                        Show s = t.getT1();
                        List<String> parentTags = Objects.requireNonNullElse(s.getTags(), Collections.emptyList());
                        String seasonName = s.getSeasonNameMap()==null?"":s.getSeasonNameMap().getOrDefault(season, season);
                        return t.getT2().withParentTags(parentTags).withSeasonName(seasonName).withSlug(getSlug(show, s.getSeasonNameMap(), season, episode));
                    })
                    .doOnSuccess(ee -> statusUpdater.updateStatus(IndexingStatusType.COMPLETE, "Complete", null, null))
                    .doOnError(ee -> statusUpdater.updateStatus(IndexingStatusType.ERROR, "Error: " + ee.getMessage(), null, null))
                    ;
        } catch (SubtitleNotFoundException e){
            statusUpdater.updateStatus(IndexingStatusType.ERROR, "Error: No subtitles found", null, null);
            log.error("No subtitles found for {} {} {}", show, season, episode);
            return Mono.empty();
        } catch (MetadataParseException e){
            statusUpdater.updateStatus(IndexingStatusType.ERROR, "Error: Metadata parse error", null, null);
            log.error("Metadata parse error for {} {} {}", show, season, episode);
            return Mono.empty();
        } catch (RuntimeException e) {
            statusUpdater.updateStatus(IndexingStatusType.ERROR, "Error: " + e.getMessage(), null, null);
            log.error("Error indexing {} {} {}", show, season, episode, e);
            throw e;
        }
    }

    @SneakyThrows(IOException.class)
    Episode buildEpisode(String show, String season, String episode, PlexService.PlexEpisode plexEpisode, StatusUpdater statusUpdater){
        statusUpdater.updateStatus(IndexingStatusType.EXTRACTING_DURATION, "Extracting duration", null, null);
        long duration = new GetDuration(plexEpisode.videoFile()).execute(ffmpegTools);
        long size = Files.size(Path.of(plexEpisode.videoFile()));
        statusUpdater.updateStatus(IndexingStatusType.EXTRACTING_METADATA, "Extracting metadata", null, null);
        FfmpegOutput metadata = new GetMetadata(plexEpisode.videoFile(), "video").execute(ffmpegTools);
        Stream videoStream = metadata.getStreams().stream().findFirst().orElseThrow();
        return Episode.builder()
                .show(show).season(season).episode(episode)
                .previewSubtitleId("").indexedSubtitleCount(0L).subtitleCount(0L)
                .videoFile(plexEpisode.videoFile()).sourceVideo(plexEpisode.videoFile()).sourceVideoBytes(size)
                .title(plexEpisode.title()).summary(plexEpisode.summary()).studio(plexEpisode.studio())
                .originallyAvailableAt(plexEpisode.originallyAvailableAt())
                .height(videoStream.getHeight()).width(videoStream.getWidth())
                .duration(duration)
                .tags(Collections.emptyList())
                .build();
    }

    void withSourceHash(Episode episode,StatusUpdater statusUpdater){
        long size = episode.getSourceVideoBytes();
        AtomicLong lastUpdateTime = new AtomicLong(System.currentTimeMillis());
        Consumer<Long> hashProgressConsumer = (bytes) -> {
            if (bytes == size)
                statusUpdater.updateStatus(IndexingStatusType.HASHING_FILE, "Hashing source file", size, bytes);
            else if (System.currentTimeMillis() - lastUpdateTime.get() > 200) {
                statusUpdater.updateStatus(IndexingStatusType.HASHING_FILE, "Hashing source file", size, bytes);
                lastUpdateTime.set(System.currentTimeMillis());
            }
        };
        episode.setSourceHash(hashFile(Path.of(episode.getSourceVideo()), hashProgressConsumer));
    }

    void withChapters(Episode episode){
        FfmpegOutput metadata = new GetMetadata(episode.getSourceVideo(), "video").execute(ffmpegTools);
        Predicate<Chapter> isPrologue = isChapter("prologue");
        Predicate<Chapter> isOpening = isChapter("op|opening");
        Predicate<Chapter> isEnding = isChapter("ending|ed|credits");
        Predicate<Chapter> isPreview = isChapter("preview|pv|next");
        applyChapter(episode, metadata, isPrologue, episode::setPrologueStart, episode::setPrologueEnd);
        applyChapter(episode, metadata, isOpening, episode::setOpeningStart, episode::setOpeningEnd);
        applyChapter(episode, metadata, isEnding, episode::setEndingStart, episode::setEndingEnd);
        applyChapter(episode, metadata, isPreview, episode::setPreviewStart, episode::setPreviewEnd);
    }

    private Predicate<Chapter> isChapter(String regex) {
        return c -> {
            if (c != null && c.getTags() != null && c.getTags().getTitle() != null)
                return c.getTags().getTitle().toLowerCase().matches(regex);
            return false;
        };
    }

    private record StatusUpdater(String show, String season, String episode,
                                 IndexingStatusInteractions indexingStatusInteractions) {
        void updateStatus(IndexingStatusType status, String message, Long totalTaskQuantity, Long taskQuantity) {
            IndexingStatusV1 indexingStatusV1 = new IndexingStatusV1(show, season, episode, message, totalTaskQuantity, taskQuantity, status, System.currentTimeMillis());
            indexingStatusInteractions.sendIndexingStatus(indexingStatusV1);
        }
    }


    private void applyOpening(Episode episode, PlexOpening opening) {
        if (opening.getMarkerType().equals("intro") && episode.getOpeningStart() == null) {
            episode.setOpeningStart(opening.getStart());
            episode.setOpeningEnd(opening.getEnd());
        } else if (opening.getMarkerType().equals("credits") && episode.getEndingStart() == null) {
            episode.setEndingStart(opening.getStart());
            episode.setEndingEnd(opening.getEnd());
        }

    }

    private Long convertTime(String s) {

        return ((long) Double.parseDouble(s) * 1000);
    }

    private void applyChapter(Episode episode, FfmpegOutput metadata, Predicate<Chapter> predicate,
                              Consumer<Long> startSetter, Consumer<Long> endSetter) {

        Optional<Chapter> openingChapter = metadata.getChapters().stream().filter(predicate).findFirst();
        openingChapter.ifPresent(c -> {
            log.debug("Found chapter {} for {} {} {}", c.getTags().getTitle(), episode.getShow(), episode.getSeason(), episode.getEpisode());
            startSetter.accept(convertTime(c.getStartTime()));
            endSetter.accept(convertTime(c.getEndTime()));
        });
    }

}
