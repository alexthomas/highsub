package com.alexk8s.highsub.ffmpeg;

import lombok.Data;

@Data
public class Tags {
    private String language;

    private String title;
}
