package com.alexk8s.highsub.ffmpeg;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Disposition {
    @JsonFormat(shape = JsonFormat.Shape.NUMBER)
    @JsonProperty("metadata")
    private boolean metadata;

    @JsonFormat(shape = JsonFormat.Shape.NUMBER)
    @JsonProperty("original")
    private boolean original;

    @JsonFormat(shape = JsonFormat.Shape.NUMBER)
    @JsonProperty("visual_impaired")
    private boolean visualImpaired;

    @JsonFormat(shape = JsonFormat.Shape.NUMBER)
    @JsonProperty("forced")
    private boolean forced;

    @JsonFormat(shape = JsonFormat.Shape.NUMBER)
    @JsonProperty("attached_pic")
    private boolean attachedPic;

    @JsonFormat(shape = JsonFormat.Shape.NUMBER)
    @JsonProperty("still_image")
    private boolean stillImage;

    @JsonFormat(shape = JsonFormat.Shape.NUMBER)
    @JsonProperty("descriptions")
    private boolean descriptions;

    @JsonFormat(shape = JsonFormat.Shape.NUMBER)
    @JsonProperty("captions")
    private boolean captions;

    @JsonFormat(shape = JsonFormat.Shape.NUMBER)
    @JsonProperty("dub")
    private boolean dub;

    @JsonFormat(shape = JsonFormat.Shape.NUMBER)
    @JsonProperty("karaoke")
    private boolean karaoke;

    @JsonFormat(shape = JsonFormat.Shape.NUMBER)
    @JsonProperty("default")
    private boolean jsonMemberDefault;

    @JsonFormat(shape = JsonFormat.Shape.NUMBER)
    @JsonProperty("timed_thumbnails")
    private boolean timedThumbnails;

    @JsonFormat(shape = JsonFormat.Shape.NUMBER)
    @JsonProperty("comment")
    private boolean comment;

    @JsonFormat(shape = JsonFormat.Shape.NUMBER)
    @JsonProperty("hearing_impaired")
    private boolean hearingImpaired;

    @JsonFormat(shape = JsonFormat.Shape.NUMBER)
    @JsonProperty("lyrics")
    private boolean lyrics;

    @JsonFormat(shape = JsonFormat.Shape.NUMBER)
    @JsonProperty("dependent")
    private boolean dependent;

    @JsonFormat(shape = JsonFormat.Shape.NUMBER)
    @JsonProperty("clean_effects")
    private boolean cleanEffects;


}
