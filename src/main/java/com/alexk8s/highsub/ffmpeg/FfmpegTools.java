package com.alexk8s.highsub.ffmpeg;


import com.alexk8s.highsub.configuration.HighsubConfig;
import com.alexk8s.highsub.ffmpeg.operation.Argument;
import com.alexk8s.highsub.ffmpeg.operation.FfmpegOperation;
import com.alexk8s.highsub.model.Language;
import com.alexk8s.highsub.util.CommandUtils;
import datadog.trace.api.Trace;
import io.opentracing.Span;
import io.opentracing.util.GlobalTracer;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

@RequiredArgsConstructor
@Component
@Slf4j
public class FfmpegTools {


    private final HighsubConfig config;
    private final CommandUtils cmdUtils;

    @Trace
    public String execute(FfmpegOperation.Function function, List<Argument> arguments) {
        applyArgumentsToSpan(arguments);
        List<String> command = new ArrayList<>();
        command.add(function.getTool(config.getTools()));
        arguments.stream().flatMap(a -> a.getArguments().stream()).forEach(command::add);
        return cmdUtils.executeCommand(command);
    }

    @Trace
    public void execute(FfmpegOperation.Function function, List<Argument> arguments, Consumer<String> outputLineHandler) {
        applyArgumentsToSpan(arguments);
        List<String> command = new ArrayList<>();
        command.add(function.getTool(config.getTools()));
        arguments.stream().flatMap(a -> a.getArguments().stream()).forEach(command::add);
        cmdUtils.executeCommand(command, outputLineHandler);
    }

    void applyArgumentsToSpan(List<Argument> arguments){
        Span span = GlobalTracer.get().activeSpan();
        if (span != null) {
            for (int i = 0; i < arguments.size(); i++) {
                Argument a = arguments.get(i);
                span.setTag("arguments." + i, a.getSpanTag());
            }
        }
    }



    public Stream getVideoStream(FfmpegOutput metadata) {
        return metadata
                .getStreams()
                .stream()
                .findFirst()
                .orElse(null);
    }

    public Integer getAudioIndex(FfmpegOutput metadata) {
        List<Stream> audioStreams = metadata
                .getStreams()
                .stream()
                .filter(stream -> stream.getCodecType().equals("audio")).toList();
        Integer index = audioStreams.stream()
                .filter(stream -> Language.findByCode(stream.getTags().getLanguage()).equals(config.getTargetLanguage()))
                .map(Stream::getIndex)
                .findFirst()
                .orElse(null);

        if (index == null) {
            if (audioStreams.size() == 1) {
                // If there's only one audio stream, we'll just use that.
                return audioStreams.get(0).getIndex();
            } else {
                log.warn("The video has multiple audio streams, and none of them are the target language ({}).", config.getTargetLanguage().getName());
                if (!audioStreams.isEmpty())
                    return audioStreams.get(0).getIndex();
                return null;
            }
        }
        return index;
    }
}
