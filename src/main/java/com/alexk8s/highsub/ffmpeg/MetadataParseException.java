package com.alexk8s.highsub.ffmpeg;

public class MetadataParseException extends RuntimeException{
    public MetadataParseException(Throwable e) {
        super(e);
    }
}
