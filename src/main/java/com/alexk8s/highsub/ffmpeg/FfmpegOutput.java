package com.alexk8s.highsub.ffmpeg;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties("programs")
public class FfmpegOutput {
    private List<Stream> streams;
    private List<Chapter> chapters;


}
