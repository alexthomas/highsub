package com.alexk8s.highsub.ffmpeg;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties("side_data_list")
public class Stream {
    @JsonAlias("time_base")
    private String timeBase;

    @JsonAlias("r_frame_rate")
    private String rFrameRate;

    @JsonAlias("start_pts")
    private int startPts;

    @JsonAlias("index")
    private int index;

    @JsonAlias("extradata_size")
    private int extraDataSize;

    @JsonAlias("duration_ts")
    private int durationTs;

    @JsonAlias("width")
    private int width;
    @JsonAlias("height")
    private int height;

    @JsonAlias("codec_name")
    private String codecName;

    @JsonAlias("tags")
    private Tags tags;

    @JsonAlias("duration")
    private String duration;

    @JsonAlias("start_time")
    private String startTime;

    @JsonAlias("disposition")
    private Disposition disposition;

    @JsonAlias("codec_tag")
    private String codecTag;

    @JsonAlias("codec_tag_string")
    private String codecTagString;

    @JsonAlias("avg_frame_rate")
    private String avgFrameRate;

    @JsonAlias("codec_type")
    private String codecType;

    @JsonAlias("codec_long_name")
    private String codecLongName;

    public Tags getTags() {
        if (tags == null) {
            tags = new Tags();
        }
        return tags;
    }

}
