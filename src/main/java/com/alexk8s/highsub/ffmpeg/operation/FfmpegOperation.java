package com.alexk8s.highsub.ffmpeg.operation;

import com.alexk8s.highsub.configuration.HighsubConfig;
import com.alexk8s.highsub.ffmpeg.FfmpegTools;

import java.util.List;
import java.util.stream.Collectors;

public interface FfmpegOperation<O> {

    enum Function {
        FFMPEG(HighsubConfig.Tools::getFfmpeg), FFPROBE(HighsubConfig.Tools::getFfprobe);

        private final java.util.function.Function<HighsubConfig.Tools,String> toolGetter;
        Function(java.util.function.Function<HighsubConfig.Tools,String> toolGetter){
            this.toolGetter = toolGetter;
        }

        public String getTool(HighsubConfig.Tools tools){
            return toolGetter.apply(tools);
        }

    }
    ;

    Function getFunction();

    List<Argument> getArguments();

    O execute(FfmpegTools ffmpegTools);

    default String msToFfmpegFormat(long ms) {
        long hours = ms / 3_600_000;
        ms %= 3_600_000;
        long minutes = ms / 60_000;
        ms %= 60_000;
        long seconds = ms / 1_000;
        ms %= 1_000;
        return String.format("%02d:%02d:%02d.%03d", hours, minutes, seconds, ms);
    }

    default String getCommand(HighsubConfig.Tools tools){
        String args = getArguments().stream()
                .flatMap(a -> a.getArguments().stream())
                .map(s -> s.contains(" ") ? String.format("\"%s\"", s) : s)
                .collect(Collectors.joining(" "));
        return String.format("%s %s", getFunction().getTool(tools), args);
    }

}
