package com.alexk8s.highsub.ffmpeg.operation;

import com.alexk8s.highsub.ffmpeg.FfmpegTools;

import java.util.List;

import static com.alexk8s.highsub.util.PathUtils.getTempFile;

public class Resize implements FfmpegOperation<String>{

    private  final String source;
    private final int height;
    private final String output;

    public Resize(String source, int height,String format) {
        this.source = source;
        this.height = height;
        this.output = getTempFile(format);
    }

    @Override
    public Function getFunction() {
        return Function.FFMPEG;
    }

    @Override
    public List<Argument> getArguments() {
        return List.of(
                Argument.of("-i", source),
                Argument.of("-vf", "scale=-2:"+height),
                Argument.of(output)
        );
    }

    @Override
    public String execute(FfmpegTools ffmpegTools) {
        ffmpegTools.execute(getFunction(), getArguments());
        return output;
    }
}
