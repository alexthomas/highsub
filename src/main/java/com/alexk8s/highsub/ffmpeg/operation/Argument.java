package com.alexk8s.highsub.ffmpeg.operation;

import java.util.List;

public interface Argument {
    List<String> getArguments();

    String getSpanTag();

    static Argument of(String flag) {
        return new Flag(flag);
    }

    static Argument of(String option, String value) {
        return new Option(option, value);
    }

    record Flag(String flag) implements Argument {
        @Override
        public List<String> getArguments() {
            return List.of(flag);
        }

        @Override
        public String getSpanTag() {
            return flag;
        }

    }

    record Option(String option, String value) implements Argument {
        @Override
        public List<String> getArguments() {
            return List.of(option, value);
        }

        @Override
        public String getSpanTag() {
            return option +" "+ value;
        }
    }
}
