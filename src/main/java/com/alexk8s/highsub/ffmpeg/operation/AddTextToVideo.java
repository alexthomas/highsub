package com.alexk8s.highsub.ffmpeg.operation;

import com.alexk8s.highsub.ffmpeg.FfmpegTools;

import java.util.List;

import static com.alexk8s.highsub.util.PathUtils.getTempFile;

public class AddTextToVideo extends AddTextOperation{
    private final String videoPath;
    private final String outputPath;

    public AddTextToVideo(String videoPath,String format,String text, int width) {
        super(text, width);
        this.videoPath = videoPath;
        outputPath = getTempFile(format);
    }

    @Override
    public List<Argument> getArguments() {
        return List.of(
                Argument.of("-i", videoPath),
                Argument.of("-vf", buildFiltersForText()),
                Argument.of("-c:a", "copy"),
                Argument.of(outputPath)
        );
    }

    @Override
    public String execute(FfmpegTools ffmpegTools) {
        ffmpegTools.execute(getFunction(),getArguments());
        return outputPath;
    }
}
