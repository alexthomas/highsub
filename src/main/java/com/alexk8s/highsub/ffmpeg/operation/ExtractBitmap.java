package com.alexk8s.highsub.ffmpeg.operation;

import com.alexk8s.highsub.ffmpeg.FfmpegTools;

import java.util.List;

import static com.alexk8s.highsub.util.PathUtils.getTempFile;

public class ExtractBitmap implements FfmpegOperation<String> {
    private static final String FORMAT = "bmp";
    private final String videoPath;
    private final Long time;
    private final boolean useLastFrame;
    private final String outputPath;

    public ExtractBitmap(String videoPath, Long time, boolean useLastFrame) {
        this.videoPath = videoPath;
        this.time = time;
        this.useLastFrame = useLastFrame;
        outputPath = getTempFile(FORMAT);
    }


    public static ExtractBitmap of(String videoPath, Long time) {
        return new ExtractBitmap(videoPath, time, false);
    }

    public static ExtractBitmap ofLastFrame(String videoPath) {
        return new ExtractBitmap(videoPath, null, true);
    }

    @Override
    public Function getFunction() {
        return Function.FFMPEG;
    }

    @Override
    public List<Argument> getArguments() {
        Argument seekArgument = useLastFrame ? Argument.of("-sseof", "-1") : Argument.of("-ss", msToFfmpegFormat(time));
        return List.of(
                seekArgument,
                Argument.of("-i", videoPath),
                Argument.of("-vframes", "1"),
                Argument.of("-vsync", "0"),
                Argument.of("-update", "1"),
                Argument.of("-copyts"),
                Argument.of(outputPath)
        );

    }

    @Override
    public String execute(FfmpegTools ffmpegTools) {
        ffmpegTools.execute(getFunction(), getArguments());
        return outputPath;
    }
}
