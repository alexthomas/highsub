package com.alexk8s.highsub.ffmpeg.operation;

import com.alexk8s.highsub.ffmpeg.FfmpegOutput;
import com.alexk8s.highsub.ffmpeg.FfmpegTools;
import com.alexk8s.highsub.ffmpeg.MetadataParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@AllArgsConstructor
public class GetMetadata implements FfmpegOperation<FfmpegOutput> {

    private static final Map<String, FfmpegOutput> METADATA_CACHE = new ConcurrentHashMap<>();
    private static final Pattern HEADER_MISSING_REGEX = Pattern.compile("((\\[mp3float @ 0x\\w+] Header missing)|(\\[matroska,webm @ 0x\\w+] Read error at pos. \\d+ \\(0x\\w+\\)))");

    private final String path;
    private final String type;
    private final ObjectMapper objectMapper = new ObjectMapper();

    String getCacheKey() {
        return path + '|' + type;
    }

    @Override
    public Function getFunction() {
        return Function.FFPROBE;
    }

    @Override
    public List<Argument> getArguments() {
        return List.of(
                Argument.of("-v", "error"),
                Argument.of("-of", "json"),
                Argument.of(path),
                Argument.of("-show_chapters"),
                Argument.of("-show_entries", "stream=index,width,height,codec_name,r_frame_rate,codec_type:disposition=forced:stream_tags=language,title"),
                Argument.of("-select_streams", String.valueOf(type.charAt(0)))
        );
    }

    @Override
    public FfmpegOutput execute(FfmpegTools ffmpegTools) {
        String cacheKey = getCacheKey();
        if (METADATA_CACHE.containsKey(cacheKey)) {
            return METADATA_CACHE.get(cacheKey);
        }
        String output = ffmpegTools.execute(getFunction(), getArguments());
        Matcher matcher = HEADER_MISSING_REGEX.matcher(output);
        if (matcher.find()) {
            String headerMissingText = matcher.group();
            output = output.substring(0, output.indexOf(headerMissingText)) + output.substring(output.indexOf(headerMissingText) + headerMissingText.length());
        }
        try{

        FfmpegOutput ffmpegOutput = objectMapper.readValue(output, FfmpegOutput.class);
        METADATA_CACHE.put(cacheKey, ffmpegOutput);
        return ffmpegOutput;
        } catch (JsonProcessingException e) {
            throw new MetadataParseException(e);
        }
    }


}
