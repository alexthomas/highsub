package com.alexk8s.highsub.ffmpeg.operation;

import com.alexk8s.highsub.ffmpeg.FfmpegTools;
import lombok.SneakyThrows;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import static com.alexk8s.highsub.util.PathUtils.getTempFile;

public class SimpleConcatVideos implements FfmpegOperation<String> {
    private final List<String> videos;
    private final String outputPath;
    private final String sourcePath;

    public SimpleConcatVideos(List<String> videos, String format) {
        this.videos = videos;
        outputPath = getTempFile(format);
        sourcePath = getTempFile("text");
    }


    @Override
    public Function getFunction() {
        return Function.FFMPEG;
    }

    @Override
    public List<Argument> getArguments() {
        return List.of(
                Argument.of("-safe", "0"),
                Argument.of("-f", "concat"),
                Argument.of("-i", sourcePath),
                Argument.of("-c", "copy"),
                Argument.of(outputPath)
        );
    }

    @Override
    @SneakyThrows(IOException.class)
    public String execute(FfmpegTools ffmpegTools) {
        List<String> files = videos.stream().map(s -> "file '" + s + "'").toList();
        Files.write(Path.of(sourcePath), files);
        ffmpegTools.execute(getFunction(), getArguments());
        return outputPath;
    }
}
