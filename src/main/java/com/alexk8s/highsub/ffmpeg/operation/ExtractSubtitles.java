package com.alexk8s.highsub.ffmpeg.operation;

import com.alexk8s.highsub.ffmpeg.FfmpegTools;
import com.alexk8s.highsub.model.SubtitleFormat;

import java.util.List;
import java.util.function.Consumer;

import static com.alexk8s.highsub.util.PathUtils.getTempFile;

public class ExtractSubtitles implements FfmpegOperation<String> {
    private final String videoPath;
    private final String outputPath;
    private final Integer streamIndex;
    private final Consumer<Long> progressHandler;

    public ExtractSubtitles(String videoPath, SubtitleFormat subtitleFormat, Integer streamIndex, Consumer<Long> progressHandler) {
        this.videoPath = videoPath;
        outputPath = getTempFile(subtitleFormat.getExtension());
        this.streamIndex = streamIndex;
        this.progressHandler = progressHandler;
    }


    @Override
    public Function getFunction() {
        return Function.FFMPEG;
    }

    @Override
    public List<Argument> getArguments() {
        return List.of(
                Argument.of("-i", videoPath),
                Argument.of("-map", "0:" + streamIndex),
                Argument.of("-progress","-"),
                Argument.of(outputPath)
        );
    }

    void onProgressUpdate(String outputLine) {
        if (progressHandler != null) {
            Long progress = parseProgress(outputLine);
            if(progress != -1)
                progressHandler.accept(progress);
        }
    }

    Long parseProgress(String line) {
        if (line.trim().equals("progress=end")) {
            return -1L;
        }

        if (!line.trim().startsWith("out_time=")) {
            return -1L;
        }

        final String[] split = line.split("=");
        if (split.length != 2) {
            return -1L;
        }

        final long progress = timeToMilliseconds(split[1]);
        if (progress < 0) {
            return -1L;
        }
        return progress;
    }

    private static Long timeToMilliseconds(final String time) {
        final String[] parts = time.replace(".", ":").split(":");
        long milliseconds = 0L;
        for (int i = 0; i < parts.length; i++) {
            if (i == 0) {
                milliseconds += Long.parseLong(parts[i]) * 3600000;
            } else if (i == 1) {
                milliseconds += Long.parseLong(parts[i]) * 60000;
            } else if (i == 2) {
                milliseconds += Long.parseLong(parts[i]) * 1000;
            } else if (i == 3) {
                milliseconds += Long.parseLong(parts[i].substring(0, 3));
            }
        }
        return milliseconds;
    }


    @Override
    public String execute(FfmpegTools ffmpegTools) {
        ffmpegTools.execute(getFunction(), getArguments(), this::onProgressUpdate);
        return outputPath;
    }


}
