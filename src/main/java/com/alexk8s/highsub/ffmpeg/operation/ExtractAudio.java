package com.alexk8s.highsub.ffmpeg.operation;

import com.alexk8s.highsub.ffmpeg.FfmpegTools;
import lombok.Builder;

import java.util.ArrayList;
import java.util.List;

import static com.alexk8s.highsub.util.PathUtils.getTempFile;


public class ExtractAudio implements FfmpegOperation<String>{
    private final String videoPath;
    private final Long startTime;
    private final Long endTime;
    private final Integer audioIndex;
    private final String outputPath;

    @Builder
    public ExtractAudio(String videoPath, Long startTime, Long endTime, Integer audioIndex, String format) {
        this.videoPath = videoPath;
        this.startTime = startTime;
        this.endTime = endTime;
        this.audioIndex = audioIndex;
        this.outputPath = getTempFile(format);
    }

    @Override
    public Function getFunction() {
        return Function.FFMPEG;
    }

    @Override
    public List<Argument> getArguments() {
        List<Argument> arguments = new ArrayList<>();
        arguments.add(Argument.of("-nostats"));
        arguments.add(Argument.of("-i", videoPath));
        if (startTime != null)
            arguments.add(Argument.of("-ss", msToFfmpegFormat(startTime)));
        if (startTime != null && endTime != null) {
            arguments.add(Argument.of("-t", msToFfmpegFormat(endTime - startTime)));
        }
        if (audioIndex != null) {
            arguments.add(Argument.of("-map", "0:" + audioIndex));
        }
        arguments.add(Argument.of(outputPath));
        return arguments;
    }

    @Override
    public String execute(FfmpegTools ffmpegTools) {
        ffmpegTools.execute(getFunction(),getArguments());
        return outputPath;
    }
}
