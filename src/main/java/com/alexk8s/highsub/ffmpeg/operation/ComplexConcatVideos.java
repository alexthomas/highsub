package com.alexk8s.highsub.ffmpeg.operation;

import com.alexk8s.highsub.ffmpeg.FfmpegTools;

import java.util.ArrayList;
import java.util.List;

import static com.alexk8s.highsub.util.PathUtils.getTempFile;

public class ComplexConcatVideos implements FfmpegOperation<String>{
    private static final int TARGET_WIDTH = 854;
    private static final int TARGET_HEIGHT = 480;
    private final List<String> videos;
    private final String outputPath;

    public ComplexConcatVideos(List<String> videos, String format) {
        this.videos = videos;
        this.outputPath = getTempFile(format);
    }


    @Override
    public Function getFunction() {
        return Function.FFMPEG;
    }

    Argument getFilterComplex(){
        StringBuilder filterComplex = new StringBuilder();
        for (int i = 0; i < videos.size(); i++) {
            filterComplex.append('[').append(i).append(":v]pad=width=").append(TARGET_WIDTH).append(":height=").append(TARGET_HEIGHT).append(":x=(ow-iw)/2:y=(oh-ih)/2, setdar=16/9[v").append(i).append("];"); //resize videos so they will all be 16:9, adding black bars on the sides if required
        }
        filterComplex.append("      ");
        for (int i = 0; i < videos.size(); i++) {
            filterComplex.append("[v").append(i).append("][").append(i).append(":a] "); //list sources for concat
        }
        filterComplex.append("      ");
        filterComplex.append("concat=n=").append(videos.size()).append(":v=1:a=1 [v] [a]"); //concat with 'n' videos, output a single video and audio stream
        return Argument.of("-filter_complex",filterComplex.toString());
    }

    @Override
    public List<Argument> getArguments() {
        List<Argument> arguments = new ArrayList<>();
        videos.forEach(v->arguments.add(Argument.of("-i",v)));
        arguments.add(getFilterComplex());
        arguments.add(Argument.of("-map","[v]"));
        arguments.add(Argument.of("-map","[a]"));
        arguments.add(Argument.of("-vsync","2"));
        arguments.add(Argument.of(outputPath));
        return arguments;
    }

    @Override
    public String execute(FfmpegTools ffmpegTools) {
        ffmpegTools.execute(getFunction(),getArguments());
        return outputPath;
    }
}
