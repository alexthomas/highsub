package com.alexk8s.highsub.ffmpeg.operation;

import com.alexk8s.highsub.ffmpeg.FfmpegTools;
import lombok.AllArgsConstructor;

import java.util.List;

@AllArgsConstructor
public class GetDuration implements FfmpegOperation<Long>{
    private final String path;
    @Override
    public Function getFunction() {
        return Function.FFPROBE;
    }

    @Override
    public List<Argument> getArguments() {
        return List.of(
                Argument.of("-v","error"),
                Argument.of("-show_entries","format=duration"),
                Argument.of("-of","default=noprint_wrappers=1:nokey=1"),
                Argument.of(path)
        );
    }

    @Override
    public Long execute(FfmpegTools ffmpegTools) {
        String output = ffmpegTools.execute(getFunction(), getArguments());
        return (long) (Double.parseDouble(output) * 1000);
    }
}
