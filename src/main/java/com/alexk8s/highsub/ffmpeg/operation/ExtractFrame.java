package com.alexk8s.highsub.ffmpeg.operation;

import com.alexk8s.highsub.ffmpeg.FfmpegTools;
import lombok.Builder;

import java.util.List;

import static com.alexk8s.highsub.util.PathUtils.getTempFile;

public class ExtractFrame implements FfmpegOperation<String>{

    private static final String IMAGE_SCALE_FILTER =  "scale=iw*sar:ih";

    private final String videoPath;
    private final Long time;
    private final String quality;
    private final String subtitlePath;
    private final String outputPath;

    @Builder
    public ExtractFrame(String videoPath, Long time, String format, String quality, String subtitlePath) {
        this.videoPath = videoPath;
        this.time = time;
        this.quality = quality;
        this.subtitlePath = subtitlePath;
        outputPath = getTempFile(format);
    }


    @Override
    public Function getFunction() {
        return Function.FFMPEG;
    }

    @Override
    public List<Argument> getArguments() {
        String filter = subtitlePath!=null?String.format("subtitles=%s,%s",subtitlePath,IMAGE_SCALE_FILTER):IMAGE_SCALE_FILTER;
        return List.of(
                Argument.of("-ss",msToFfmpegFormat(time)),
                Argument.of("-i",videoPath),
                Argument.of("-vf",filter),
                Argument.of("-vframes","1"),
                Argument.of("-q:v",quality),
                Argument.of("-copyts"),
                Argument.of(outputPath)
        );
    }

    @Override
    public String execute(FfmpegTools ffmpegTools) {
        ffmpegTools.execute(getFunction(),getArguments());
        return outputPath;
    }
}
