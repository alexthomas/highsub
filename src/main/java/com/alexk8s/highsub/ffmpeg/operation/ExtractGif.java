package com.alexk8s.highsub.ffmpeg.operation;

import com.alexk8s.highsub.ffmpeg.FfmpegTools;

import java.util.ArrayList;
import java.util.List;

import static com.alexk8s.highsub.util.PathUtils.getTempFile;

public class ExtractGif implements FfmpegOperation<String> {
    private static final String FORMAT = "gif";
    private final String videoPath;
    private final Integer videoIndex;
    private final String subtitlePath;
    private final Long startTime;
    private final Long endTime;
    private final Integer height;
    private final String outputPath;

    public ExtractGif(String videoPath, Integer videoIndex, String subtitlePath, Long startTime, Long endTime, Integer height) {
        this.videoPath = videoPath;
        this.videoIndex = videoIndex;
        this.subtitlePath = subtitlePath;
        this.startTime = startTime;
        this.endTime = endTime;
        this.height = height;
        this.outputPath = getTempFile(FORMAT);
    }

    @Override
    public Function getFunction() {
        return Function.FFMPEG;
    }

    Argument getFilter() {
        List<String> filter = new ArrayList<>();
        filter.add("fps=10");
        filter.add("split[s0][s1];[s0]palettegen[p];[s1][p]paletteuse");
        if (height != null)
            filter.add("scale=-2:%d:flags=lanczos".formatted(height));
       if(subtitlePath!=null)
            filter.add("subtitles=%s".formatted(subtitlePath));
        return Argument.of("-vf", String.join(",", filter));
    }

    @Override
    public List<Argument> getArguments() {
        List<Argument> arguments = new ArrayList<>();
        arguments.add(Argument.of("-ss", msToFfmpegFormat(startTime)));
        arguments.add(Argument.of("-t", msToFfmpegFormat(endTime - startTime)));
        arguments.add(Argument.of("-i", videoPath));
        if(videoIndex!=null)
            arguments.add(Argument.of("-map", "0:" + videoIndex));
        arguments.add(getFilter());
        arguments.add(Argument.of(outputPath));
        return arguments;
    }

    @Override
    public String execute(FfmpegTools ffmpegTools) {
        ffmpegTools.execute(getFunction(), getArguments());
        return outputPath;
    }
}
