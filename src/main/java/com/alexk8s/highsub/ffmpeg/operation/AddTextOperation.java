package com.alexk8s.highsub.ffmpeg.operation;

import lombok.AllArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
public abstract class AddTextOperation implements FfmpegOperation<String>{

    private static final int TEXT_PADDING = 20;
    private static final int APROX_CHARACTER_WIDTH = 16;
    private final String text;
    private final int videoWidth;

    @Override
    public Function getFunction() {
        return Function.FFMPEG;
    }

    String buildFiltersForText() {
        List<String> lines = new ArrayList<>();
        String[] words = text.split(" ");
        int maxLineLength = (videoWidth - TEXT_PADDING * 2) / APROX_CHARACTER_WIDTH;
        int cursor = 0;
        while (cursor < words.length) {
            StringBuilder line = new StringBuilder(words[cursor++].replaceAll("[:']", ""));
            while (cursor < words.length && line.length() + words[cursor].length() < maxLineLength)
                line.append(' ').append(words[cursor++].replaceAll("[:']", ""));
            lines.add(line.toString());
        }
        int textAreaHeight = 35 * lines.size() + TEXT_PADDING;
        String formattedText = String.join(((char) 10) + "", lines);
        String padFilter = "pad=width=iw:height=ih+" + textAreaHeight + ":y=" + textAreaHeight + ":color=white";
        String textFilter = "drawtext=font='sans-serif':text='" + formattedText + "':fontcolor=black:fontsize=30:line_spacing=5:x=(w-text_w)/2:y=" + TEXT_PADDING;

        return padFilter + "," + textFilter;
    }

    }
