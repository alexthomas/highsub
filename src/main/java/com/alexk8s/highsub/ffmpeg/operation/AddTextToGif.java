package com.alexk8s.highsub.ffmpeg.operation;

import com.alexk8s.highsub.ffmpeg.FfmpegTools;

import java.util.List;

import static com.alexk8s.highsub.util.PathUtils.getTempFile;

public class AddTextToGif extends AddTextOperation{
    private static final String FORMAT = "gif";
    private final String gifPath;
    private final String outputPath;

    public AddTextToGif(String gifPath,String text, int width) {
        super(text, width);
        this.gifPath = gifPath;
        outputPath = getTempFile(FORMAT);
    }

    @Override
    public List<Argument> getArguments() {
        return List.of(
                Argument.of("-i", gifPath),
                Argument.of("-vf", buildFiltersForText()),
                Argument.of(outputPath)
        );
    }

    @Override
    public String execute(FfmpegTools ffmpegTools) {
        ffmpegTools.execute(getFunction(),getArguments());
        return outputPath;
    }
}
