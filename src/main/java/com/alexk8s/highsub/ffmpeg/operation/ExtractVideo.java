package com.alexk8s.highsub.ffmpeg.operation;

import com.alexk8s.highsub.ffmpeg.FfmpegTools;
import lombok.Builder;
import lombok.SneakyThrows;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.alexk8s.highsub.util.PathUtils.getTempFile;

public class ExtractVideo implements FfmpegOperation<String> {
    private final String videoPath;
    private final String subtitlePath;
    private final Long startTime;
    private final Long endTime;
    private final Integer height;
    private final Integer videoIndex;
    private final Integer audioIndex;
    private final Boolean fullEncode; //if this is set to true chapters and metadata will be removed and color format will be set during encoding
    private final String outputPath;
    private final String videoEncoding;
    private final String encodingPreset;
    private final String crf;
    private final String format;
    private final String loop;
    private String compressionLevel;
    private String quality;

    @Builder
    public ExtractVideo(String videoPath, String subtitlePath, Long startTime, Long endTime, Integer height, Integer videoIndex, Integer audioIndex, String format, Boolean fullEncode, String videoEncoding, String encodingPreset, String crf,String loop, String compressionLevel, String quality) {
        this.videoPath = videoPath;
        this.subtitlePath = subtitlePath;
        this.startTime = startTime;
        this.endTime = endTime;
        this.height = height;
        this.videoIndex = videoIndex;
        this.audioIndex = audioIndex;
        this.outputPath = getTempFile(format);
        this.fullEncode = fullEncode != null && fullEncode;
        this.format = format;
        this.loop = loop;
        this.compressionLevel = compressionLevel;
        this.quality = quality;
        if (!this.fullEncode && videoEncoding == null) {
            this.videoEncoding = "libx264";
            this.encodingPreset = "ultrafast";
            this.crf = "22";
        } else if ("libx264".equals(videoEncoding) && (encodingPreset == null || crf == null)) {
            throw new IllegalArgumentException("If videoEncoding is set, encodingPreset and crf must be set as well");
        } else {
            this.videoEncoding = videoEncoding;
            this.encodingPreset = encodingPreset;
            this.crf = crf;

        }
    }

    @Override
    public Function getFunction() {
        return Function.FFMPEG;
    }

    Optional<Argument> getFilter() {

        List<String> filter = new ArrayList<>();
        if (height != null)
            filter.add(String.format("scale=-2:%d", height));
        if (subtitlePath != null)
            filter.add(String.format("subtitles=%s", subtitlePath));
        if (fullEncode)
            filter.add("format=yuv420p");
        if (!filter.isEmpty())
            return Optional.of(Argument.of("-vf", String.join(",", filter)));
        return Optional.empty();
    }

    @Override
    public List<Argument> getArguments() {
        List<Argument> arguments = new ArrayList<>();
        arguments.add(Argument.of("-nostats"));
        if (startTime != null)
            arguments.add(Argument.of("-ss", msToFfmpegFormat(startTime)));
        if (startTime != null && endTime != null) {
            arguments.add(Argument.of("-t", msToFfmpegFormat(endTime - startTime)));
        }
        arguments.add(Argument.of("-i", videoPath));
        arguments.add(Argument.of("-copyts"));
        if (startTime != null)
            arguments.add(Argument.of("-ss", msToFfmpegFormat(startTime)));
        if (startTime != null && endTime != null) {
            arguments.add(Argument.of("-t", msToFfmpegFormat(endTime - startTime)));
        }
        if (videoEncoding != null)
            arguments.add(Argument.of("-c:v", videoEncoding));
        if (encodingPreset != null)
            arguments.add(Argument.of("-preset", encodingPreset));
        if (crf != null)
            arguments.add(Argument.of("-crf", crf));
        if (audioIndex != null) {
            arguments.add(Argument.of("-map", String.format("0:%d", audioIndex)));
        }
        if (videoIndex != null) {
            arguments.add(Argument.of("-map", String.format("0:%d", videoIndex)));
        }
        if (fullEncode) {
            arguments.add(Argument.of("-dn"));
            arguments.add(Argument.of("-map_metadata:c", "-1"));
        }
        if(loop != null){
            arguments.add(Argument.of("-loop",loop));
        }
        if (compressionLevel != null) {
            arguments.add(Argument.of("-compression_level", compressionLevel));
        }
        if (quality != null) {
            arguments.add(Argument.of("-q:v", quality));
        }
        getFilter().ifPresent(arguments::add);
        arguments.add(Argument.of(outputPath));
        return arguments;
    }

    List<Argument> getRemoveChaptersArguments(String source, String output) {
        return List.of(
                Argument.of("-nostats"),
                Argument.of("-ignore_chapters", "1"),
                Argument.of("-i", source),
                Argument.of("-c","copy"),
                Argument.of(output)
        );
    }

    @Override
    @SneakyThrows(IOException.class)
    public String execute(FfmpegTools ffmpegTools) {
        ffmpegTools.execute(getFunction(), getArguments());
        if (!fullEncode)
            return outputPath;
        String output = getTempFile(format);
        ffmpegTools.execute(getFunction(), getRemoveChaptersArguments(outputPath, output));
        Files.delete(Path.of(outputPath));
        return output;
    }
}
