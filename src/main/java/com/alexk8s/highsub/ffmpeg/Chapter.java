package com.alexk8s.highsub.ffmpeg;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Data;

@Data
public class Chapter {
    private Long id;
    @JsonAlias("time_base")
    private String timeBase;
    private Long start;
    @JsonAlias("start_time")
    private String startTime;
    private Long end;
    @JsonAlias("end_time")
    private String endTime;
    private Tags tags;
}
