package com.alexk8s.highsub.elasticsearch;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.With;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import java.util.List;

@Document(indexName = "subtitles",createIndex = false)
@Data
@AllArgsConstructor
@NoArgsConstructor
@With
@JsonIgnoreProperties(ignoreUnknown = true)
public class SearchResult {
    @Id
    private String id;
    private String show;
    private String season;
    private String episode;
    private Integer subtitleNumber;
    private String text;
    private String romaji;
    private String kanji;
    private String style;
    private String name;
    private String matchRate;
    private Long startTime;
    private Long endTime;
    private Long japaneseStartTime;
    private Long japaneseEndTime;
    private List<String> tags;
    private List<String> context;
    private List<String> rawSubtitle;
    private double score;

}
