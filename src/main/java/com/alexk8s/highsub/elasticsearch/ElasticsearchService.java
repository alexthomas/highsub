package com.alexk8s.highsub.elasticsearch;

import co.elastic.clients.elasticsearch._types.KnnQuery;
import co.elastic.clients.elasticsearch._types.aggregations.Aggregate;
import co.elastic.clients.elasticsearch._types.aggregations.Aggregation;
import co.elastic.clients.elasticsearch._types.aggregations.TermsAggregation;
import co.elastic.clients.elasticsearch._types.query_dsl.BoolQuery;
import co.elastic.clients.elasticsearch._types.query_dsl.MatchQuery;
import co.elastic.clients.elasticsearch._types.query_dsl.Operator;
import co.elastic.clients.elasticsearch._types.query_dsl.Query;
import co.elastic.clients.elasticsearch._types.query_dsl.RangeQuery;
import co.elastic.clients.elasticsearch._types.query_dsl.TermQuery;
import co.elastic.clients.elasticsearch.core.SearchRequest;
import co.elastic.clients.elasticsearch.core.search.ResponseBody;
import co.elastic.clients.elasticsearch.core.search.SourceConfig;
import co.elastic.clients.json.JsonData;
import com.alexk8s.highsub.kafka.model.SubtitleV4;
import com.alexk8s.highsub.model.Chapter;
import com.alexk8s.highsub.service.EmbeddingService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.data.elasticsearch.client.elc.ReactiveElasticsearchClient;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.retry.Retry;

import java.time.Duration;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
@Slf4j
@AllArgsConstructor
public class ElasticsearchService {
    private static final ZonedDateTime CONTENT_HIT_UPDATE_TIME = ZonedDateTime.of(2024, 9, 10, 0, 0, 0, 0, ZoneId.of("America/New_York"));

    private final EmbeddingService embeddingService;
    private final IndexedSubtitleRepository indexedSubtitleRepository;
    private final ReactiveElasticsearchClient reactiveElasticsearchClient;


    public Mono<IndexedSubtitle> mapSubtitle(SubtitleV4 subtitleV4) {
        IndexedSubtitle indexedSubtitle = new IndexedSubtitle();
        BeanUtils.copyProperties(subtitleV4, indexedSubtitle);
        indexedSubtitle.setId(subtitleV4.getIndexedId());
        indexedSubtitle.setKanji(subtitleV4.getJapaneseText());
        indexedSubtitle.setChapter(Chapter.valueOf(subtitleV4.getChapter()));
        if (indexedSubtitle.getTags() != null && subtitleV4.getParentTags() != null)
            indexedSubtitle.getTags().addAll(subtitleV4.getParentTags());
        return embeddingService.getEmbedding(indexedSubtitle.getText()).retryWhen(Retry.backoff(5, Duration.ofMillis(400))).map(indexedSubtitle::withEmbedding);
    }

    public Flux<IndexedSubtitle> indexSubtitles(List<IndexedSubtitle> indexedSubtitles) {
        return indexedSubtitleRepository.saveAll(indexedSubtitles);

    }

    public Mono<Void> deleteSubtitle(SubtitleV4 subtitleV4) {
        return indexedSubtitleRepository.deleteById(subtitleV4.getIndexedId());
    }

    public Flux<SearchResult> query(SearchCriteria searchCriteria) {
        if (searchCriteria.getQ() == null || searchCriteria.getQ().isEmpty()) {
            SearchRequest searchRequest = SearchRequest.of(b -> buildSearch(b, searchCriteria));
            return reactiveElasticsearchClient.search(searchRequest, IndexedSubtitle.class)
                    .flatMapIterable(this::mapSearchResults);
        }
        return embeddingService.getEmbedding(searchCriteria.getQ())
                .flatMapMany(e -> query(e, searchCriteria));
    }

    public Flux<SearchResult> exactSearch(String text){
        Query query = Query.of(b->b.term(t->t.field("text.keyword").value(text).caseInsensitive(true)));
        SearchRequest searchRequest = SearchRequest.of(b-> b.index("subtitles").query(query).size(300));
        return reactiveElasticsearchClient.search(searchRequest,IndexedSubtitle.class)
                .flatMapIterable(this::mapSearchResults);

    }

    private Flux<SearchResult> query(Double[] embedding, SearchCriteria searchCriteria) {

        SearchRequest searchRequest = SearchRequest.of(b -> buildSearch(b, embedding, searchCriteria));

        return reactiveElasticsearchClient.search(searchRequest, IndexedSubtitle.class)
                .flatMapIterable(this::mapSearchResults);
    }

    private SearchRequest.Builder buildSearch(SearchRequest.Builder builder, SearchCriteria searchCriteria) {
        List<Query> filters = getFilters(searchCriteria);
        BoolQuery boolQuery = BoolQuery.of(b -> {
            filters.forEach(b::filter);
            return b;
        });
        builder.query(Query.of(b -> b.bool(boolQuery)));
        builder.from(searchCriteria.getSize() * searchCriteria.getPage());
        builder.size(searchCriteria.getSize());
        builder.index("subtitles");

        return builder;
    }

    private SearchRequest.Builder buildSearch(SearchRequest.Builder builder, Double[] embedding, SearchCriteria searchCriteria) {
        KnnQuery knnQuery = KnnQuery.of(b -> buildKnnQuery(b, embedding, searchCriteria));
        builder.source(SourceConfig.of(b -> b.fetch(true)));
        builder.from(searchCriteria.getSize() * searchCriteria.getPage());
        builder.size(searchCriteria.getSize());
        return builder.knn(knnQuery);
    }

    private KnnQuery.Builder buildKnnQuery(KnnQuery.Builder builder, Double[] embedding, SearchCriteria searchCriteria) {
        getFilters(searchCriteria).forEach(builder::filter);
        int offset = searchCriteria.getSize() * searchCriteria.getPage();
        return builder.field("embedding")
                .k((long) searchCriteria.getSize() * (searchCriteria.getPage() + 1))
                .numCandidates(100 + offset)

                .queryVector(Arrays.asList(embedding));
    }

    private List<Query> getFilters(SearchCriteria searchCriteria) {
        List<Query> filters = new ArrayList<>();
        if (searchCriteria.getText() != null) {
            MatchQuery matchQuery = MatchQuery.of(b -> b.query(searchCriteria.getText()).field("text").operator(Operator.And));
            filters.add(Query.of(b -> b.match(matchQuery)));
        }
        if (searchCriteria.getRomaji() != null) {
            MatchQuery matchQuery = MatchQuery.of(b -> b.query(searchCriteria.getRomaji()).field("romaji").operator(Operator.And));
            filters.add(Query.of(b -> b.match(matchQuery)));
        }
        if (searchCriteria.getKanji() != null) {
            MatchQuery matchQuery = MatchQuery.of(b -> b.query(searchCriteria.getKanji()).field("kanji").operator(Operator.And));
            filters.add(Query.of(b -> b.match(matchQuery)));
        }
        if (searchCriteria.getShow() != null) {
            TermQuery termQuery = TermQuery.of(b -> b.value(searchCriteria.getShow()).field("show.keyword").caseInsensitive(true));
            filters.add(Query.of(b -> b.term(termQuery)));
        }
        if (searchCriteria.getName() != null) {
            TermQuery termQuery = TermQuery.of(b -> b.value(searchCriteria.getName()).field("name.keyword").caseInsensitive(true));
            filters.add(Query.of(b -> b.term(termQuery)));
        }
        if (searchCriteria.getTags() != null && !searchCriteria.getTags().isEmpty()) {
            List<Query> tagQueries = searchCriteria.getTags().stream()
                    .map(t -> MatchQuery.of(b -> b.query(t).field("tags.keyword")))
                    .map(m -> Query.of(b -> b.match(m)))
                    .toList();
            BoolQuery boolQuery = BoolQuery.of(b -> b.must(tagQueries));
            filters.add(Query.of(b -> b.bool(boolQuery)));
        }
        if (searchCriteria.getBlacklistShow() != null && !searchCriteria.getBlacklistShow().isEmpty()) {
            List<Query> blacklistShowQueries = searchCriteria.getBlacklistShow().stream()
                    .map(t -> MatchQuery.of(b -> b.query(t).field("show.keyword")))
                    .map(m -> Query.of(b -> b.match(m)))
                    .toList();
            BoolQuery boolQuery = BoolQuery.of(b -> b.mustNot(blacklistShowQueries));
            filters.add(Query.of(b -> b.bool(boolQuery)));
        }
        if (searchCriteria.getBlacklistTags() != null && !searchCriteria.getBlacklistTags().isEmpty()) {
            List<Query> tagQueries = searchCriteria.getBlacklistTags().stream()
                    .map(t -> MatchQuery.of(b -> b.query(t).field("tags.keyword")))
                    .map(m -> Query.of(b -> b.match(m)))
                    .toList();
            BoolQuery boolQuery = BoolQuery.of(b -> b.mustNot(tagQueries));
            filters.add(Query.of(b -> b.bool(boolQuery)));
        }
        return filters;
    }

    private List<SearchResult> mapSearchResults(ResponseBody<IndexedSubtitle> responseBody) {
        return responseBody.hits().hits().stream()
                .map(h -> {
                    SearchResult searchResult = new SearchResult();
                    BeanUtils.copyProperties(h.source(), searchResult);
                    searchResult.setScore(h.score());
                    return searchResult;
                })
                .toList();
    }


    public Flux<IndexedContentHit> getPopular(int count) {
        ZonedDateTime fromZonedDateTime = ZonedDateTime.now().minusWeeks(1).withNano(0);
        if (fromZonedDateTime.isBefore(CONTENT_HIT_UPDATE_TIME))
            fromZonedDateTime = CONTENT_HIT_UPDATE_TIME;
        String fromTime = fromZonedDateTime.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
        Query query = Query.of(b1 -> b1.bool(BoolQuery.of(b2 -> b2.filter(Query.of(b3 -> b3.range(RangeQuery.of(b4 -> b4.field("hitTime").gte(JsonData.of(fromTime)))))))));
        return getPopular(count, query);
    }

    public Flux<IndexedContentHit> getPopular(int count, String show) {
        JsonData fromTime = JsonData.of(CONTENT_HIT_UPDATE_TIME.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME));
        Query query = Query.of(b1 -> b1.bool(BoolQuery.of(b2 -> b2.filter(Query.of(b3 -> b3.term(TermQuery.of(b4 -> b4.value(show).field("show.keyword")))))
                .filter(Query.of(b5 -> b5.range(RangeQuery.of(b6 -> b6.field("hitTime").gte(fromTime))))))));
        return getPopular(count, query);
    }

    private Flux<IndexedContentHit> getPopular(int count, Query query) {
        SearchRequest searchRequest = SearchRequest.of(b -> {
            b.aggregations("subtitle", Aggregation.of(b1 -> b1.terms(TermsAggregation.of(b2 -> b2.field("subtitleId.keyword").size(count)))));
            b.query(query);
            b.index("content-hits");
            b.size(0);
            return b;
        });
        return reactiveElasticsearchClient.search(searchRequest, IndexedContentHit.class)
                .flatMapIterable(r -> {
                    Aggregate subtitle = r.aggregations().get("subtitle");
                    return subtitle.sterms().buckets().array().stream().map(s -> s.key().stringValue()).toList();
                })
                .map(IndexedContentHit::fromSubtitleId);
    }
}
