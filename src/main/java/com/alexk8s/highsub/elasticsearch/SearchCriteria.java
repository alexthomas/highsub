package com.alexk8s.highsub.elasticsearch;

import lombok.Data;

import java.util.List;

@Data
public class SearchCriteria {
    private String q;
    private String text;
    private String romaji;
    private String kanji;
    private String show;
    private String season;
    private String episode;
    private List<String> tags;
    private String name;
    private List<String> blacklistShow;
    private List<String> blacklistTags;
    private int size=10;
    private int page=0;
}
