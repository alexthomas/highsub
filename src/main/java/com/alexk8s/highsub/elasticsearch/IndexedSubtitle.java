package com.alexk8s.highsub.elasticsearch;

import com.alexk8s.highsub.model.Chapter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.With;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import java.util.Arrays;
import java.util.List;

@Document(indexName = "subtitles",createIndex = false)
@Data
@AllArgsConstructor
@NoArgsConstructor
@With
@JsonIgnoreProperties(ignoreUnknown = true)
public class IndexedSubtitle {
    @Id
    private String id;
    private String show;
    private String season;
    private String episode;
    private Integer subtitleNumber;
    private String text;
    private String japaneseText;
    private String kanji;
    private String romaji;
    private String style;
    private String name;
    private String matchRate;
    private Long startTime;
    private Long endTime;
    private Long japaneseStartTime;
    private Long japaneseEndTime;
    private List<String> tags;
    private List<String> context;
    private List<String> rawSubtitle;
    private Double[] embedding;
    private Chapter chapter;

    @Override
    public String toString() {
        String embeddingString;
        if(embedding.length<5)
            embeddingString = Arrays.toString(embedding);
        else{
            StringBuilder stringBuilder = new StringBuilder("[");
            for (int i = 0; i < 4; i++) {
               stringBuilder.append(embedding[i]).append(",");
            }
            stringBuilder.append("(").append(embedding.length).append(")...]");
            embeddingString=stringBuilder.toString();
        }
        return "IndexedSubtitle{" +
                "id='" + id + '\'' +
                ", show='" + show + '\'' +
                ", season='" + season + '\'' +
                ", episode='" + episode + '\'' +
                ", subtitleNumber=" + subtitleNumber +
                ", text='" + text + '\'' +
                ", japaneseText='" + japaneseText + '\'' +
                ", kanji='" + kanji + '\'' +
                ", style='" + style + '\'' +
                ", name='" + name + '\'' +
                ", matchRate='" + matchRate + '\'' +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", japaneseStartTime=" + japaneseStartTime +
                ", japaneseEndTime=" + japaneseEndTime +
                ", tags=" + tags +
                ", context=" + context +
                ", rawSubtitle=" + rawSubtitle +
                ", chapter=" + chapter +
                ", embedding=" + embeddingString +
                '}';
    }
}
