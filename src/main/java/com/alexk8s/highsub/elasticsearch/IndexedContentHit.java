package com.alexk8s.highsub.elasticsearch;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.time.ZonedDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(indexName = "content-hits")
@JsonIgnoreProperties(ignoreUnknown = true)
public class IndexedContentHit {
    @Id
    private String id;
    private String show;
    private String season;
    private String episode;
    private String subtitle;
    private String type;
    @Field(type = FieldType.Date)
    private ZonedDateTime hitTime;
    private String episodeId;
    private String subtitleId;
    private String user;
    private int height;

    public static String getEpisodeId(String show, String season, String episode) {
        return show + "-" + season + "-" + episode;
    }

    public static String getSubtitleId(String show, String season, String episode, String subtitleNumber) {
        return show + "-" + season + "-" + episode + "-" + subtitleNumber;
    }

    public static IndexedContentHit fromSubtitleId(String subtitleId) {
        String[] parts = new String[4];
        int cursor = subtitleId.length();
        for (int i = parts.length-1; i >= 0; i--) {
            int nextDash = subtitleId.lastIndexOf("-", cursor-1);
            parts[i] = subtitleId.substring(nextDash+1, cursor);
            cursor = nextDash;
        }
        return new IndexedContentHit(null, parts[0], parts[1], parts[2], parts[3], null, null, null, subtitleId, null, 0);
    }
}
