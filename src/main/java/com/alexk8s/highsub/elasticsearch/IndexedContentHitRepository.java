package com.alexk8s.highsub.elasticsearch;

import org.springframework.data.elasticsearch.repository.ReactiveElasticsearchRepository;
import reactor.core.publisher.Mono;

public interface IndexedContentHitRepository extends ReactiveElasticsearchRepository<IndexedContentHit,String> {

    Mono<IndexedContentHit> findBySubtitleId(String subtitleId);
}
