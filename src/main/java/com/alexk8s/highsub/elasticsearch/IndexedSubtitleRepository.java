package com.alexk8s.highsub.elasticsearch;

import org.springframework.data.elasticsearch.repository.ReactiveElasticsearchRepository;

public interface IndexedSubtitleRepository extends ReactiveElasticsearchRepository<IndexedSubtitle, String> {
}
