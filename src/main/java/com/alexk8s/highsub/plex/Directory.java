package com.alexk8s.highsub.plex;

import com.fasterxml.jackson.annotation.JsonAlias;

import java.util.List;

public record Directory(
        boolean allowSync,
        String art,
        String composite,
        boolean filters,
        boolean refreshing,
        String thumb,
        String key,
        String type,
        String title,
        String agent,
        String scanner,
        String language,
        String uuid,
        Long updatedAt,
        Long scannedAt,
        Long createdAt,
        boolean content,
        boolean directory,
        Long contentChangedAt,
        Integer hidden,
        @JsonAlias("Location") List<Location> locations


) {
}
