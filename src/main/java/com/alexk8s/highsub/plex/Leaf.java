package com.alexk8s.highsub.plex;

import com.fasterxml.jackson.annotation.JsonAlias;

import java.util.List;
import java.util.Map;

public record Leaf(
        String ratingKey,
        String key,
        String parentRatingKey,
        String grandparentRatingKey,
        String guid,
        String studio,
        String type,
        String title,
        String originalTitle,
        Integer viewCount,
        Long lastViewedAt,
        String titleSort,
        Integer skipCount,
        String skipParent,
        Integer parentYear,
        String viewOffset,
        Integer year,
        String grandparentKey,
        String parentKey,
        String grandparentTitle,
        String parentTitle,
        String contentRating,
        String summary,
        Integer index,
        Integer parentIndex,
        Double audienceRating,
        String thumb,
        String art,
        String parentThumb,
        String grandparentThumb,
        String grandparentArt,
        String grandparentTheme,
        Long duration,
        String originallyAvailableAt,
        Long addedAt,
        Long updatedAt,
        String audienceRatingImage,
        String chapterSource,
        @JsonAlias("Media")
        List<Medium> media,
        @JsonAlias("Director")
        List<Tag> directors,
        @JsonAlias("Writer")
        List<Tag> writers,
        @JsonAlias("Role")
        List<Tag> roles,
        String grandparentSlug,
        @JsonAlias("UltraBlurColors")
        Map<String,String> ultraBlurColors,
        @JsonAlias("Image")
        List<Image> images
) {
}
