package com.alexk8s.highsub.plex;

public record MediumPart(
        Integer id,
        String key,
        Long duration,
        String file,
        Long size,
        String audioProfile,
        String container,
        String videoProfile,
        String hasThumbnail,
        String has64bitOffsets,
        String optimizedForStreaming,
        String packetLength

) {

}
