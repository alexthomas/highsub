package com.alexk8s.highsub.plex;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonRootName;

import java.util.List;

@JsonRootName("MediaContainer")
public record LeafsMediaContainer(
        Integer size,
        boolean allowSync,
        String art,
        String identifier,
        String key,
        Integer librarySectionID,
        String librarySectionTitle,
        String librarySectionUUID,
        String mediaTagPrefix,
        Long mediaTagVersion,
        boolean mixedParents,
        boolean nocache,
        Integer parentIndex,
        String parentTitle,
        Integer parentYear,
        String theme,
        String title1,
        String title2,
        String viewGroup,
        Integer viewMode,
        @JsonAlias("Metadata")
        List<Leaf> leaves

) {
}
