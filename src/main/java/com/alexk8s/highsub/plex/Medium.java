package com.alexk8s.highsub.plex;

import com.fasterxml.jackson.annotation.JsonAlias;

import java.util.List;

public record Medium(
        Integer id,
        Long duration,
        Long bitrate,
        Integer width,
        Integer height,
        Double aspectRatio,
        Integer audioChannels,
        String audioCodec,
        String videoCodec,
        String videoResolution,
        String container,
        String videoFrameRate,
        String audioProfile,
        String videoProfile,
        String displayOffset,
        String optimizedForStreaming,
        Boolean hasVoiceActivity,

        String has64bitOffsets,
        @JsonAlias("Part")
        List<MediumPart> parts
) {
}
