package com.alexk8s.highsub.plex;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonRootName;

import java.util.List;

@JsonRootName("MediaContainer")
public record LibraryMediaContainer (
        int size,
        boolean allowSync,
        String art,
        String identifier,
        Integer librarySectionID,
        String librarySectionTitle,
        String librarySectionUUID,
        String mediaTagPrefix,
        Long mediaTagVersion,
        boolean nocache,
        String thumb,
        String title1,
        String title2,
        String viewGroup,
        Long viewMode,
        @JsonAlias("Metadata")
        List<LibraryMetadata> metadata,
        String content
){
}
