package com.alexk8s.highsub.plex;

public record Image(String alt,String type,String url) {
}
