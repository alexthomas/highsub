package com.alexk8s.highsub.plex;

import com.fasterxml.jackson.annotation.JsonAlias;

import java.util.List;
import java.util.Map;

public record LibraryMetadata(
        String ratingKey,
        String key,
        String guid,
        String studio,
        String type,
        String title,
        String originalTitle,
        String contentRating,
        String summary,
        Integer index,
        Double rating,
        Double audienceRating,
        Integer year,
        String tagline,
        String thumb,
        String art,
        String theme,
        Long duration,
        String originallyAvailableAt,
        Integer viewCount,
        Long lastViewedAt,
        String titleSort,
        boolean skipChildren,
        String showOrdering,
        Integer skipCount,
        Integer leafCount,
        Integer viewedLeafCount,
        Integer childCount,
        Long  addedAt,
        Long updatedAt,
        String ratingImage,
        String audienceRatingImage,
        String primaryExtraKey,
        @JsonAlias("Genre")
        List<Tag> genres,
        @JsonAlias("Country")
        List<Tag> countries,
        @JsonAlias("Role")
        List<Tag> roles,
        @JsonAlias("Director")
        List<Tag> directors,
        @JsonAlias("Writer")
        List<Tag> writers,
        Integer seasonCount,
        String audioLanguage,
        String  subtitleLanguage,
        String subtitleMode,
        @JsonAlias("Media")
        List<Medium> media,
        String chapterSource,
        Long viewOffset,
        Double userRating,
        Long lastRatedAt,
        String subtype,
        String slug,
        @JsonAlias("UltraBlurColors")
        Map<String,String> ultraBlurColors,
        @JsonAlias("Image")
        List<Image> images
) {
}
