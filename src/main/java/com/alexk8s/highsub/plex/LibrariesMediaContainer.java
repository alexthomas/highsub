package com.alexk8s.highsub.plex;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonRootName;

import java.util.List;

@JsonRootName("MediaContainer")
public record LibrariesMediaContainer(int size, boolean allowSync, @JsonAlias("title1")String title,
                                      @JsonAlias("Directory")List<Directory> directories) {
}
