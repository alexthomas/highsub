package com.alexk8s.highsub.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.Map;

@RestController
@RequestMapping("/v1/authentication")
public class AuthenticationController {

    @PostMapping()
    public Mono<ResponseEntity<Map<String, Object>>> authenticate(Authentication authentication) {
        if(authentication.getPrincipal() instanceof Jwt){
            Jwt jwt = (Jwt) authentication.getPrincipal();
            Map<String, Object> user = Map.of("user", jwt.getSubject(), "name", jwt.getClaim("name"), "email", jwt.getClaim("email"), "authenticated", jwt.getSubject(),
                    "authorities",authentication.getAuthorities());
            ResponseEntity<Map<String, Object>> response = ResponseEntity.ok(user);
            return Mono.just(response);
        }
        Map<String, Object> user = Map.of("user", authentication.getPrincipal().toString(), "name", authentication.getName());
        ResponseEntity<Map<String, Object>> response = ResponseEntity.ok(user);
        return Mono.just(response);
    }
}
