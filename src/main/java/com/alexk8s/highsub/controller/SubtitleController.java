package com.alexk8s.highsub.controller;

import com.alexk8s.highsub.configuration.HighsubConfig;
import com.alexk8s.highsub.model.FlatSubtitle;
import com.alexk8s.highsub.repository.FlatSubtitleRepository;
import com.alexk8s.highsub.repository.HashedSubtitleRepository;
import com.alexk8s.highsub.service.SubtitleService;
import com.alexk8s.highsub.util.HashUtil;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import software.amazon.awssdk.services.s3.model.ListObjectVersionsResponse;
import software.amazon.awssdk.services.s3.model.ObjectVersion;

import java.util.Base64;
import java.util.Comparator;
import java.util.List;

@RestController
@RequestMapping("v1/subtitle")
@AllArgsConstructor
@Slf4j
public class SubtitleController {

    private final FlatSubtitleRepository flatSubtitleRepository;
    private final HashedSubtitleRepository hashedSubtitleRepository;
    private final HighsubConfig highsubConfig;
    private final SubtitleService subtitleService;


    @GetMapping(value = "{show}-{season}-{episode}-{subtitleNumber}")
    public Mono<FlatSubtitle> getSubtitle(@PathVariable String show, @PathVariable String season, @PathVariable String episode, @PathVariable int subtitleNumber) {
        return flatSubtitleRepository.findByShowAndSeasonAndEpisodeAndSubtitleNumber(show, season, episode, subtitleNumber).map(this::applyUrls);
    }

    @GetMapping(value = "episode/{show}-{season}-{episode}")
    public Flux<FlatSubtitle> getSubtitles(@PathVariable String show, @PathVariable String season, @PathVariable String episode) {
        return flatSubtitleRepository.findByShowAndSeasonAndEpisode(show, season, episode).map(this::applyUrls);
    }

    @GetMapping(value = "{show}-{season}-{episode}-{subtitleNumber}/next")
    public Flux<FlatSubtitle> getNextSubtitle(@PathVariable String show, @PathVariable String season, @PathVariable String episode, @PathVariable int subtitleNumber,@RequestParam(defaultValue = "1") int count) {
        return flatSubtitleRepository.findNext(show, season, episode, subtitleNumber,count).map(this::applyUrls);
    }

    @GetMapping(value = "{show}-{season}-{episode}-{subtitleNumber}/previous")
    public Flux<FlatSubtitle> getPreviousSubtitle(@PathVariable String show, @PathVariable String season, @PathVariable String episode, @PathVariable int subtitleNumber,@RequestParam(defaultValue = "1") int count) {
        return flatSubtitleRepository.findPrevious(show, season, episode, subtitleNumber,count).map(this::applyUrls)
                .sort(Comparator.comparing(FlatSubtitle::getSubtitleNumber));
    }

    @GetMapping(value = "{show}-{season}-{episode}-{subtitleNumber}/more-from/episode")
    public Flux<FlatSubtitle> getMoreFromEpisode(@PathVariable String show, @PathVariable String season, @PathVariable String episode, @PathVariable int subtitleNumber,
                                                 @RequestParam(defaultValue = "5") int count) {
        Flux<FlatSubtitle> subtitles = flatSubtitleRepository.findByShowAndSeasonAndEpisode(show, season, episode,count);
        return flatSubtitleRepository.findPrevious(show, season, episode, subtitleNumber,count)
                .sort(Comparator.comparing(f -> f.getText().replaceAll("\\W", "").toLowerCase()))
                .concatWith(subtitles)
                .distinct()
                .take(count)
                .map(this::applyUrls);
    }

    @GetMapping("show/{show}/random")
    public Flux<FlatSubtitle> getRandomSubtitles(@PathVariable String show, @RequestParam(defaultValue = "5") int count) {
        String randomHash = HashUtil.hashStrings(String.valueOf(Math.random()));
        log.info("Using hash {}",randomHash);
        return hashedSubtitleRepository.findByShowAndHashGreaterThanOrderByHashDescLimit(show,randomHash, count)
                .doOnNext(hashedSubtitle -> log.info("Found subtitle with hash {}",hashedSubtitle.getHash()))
                .flatMapSequential(hashedSubtitle -> flatSubtitleRepository.findByShowAndSeasonAndEpisodeAndSubtitleNumber(hashedSubtitle.getShow(),hashedSubtitle.getSeason(),hashedSubtitle.getEpisode(),hashedSubtitle.getSubtitleNumber()))
                .map(this::applyUrls);
    }


    private FlatSubtitle applyUrls(FlatSubtitle flatSubtitle) {
        flatSubtitle.setDomain(highsubConfig.getServerUrl());
        return flatSubtitle;
    }

    public record SubtitleFileVersion(String eTag, List<String> checksumAlgorithm, Long size, String storageClass,
                                      String key, String versionId, Boolean isLatest, Long lastModifiedUtc) {
        public SubtitleFileVersion(ObjectVersion objectVersion) {
            this(objectVersion.eTag(), objectVersion.checksumAlgorithmAsStrings(), objectVersion.size(),
                    objectVersion.storageClassAsString(), objectVersion.key(), objectVersion.versionId(),
                    objectVersion.isLatest(), objectVersion.lastModified().toEpochMilli());
        }
    }

    @GetMapping(value = "file-versions/{base64EncodedKey}")
    public Flux<SubtitleFileVersion> getFileVersions(@PathVariable String base64EncodedKey) {
        String key = new String(Base64.getDecoder().decode(base64EncodedKey));
        return subtitleService.getVersions(key).flatMapIterable(ListObjectVersionsResponse::versions).map(SubtitleFileVersion::new);
    }


}
