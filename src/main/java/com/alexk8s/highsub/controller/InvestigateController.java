package com.alexk8s.highsub.controller;

import com.alexk8s.highsub.configuration.HighsubConfig;
import com.alexk8s.highsub.ffmpeg.Chapter;
import com.alexk8s.highsub.ffmpeg.FfmpegOutput;
import com.alexk8s.highsub.ffmpeg.FfmpegTools;
import com.alexk8s.highsub.ffmpeg.Stream;
import com.alexk8s.highsub.ffmpeg.operation.GetMetadata;
import com.alexk8s.highsub.model.Episode;
import com.alexk8s.highsub.model.Language;
import com.alexk8s.highsub.model.Subtitle;
import com.alexk8s.highsub.model.SubtitleFormat;
import com.alexk8s.highsub.repository.EpisodeRepository;
import com.alexk8s.highsub.service.SubtitleNotFoundException;
import com.alexk8s.highsub.service.SubtitleService;
import com.alexk8s.highsub.util.CommandUtils;
import fr.free.nrw.jakaroma.Jakaroma;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.FileSystemResource;
import org.springframework.data.redis.core.ReactiveHashOperations;
import org.springframework.data.redis.core.ReactiveRedisOperations;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;
import reactor.util.function.Tuples;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;


@RestController
@AllArgsConstructor
@RequestMapping("v1/investigate")
@Slf4j
public class InvestigateController {
    private final EpisodeRepository episodeRepository;
    private final SubtitleService subtitleService;
    private final FfmpegTools ffmpegTools;
    private final HighsubConfig highsubConfig;
    private final CommandUtils commandUtils;

    private final ReactiveRedisOperations<String, String> redisOperations;
    private final List<String> cachedShows = new ArrayList<>();
    private final Jakaroma jakaroma = new Jakaroma();

    @GetMapping("show")
    public Mono<List<String>> getShows() {
        if (!cachedShows.isEmpty())
            return Mono.just(cachedShows);
        return episodeRepository.findAll()
                .map(Episode::getShow)
                .distinct()
                .sort().collectList().doOnNext(cachedShows::addAll);
    }

    @GetMapping("show/{show}")
    public Flux<Episode> getEpisodes(@PathVariable String show) {
        return episodeRepository.findByShow(show);
    }

    public record StreamsResponse(List<Stream> audio, List<Stream> video, List<Stream> subtitle, List<Chapter> chapters,Integer videoIndex,
                                  Integer audioIndex, Integer englishSubtitleIndex, Integer japaneseSubtitleIndex,String externalEnglishSubtitleFile,String externalJapaneseSubtitleFile) {

    }

    @GetMapping("show/{show}/{season}/{episode}/stream")
    public Mono<StreamsResponse> getStreams(@PathVariable String show, @PathVariable String season, @PathVariable String episode) {
        return episodeRepository.findByShowAndSeasonAndEpisode(show, season, episode)
                .map(f -> {
                    String file = f.getSourceVideo();
                    FfmpegOutput video = new GetMetadata(file, "video").execute(ffmpegTools);
                    FfmpegOutput audio = new GetMetadata(file, "audio").execute(ffmpegTools);
                    FfmpegOutput subtitle = new GetMetadata(file, "subtitle").execute(ffmpegTools);
                    Path filePath = Path.of(file);
                    return new StreamsResponse(
                            audio.getStreams(),
                            video.getStreams(),
                            subtitle.getStreams(),
                            video.getChapters(),
                            ffmpegTools.getVideoStream(video).getIndex(),
                            ffmpegTools.getAudioIndex(audio),
                            subtitleService.resolveSubtitlesStream(subtitle.getStreams(), Language.ENGLISH,true).map(Stream::getIndex).orElse(null),
                            subtitleService.resolveSubtitlesStream(subtitle.getStreams(), Language.JAPANESE,false).map(Stream::getIndex).orElse(null),
                            subtitleService.findExternalSubtitlesFile(filePath, Language.ENGLISH).map(Path::toString).orElse(null),
                            subtitleService.findExternalSubtitlesFile(filePath, Language.JAPANESE).map(Path::toString).orElse(null)

                    );
                });
    }

    @GetMapping(value = "show/{show}/{season}/{episode}/stream/subtitle/{streamIndex}", produces = MediaType.TEXT_PLAIN_VALUE)
    public Mono<FileSystemResource> getSubtitles(@PathVariable String show, @PathVariable String season, @PathVariable String episode, @PathVariable int streamIndex) {
        return episodeRepository.findByShowAndSeasonAndEpisode(show, season, episode)
                .map(f -> {

                    FfmpegOutput subtitleMetadata = new GetMetadata(f.getSourceVideo(), "subtitle").execute(ffmpegTools);
                    Stream subtitleStream = subtitleMetadata.getStreams().stream().filter(s -> s.getIndex() == streamIndex)
                            .findFirst().get();
                    String path = subtitleService.extractSubtitles(subtitleStream, f.getSourceVideo(),l->{});
                    return new FileSystemResource(path);
                });
    }

    @GetMapping(value = "show/{show}/{season}/{episode}/local/subtitle/{language}", produces = MediaType.TEXT_PLAIN_VALUE)
    public Mono<FileSystemResource> getLocalSubtitleFile(@PathVariable String show, @PathVariable String season, @PathVariable String episode, @PathVariable String language) {
        return episodeRepository.findByShowAndSeasonAndEpisode(show, season, episode)
                .mapNotNull(f -> {
                    String fileBase = f.getSourceVideo().substring(0, f.getSourceVideo().lastIndexOf("."));
                    Path srtPath = Path.of(fileBase + "." + language + ".srt");
                    Path assPath = Path.of(fileBase + "." + language + ".ass");
                    Path vttPath = Path.of(fileBase+"."+language+".vtt");
                    if (Files.exists(srtPath))
                        return srtPath;
                    if (Files.exists(assPath))
                        return assPath;
                    if (Files.exists(vttPath))
                        return vttPath;
                    return null;
                })
                .map(FileSystemResource::new);
    }

    private Path resolveSourceSubtitlePath(Episode episode, String language, String format) {
        String videoFile = episode.getSourceVideo();
        String fileBase = videoFile.substring(0, videoFile.lastIndexOf("."));
        return Path.of(fileBase + "." + language + "." + format);
    }

    @GetMapping(value = "show/{show}/{season}/{episode}/local/subtitle/{language}/parse")
    public Mono<List<Subtitle>> parseLocalSubtitleFile(@PathVariable String show, @PathVariable String season, @PathVariable String episode, @PathVariable String language) {
        return episodeRepository.findByShowAndSeasonAndEpisode(show, season, episode)
                .mapNotNull(f -> {
                    Path srtPath = resolveSourceSubtitlePath(f, language, "srt");
                    Path assPath = resolveSourceSubtitlePath(f, language, "ass");
                    if (Files.exists(srtPath))
                        return subtitleService.readSubtitles(SubtitleFormat.SRT, srtPath.toString(), Language.findByCode(language));
                    if (Files.exists(assPath))
                        return subtitleService.readSubtitles(SubtitleFormat.ASS, assPath.toString(), Language.findByCode(language));
                    return null;
                });
    }

    @GetMapping(value = "show/{show}/{season}/{episode}/subtitle/{language}/parse")
    public Mono<List<Subtitle>> parseSubtitleFile(@PathVariable String show, @PathVariable String season, @PathVariable String episode, @PathVariable String language) {
        return episodeRepository.findByShowAndSeasonAndEpisode(show, season, episode)
                .flatMap(e->subtitleService.getCacheFile(e.getEnglishSubtitles()))
                .mapNotNull(subtitleFile -> {
                    if (subtitleFile.endsWith("srt"))
                        return subtitleService.readSubtitles(SubtitleFormat.SRT, subtitleFile, Language.findByCode(language));
                    if (subtitleFile.endsWith("ass"))
                        return subtitleService.readSubtitles(SubtitleFormat.ASS, subtitleFile, Language.findByCode(language));
                    return null;
                });
    }


    private record ShiftRequest(Integer msShift, Integer after) {
    }

    @PostMapping(value = "show/{show}/{season}/{episode}/local/subtitle/{language}/sync")
    public Mono<Void> syncSubtitle(@PathVariable String show, @PathVariable String season,
                                   @PathVariable String episode, @PathVariable String language) {
        if (!language.equals("ja"))
            throw new RuntimeException("I don't know how to handle this request");
        return episodeRepository.findByShowAndSeasonAndEpisode(show, season, episode)
                .flatMap(e->subtitleService.getCacheFile(e.getEnglishSubtitles()).map(s-> Tuples.of(e,s)))
                .flatMap(t -> {
                    Episode e = t.getT1();
                    Path subtitlePath;
                    if (Files.exists(resolveSourceSubtitlePath(e, language, "srt")))
                        subtitlePath = resolveSourceSubtitlePath(e, language, "srt");
                    else if (Files.exists(resolveSourceSubtitlePath(e, language, "ass")))
                        subtitlePath = resolveSourceSubtitlePath(e, language, "ass");
                    else
                        throw new SubtitleNotFoundException();

                    String output = commandUtils.executeCommand(highsubConfig.getTools().getAlass(),
                            t.getT2(), subtitlePath.toString(), subtitlePath.toString());
                    log.debug("Alass Sync output\n{}", output);
                    return subtitleService.putCacheFile(e, Language.findByCode(language), subtitlePath);

                })
                .then();
    }




    @GetMapping(value = "romaji/{kanji}", produces = MediaType.TEXT_PLAIN_VALUE)
    public Mono<String> getromaji(@PathVariable String kanji) {
        ReactiveHashOperations<String, String, String> hashOperations = redisOperations.opsForHash();
        return hashOperations.get("kanji-romaji", kanji)
                .publishOn(Schedulers.boundedElastic())
                .switchIfEmpty(Mono.fromCallable(() -> jakaroma.convert(kanji, true, true)).flatMap(c -> hashOperations.put("kanji-romaji", kanji, c).thenReturn(c)));
    }


}
