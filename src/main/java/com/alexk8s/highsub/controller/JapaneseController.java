package com.alexk8s.highsub.controller;

import com.alexk8s.highsub.util.KanaMap;
import com.atilika.kuromoji.ipadic.Token;
import com.atilika.kuromoji.ipadic.Tokenizer;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("v1/japanese")
@AllArgsConstructor
@Slf4j
public class JapaneseController {
    private final KanaMap kanaMap;
    private final Scheduler scheduler = Schedulers.newBoundedElastic(10, 100, "kana-katakana");
    private final Tokenizer tokenizer = new Tokenizer();

    public record Reading(String katakana, String hiragana, String romaji) {
    }

    public record ReadingBreakdown(String katakana, String hiragana, String romaji,
                                   List<ReadingBreakdownToken> tokens) {
    }

    public record ReadingBreakdownToken(String katakana, String hiragana, String romaji) {
    }
    private static final ReadingBreakdownToken DOUBLE_N_TOKEN = new ReadingBreakdownToken("ン", "ん", "n'");

    @GetMapping("{word}/reading")
    @CrossOrigin(originPatterns = "*")
    public Mono<Reading> getReading(@PathVariable String word) {
        return Mono.fromCallable(() -> getReadingInternal(word))
                .subscribeOn(scheduler);
    }


    private Reading getReadingInternal(String word) {
        List<Token> tokens = tokenizer.tokenize(word);
        StringBuilder reading = new StringBuilder();
        StringBuilder hiragana = new StringBuilder();
        for (Token token : tokens) {
            String readingString = token.getReading();
            if (readingString == null || readingString.isBlank() || readingString.equals("*")) {
                readingString = token.getSurface();
            }
            reading.append(readingString);
            hiragana.append(kanaMap.getHiragana(readingString));
        }
        String romaji = kanaMap.getRomaji(reading.toString());
        return new Reading(reading.toString(), hiragana.toString(), romaji);
    }

    @GetMapping("{word}/reading-breakdown")
    public Mono<ReadingBreakdown> getReadingBreakdown(@PathVariable String word) {
        return Mono.fromCallable(() -> getReadingBreakdownInternal(word))
                .subscribeOn(scheduler);
    }

    public ReadingBreakdown getReadingBreakdownInternal(String word) {
        Reading reading = getReadingInternal(word);
        List<ReadingBreakdownToken> tokens = new ArrayList<>();
        for (int i = 0; i < reading.katakana.length(); i++) {
            String token;
            if (i < reading.katakana.length() - 1){
                token = reading.katakana.substring(i, i + 2);
                if(kanaMap.isLittleTsu(token.charAt(0)) || kanaMap.isLittleVowel(token.charAt(1))){
                    tokens.add(new ReadingBreakdownToken(
                            token.substring(0,1),
                            kanaMap.getHiragana(token.substring(0,1)),
                            kanaMap.getRomaji(token,0)
                    ));
                    continue;
                }
                if(kanaMap.isDoubleNCase(token)){
                    tokens.add(DOUBLE_N_TOKEN);
                    continue;
                }
                if(kanaMap.isCombinationKana(token))
                    i++;
                else
                    token = token.substring(0,1);
            }
            else
                token = reading.katakana.substring(i, i + 1);
            tokens.add(new ReadingBreakdownToken(token, kanaMap.getHiragana(token), kanaMap.getRomaji(token)));
        }
        return new ReadingBreakdown(reading.katakana(), reading.hiragana(), reading.romaji(), tokens);
    }

}
