package com.alexk8s.highsub.controller;

import com.alexk8s.highsub.configuration.HighsubConfig;
import com.alexk8s.highsub.elasticsearch.ElasticsearchService;
import com.alexk8s.highsub.model.FlatSubtitle;
import com.alexk8s.highsub.repository.FlatSubtitleRepository;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.List;

@RestController
@RequestMapping("/v1/landing")
@AllArgsConstructor
public class LandingController {
    private final ElasticsearchService elasticsearchService;
    private final FlatSubtitleRepository flatSubtitleRepository;
    private final HighsubConfig config;

    @GetMapping("popular")
    public Mono<List<FlatSubtitle>> getPopular(@RequestParam(required = false,defaultValue = "100")int count){
       return elasticsearchService.getPopular(count*2)
               .flatMapSequential(i->flatSubtitleRepository.findByShowAndSeasonAndEpisodeAndSubtitleNumber(i.getShow(),i.getSeason(),i.getEpisode(),Integer.parseInt(i.getSubtitle())))
               .filter(f->config.getBlacklistTags().stream().noneMatch(t->hasTag(f,t)))
               .doOnNext(f->f.setDomain(config.getServerUrl()))
               .doOnNext(f->f.setEncodeId(true))
               .collectList();
    }

    boolean hasTag(FlatSubtitle subtitle,String tag){
        if(subtitle.getTags()!=null && subtitle.getParentTags()!=null)
            return subtitle.getTags().contains(tag) || subtitle.getParentTags().contains(tag);
        if(subtitle.getTags()!=null)
            return subtitle.getTags().contains(tag);
        if(subtitle.getParentTags()!=null)
            return subtitle.getParentTags().contains(tag);
        return false;
    }
}
