package com.alexk8s.highsub.controller;

import com.alexk8s.highsub.scrape.ActorRole;
import com.alexk8s.highsub.scrape.ActorRoleRepository;
import com.alexk8s.highsub.scrape.ActorRoleRule;
import com.alexk8s.highsub.service.ActorService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.neo4j.driver.Value;
import org.springframework.data.neo4j.core.ReactiveNeo4jClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Comparator;
import java.util.List;
import java.util.Map;

@RestController
@AllArgsConstructor
@Slf4j
public class ActorController {

    private final ReactiveNeo4jClient neo4jClient;
    private final ActorService actorService;
    private final ActorRoleRepository actorRoleRepository;



    public record SameActor2(String actorName, String originalShow,
                             List<Map<String,Object>> originalShowCharacters, List<Map<String,Object>> otherShowCharacters, double score) {
    }

    @GetMapping("v2/actor/same-actor/{show}")
    public Flux<SameActor2> getSameActorV2(@PathVariable String show) {
        String query = """
                MATCH (originalShow:SHOW {show: $show})
                 MATCH  (actor:ACTOR)-[playedOriginal:HAS_ROLE]->(originalShow)
                 WITH DISTINCT actor,playedOriginal,originalShow.show AS originalShow ORDER BY playedOriginal.score DESC
                 MATCH (actor)-[playedOther]->(otherShow:SHOW)
                 WHERE NOT otherShow.show=originalShow
                 WITH  actor,playedOriginal,originalShow,playedOther
                 ORDER BY playedOther.score DESC
                RETURN DISTINCT actor.name AS actorName,
                                collect(distinct playedOriginal) AS originalCharacters,
                                originalShow,
                                collect(distinct playedOther) AS otherCharacters,
                                (avg(playedOriginal.score) + sum(playedOther.score)/3) AS score ORDER BY score DESC
                """;
        return neo4jClient.query(query)
                .bind(show).to("show")
                .fetchAs(SameActor2.class)
                .mappedBy((type, record) -> mapRecord(record))
                .all();

    }

    SameActor2 mapRecord(org.neo4j.driver.Record record) {
        return new SameActor2(record.get("actorName").asString(),
                        record.get("originalShow").asString(),
                        record.get("originalCharacters").asList(Value::asMap),
                        record.get("otherCharacters").asList(Value::asMap),
                        record.get("score").asDouble());
    }

    @GetMapping("v1/actor/actor/{actor}")
    public Flux<ActorRole> getRolesByActor(@PathVariable String actor) {
        Comparator<ActorRole> actorRoleComparator = (a1, a2) -> {
            int roleComparison = getRoleValue(a1.getCharacterRole()) - getRoleValue(a2.getCharacterRole());
            if (roleComparison != 0)
                return roleComparison;
            return a1.getShow().compareToIgnoreCase(a2.getShow());
        };
        return actorRoleRepository.findByActor(actor)
                .sort(actorRoleComparator.reversed());
    }

    @GetMapping("v1/actor/show/{show}")
    public Flux<ActorRole> getRolesByShow(@PathVariable String show) {
        Comparator<ActorRole> actorRoleComparator = (a1, a2) -> {
            int roleComparison = getRoleValue(a1.getCharacterRole()) - getRoleValue(a2.getCharacterRole());
            if (roleComparison != 0)
                return roleComparison;
            return a1.getActor().compareToIgnoreCase(a2.getActor());
        };
        return actorRoleRepository.findByShow(show)
                .sort(actorRoleComparator.reversed());
    }


    int getRoleValue(String role) {
        if (role == null)
            return 0;
        return switch (role.toLowerCase()) {
            case "main" -> 100;
            case "major" -> 50;
            case "supporting" -> 10;
            default -> 1;
        };
    }

    @PostMapping(value = "v1/actor/rule", consumes = MediaType.APPLICATION_JSON_VALUE)
    public Mono<Void> saveRule(@RequestBody ActorRoleRule actorRoleRule) {
        return actorService.saveActorRoleRule(actorRoleRule);
    }

    public record ActorRename(String originalName, String correctName) {
    }

    @PostMapping(value = "v1/actor/rename", consumes = MediaType.APPLICATION_JSON_VALUE)
    public Mono<Void> rename(@RequestBody ActorRename actorRename) {
        return actorService.mapActorName(actorRename.originalName, actorRename.correctName);
    }
}
