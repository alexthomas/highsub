package com.alexk8s.highsub.controller;

import com.alexk8s.highsub.model.SavedEvent;
import com.alexk8s.highsub.repository.SavedEventRepository;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

@RestController
@RequestMapping("/v1/event")
@AllArgsConstructor
public class EventController {
   private final SavedEventRepository savedEventRepository;

   @GetMapping("/episode/{id}")
    public Flux<SavedEvent> getEpisodeEvents(@PathVariable String id) {
        return savedEventRepository.findByDomainAndIdOrderByEventUtcDesc("EPISODE", id);
    }

}
