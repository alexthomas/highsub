package com.alexk8s.highsub.controller;

import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/v1/util")
public class UtilController {

    @GetMapping("/headers")
    public Map<String, List<String>> getHeaders(ServerHttpRequest request) {
        return request.getHeaders();
    }
}
