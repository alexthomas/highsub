package com.alexk8s.highsub.controller;

import com.alexk8s.highsub.configuration.HighsubConfig;
import com.alexk8s.highsub.kafka.ShowsInteractions;
import com.alexk8s.highsub.model.Show;
import com.alexk8s.highsub.repository.ShowRepository;
import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/v1/tag")
@AllArgsConstructor
@Slf4j
public class TagController {

    private final ShowsInteractions showsInteractions;
    private final ShowRepository showRepository;
    private final HighsubConfig highsubConfig;

    public record TagRequest(String tag, @JsonAlias("show") String key, boolean remove) {

    }

    @PostMapping("/show")
    public Mono<Show> tagShow(@RequestBody TagRequest request) {
        return showRepository.findById(request.key())
                .map(show -> {
                    if (show.getTags() == null)
                        show.setTags(new ArrayList<>());
                    if (request.remove()) {
                        show.getTags().remove(request.tag());
                    } else {
                        show.getTags().add(request.tag());
                    }
                    return show;
                })
                .delayUntil(showsInteractions::updateTags);
    }

    @GetMapping("blacklist")
    public List<String> getBlacklist(){
        return highsubConfig.getBlacklistTags();
    }
}
