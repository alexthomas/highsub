package com.alexk8s.highsub.controller;

import com.alexk8s.highsub.configuration.HighsubConfig;
import com.alexk8s.highsub.kafka.EpisodeProducer;
import com.alexk8s.highsub.model.Episode;
import com.alexk8s.highsub.model.Language;
import com.alexk8s.highsub.repository.EpisodeRepository;
import com.alexk8s.highsub.service.EpisodeResolverService;
import com.alexk8s.highsub.service.PlexService;
import com.alexk8s.highsub.service.SubtitleService;
import com.alexk8s.highsub.util.PathUtils;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;
import reactor.util.function.Tuple2;
import sh.casey.subtitler.model.SubtitleFile;
import sh.casey.subtitler.model.SubtitleType;
import sh.casey.subtitler.shifter.ShiftConfig;
import sh.casey.subtitler.shifter.SubtitleShifter;
import sh.casey.subtitler.shifter.SubtitleShifterFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/v1/index")
@RequiredArgsConstructor
@Slf4j
public class IndexController {
    private final SubtitleService subtitleService;
    private final HighsubConfig highsubConfig;
    private final EpisodeProducer episodeProducer;
    private final EpisodeRepository episodeRepository;
    private final EpisodeResolverService episodeResolverService;

    @PostMapping("show")
    public String indexFile(@RequestBody PlexService.PlexEpisode plexEpisode) {
        Mono.defer(()->episodeResolverService.resolveEpisode(plexEpisode)).checkpoint("Resolve Episode")
                .subscribeOn(Schedulers.boundedElastic()).subscribe(episodeProducer::createEpisode);
        return "Index Request Submitted";
    }

    public enum SyncType {
        NONE, ALASS_ENGLISH_SUBTITLES, ALASS_ENGLISH_SUBTITLES_NO_SPLIT, SUBTITLER_OFFSET
    }

    @PostMapping("episode/{show}-{season}-{episode}/add-japanese-subtitles")
    public Mono<Void> addJapaneseSubtitles(
            @PathVariable("show") String show, @PathVariable("season") String season, @PathVariable("episode") String episode
            , @RequestPart("files") Mono<FilePart> filePartMono, @RequestParam("syncType") SyncType syncType, @RequestParam(required = false) Integer offset) {
        Mono<Path> transferMono = filePartMono.flatMap(this::transferFile);
        Mono<Episode> episodeMono = episodeRepository.findByShowAndSeasonAndEpisode(show, season, episode);
        return Mono.zip(transferMono, episodeMono).flatMap(t -> resolveJapaneseSubtitleUpdate(t, syncType, offset)).delayUntil(episodeProducer::updateJapaneseSubtitles).then();

    }

    @PostMapping("episode/{show}-{season}-{episode}/add-english-subtitles")
    public Mono<Void> addEnglishSubtitles(
            @PathVariable("show") String show, @PathVariable("season") String season, @PathVariable("episode") String episode
            , @RequestPart("files") Mono<FilePart> filePartMono) {
        Mono<Path> transferMono = filePartMono.flatMap(this::transferFile);
        Mono<Episode> episodeMono = episodeRepository.findByShowAndSeasonAndEpisode(show, season, episode).cache();
        return Mono.zip(transferMono, episodeMono)
                .publishOn(Schedulers.boundedElastic())
                .delayUntil(t -> subtitleService.putCacheFile(t.getT2(), Language.ENGLISH, t.getT1()).doOnSuccess(t.getT2()::setEnglishSubtitles))
                .publishOn(Schedulers.boundedElastic())
                .doOnSuccess(t -> {
                    Episode e = t.getT2();
                    cleanupEpisode(e,t.getT1(),Language.ENGLISH);
                    e.setScaled480pVideo(null);
                    e.setScaled150pVideo(null);
                    episodeProducer.createEpisode(e);
                }).then();
    }

    @SneakyThrows(IOException.class)
    private void cleanupEpisode(Episode episode,Path newSubtitlePath,Language newSubtitleLanguage){
        if(episode.getScaled480pVideo()!=null){
            boolean deleted480p = Files.deleteIfExists(Path.of(episode.getScaled480pVideo()));
            log.info("{} 480p video {}", deleted480p ? "Deleted" : "Didn't delete", episode.getScaled480pVideo());
        }
        if(episode.getScaled150pVideo()!=null) {
            boolean deleted150p = Files.deleteIfExists(Path.of(episode.getScaled150pVideo()));
            log.info("{} 150p video {}", deleted150p ? "Deleted" : "Didn't delete", episode.getScaled150pVideo());
        }
        List<Path> cachedItems = Files.list(Path.of(highsubConfig.getCacheRoot(), "cache", episode.getShow(), episode.getSeason(), episode.getEpisode()))
                .toList();
        for (Path cachedItem : cachedItems) {
            Files.delete(cachedItem);
            log.info("Deleted cached {}",cachedItem);
        }
        Optional<Path> externalSubtitlesFile = subtitleService.findExternalSubtitlesFile(Path.of(episode.getSourceVideo()), newSubtitleLanguage);
        if(externalSubtitlesFile.isPresent()){
            Files.copy(newSubtitlePath,externalSubtitlesFile.get(),StandardCopyOption.REPLACE_EXISTING);
            log.info("Copied {} to {}",newSubtitlePath,externalSubtitlesFile.get());
        }
    }

    @PostMapping("episode/{show}-{season}-{episode}/remove-japanese-subtitles")
    public Mono<Void> removeJapaneseSubtitles(
            @PathVariable("show") String show, @PathVariable("season") String season, @PathVariable("episode") String episode) {
        return episodeRepository.findByShowAndSeasonAndEpisode(show, season, episode)
                .map(e -> e.withJapaneseSubtitles(null))
                .delayUntil(episodeProducer::updateJapaneseSubtitles).then();

    }

    Mono<Path> transferFile(FilePart filePart) {
        String extension = PathUtils.getExtension(filePart.filename());
        Path tempPath = Path.of(PathUtils.getTempFile(extension));
        return filePart.transferTo(tempPath).thenReturn(tempPath);
    }

    Mono<Episode> resolveJapaneseSubtitleUpdate(Tuple2<Path, Episode> tuple2, SyncType syncType, Integer offset) {
        String sourceVideoPath = tuple2.getT2().getSourceVideo();
        String japaneseSubtitleTempPath = tuple2.getT1().toString();
        String japaneseSubtitlePath = sourceVideoPath.substring(0, sourceVideoPath.lastIndexOf(".")) + "." + Language.JAPANESE.getSubtitleCode() + "." + PathUtils.getExtension(japaneseSubtitleTempPath);
        Mono<Void> syncMono = switch (syncType) {
            case NONE -> Mono.fromRunnable(() -> {
                try {
                    Files.copy(Path.of(japaneseSubtitleTempPath), Path.of(japaneseSubtitlePath), StandardCopyOption.REPLACE_EXISTING);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }).then();
            case ALASS_ENGLISH_SUBTITLES -> subtitleService.getCacheFile(tuple2.getT2().getEnglishSubtitles())
                    .doOnNext(englishSubtitlePath -> {
                        String output = subtitleService.alassSync(englishSubtitlePath, japaneseSubtitleTempPath, japaneseSubtitlePath, SubtitleService.SyncMode.SPLIT_10);
                        logAlassOutput(tuple2.getT2(), output);
                    }).then();
            case ALASS_ENGLISH_SUBTITLES_NO_SPLIT -> subtitleService.getCacheFile(tuple2.getT2().getEnglishSubtitles())
                    .doOnNext(englishSubtitlePath -> {
                        String output = subtitleService.alassSync(englishSubtitlePath, japaneseSubtitleTempPath, japaneseSubtitlePath, SubtitleService.SyncMode.NO_SPLIT);
                        logAlassOutput(tuple2.getT2(), output);
                    }).then();
            case SUBTITLER_OFFSET ->
                    Mono.fromRunnable(() -> shiftSubtitle(japaneseSubtitleTempPath, japaneseSubtitlePath, offset)).then();
        };
        Mono<Episode> saveMono = Mono.defer(() -> subtitleService.putCacheFile(tuple2.getT2(), Language.JAPANESE, Path.of(japaneseSubtitlePath)))
                .map(p -> tuple2.getT2().withJapaneseSubtitles(p));
        return syncMono.then(saveMono);
    }

    @SneakyThrows
    private void shiftSubtitle(String inputPath, String outputPath, Integer offset) {
        SubtitleShifterFactory subtitleShifterFactory = new SubtitleShifterFactory();
        SubtitleShifter<? extends SubtitleFile> shifter;
        if (inputPath.endsWith("ass"))
            shifter = subtitleShifterFactory.getInstance(SubtitleType.ASS);
        else if (inputPath.endsWith("srt"))
            shifter = subtitleShifterFactory.getInstance(SubtitleType.SRT);
        else
            throw new RuntimeException("Unknown subtitle type " + inputPath);
        ShiftConfig.Builder shiftConfigBuilder = ShiftConfig.builder()
                .input(inputPath)
                .output(outputPath)
                .ms(offset);
        ShiftConfig shiftConfig = shiftConfigBuilder
                .build();
        shifter.shift(shiftConfig);
    }

    private void logAlassOutput(Episode episode, String output) {
        log.debug("Alass Sync output\n{}", output);
        String[] lines = output.split("\n");
        for (String line : lines) {
            if (line.startsWith("shifted block of")) {
                log.info("{} {} {} {}", episode.getShow(), episode.getSeason(), episode.getEpisode(), line);
            }
        }
    }









}
