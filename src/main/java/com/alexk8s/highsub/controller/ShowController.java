package com.alexk8s.highsub.controller;

import com.alexk8s.highsub.elasticsearch.ElasticsearchService;
import com.alexk8s.highsub.kafka.ShowsInteractions;
import com.alexk8s.highsub.model.Episode;
import com.alexk8s.highsub.model.FlatSubtitle;
import com.alexk8s.highsub.model.Show;
import com.alexk8s.highsub.repository.EpisodeRepository;
import com.alexk8s.highsub.repository.ShowRepository;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.cassandra.core.ReactiveCassandraTemplate;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Collections;

@AllArgsConstructor
@RestController
@RequestMapping("/v1/show")
@Slf4j
public class ShowController {
    private final EpisodeRepository episodeRepository;
    private final ReactiveCassandraTemplate cassandraTemplate;
    private final ShowRepository showRepository;
    private final ShowsInteractions showsInteractions;
    private final ElasticsearchService elasticsearchService;
    private final SubtitleControllerV2 subtitleControllerV2;
    @GetMapping
    public Flux<Show> getShows() {
        return cassandraTemplate.query(Show.class).all();
    }

    @GetMapping("{show}/episodes")
    public Flux<Episode> getEpisodes(@PathVariable String show) {
        return episodeRepository.findByShow(show);
    }

    public record IsAnimeParams(@JsonProperty("isAnime") boolean isAnime) {
    }

    @PutMapping("{show}/is-anime")
    public Mono<Void> setIsAnime(@PathVariable String show, @RequestBody IsAnimeParams params) {
        return showRepository.findById(show)
                .map(s -> {
                    Show s1 = s.withAnime(params.isAnime());
                    if (s1.getTags() == null)
                        s1.setTags(Collections.emptyList());
                    return s1;
                })
                .flatMap(showsInteractions::updateAnime)
                .then()
                ;
    }

    @DeleteMapping("{show}")
    public Mono<Void> deleteShow(@PathVariable String show) {
        return showRepository.findById(show).flatMap(showsInteractions::deleteShow).then();
    }

    @GetMapping("{show}/popular")
    public Flux<FlatSubtitle> getPopular(@RequestParam(defaultValue = "10") int count,@PathVariable String show) {
        return elasticsearchService.getPopular(count,show)
                .flatMapSequential(i -> subtitleControllerV2.getSubtitle(i.getShow(), i.getSeason(), i.getEpisode(), Integer.parseInt(i.getSubtitle())));
    }
}
