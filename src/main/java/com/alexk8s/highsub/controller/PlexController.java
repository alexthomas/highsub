package com.alexk8s.highsub.controller;

import com.alexk8s.highsub.kafka.model.PlexOpeningEventType;
import com.alexk8s.highsub.kafka.model.PlexOpeningV1;
import com.alexk8s.highsub.model.PlexOpening;
import com.alexk8s.highsub.service.PlexService;
import com.alexk8s.highsub.service.ThumbnailService;
import com.alexk8s.highsub.util.ThumbnailUtility;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.http.MediaType;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

@RestController()
@RequestMapping(value = "/v1/plex", produces = MediaType.APPLICATION_JSON_VALUE)
@AllArgsConstructor
public class PlexController {
    private final PlexService service;
    private final ThumbnailService thumbnailService;
    private final KafkaTemplate<String, PlexOpeningV1> kafkaTemplate;


    @GetMapping("shows")
    public Flux<PlexService.PlexShow> getShows() {
        return service.getShows();
    }

    @GetMapping("shows/{showKey}/episodes")
    public Flux<PlexService.PlexEpisode> getEpisodes(@PathVariable String showKey) {
        return service.getEpisodes(showKey);

    }

    @PostMapping("shows/{showKey}/update-poster")
    public Mono<Void> updatePoster(@PathVariable String showKey) {
        return service.getShows().filter(s -> s.key().equals(showKey))
                .flatMap(s -> thumbnailService.updateThumbnail(ThumbnailUtility.getKey(s.title()), s.thumb())).last();
    }

    @PostMapping("/openings")
    public Mono<Void> updateOpenings(@RequestBody List<PlexOpening> openings) {
        for (PlexOpening opening : openings) {
            PlexOpeningV1 plexOpeningV1 = new PlexOpeningV1();
            BeanUtils.copyProperties(opening, plexOpeningV1);
            plexOpeningV1.setType$1(PlexOpeningEventType.CREATE);
            kafkaTemplate.send("plex-openings", plexOpeningV1);
        }
        return Mono.empty();
    }
}
