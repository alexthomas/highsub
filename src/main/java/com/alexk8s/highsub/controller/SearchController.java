package com.alexk8s.highsub.controller;

import com.alexk8s.highsub.configuration.HighsubConfig;
import com.alexk8s.highsub.elasticsearch.ElasticsearchService;
import com.alexk8s.highsub.elasticsearch.SearchCriteria;
import com.alexk8s.highsub.elasticsearch.SearchResult;
import com.alexk8s.highsub.kafka.model.SearchEventType;
import com.alexk8s.highsub.kafka.model.SearchRequestV2;
import com.alexk8s.highsub.kafka.model.SearchResponseV2;
import com.alexk8s.highsub.kafka.model.SearchV3;
import com.alexk8s.highsub.kafka.model.SubtitleV4;
import com.alexk8s.highsub.model.FlatSubtitle;
import com.alexk8s.highsub.parse.SearchQueryLexer;
import com.alexk8s.highsub.parse.SearchQueryParser;
import com.alexk8s.highsub.repository.FlatSubtitleRepository;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static com.alexk8s.highsub.kafka.SubtitlesProducer.mapSubtitle;
import static com.alexk8s.highsub.kafka.model.SubtitleEventType.CREATE;
import static com.alexk8s.highsub.util.LoggingUtils.logJson;
import static com.alexk8s.highsub.util.ServerUtils.getRequestIp;

@RestController
@AllArgsConstructor
@Slf4j
public class SearchController {
    private final ElasticsearchService elasticsearchService;
    private final FlatSubtitleRepository flatSubtitleRepository;
    private final HighsubConfig highsubConfig;
    private final KafkaTemplate<String, SearchV3> kafkaTemplate;

    @GetMapping("/v1/search")
    public Flux<FlatSubtitle> search(@Valid SearchCriteria searchCriteria) {
        return elasticsearchService.query(searchCriteria)
                .flatMapSequential(this::fetchSubtitle)
                .doOnNext(f -> f.setDomain(highsubConfig.getServerUrl()))
                ;
    }

    private Mono<FlatSubtitle>  fetchSubtitle(SearchResult s){
        return flatSubtitleRepository.findByShowAndSeasonAndEpisodeAndSubtitleNumber(s.getShow(), s.getSeason(), s.getEpisode(), s.getSubtitleNumber())
                .doOnSuccess(x -> {
                    if (x == null)
                        log.warn("Subtitle not found: {}-{}-{}-{}", s.getShow(), s.getSeason(), s.getEpisode(), s.getSubtitleNumber());
                });
    }

    private interface TagProvider {
        default Set<String> getPositiveTags(HighsubConfig highsubConfig) {
            return Collections.emptySet();
        }

        default Set<String> getNegativeTags(HighsubConfig highsubConfig) {
            return new HashSet<>(highsubConfig.getBlacklistTags());

        }
    }

    @AllArgsConstructor
    public enum SafeSearchType {
        ON(new TagProvider() {
        }),
        OFF(new TagProvider() {
            @Override
            public Set<String> getNegativeTags(HighsubConfig highsubConfig) {
                return Collections.emptySet();
            }
        }), ANIME(new TagProvider() {
            @Override
            public Set<String> getPositiveTags(HighsubConfig highsubConfig) {
                return Set.of("anime");
            }
        }), NO_ANIME(new TagProvider() {
            @Override
            public Set<String> getNegativeTags(HighsubConfig highsubConfig) {
                HashSet<String> tags = new HashSet<>(TagProvider.super.getNegativeTags(highsubConfig));
                tags.add("anime");
                return tags;
            }
        });
        private final TagProvider tagProvider;

        public void apply(HighsubConfig highsubConfig, SearchCriteria searchCriteria) {
            Set<String> negativeTags = tagProvider.getNegativeTags(highsubConfig);
            searchCriteria.getBlacklistTags().addAll(negativeTags);
            searchCriteria.getTags().removeAll(negativeTags);

            Set<String> positiveTags = tagProvider.getPositiveTags(highsubConfig);
            searchCriteria.getTags().addAll(positiveTags);
            searchCriteria.getBlacklistTags().removeAll(positiveTags);
        }
    }

    @GetMapping("/v2/search")
    public Mono<List<FlatSubtitle>> search(@RequestParam(name = "q") String query, @RequestParam(defaultValue = "0") int page,
                                           @RequestParam(defaultValue = "10") int size,
                                           @RequestParam(required = false) List<String> blacklistShow,
                                           @RequestParam(required = false) String show,
                                           @RequestParam(required = false, defaultValue = "ON") SafeSearchType safeSearch,
                                           @RequestParam(required = false) String context,
                                           @RequestHeader("User-Agent") String userAgent,
                                           ServerHttpRequest request,
                                           Authentication authentication) {
        SearchCriteria searchCriteria = parseRequest(query, page, size, blacklistShow, show, safeSearch);
        com.alexk8s.highsub.kafka.model.SearchCriteria sc = new com.alexk8s.highsub.kafka.model.SearchCriteria(searchCriteria.getQ(),
                searchCriteria.getText(), searchCriteria.getRomaji(), searchCriteria.getKanji(), searchCriteria.getShow(),
                searchCriteria.getSeason(), searchCriteria.getEpisode(), searchCriteria.getTags(), searchCriteria.getName(),
                searchCriteria.getBlacklistShow(), searchCriteria.getBlacklistTags(), searchCriteria.getSize(), searchCriteria.getPage());
        SearchRequestV2 build = SearchRequestV2.newBuilder()
                .setQuery(query).setPage(page).setSize(size).setBlacklistShows(blacklistShow)
                .setShow(show).setSafeSearch(safeSearch.name())
                .setType("v2")
                .setContext(context)
                .setUserAgent(userAgent)
                .setIp(getRequestIp(request))
                .setUser(authentication!=null?authentication.getName():null)
                .setSearchCriteria(sc).build();
        logJson("SearchRequest", build);
        String searchId = UUID.randomUUID().toString();
        long startTime = System.currentTimeMillis();
        kafkaTemplate.send("search-v3", searchId, new SearchV3(searchId, build, null, startTime, SearchEventType.REQUEST));
        return search(searchCriteria)
                .doOnNext(f -> f.setEncodeId(true))
                .collectList()
                .doOnSuccess(result -> writeResult(searchId, startTime, result));
    }

    private void writeResult(String id, long startTime, List<FlatSubtitle> result) {
        long endTime = System.currentTimeMillis();
        List<SubtitleV4> list = result.stream().map(mapSubtitle(CREATE)).toList();
        SearchResponseV2 searchResponseV1 = SearchResponseV2.newBuilder()
                .setResults(list).setDurationMs(endTime - startTime).build();
        kafkaTemplate.send("search-v3", id, new SearchV3(id, null, searchResponseV1, startTime, SearchEventType.RESPONSE));
    }

    @GetMapping("/v2/search/parse")
    public SearchCriteria parseRequest(@RequestParam(name = "q") String query, @RequestParam(defaultValue = "0") int page,
                                       @RequestParam(defaultValue = "10") int size, @RequestParam(required = false) List<String> blacklistShow,
                                       @RequestParam(required = false) String show, @RequestParam(required = false, defaultValue = "ON") SafeSearchType safeSearch) {

        SearchCriteria searchCriteria = parseQuery(query, page, size);
        searchCriteria.setBlacklistShow(blacklistShow);
        if (show != null)
            searchCriteria.setShow(show);
        safeSearch.apply(highsubConfig, searchCriteria);
        return searchCriteria;
    }

    @GetMapping("/v2/search/version")
    public String getVersion() {
        return "2";
    }

    @GetMapping("/v2/search/exact")
    public Flux<FlatSubtitle> searchExact(@RequestParam(name = "text")String text){
        return elasticsearchService.exactSearch(text)
                .flatMapSequential(this::fetchSubtitle)
                .doOnNext(f->f.setEncodeId(true))
                .doOnNext(f -> f.setDomain(highsubConfig.getServerUrl()));
    }

    private static class QueryStringBuilder {
        private final StringBuilder stringBuilder = new StringBuilder();
        private boolean first = true;

        public void append(String s) {
            if (first) {
                first = false;
            } else {
                stringBuilder.append(" ");
            }
            stringBuilder.append(s);
        }

        public boolean hasText() {
            return !stringBuilder.isEmpty();
        }

        public String toString() {
            return stringBuilder.toString();
        }
    }


    static SearchCriteria parseQuery(String query, int page, int size) {
        SearchQueryLexer searchQueryLexer = new SearchQueryLexer(CharStreams.fromString(query));
        SearchQueryParser searchQueryParser = new SearchQueryParser(new CommonTokenStream(searchQueryLexer));
        List<SearchQueryParser.Query_partContext> queryParts = searchQueryParser.query().query_part();
        if (!queryParts.isEmpty()) {
            SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria.setTags(new ArrayList<>());
            QueryStringBuilder q = new QueryStringBuilder();
            QueryStringBuilder requiredText = new QueryStringBuilder();
            for (SearchQueryParser.Query_partContext queryPart : queryParts) {
                if (queryPart.filter() != null) {
                    SearchQueryParser.FilterContext filter = queryPart.filter();
                    applyFilter(filter, searchCriteria);
                } else if (queryPart.word != null) {
                    q.append(queryPart.word.getText());
                } else if (queryPart.required != null) {
                    String r = parseText(queryPart.required.getText());
                    requiredText.append(r);
                    q.append(r);
                } else if (queryPart.tag != null) {
                    searchCriteria.getTags().add(queryPart.tag.getText().substring(1).toLowerCase());
                } else {
                    log.warn("Unknown query part: {}", queryPart.getText());
                }
            }
            if (requiredText.hasText())
                searchCriteria.setText(requiredText.toString());
            if (q.hasText() && !q.toString().equals(requiredText.toString()))
                searchCriteria.setQ(q.toString());
            searchCriteria.setBlacklistTags(new ArrayList<>());
            searchCriteria.setPage(page);
            searchCriteria.setSize(size);
            log.debug("Parsed query: {}", searchCriteria);
            return searchCriteria;
        }
        throw new RuntimeException("Unable to parse query");
    }

    private static String parseText(String text) {
        if (text.charAt(0) == '"' || text.charAt(0) == 8221)
            text = text.substring(1, text.length() - 1);
        return text;
    }

    private static void applyFilter(SearchQueryParser.FilterContext filter, SearchCriteria searchCriteria) {

        String text = parseText(filter.filterValue.getText());
        switch (filter.filterField.getText()) {
            case "name:" -> searchCriteria.setName(text);
            case "show:", "series:" -> searchCriteria.setShow(text);
            case "season:" -> searchCriteria.setSeason(text);
            case "episode:" -> searchCriteria.setEpisode(text);
            case "romaji:" -> searchCriteria.setRomaji(text);
            case "kanji:", "japanese:" -> searchCriteria.setKanji(text);
            default -> log.warn("Unknown filter field: {}", filter.filterField.getText());
        }
    }
}
