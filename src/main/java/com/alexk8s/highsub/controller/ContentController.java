package com.alexk8s.highsub.controller;

import com.alexk8s.highsub.configuration.HighsubConfig;
import com.alexk8s.highsub.controller.content.ContentRequest;
import com.alexk8s.highsub.controller.content.ContentStrategy;
import com.alexk8s.highsub.controller.content.PngContentStrategy;
import com.alexk8s.highsub.ffmpeg.FfmpegTools;
import com.alexk8s.highsub.ffmpeg.operation.ExtractVideo;
import com.alexk8s.highsub.model.Episode;
import com.alexk8s.highsub.repository.EpisodeRepository;
import com.alexk8s.highsub.util.ThumbnailUtility;
import jakarta.annotation.PostConstruct;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import net.coobird.thumbnailator.Thumbnails;
import org.springframework.beans.BeanUtils;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.core.io.buffer.DefaultDataBufferFactory;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;
import software.amazon.awssdk.core.ResponseBytes;
import software.amazon.awssdk.core.async.AsyncRequestBody;
import software.amazon.awssdk.core.async.AsyncResponseTransformer;
import software.amazon.awssdk.services.s3.S3AsyncClient;
import software.amazon.awssdk.services.s3.model.GetObjectRequest;
import software.amazon.awssdk.services.s3.model.GetObjectResponse;
import software.amazon.awssdk.services.s3.model.NoSuchKeyException;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.Duration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.alexk8s.highsub.util.LoggingUtils.logJson;
import static com.alexk8s.highsub.util.PathUtils.getTempFile;
import static com.alexk8s.highsub.util.ServerUtils.getRequestIp;
import static com.alexk8s.highsub.util.ThumbnailUtility.getActorKey;
import static com.alexk8s.highsub.util.ThumbnailUtility.getCharacterKey;

@RestController()
@RequestMapping("/v1/content")
@AllArgsConstructor
@Slf4j
public class ContentController {
    private static final List<String> BOT_USER_AGENT_FAMILIES = List.of("googlebot-image","googlebot", "bytespider");

    private final FfmpegTools ffmpegTools;
    private final HighsubConfig highsubConfig;
    private final EpisodeRepository episodeRepository;
    private final S3AsyncClient s3Client;
    private final List<ContentStrategy> contentStrategies;
    private final PngContentStrategy pngContentStrategy;
    private final Map<String, ContentStrategy> contentStrategyMap = new HashMap<>();


    @PostConstruct
    public void init() {
        contentStrategies.forEach(i -> contentStrategyMap.put(i.getExtension(), i));
    }


    @GetMapping(value = "subtitle/combined/{show}-{season}-{episode}.png", produces = MediaType.IMAGE_PNG_VALUE)
    public Mono<FileSystemResource> getCombinedSubtitlePng(@PathVariable String show, @PathVariable String season, @PathVariable String episode,
                                                           @RequestParam List<Integer> subtitle, Authentication authentication) {
        String user;
        if (authentication != null)
            user = authentication.getPrincipal().toString();
        else
            user = null;

        return pngContentStrategy.getCombinedImage(show, season, episode, subtitle, user).map(FileSystemResource::new);
    }

    @Data
    @NoArgsConstructor
    public static class ContentRequestParameters {
        private Long plus;
        private Long minus;
        private String text;
        private int height = 480;
        private boolean countHit = false;
        private boolean includeSubtitles = true;
        private boolean animated = false;
        private boolean firstFrame = false;
        private String characterSet = "!@#$%^&*()_+-=[]\\\\{}|;':\\\",./<>?`~aeiouAEIOU ";
        private String font = "Courier";
        private int fontSize = 8;
    }


    @GetMapping(value = {
            "subtitle/{show}-{season}-{episode}-{subtitleNumber}.{extension:png|jpeg|jpg|webp}",
            "subtitle/{show}-{season}-{episode}-{subtitleNumber}.{extension:mp4|webm}",
            "subtitle/{show}-{season}-{episode}-{subtitleNumber}.{extension:wav|mp3}",
            "subtitle/{show}-{season}-{episode}-{subtitleNumber}.{extension:gif}",
            "subtitle/{show}-{season}-{episode}-{subtitleNumber}.{extension:txt|html|cc|sh}"})
    public Mono<ResponseEntity<FileSystemResource>> getContent(@PathVariable String show, @PathVariable String season, @PathVariable String episode, @PathVariable int subtitleNumber,
                                                               @PathVariable String extension, ContentRequestParameters contentRequestParameters, Authentication authentication, ServerHttpRequest request) {
        if (!contentStrategyMap.containsKey(extension)) {
            log.warn("Unsupported extension {}", extension);
            return Mono.just(ResponseEntity.status(HttpStatus.UNSUPPORTED_MEDIA_TYPE).build());
        }
        String userAgent = request.getHeaders().getFirst("User-Agent");
        String user;
        if (userAgent != null && BOT_USER_AGENT_FAMILIES.stream().anyMatch(b -> userAgent.toLowerCase().contains(b))) {
            contentRequestParameters.setCountHit(false);
            user = BOT_USER_AGENT_FAMILIES.stream().filter(b -> userAgent.toLowerCase().contains(b)).findFirst().orElse("bot");
        } else if (authentication != null)
            user = authentication.getName();
        else
            user = null;
        ContentRequest contentRequest = new ContentRequest(show, season, episode, subtitleNumber, extension);
        contentRequest.setUser(user);
        contentRequest.setUserAgent(userAgent);
        contentRequest.setIp(getRequestIp(request));
        contentRequest.setReferer(request.getHeaders().getFirst("Referer"));
        if (contentRequestParameters != null) {
            BeanUtils.copyProperties(contentRequestParameters, contentRequest);
            contentRequest.setFocused(contentRequestParameters.countHit);
        }
        ContentStrategy contentStrategy = contentStrategyMap.get(extension);
        logJson("ContentRequest", contentRequest);
        log.debug("Handling content request with {}", contentStrategy.getClass().getSimpleName());
        return Mono.just(contentStrategy)
                .publishOn(Schedulers.boundedElastic())
                .flatMap(i -> i.handleContent(contentRequest))
                .map(FileSystemResource::new)
                .map(i -> ResponseEntity.ok().contentType(contentStrategy.getMediaType()).body(i))
                .onErrorResume(e -> {
                    log.error("Error handling content request", e);
                    if (e instanceof ResponseStatusException)
                        return Mono.error(e);
                    return Mono.just(ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build());
                });
    }


    @GetMapping(value = "subtitle/{show}-{season}-{episode}-{subtitleNumber}.{extension:png|gif|mp4|webm|jpeg|webp}/ensure")
    public Mono<ResponseEntity<?>> ensureContent(@PathVariable String show, @PathVariable String season, @PathVariable String episode, @PathVariable int subtitleNumber,
                                                 @PathVariable String extension, ContentRequestParameters contentRequestParameters,
                                                 Authentication authentication, ServerHttpRequest request) {
        contentRequestParameters.setCountHit(false);
        return getContent(show, season, episode, subtitleNumber, extension, contentRequestParameters, authentication, request)
                .flatMap(i -> Mono.just(ResponseEntity.ok().build()));
    }


    @GetMapping(value = "subtitle/combined/{show}-{season}-{episode}.png/ensure")
    public Mono<Void> ensureSubtitlePng(@PathVariable String show, @PathVariable String season, @PathVariable String episode,
                                        @RequestParam List<Integer> subtitle, Authentication authentication) {
        return getCombinedSubtitlePng(show, season, episode, subtitle, authentication).then();
    }

    @GetMapping(value = "op/{show}-{season}-{episode}.mp4", produces = "video/mp4")
    public Mono<FileSystemResource> getOp(@PathVariable String show, @PathVariable String season, @PathVariable String episode) {
        return episodeRepository.findByShowAndSeasonAndEpisode(show, season, episode)
                .publishOn(Schedulers.boundedElastic())
                .map(this::getOpening)
                .map(FileSystemResource::new);
    }

    @GetMapping(value = "ed/{show}-{season}-{episode}.mp4", produces = "video/mp4")
    public Mono<FileSystemResource> getEd(@PathVariable String show, @PathVariable String season, @PathVariable String episode) {
        return episodeRepository.findByShowAndSeasonAndEpisode(show, season, episode)
                .publishOn(Schedulers.boundedElastic())
                .map(this::getEnding)
                .map(FileSystemResource::new);
    }

    @SneakyThrows
    Path getOpening(Episode episode) {
        Path cachePath = Path.of(highsubConfig.getCacheRoot(), "cached", episode.getShow(), episode.getSeason(), episode.getEpisode(), "opening.mp4");
        if (Files.exists(cachePath))
            return cachePath;
        String tempPath = ExtractVideo.builder()
                .videoPath(episode.getSourceVideo())
                .startTime(episode.getOpeningStart())
                .endTime(episode.getOpeningEnd())
                .format("mp4")
                .videoEncoding("libx264")
                .crf("19")
                .encodingPreset("medium")
                .quality("1")
                .build()
                .execute(ffmpegTools);
        Files.createDirectories(cachePath.getParent());
        Files.move(Path.of(tempPath), cachePath, StandardCopyOption.REPLACE_EXISTING);
        return cachePath;
    }

    @SneakyThrows
    Path getEnding(Episode episode) {
        Path cachePath = Path.of(highsubConfig.getCacheRoot(), "cached", episode.getShow(), episode.getSeason(), episode.getEpisode(), "ending.mp4");
        if (Files.exists(cachePath))
            return cachePath;
        String tempPath = ExtractVideo.builder()
                .videoPath(episode.getSourceVideo())
                .startTime(episode.getEndingStart())
                .endTime(episode.getEndingEnd())
                .format("mp4")
                .videoEncoding("libx264")
                .crf("19")
                .encodingPreset("medium")
                .quality("1")
                .build()
                .execute(ffmpegTools);
        Files.createDirectories(cachePath.getParent());
        Files.move(Path.of(tempPath), cachePath, StandardCopyOption.REPLACE_EXISTING);
        return cachePath;
    }

    @GetMapping(value = "poster/{show}.png", produces = MediaType.IMAGE_PNG_VALUE)
    public ResponseEntity<Flux<DataBuffer>> getPoster(@PathVariable String show, @RequestParam(required = false, defaultValue = "0") int height) {
        GetObjectRequest getObjectRequest = GetObjectRequest.builder()
                .bucket(highsubConfig.getMinio().getThumbnailBucket())
                .key(height > 0 ? ThumbnailUtility.getKey(show, height) : ThumbnailUtility.getKey(show))
                .build();
        Mono<ResponseBytes<GetObjectResponse>> responseBytesMono = Mono.fromFuture(s3Client.getObject(getObjectRequest, AsyncResponseTransformer.toBytes()));
        Flux<DataBuffer> dataBufferFlux = responseBytesMono
                .flatMapMany(i -> DataBufferUtils.readInputStream(i::asInputStream, new DefaultDataBufferFactory(), 5000))
                .onErrorResume(NoSuchKeyException.class, e -> {
                    if (height > 0)
                        return resizePoster(show, height);
                    return Mono.error(e);
                });
        return ResponseEntity.ok().cacheControl(CacheControl.maxAge(Duration.ofDays(14))).body(dataBufferFlux);
    }

    Flux<DataBuffer> resizePoster(String show, int height) {
        GetObjectRequest getObjectRequest = GetObjectRequest.builder()
                .bucket(highsubConfig.getMinio().getThumbnailBucket())
                .key(ThumbnailUtility.getKey(show))
                .build();
        PutObjectRequest putObjectRequest = PutObjectRequest.builder()
                .bucket(highsubConfig.getMinio().getThumbnailBucket())
                .key(ThumbnailUtility.getKey(show, height))
                .build();
        String sourcePath = getTempFile("png");
        String targetPath = getTempFile("png");
        return Mono.fromFuture(s3Client.getObject(getObjectRequest, AsyncResponseTransformer.toFile(Paths.get(sourcePath))))
                .publishOn(Schedulers.boundedElastic())
                .then(Mono.fromCallable(() -> {
                    resizeImage(sourcePath, targetPath, height);

                    return targetPath;
                }))
                .flatMap(i -> Mono.fromFuture(s3Client.putObject(putObjectRequest, AsyncRequestBody.fromFile(Paths.get(i)))))
                .flatMapMany(i -> DataBufferUtils.readInputStream(() -> Files.newInputStream(Path.of(targetPath)), new DefaultDataBufferFactory(), 5000))
                ;
    }

    @SneakyThrows
    void resizeImage(String source, String target, int height) {
        Thumbnails.of(source)
                .height(height)
                .toFile(target);
    }

    @GetMapping(value = "actor/{actor}.png", produces = MediaType.IMAGE_PNG_VALUE)
    public ResponseEntity<Flux<DataBuffer>> getActor(@PathVariable String actor) {
        GetObjectRequest getObjectRequest = GetObjectRequest.builder()
                .bucket("scraped")
                .key(getActorKey(actor))
                .build();
        return getObjectWithCache(getObjectRequest);
    }

    @GetMapping(value = "character/{show}/{character}.png", produces = MediaType.IMAGE_PNG_VALUE)
    public ResponseEntity<Flux<DataBuffer>> getCharacter(@PathVariable String show, @PathVariable String character) {
        GetObjectRequest getObjectRequest = GetObjectRequest.builder()
                .bucket("scraped")
                .key(getCharacterKey(show, character))
                .build();
        return getObjectWithCache(getObjectRequest);
    }

    private ResponseEntity<Flux<DataBuffer>> getObjectWithCache(GetObjectRequest getObjectRequest) {
        Flux<DataBuffer> dataBufferFlux = Mono.fromFuture(s3Client.getObject(getObjectRequest, AsyncResponseTransformer.toBytes()))
                .flatMapMany(i -> DataBufferUtils.readInputStream(i::asInputStream, new DefaultDataBufferFactory(), 5000));
        return ResponseEntity.ok().cacheControl(CacheControl.maxAge(Duration.ofDays(14))).body(dataBufferFlux);
    }

}
