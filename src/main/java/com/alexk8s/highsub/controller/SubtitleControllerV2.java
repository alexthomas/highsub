package com.alexk8s.highsub.controller;

import com.alexk8s.highsub.configuration.HighsubConfig;
import com.alexk8s.highsub.kafka.model.SubtitleEventType;
import com.alexk8s.highsub.kafka.model.SubtitleType;
import com.alexk8s.highsub.kafka.model.SubtitleV4;
import com.alexk8s.highsub.model.FlatSubtitle;
import com.alexk8s.highsub.model.Show;
import com.alexk8s.highsub.repository.FlatSubtitleRepository;
import com.alexk8s.highsub.repository.ShowRepository;
import com.alexk8s.highsub.service.RandomSubtitleService;
import com.alexk8s.highsub.service.SubtitleService;
import com.alexk8s.highsub.subtitle.TxtWriter;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import software.amazon.awssdk.services.s3.model.ListObjectVersionsResponse;
import software.amazon.awssdk.services.s3.model.ObjectVersion;

import java.time.Duration;
import java.util.Base64;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static com.alexk8s.highsub.kafka.SubtitlesProducer.mapSubtitle;

@RestController
@RequestMapping("v2/subtitle")
@AllArgsConstructor
@Slf4j
public class SubtitleControllerV2 {

    private final FlatSubtitleRepository flatSubtitleRepository;
    private final HighsubConfig highsubConfig;
    private final SubtitleService subtitleService;
    private final RandomSubtitleService randomSubtitleService;
    private final ShowRepository showRepository;
    private final KafkaTemplate<String, Object> kafkaTemplate;

    @GetMapping(value = "{show}-{season}-{episode}-{subtitleNumber}")
    public Mono<FlatSubtitle> getSubtitle(@PathVariable String show, @PathVariable String season, @PathVariable String episode, @PathVariable int subtitleNumber) {
        return flatSubtitleRepository.findByShowAndSeasonAndEpisodeAndSubtitleNumber(show, season, episode, subtitleNumber)
                .map(this::applyUrls);
    }

    @GetMapping(value = "episode/{show}-{season}-{episode}")
    public Flux<FlatSubtitle> getSubtitles(@PathVariable String show, @PathVariable String season, @PathVariable String episode) {
        return flatSubtitleRepository.findByShowAndSeasonAndEpisode(show, season, episode)
                .filter(f -> f.getType() == SubtitleType.DIALOGUE)
                .map(this::applyUrls);
    }

    @GetMapping(value = "episode/{show}-{season}-{episode}.m3u")
    public Mono<String> getSubtitlesM3U(@PathVariable String show, @PathVariable String season, @PathVariable String episode) {
        return getSubtitles(show, season, episode)
                .distinct(FlatSubtitle::getStartTime)
                .sort(Comparator.comparing(FlatSubtitle::getStartTime))
                .map(FlatSubtitle::getVideoUrl)
                .collect(Collectors.joining("\n"));
    }

    @GetMapping(value = "episode/{show}-{season}-{episode}.txt",produces = MediaType.TEXT_PLAIN_VALUE)
    public Mono<String> getSubtitlesTXT(@PathVariable String show, @PathVariable String season, @PathVariable String episode) {
        return getSubtitles(show, season, episode)
                .sort(Comparator.comparing(FlatSubtitle::getStartTime))
                .transform(f->new TxtWriter(f).getText())
                .collect(Collectors.joining("\n"));
    }


    @GetMapping(value = "{show}-{season}-{episode}-{subtitleNumber}/next")
    public Flux<FlatSubtitle> getNextSubtitle(@PathVariable String show, @PathVariable String season, @PathVariable String episode, @PathVariable int subtitleNumber, @RequestParam(defaultValue = "1") int count) {
        return flatSubtitleRepository.findNext(show, season, episode, subtitleNumber, count).map(this::applyUrls);
    }

    @GetMapping(value = "{show}-{season}-{episode}-{subtitleNumber}/previous")
    public Flux<FlatSubtitle> getPreviousSubtitle(@PathVariable String show, @PathVariable String season, @PathVariable String episode, @PathVariable int subtitleNumber, @RequestParam(defaultValue = "1") int count) {
        return flatSubtitleRepository.findPrevious(show, season, episode, subtitleNumber, count).map(this::applyUrls)
                .sort(Comparator.comparing(FlatSubtitle::getSubtitleNumber));
    }

    @GetMapping(value = "{show}-{season}-{episode}-{subtitleNumber}/more-from/episode")
    public Flux<FlatSubtitle> getMoreFromEpisode(@PathVariable String show, @PathVariable String season, @PathVariable String episode, @PathVariable int subtitleNumber,
                                                 @RequestParam(defaultValue = "5") int count) {
        Flux<FlatSubtitle> subtitles = flatSubtitleRepository.findByShowAndSeasonAndEpisode(show, season, episode, count);
        return flatSubtitleRepository.findPrevious(show, season, episode, subtitleNumber, count)
                .sort(Comparator.comparing(f -> f.getText().replaceAll("\\W", "").toLowerCase()))
                .concatWith(subtitles)
                .distinct(FlatSubtitle::getId)
                .take(count)
                .map(this::applyUrls);
    }

    @GetMapping("show/{show}/random")
    public Flux<FlatSubtitle> getRandomSubtitles(@PathVariable String show, @RequestParam(defaultValue = "5") int count) {
        Mono<Show> showFlux = showRepository.findById(show).cache();
        return showFlux.repeat(count * 5L)
                .flatMap(randomSubtitleService::getRandomSubtitle, 1)
                .take(count)
                .distinct(FlatSubtitle::getId)
                .map(this::applyUrls)
                .timeout(Duration.ofSeconds(1));
    }

    @GetMapping
    public Mono<FlatSubtitle> getSubtitle(@RequestParam(defaultValue = "ON") String safeSearch) {
        return randomSubtitleService.getRandomSubtitle(!safeSearch.equals("OFF")).map(this::applyUrls).timeout(Duration.ofSeconds(1));
    }

    @GetMapping("/random")
    public Flux<FlatSubtitle> getRandomSubtitles(@RequestParam(defaultValue = "5") int count,@RequestParam(defaultValue = "ON") String safeSearch) {
        boolean clean = !safeSearch.equals("OFF");
        return Flux.merge(Collections.nCopies(Math.min(10,count), randomSubtitleService.getRandomSubtitle(clean)))
                .repeat( 5L)
                .take(count)
                .distinct(FlatSubtitle::getId)
                .map(this::applyUrls)
                .timeout(Duration.ofSeconds(1));
    }

    @GetMapping(value = "show/{show}/random.m3u", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public Mono<String> getRandomSubtitlesM3U(@PathVariable String show, @RequestParam(defaultValue = "1000") int count) {
        return getRandomSubtitles(show, count)
                .sort()
                .map(FlatSubtitle::getVideoUrl)
                .collect(Collectors.joining("\n"));
    }

    @GetMapping(value = "/random.m3u", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public Mono<String> getRandomSubtitlesM3U(@RequestParam(defaultValue = "1000") int count,@RequestParam(defaultValue = "ON") String safeSearch) {
        return getRandomSubtitles(count,safeSearch)
                .sort()
                .map(FlatSubtitle::getVideoUrl)
                .collect(Collectors.joining("\n"));
    }


    private FlatSubtitle applyUrls(FlatSubtitle flatSubtitle) {
        flatSubtitle.setDomain(highsubConfig.getServerUrl());
        flatSubtitle.setEncodeId(true);
        flatSubtitle.setApiVersion("v2");
        return flatSubtitle;
    }

    public record SubtitleFileVersion(String eTag, List<String> checksumAlgorithm, Long size, String storageClass,
                                      String key, String versionId, Boolean isLatest, Long lastModifiedUtc) {
        public SubtitleFileVersion(ObjectVersion objectVersion) {
            this(objectVersion.eTag(), objectVersion.checksumAlgorithmAsStrings(), objectVersion.size(),
                    objectVersion.storageClassAsString(), objectVersion.key(), objectVersion.versionId(),
                    objectVersion.isLatest(), objectVersion.lastModified().toEpochMilli());
        }
    }

    @GetMapping(value = "file-versions/{base64EncodedKey}")
    public Flux<SubtitleFileVersion> getFileVersions(@PathVariable String base64EncodedKey) {
        String key = new String(Base64.getDecoder().decode(base64EncodedKey));
        return subtitleService.getVersions(key).flatMapIterable(ListObjectVersionsResponse::versions).map(SubtitleFileVersion::new);
    }

    @PostMapping
    public Mono<FlatSubtitle> createSubtitle(@RequestBody FlatSubtitle flatSubtitle) {
        if (flatSubtitle.getShouldIndex() == Boolean.TRUE && flatSubtitle.getType() != SubtitleType.DIALOGUE)
            flatSubtitle.setShouldIndex(false);
        SubtitleV4 subtitleV4 = mapSubtitle(SubtitleEventType.CREATE).apply(flatSubtitle);
        return Mono.fromFuture(kafkaTemplate.send("subtitles-v4", subtitleV4)).thenReturn(flatSubtitle);
    }
}
