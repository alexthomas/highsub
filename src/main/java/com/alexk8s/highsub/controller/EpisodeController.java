package com.alexk8s.highsub.controller;

import com.alexk8s.highsub.configuration.HighsubConfig;
import com.alexk8s.highsub.kafka.EpisodeProducer;
import com.alexk8s.highsub.kafka.model.EpisodeEventType;
import com.alexk8s.highsub.kafka.model.EpisodeV2;
import com.alexk8s.highsub.model.Episode;
import com.alexk8s.highsub.model.FlatSubtitle;
import com.alexk8s.highsub.repository.EpisodeRepository;
import com.alexk8s.highsub.repository.FlatSubtitleRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.alexk8s.highsub.util.EpisodeUtils.getSlug;

@RestController
@RequestMapping("v1/episode/{show}-{season}-{episode}")
@AllArgsConstructor
public class EpisodeController {

    private final EpisodeRepository episodeRepository;
    private final HighsubConfig highsubConfig;
    private final EpisodeProducer episodeProducer;
    private final FlatSubtitleRepository flatSubtitleRepository;
    private final KafkaTemplate<String, EpisodeV2> kafkaTemplate;

    @GetMapping(value = "")
    public Mono<Episode> getEpisode(@PathVariable String show, @PathVariable String season, @PathVariable String episode) {
        return episodeRepository.findByShowAndSeasonAndEpisode(show, season, episode).map(e -> e.withDomain(highsubConfig.getServerUrl()));
    }

    @DeleteMapping(value = "")
    public Mono<Void> deleteEpisode(@PathVariable String show, @PathVariable String season, @PathVariable String episode) {
        return episodeRepository.findByShowAndSeasonAndEpisode(show, season, episode).flatMap(episodeProducer::deleteEpisode).then();
    }

    @PostMapping(value = "add-tag")
    public Mono<Episode> addTag(@PathVariable String show, @PathVariable String season, @PathVariable String episode, @RequestBody String tag) {
        return episodeRepository.findByShowAndSeasonAndEpisode(show, season, episode)
                .map(e -> {
                    if (e.getTags() == null)
                        e.setTags(new ArrayList<>());
                    if (e.getTags().contains(tag))
                        return e;
                    e.getTags().add(tag);
                    return e;
                }).delayUntil(episodeProducer::updateEpisodeTags);
    }

    @PostMapping(value = "remove-tag")
    public Mono<Episode> removeTag(@PathVariable String show, @PathVariable String season, @PathVariable String episode, @RequestBody String tag) {
        return episodeRepository.findByShowAndSeasonAndEpisode(show, season, episode)
                .map(e -> {
                    if (e.getTags() == null)
                        e.setTags(new ArrayList<>());
                    if (!e.getTags().contains(tag))
                        return e;
                    e.getTags().remove(tag);
                    return e;
                }).delayUntil(episodeProducer::updateEpisodeTags);
    }

    @PostMapping(value = "reindex")
    public Mono<Episode> reindex(@PathVariable String show, @PathVariable String season, @PathVariable String episode) {
        return episodeRepository.findByShowAndSeasonAndEpisode(show, season, episode).delayUntil(episodeProducer::reindexEpisode);
    }

    @PostMapping(value = "rescale")
    public Mono<Episode> rescale(@PathVariable String show, @PathVariable String season, @PathVariable String episode) {
        return episodeRepository.findByShowAndSeasonAndEpisode(show, season, episode).delayUntil(episodeProducer::rescaleEpisode);
    }

    @PostMapping(value = "migrate")
    public Mono<Episode> migrate(@PathVariable String show, @PathVariable String season, @PathVariable String episode) {
        Mono<Episode> episodeMono = episodeRepository.findByShowAndSeasonAndEpisode(show, season, episode);
        Mono<List<FlatSubtitle>> subtitlesMono = flatSubtitleRepository.findByShowAndSeasonAndEpisode(show, season, episode).collectList();
        return Mono.zip(episodeMono,subtitlesMono).map(t->{
            Episode e = t.getT1();
            List<FlatSubtitle> subtitles = t.getT2();
            e.setSubtitleCount((long) subtitles.size());
            Long indexedCount = subtitles.stream().filter(FlatSubtitle::getShouldIndex).count();
            e.setIndexedSubtitleCount(indexedCount);
            e.setSlug(getSlug(e.getShow(),Collections.emptyMap(),e.getSeason(),e.getEpisode()));
            e.setSeasonName(e.getSeason());
            e.setPreviewSubtitleId(subtitles.get((int)Math.floor(Math.random()*subtitles.size())).getId());
            if (e.getTags() == null)
                e.setTags(Collections.emptyList());
            if (e.getParentTags() == null)
                e.setParentTags(Collections.emptyList());
            EpisodeV2 kafkaEpisode = new EpisodeV2();
            BeanUtils.copyProperties(e, kafkaEpisode);
            kafkaEpisode.setType$1(EpisodeEventType.UPDATE_PREVIEW_SUBTITLE_ID);
            kafkaTemplate.send("episodes-v2",  kafkaEpisode);
            return e;
        });
    }

}
