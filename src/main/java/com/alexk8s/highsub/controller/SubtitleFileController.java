package com.alexk8s.highsub.controller;

import com.alexk8s.highsub.model.Episode;
import com.alexk8s.highsub.repository.EpisodeRepository;
import com.alexk8s.highsub.service.SubtitleService;
import com.alexk8s.highsub.util.PathUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.core.io.buffer.DefaultDataBuffer;
import org.springframework.core.io.buffer.DefaultDataBufferFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.function.Function;

@RestController
@RequestMapping("v1/subtitle-file")
@AllArgsConstructor
@Slf4j
public class SubtitleFileController {

    private final SubtitleService subtitleService;
    private final EpisodeRepository episodeRepository;

    public record SyncRequest(String fileData, String fileName, SubtitleService.SyncMode syncMode) {
    }

    public record SyncResponse(String fileData, String fileName, String alassOutput) {
    }

    @PostMapping("sync/{show}-{season}-{episode}")
    public Mono<SyncResponse> syncSubtitleFile(@PathVariable String show, @PathVariable String season, @PathVariable String episode, @RequestBody SyncRequest syncRequest) {
        Mono<Episode> episodeMono = episodeRepository.findByShowAndSeasonAndEpisode(show, season, episode);
        String sourceSubtitleFile = PathUtils.getTempFile(PathUtils.getExtension(syncRequest.fileName));
        String targetSubtitleFile = PathUtils.getTempFile(PathUtils.getExtension(syncRequest.fileName));
        DefaultDataBufferFactory bufferFactory = new DefaultDataBufferFactory();
        DefaultDataBuffer dataBuffer = bufferFactory.wrap(syncRequest.fileData.getBytes());
        Mono<Void> sourceWriteMono = DataBufferUtils.write(Mono.just(dataBuffer), Path.of(sourceSubtitleFile), StandardOpenOption.CREATE_NEW);
        return episodeMono
                .delayUntil(e -> sourceWriteMono)
                .flatMap(e -> {
                    String output = subtitleService.alassSync(e.getSourceVideo(), sourceSubtitleFile, targetSubtitleFile, syncRequest.syncMode);
                    return DataBufferUtils.join(DataBufferUtils.read(Path.of(targetSubtitleFile), bufferFactory, 4096))
                            .map(d -> buildResponse(d, syncRequest.fileName, output));
                });
    }

    private SyncResponse buildResponse(DataBuffer targetFileData, String fileName, String alassOutput) {
        byte[] bytes = new byte[targetFileData.readableByteCount()];
        targetFileData.read(bytes);
        return new SyncResponse(new String(bytes), fileName, alassOutput);
    }

    @GetMapping("{show}-{season}-{episode}/{language}")
    public Mono<FileSystemResource> getSubtitleFile(@PathVariable String show, @PathVariable String season, @PathVariable String episode, @PathVariable String language) {
        Function<Episode,String> fileNameExtractor = switch (language) {
            case "en" -> Episode::getEnglishSubtitles;
            case "jp" -> Episode::getJapaneseSubtitles;
            default -> throw new IllegalArgumentException("Invalid language: " + language);
        };
        return episodeRepository.findByShowAndSeasonAndEpisode(show, season, episode)
                .map(fileNameExtractor)
                .flatMap(subtitleService::getCacheFile)
                .map(FileSystemResource::new);
    }
}

