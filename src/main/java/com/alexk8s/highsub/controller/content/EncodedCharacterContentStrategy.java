package com.alexk8s.highsub.controller.content;

import com.alexk8s.highsub.configuration.HighsubConfig;
import com.alexk8s.highsub.kafka.model.ContentHitV4;
import com.alexk8s.highsub.repository.EpisodeRepository;
import com.alexk8s.highsub.repository.FlatSubtitleRepository;
import com.alexk8s.highsub.service.SubtitleService;
import com.alexk8s.highsub.util.AsciiArtCharacterSet;
import com.alexk8s.highsub.util.PathUtils;
import com.timgroup.statsd.StatsDClient;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import javax.imageio.ImageIO;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;

import static com.alexk8s.highsub.util.AsciiArtUtils.generateGreyArt;

@Slf4j
@Component
public abstract class EncodedCharacterContentStrategy extends ImageDerivativeContentStrategy {


    public EncodedCharacterContentStrategy(HighsubConfig config, StatsDClient statsDClient, FlatSubtitleRepository flatSubtitleRepository, EpisodeRepository episodeRepository, SubtitleService subtitleService, KafkaTemplate<String, ContentHitV4> kafkaTemplate, PngContentStrategy pngContentStrategy) {
        super(config, statsDClient, flatSubtitleRepository, episodeRepository, subtitleService, kafkaTemplate, pngContentStrategy);
    }

    @Override
    @SneakyThrows
    GenerationResult generateContent(ContentMetadata contentMetadata, Long startTime, Long endTime, ContentRequest contentRequest) {
        BufferedImage sourceImage = ImageIO.read(new File(contentMetadata.videoPath()));
        AsciiArtCharacterSet asciiArtCharacterSet = getAsciiArtCharacterSet(contentRequest);
        String asciiArt = generateGreyArt(sourceImage, asciiArtCharacterSet);
        String tempFile = PathUtils.getTempFile(getExtension());
        String encodedArt = encodeArt(asciiArt, sourceImage, asciiArtCharacterSet);
        Files.write(Path.of(tempFile), encodedArt.getBytes());
        return new GenerationResult(tempFile, Collections.emptyList());
    }

    String encodeArt(String asciiArt, BufferedImage sourceImage, AsciiArtCharacterSet asciiArtCharacterSet) {
        String flatAsciiArt = asciiArt.replaceAll("\n", "");
        int chunkWidth = asciiArtCharacterSet.getChunkWidth();
        int chunkHeight = asciiArtCharacterSet.getChunkHeight();
        int width = sourceImage.getWidth();
        int height = sourceImage.getHeight();
        StringBuilder encodedArt = new StringBuilder();
        int charIndex = 0;
        for (int y = 0; y + chunkHeight < height; y += chunkHeight) {
            for (int x = 0; x + chunkWidth < width; x += chunkWidth) {
                Color color = getAverageColor(sourceImage, x, y, chunkWidth, chunkHeight);
                encodedArt.append(getEncodedCharacter(flatAsciiArt.charAt(charIndex++), color));
            }
            encodedArt.append(getLineBreak());
        }
        return encodedArt.toString();
    }

    String getLineBreak() {
        return "\n";
    }


    static Color getAverageColor(BufferedImage image, int x, int y, int w, int h) {
        int r = 0;
        int g = 0;
        int b = 0;
        for (int i = x; i < x + w; i++) {
            for (int j = y; j < y + h; j++) {
                Color color = new Color(image.getRGB(i, j));
                r += color.getRed();
                g += color.getGreen();
                b += color.getBlue();
            }
        }
        int total = w * h;
        return new Color(r / total, g / total, b / total);
    }

    abstract String getEncodedCharacter(char c, Color color);

}
