package com.alexk8s.highsub.controller.content;

import com.alexk8s.highsub.configuration.HighsubConfig;
import com.alexk8s.highsub.ffmpeg.operation.FfmpegOperation;
import com.alexk8s.highsub.kafka.model.ContentHitEventType;
import com.alexk8s.highsub.kafka.model.ContentHitV4;
import com.alexk8s.highsub.kafka.model.HitType;
import com.alexk8s.highsub.model.Episode;
import com.alexk8s.highsub.model.FlatSubtitle;
import com.alexk8s.highsub.repository.EpisodeRepository;
import com.alexk8s.highsub.repository.FlatSubtitleRepository;
import com.alexk8s.highsub.service.SubtitleService;
import com.timgroup.statsd.StatsDClient;
import datadog.trace.api.GlobalTracer;
import datadog.trace.api.Trace;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.kafka.core.KafkaTemplate;
import reactor.core.publisher.Flux;
import reactor.core.publisher.FluxSink;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;
import reactor.util.function.Tuple2;

import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.time.Duration;
import java.util.*;

import static com.alexk8s.highsub.util.LoggingUtils.logJson;
import static com.alexk8s.highsub.util.PathUtils.cleanPath;

@Slf4j
public abstract class ContentStrategy {

    private final HighsubConfig config;
    private final StatsDClient statsDClient;
    private final FlatSubtitleRepository flatSubtitleRepository;
    private final EpisodeRepository episodeRepository;
    private final SubtitleService subtitleService;
    private final KafkaTemplate<String, ContentHitV4> kafkaTemplate;

    private final Map<String,GenerationLock> generatingContent = Collections.synchronizedMap(new HashMap<>());
    private final Flux<ContentRequest> cacheHitsQueue;
    private FluxSink<ContentRequest> cacheHitsSink;

    protected ContentStrategy(HighsubConfig config, StatsDClient statsDClient, FlatSubtitleRepository flatSubtitleRepository, EpisodeRepository episodeRepository, SubtitleService subtitleService, KafkaTemplate<String, ContentHitV4> kafkaTemplate) {
        this.config = config;
        this.statsDClient = statsDClient;
        this.flatSubtitleRepository = flatSubtitleRepository;
        this.episodeRepository = episodeRepository;
        this.subtitleService = subtitleService;
        this.kafkaTemplate = kafkaTemplate;
        this.cacheHitsQueue = Flux.create(sink -> cacheHitsSink = sink);
    }

    private record GenerationLock(String traceId,long lockTime){}

    @PostConstruct
    public void init() {
        cacheHitsQueue
                .flatMap(this::trackCachedHit)
                .onErrorContinue((e, o) -> log.error("Error tracking cached hit {}", o, e))
                .subscribe();
    }

    @PreDestroy
    public void shutdown() {
        cacheHitsSink.complete();
    }

    public abstract String getExtension();

    public abstract MediaType getMediaType();

    String calculateCacheKey(ContentRequest request) {
        Path keyPath = Path.of("cache", cleanPath(request.getShow()), cleanPath(request.getSeason()), request.getEpisode());
        return keyPath + "/" + getKey(request);
    }

    String getKey(ContentRequest contentRequest) {
        return String.join("-", getKeyParts(contentRequest));
    }

    List<String> getKeyParts(ContentRequest request) {
        List<String> keyParts = new ArrayList<>(List.of(request.getSubtitleNumber() + ""));
        if (request.getPlus() != null)
            keyParts.add("Plus" + request.getPlus());
        if (request.getMinus() != null)
            keyParts.add("Minus" + request.getMinus());
        keyParts.add(request.getHeight() + "p");
        if (request.getText() != null)
            keyParts.add("TXT" + request.getText().hashCode());
        if (!request.isIncludeSubtitles())
            keyParts.add("SUBLESS");
        if (request.isAnimated())
            keyParts.add("ANIMATED");
        if (request.isFirstFrame())
            keyParts.add("FIRSTFRAME");
        return keyParts;
    }


    @SneakyThrows
    @Trace
    void moveToContent(String tempPath, Path contentPath) {
        try {

            log.debug("Moving {} to {}", tempPath, contentPath);
            Files.move(Path.of(tempPath), contentPath, StandardCopyOption.REPLACE_EXISTING);
        } catch (FileAlreadyExistsException e) {
            log.warn("Files.move failed with FileAlreadyExistsException, even though the replace existing option was passed...", e);
        }
    }


    @SneakyThrows
    Path resolveCachedPath(String show, String season, String episode, String fileName) {
        Path path = Path.of(config.getCacheRoot(), "cache", show, season, episode, fileName);
        Files.createDirectories(path.getParent());
        return path;
    }

    public Mono<Path> handleContent(ContentRequest contentRequest) {
        return handleContent(contentRequest, true);
    }

    private boolean acquireGenerationLock(String subtitleKey){
        GenerationLock localLock = new GenerationLock(GlobalTracer.get().getTraceId(),System.currentTimeMillis());
        if(!generatingContent.containsKey(subtitleKey)){
            generatingContent.put(subtitleKey,localLock);
            return true;
        }
        GenerationLock lock = generatingContent.get(subtitleKey);
        if(System.currentTimeMillis()-lock.lockTime<10_000){
            log.warn("Content generation for {} has been running for more than 10 seconds... clearing generation lock. Trace that caused the lock was {}", subtitleKey,lock.traceId);
            generatingContent.put(subtitleKey,localLock);
            return true;
        }
        return false;
    }

    Mono<Path> handleContent(ContentRequest contentRequest, boolean retry) {
        String subtitleKey = getKey(contentRequest);
        Path path = resolveCachedPath(contentRequest.getShow(), contentRequest.getSeason(), contentRequest.getEpisode(), subtitleKey + "." + contentRequest.getExtension());
        boolean cached = Files.exists(path);
        if (cached) {
            cacheHitsSink.next(contentRequest);
            return Mono.just(path);
        }
        if (acquireGenerationLock(subtitleKey)) {
            return generateContent(contentRequest)
                    .doOnNext(tempPath -> moveToContent(tempPath, path))
                    .doFinally(s -> generatingContent.remove(subtitleKey))
                    .map(s -> path);
        }
        return Flux.interval(Duration.ofMillis(100))
                .takeUntil(i -> !generatingContent.containsKey(subtitleKey) || i >= 40)
                .flatMap(i -> {
                    log.debug("Checking for content {} for {}th time", subtitleKey, i);
                    if (Files.exists(path))
                        return Mono.just(path);
                    if (retry)
                        return handleContent(contentRequest, false);
                    log.debug("Content not yet available {}", subtitleKey);
                    return Mono.empty();
                }).last()
                .switchIfEmpty(Mono.error(new NotFoundException("Content", subtitleKey)));

    }

    Mono<Void> trackCachedHit(ContentRequest contentRequest) {
        return flatSubtitleRepository.findByShowAndSeasonAndEpisodeAndSubtitleNumber(contentRequest.getShow(), contentRequest.getSeason(), contentRequest.getEpisode(), contentRequest.getSubtitleNumber())
                .doOnNext(flatSubtitle -> {
                    trackHitCached(contentRequest, flatSubtitle);
                }).switchIfEmpty(Mono.error(new NotFoundException("Subtitle", String.join("-", contentRequest.getShow(), contentRequest.getSeason(), contentRequest.getEpisode(), String.valueOf(contentRequest.getSubtitleNumber()))))).then();
    }

    void trackHitCached(ContentRequest contentRequest, FlatSubtitle flatSubtitle) {
        trackHit(contentRequest, flatSubtitle, true,null, null, null);
    }

    void trackHit(ContentRequest contentRequest, FlatSubtitle flatSubtitle, boolean cached,String tempPath, List<String> generationCommands, Long generationTime) {
        String cachedStatus = "status:" + (cached ? "cached" : "generated");
        statsDClient.increment("content.hit", "show:" + contentRequest.getShow(), cachedStatus, "extension:" + contentRequest.getExtension(), "height:" + contentRequest.getHeight());
        long startTime = flatSubtitle.getStartTime() - (contentRequest.getMinus() == null ? 0 : contentRequest.getMinus());
        long endTime = flatSubtitle.getEndTime() + (contentRequest.getPlus() == null ? 0 : contentRequest.getPlus());
        ContentHitV4 contentHit = new ContentHitV4();
        contentHit.setShow(contentRequest.getShow());
        contentHit.setSeason(contentRequest.getSeason());
        contentHit.setEpisode(contentRequest.getEpisode());
        contentHit.setSubtitleNumber(Integer.toString(contentRequest.getSubtitleNumber()));
        contentHit.setContentStartTime(startTime);
        contentHit.setContentEndTime(endTime);
        contentHit.setContentType(contentRequest.getExtension());
        contentHit.setHitUtc(System.currentTimeMillis());
        contentHit.setType$1(ContentHitEventType.CREATE);
        contentHit.setContentHeight(contentRequest.getHeight());
        contentHit.setHitType(cached ? HitType.CACHE : HitType.GENERATE);
        contentHit.setFocused(contentRequest.isFocused());
        contentHit.setUser(contentRequest.getUser());
        contentHit.setIp(contentRequest.getIp());
        contentHit.setUserAgent(contentRequest.getUserAgent());
        contentHit.setContentKey(getKey(contentRequest));
        contentHit.setContentPath(resolveCachedPath(contentRequest.getShow(), contentRequest.getSeason(), contentRequest.getEpisode(), contentHit.getContentKey() + "." + contentRequest.getExtension()).toString());
        contentHit.setReferer(contentRequest.getReferer());
        contentHit.setGenerationCommands(generationCommands);
        contentHit.setGenerationTime(generationTime);
        contentHit.setTraceId(GlobalTracer.get().getTraceId());
        if(tempPath!=null) {
            try {
                contentHit.setSize(Files.size(Path.of(tempPath)));
            } catch (IOException e) {
                log.error("Error getting size of content {}", contentHit.getContentPath(), e);
            }
        }
        kafkaTemplate.send("content-hits-v4", contentHit);
        logJson("ContentHit", contentHit);
    }

    Mono<ContentMetadata> resolveContentMetadata(ContentRequest contentRequest) {
        return episodeRepository.findByShowAndSeasonAndEpisode(contentRequest.getShow(), contentRequest.getSeason(), contentRequest.getEpisode())
                .flatMap(episode -> resolveVideoAndHeight(episode, contentRequest.getHeight(), contentRequest.isIncludeSubtitles()))
                .switchIfEmpty(Mono.error(new NotFoundException("Episode", String.join("-", contentRequest.getShow(), contentRequest.getSeason(), contentRequest.getEpisode()))));
    }

    Mono<String> generateContent(ContentRequest contentRequest) {
        Mono<ContentMetadata> contentMetadataMono = resolveContentMetadata(contentRequest);
        Mono<FlatSubtitle> subtitleMono = flatSubtitleRepository.findByShowAndSeasonAndEpisodeAndSubtitleNumber(contentRequest.getShow(), contentRequest.getSeason(), contentRequest.getEpisode(), contentRequest.getSubtitleNumber())
                .switchIfEmpty(Mono.error(new NotFoundException("Subtitle", String.join("-", contentRequest.getShow(), contentRequest.getSeason(), contentRequest.getEpisode(), String.valueOf(contentRequest.getSubtitleNumber())))));
        return Mono.zip(contentMetadataMono, subtitleMono)
                .publishOn(Schedulers.boundedElastic())
                .map(t -> generateContent(t, contentRequest));
    }

    @Trace
    String generateContent(Tuple2<ContentMetadata, FlatSubtitle> tuple, ContentRequest contentRequest) {
        ContentMetadata contentMetadata = tuple.getT1();
        FlatSubtitle subtitle = tuple.getT2();
        long startTime = subtitle.getStartTime() - (contentRequest.getMinus() == null ? 0 : contentRequest.getMinus());
        long endTime = subtitle.getEndTime() + (contentRequest.getPlus() == null ? 0 : contentRequest.getPlus());
        long generationStartTime = System.currentTimeMillis();
        GenerationResult generationResult = generateContent(contentMetadata, startTime, endTime, contentRequest);
        trackHit(contentRequest, subtitle, false,generationResult.path, generationResult.commands.stream().map(o->o.getCommand(config.getTools())).toList(), System.currentTimeMillis()-generationStartTime);
        return generationResult.path;
    }

    public record ContentMetadata(String videoPath, int height, String subtitlePath, boolean fullEncode) {
    }

    /**
     * Resolve the video and height to use for the content and the path to the subtitles. If the height is -1, the video should not be scaled.
     * If burning the subtitles is not required the subtitle path will be null
     *
     * @param episode the episode to resolve the video from
     * @param height  the target height to resolve the video to
     * @return a tuple of the video path, the height to scale the video to and the path to the subtitles
     */
    Mono<ContentMetadata> resolveVideoAndHeight(Episode episode, int height, boolean includeSubtitles) {
        if (!includeSubtitles)
            return Mono.just(new ContentMetadata(episode.getSourceVideo(), height, null, false));
        if (height == -1)
            return subtitleService.getCacheFile(episode.getEnglishSubtitles())
                    .map(subtitlePath -> new ContentMetadata(episode.getSourceVideo(), height, subtitlePath, true));
        if (episode.getScaled150pVideo() != null) {
            if (height == 150)
                return Mono.just(new ContentMetadata(episode.getScaled150pVideo(), -1, null, false));
            if (height < 150)
                return Mono.just(new ContentMetadata(episode.getScaled150pVideo(), height, null, false));
        }
        if (episode.getScaled480pVideo() != null) {
            if (height == 480)
                return Mono.just(new ContentMetadata(episode.getScaled480pVideo(), -1, null, false));
            if (height < 480)
                return Mono.just(new ContentMetadata(episode.getScaled480pVideo(), height, null, false));
        }
        return subtitleService.getCacheFile(episode.getEnglishSubtitles())
                .switchIfEmpty(Mono.error(new NotFoundException("Subtitle", episode.getEnglishSubtitles())))
                .map(subtitlePath -> new ContentMetadata(episode.getSourceVideo(), height, subtitlePath, true));
    }

    /**
     * Generate content in a strategy specific way
     *
     * @param contentMetadata the metadata for the content
     * @param startTime       the start time of the clip in milliseconds
     * @param endTime         the end time of the clip in milliseconds
     * @param contentRequest  the content request
     * @return the path to the generated content
     */
    abstract GenerationResult generateContent(ContentMetadata contentMetadata, Long startTime, Long endTime, ContentRequest contentRequest);

    protected record GenerationResult(String path, List<FfmpegOperation<?>> commands) {

    }
}
