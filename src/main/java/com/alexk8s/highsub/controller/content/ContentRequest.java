package com.alexk8s.highsub.controller.content;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@AllArgsConstructor
@RequiredArgsConstructor
@Data
public class ContentRequest {
    private final String show;
    private final String season;
    private final String episode;
    private final int subtitleNumber;
    private final String extension;
    private Long plus;
    private Long minus;
    private int height;
    private boolean focused;
    private String user;
    private String text;
    private boolean includeSubtitles;
    private boolean animated;
    private boolean firstFrame;
    private String characterSet;
    private String font;
    private int fontSize;
    private String ip;
    private String userAgent;
    private String referer;

}
