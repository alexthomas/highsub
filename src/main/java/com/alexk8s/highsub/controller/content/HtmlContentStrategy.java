package com.alexk8s.highsub.controller.content;

import com.alexk8s.highsub.configuration.HighsubConfig;
import com.alexk8s.highsub.kafka.model.ContentHitV4;
import com.alexk8s.highsub.repository.EpisodeRepository;
import com.alexk8s.highsub.repository.FlatSubtitleRepository;
import com.alexk8s.highsub.service.SubtitleService;
import com.alexk8s.highsub.util.AsciiArtCharacterSet;
import com.alexk8s.highsub.util.PathUtils;
import com.timgroup.statsd.StatsDClient;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;

import static com.alexk8s.highsub.util.AsciiArtUtils.generateGreyArt;

@Slf4j
@Component
public class HtmlContentStrategy extends ImageDerivativeContentStrategy {
    private static final String HTML_BASE = """
            <!DOCTYPE html>
            <html>
            <head>
            <style>
            body {
              font-family: Courier, monospace;
              font-size: 12px;
              white-space: pre;
            }
            </style>
            </head>
            <body>%s</body>
            </html>""";

    public HtmlContentStrategy(HighsubConfig config, StatsDClient statsDClient, FlatSubtitleRepository flatSubtitleRepository, EpisodeRepository episodeRepository, SubtitleService subtitleService, KafkaTemplate<String, ContentHitV4> kafkaTemplate, PngContentStrategy pngContentStrategy) {
        super(config, statsDClient, flatSubtitleRepository, episodeRepository, subtitleService, kafkaTemplate, pngContentStrategy);
    }

    @Override
    public String getExtension() {
        return "html";
    }

    @Override
    public MediaType getMediaType() {
        return MediaType.TEXT_HTML;
    }

    @Override
    @SneakyThrows
    GenerationResult generateContent(ContentMetadata contentMetadata, Long startTime, Long endTime, ContentRequest contentRequest) {
        BufferedImage sourceImage = ImageIO.read(new File(contentMetadata.videoPath()));
        AsciiArtCharacterSet asciiArtCharacterSet = getAsciiArtCharacterSet(contentRequest);
        String asciiArt = generateGreyArt(sourceImage, asciiArtCharacterSet).replaceAll(" ","&nbsp;");
        String tempFile = PathUtils.getTempFile(getExtension());
        Files.write(Path.of(tempFile), HTML_BASE.formatted(asciiArt).getBytes());
        return new GenerationResult(tempFile, Collections.emptyList());
    }

    @Override
    String getBlacklistCharacters(){
        return "<>&";
    }
}
