package com.alexk8s.highsub.controller.content;


import com.alexk8s.highsub.configuration.HighsubConfig;
import com.alexk8s.highsub.ffmpeg.FfmpegOutput;
import com.alexk8s.highsub.ffmpeg.FfmpegTools;
import com.alexk8s.highsub.ffmpeg.operation.ExtractAudio;
import com.alexk8s.highsub.ffmpeg.operation.GetMetadata;
import com.alexk8s.highsub.kafka.model.ContentHitV4;
import com.alexk8s.highsub.repository.EpisodeRepository;
import com.alexk8s.highsub.repository.FlatSubtitleRepository;
import com.alexk8s.highsub.service.SubtitleService;
import com.timgroup.statsd.StatsDClient;
import org.springframework.kafka.core.KafkaTemplate;

import java.util.List;

public abstract class AudioContentStrategy extends ContentStrategy {
    private final FfmpegTools ffmpegTools;
    protected AudioContentStrategy(HighsubConfig config, StatsDClient statsDClient, FlatSubtitleRepository flatSubtitleRepository, EpisodeRepository episodeRepository, SubtitleService subtitleService, KafkaTemplate<String, ContentHitV4> kafkaTemplate, FfmpegTools ffmpegTools) {
        super(config, statsDClient, flatSubtitleRepository, episodeRepository, subtitleService, kafkaTemplate);
        this.ffmpegTools = ffmpegTools;
    }


    @Override
    GenerationResult generateContent(ContentMetadata contentMetadata, Long startTime, Long endTime, ContentRequest contentRequest) {
        FfmpegOutput audioMetadata = new GetMetadata(contentMetadata.videoPath(), "audio").execute(ffmpegTools);
        Integer audioIndex = ffmpegTools.getAudioIndex(audioMetadata);
        ExtractAudio operation = ExtractAudio.builder().videoPath(contentMetadata.videoPath()).audioIndex(audioIndex).startTime(startTime).endTime(endTime)
                .format(getExtension())
                .build();
        String execute = operation.execute(ffmpegTools);
        return new GenerationResult(execute, List.of(operation));
    }
}
