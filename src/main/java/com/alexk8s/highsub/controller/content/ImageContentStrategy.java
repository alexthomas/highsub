package com.alexk8s.highsub.controller.content;

import com.alexk8s.highsub.configuration.HighsubConfig;
import com.alexk8s.highsub.ffmpeg.FfmpegTools;
import com.alexk8s.highsub.ffmpeg.operation.ExtractFrame;
import com.alexk8s.highsub.kafka.model.ContentHitV4;
import com.alexk8s.highsub.repository.EpisodeRepository;
import com.alexk8s.highsub.repository.FlatSubtitleRepository;
import com.alexk8s.highsub.service.SubtitleService;
import com.timgroup.statsd.StatsDClient;
import datadog.trace.api.Trace;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import net.coobird.thumbnailator.Thumbnails;
import org.springframework.kafka.core.KafkaTemplate;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import javax.imageio.ImageIO;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import static com.alexk8s.highsub.util.PathUtils.getTempFile;

@Slf4j
public abstract class ImageContentStrategy extends ContentStrategy {
    private static final int IMAGE_PADDING = 10;
    private final FfmpegTools ffmpegTools;
    private final HighsubConfig config;


    protected ImageContentStrategy(HighsubConfig config, StatsDClient statsDClient, FlatSubtitleRepository flatSubtitleRepository, EpisodeRepository episodeRepository, SubtitleService subtitleService, KafkaTemplate<String, ContentHitV4> kafkaTemplate, FfmpegTools ffmpegTools) {
        super(config, statsDClient, flatSubtitleRepository, episodeRepository, subtitleService, kafkaTemplate);
        this.config = config;
        this.ffmpegTools = ffmpegTools;
    }

    String getQuality() {
        return "80";
    }

    @Override
    GenerationResult generateContent(ContentMetadata contentMetadata, Long startTime, Long endTime, ContentRequest contentRequest) {
        String videoPath = contentMetadata.videoPath();
        String subtitlePath = contentMetadata.subtitlePath();
        int height = contentMetadata.height();
        String text = contentRequest.getText();
        long frameTime = contentRequest.isFirstFrame() ? startTime : (endTime - startTime) / 2 + startTime;
        ExtractFrame extractFrame = new ExtractFrame(videoPath, frameTime, getExtension(), getQuality(), subtitlePath);
        String imagePath = extractFrame.execute(ffmpegTools);
        if (height != -1) {
            String tempPath = getTempFile(getExtension());
            resizeImage(imagePath, tempPath, height);
            imagePath = tempPath;
        }
        if (text != null)
            log.warn("Text is not supported for image content");
        return new GenerationResult(imagePath, List.of(extractFrame));
    }

    @SneakyThrows
    @Trace
    void resizeImage(String source, String target, int height) {
        Thumbnails.of(source)
                .height(height)
                .toFile(target);
    }

    public Mono<Path> getCombinedImage(String show, String season, String episode, List<Integer> ids, String user) {
        String subtitleKey = show + "-" + season + "-" + episode + "-" + String.join("-", ids.stream().map(i -> Integer.toString(i)).toList());
        Path cachedPath = resolveCachedPath(show,season,episode,subtitleKey + "." + getExtension());
        if (Files.exists(cachedPath))
            return Mono.just(cachedPath);
        List<ContentRequest> contentRequests = ids.stream().map(subtitleNumber -> new ContentRequest(show, season, episode, subtitleNumber, getExtension(), 0L, 0L, config.getImages().getMaxHeight(), true, user, null, true, false,false,null,null,8,null,null,null)).toList();
        return Flux.fromIterable(contentRequests).publishOn(Schedulers.boundedElastic())
                .flatMap(this::handleContent)
                .collectList()
                .map(this::getCombinedImage)
                .mapNotNull(p -> {
                    moveToContent(p.toString(), cachedPath);
                    return cachedPath;
                });
    }

    @SneakyThrows
    Path getCombinedImage(List<Path> paths) {
        int gridWidth = paths.size() > 8 ? 3 : 2;
        BufferedImage firstImage = ImageIO.read(paths.get(0).toFile());
        int rows = (int) Math.ceil(paths.size() / gridWidth);
        int width = firstImage.getWidth();
        int height = firstImage.getHeight();
        int combinedWidth = ((width * gridWidth) + (IMAGE_PADDING * gridWidth));
        int combinedHeight = (height * rows) + (IMAGE_PADDING * rows);
        BufferedImage combinedImage = new BufferedImage(combinedWidth, combinedHeight, BufferedImage.TYPE_INT_RGB);
        Graphics2D graphics = combinedImage.createGraphics();
        graphics.setColor(new Color(50, 53, 62));
        graphics.fillRect(0, 0, combinedWidth, combinedHeight);
        int cursor = 0;
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < gridWidth; j++) {
                if (cursor < paths.size()) {
                    BufferedImage image = ImageIO.read(paths.get(cursor).toFile());
                    graphics.drawImage(image, (width * j) + (IMAGE_PADDING * j + IMAGE_PADDING / 2), (height * i) + (IMAGE_PADDING * i + IMAGE_PADDING / 2), null);
                }
                cursor++;
            }
        }
        String combinedPath = getTempFile(getExtension());
        ImageIO.write(combinedImage, getExtension(), new File(combinedPath));
        return Path.of(combinedPath);

    }
}
