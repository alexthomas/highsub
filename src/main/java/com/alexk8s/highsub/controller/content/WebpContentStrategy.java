package com.alexk8s.highsub.controller.content;

import com.alexk8s.highsub.configuration.HighsubConfig;
import com.alexk8s.highsub.ffmpeg.FfmpegOutput;
import com.alexk8s.highsub.ffmpeg.FfmpegTools;
import com.alexk8s.highsub.ffmpeg.operation.ExtractVideo;
import com.alexk8s.highsub.ffmpeg.operation.GetMetadata;
import com.alexk8s.highsub.ffmpeg.operation.Resize;
import com.alexk8s.highsub.kafka.model.ContentHitV4;
import com.alexk8s.highsub.repository.EpisodeRepository;
import com.alexk8s.highsub.repository.FlatSubtitleRepository;
import com.alexk8s.highsub.service.SubtitleService;
import com.timgroup.statsd.StatsDClient;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

@Component
@Slf4j
public class WebpContentStrategy extends ImageContentStrategy {
    private final FfmpegTools ffmpegTools;

    public WebpContentStrategy(HighsubConfig config, StatsDClient statsDClient, FlatSubtitleRepository flatSubtitleRepository, EpisodeRepository episodeRepository, SubtitleService subtitleService, KafkaTemplate<String, ContentHitV4> kafkaTemplate, FfmpegTools ffmpegTools) {
        super(config, statsDClient, flatSubtitleRepository, episodeRepository, subtitleService, kafkaTemplate, ffmpegTools);
        this.ffmpegTools = ffmpegTools;
    }

    @Override
    public String getExtension() {
        return "webp";
    }

    @Override
    public MediaType getMediaType() {
        return MediaType.parseMediaType("image/webp");
    }


    @Override
    @SneakyThrows(IOException.class)
    void resizeImage(String source, String target, int height) {
        log.warn("Thumbnailator does not support webp, using ffmpeg instead");
        String output = new Resize(source, height, getExtension()).execute(ffmpegTools);
        Files.move(Path.of(output), Path.of(target), java.nio.file.StandardCopyOption.REPLACE_EXISTING);
    }

    @Override
    GenerationResult generateContent(ContentMetadata contentMetadata, Long startTime, Long endTime, ContentRequest contentRequest) {
        if (!contentRequest.isAnimated())
            return super.generateContent(contentMetadata, startTime, endTime, contentRequest);
        String videoPath = contentMetadata.videoPath();
        String subtitlePath = contentMetadata.subtitlePath();
        int height = contentMetadata.height();
        FfmpegOutput sourceVideoMetadata = new GetMetadata(videoPath, "video").execute(ffmpegTools);
        ExtractVideo extractVideo = ExtractVideo.builder()
                .videoPath(videoPath)
                .subtitlePath(subtitlePath)
                .startTime(startTime)
                .endTime(endTime)
                .height(height > 0 ? height : null)
                .format(getExtension())
                .videoEncoding("libwebp")
                .videoIndex(ffmpegTools.getVideoStream(sourceVideoMetadata).getIndex())
                .loop("0")
                .quality("70")
                .compressionLevel("6")
                .build();
        return new GenerationResult(extractVideo.execute(ffmpegTools), List.of(extractVideo));
    }
}
