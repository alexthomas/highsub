package com.alexk8s.highsub.controller.content;

import org.springframework.web.server.ResponseStatusException;

public class NotFoundException extends ResponseStatusException {

    public NotFoundException(String entity, String key) {
        super(org.springframework.http.HttpStatus.NOT_FOUND, entity + " with " + key + " not found");
    }
}
