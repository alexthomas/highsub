package com.alexk8s.highsub.controller.content;

import com.alexk8s.highsub.configuration.HighsubConfig;
import com.alexk8s.highsub.ffmpeg.FfmpegOutput;
import com.alexk8s.highsub.ffmpeg.FfmpegTools;
import com.alexk8s.highsub.ffmpeg.Stream;
import com.alexk8s.highsub.ffmpeg.operation.AddTextToVideo;
import com.alexk8s.highsub.ffmpeg.operation.ExtractVideo;
import com.alexk8s.highsub.ffmpeg.operation.FfmpegOperation;
import com.alexk8s.highsub.ffmpeg.operation.GetDuration;
import com.alexk8s.highsub.ffmpeg.operation.GetMetadata;
import com.alexk8s.highsub.kafka.model.ContentHitV4;
import com.alexk8s.highsub.repository.EpisodeRepository;
import com.alexk8s.highsub.repository.FlatSubtitleRepository;
import com.alexk8s.highsub.service.FlickerService;
import com.alexk8s.highsub.service.SubtitleService;
import com.timgroup.statsd.StatsDClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.util.StringUtils;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Slf4j
public abstract class VideoContentStrategy extends ContentStrategy {
    private final FfmpegTools ffmpegTools;
    private final FlickerService flickerService;

    protected VideoContentStrategy(HighsubConfig config, StatsDClient statsDClient, FlatSubtitleRepository flatSubtitleRepository, EpisodeRepository episodeRepository, SubtitleService subtitleService, KafkaTemplate<String, ContentHitV4> kafkaTemplate, FfmpegTools ffmpegTools, FlickerService flickerService) {
        super(config, statsDClient, flatSubtitleRepository, episodeRepository, subtitleService, kafkaTemplate);
        this.ffmpegTools = ffmpegTools;
        this.flickerService = flickerService;
    }

    String getCodec() {
        return null;
    }

    @Override
    GenerationResult generateContent(ContentMetadata contentMetadata, Long startTime, Long endTime, ContentRequest contentRequest) {
        String videoPath = contentMetadata.videoPath();
        String subtitlePath = contentMetadata.subtitlePath();
        int height = contentMetadata.height();
        String text = contentRequest.getText();
        boolean fullEncode = contentMetadata.fullEncode();
        List<FfmpegOperation<?>> commands = new ArrayList<>();
        GetMetadata videoMetadata = new GetMetadata(videoPath, "video");
        commands.add(videoMetadata);
        FfmpegOutput sourceVideoMetadata = videoMetadata.execute(ffmpegTools);
        GetMetadata audioMetadata = new GetMetadata(videoPath, "audio");
        commands.add(audioMetadata);
        FfmpegOutput sourceAudioMetadata = audioMetadata.execute(ffmpegTools);

        ExtractVideo extractVideo = ExtractVideo.builder()
                .videoPath(videoPath)
                .subtitlePath(subtitlePath)
                .startTime(startTime)
                .endTime(endTime)
                .height(height > 0 ? height : null)
                .format(getExtension())
                .videoEncoding(getCodec())
                .videoIndex(ffmpegTools.getVideoStream(sourceVideoMetadata).getIndex())
                .audioIndex(ffmpegTools.getAudioIndex(sourceAudioMetadata))
                .fullEncode(fullEncode)
                .build();
        commands.add(extractVideo);
        String outputPath = extractVideo
                .execute(ffmpegTools);
        commands.addAll(handleFlicker(Path.of(outputPath)));
        if (StringUtils.hasText(text)) {
            GetMetadata getMetadata = new GetMetadata(outputPath, "video");
            commands.add(getMetadata);
            Stream videoStream = ffmpegTools.getVideoStream(getMetadata.execute(ffmpegTools));
            AddTextToVideo addText = new AddTextToVideo(outputPath, getExtension(), text, videoStream.getWidth());
            commands.add(addText);
            return new GenerationResult(addText.execute(ffmpegTools), commands);
        }
        return new GenerationResult(outputPath, commands);
    }

    List<FfmpegOperation<?>> handleFlicker(Path originalPath) {

        long duration = new GetDuration(originalPath.toString()).execute(ffmpegTools);
        List<FfmpegOperation<?>> commands = Collections.synchronizedList(new ArrayList<>());
        FlickerService.FlickerType flickerType = flickerService.hasFlicker(originalPath.toString(), duration, commands::add);
        long newStart = 0;
        long newEnd = duration;
        if (flickerType == FlickerService.FlickerType.FIRST_FRAME || flickerType == FlickerService.FlickerType.BOTH)
            newStart = FlickerService.FLICKER_OFFSET;
        if (flickerType == FlickerService.FlickerType.LAST_FRAME || flickerType == FlickerService.FlickerType.BOTH)
            newEnd = duration - FlickerService.FLICKER_OFFSET;
        if (newStart != 0 || newEnd != duration) {
            log.info("Detected flicker {}", flickerType);
            ExtractVideo extractVideo = ExtractVideo.builder()
                    .videoPath(originalPath.toString())
                    .startTime(newStart)
                    .endTime(newEnd)
                    .format(getExtension())
                    .build();
            commands.add(extractVideo);
            String newPath = extractVideo
                    .execute(ffmpegTools);
            moveToContent(newPath, originalPath);
        }
        return commands;

    }
}
