package com.alexk8s.highsub.controller.content;

import com.alexk8s.highsub.configuration.HighsubConfig;
import com.alexk8s.highsub.kafka.model.ContentHitV4;
import com.alexk8s.highsub.repository.EpisodeRepository;
import com.alexk8s.highsub.repository.FlatSubtitleRepository;
import com.alexk8s.highsub.service.SubtitleService;
import com.alexk8s.highsub.util.AsciiArtCharacterSet;
import com.timgroup.statsd.StatsDClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import java.awt.Color;


@Slf4j
@Component
public class CCContentStrategy extends EncodedCharacterContentStrategy {
    private static final Color[] PALETTE = new Color[]{
            new Color(240, 240, 240),
            new Color(242, 178, 51),
            new Color(229, 127, 216),
            new Color(153, 178, 242),
            new Color(222, 222, 108),
            new Color(127, 204, 25),
            new Color(242, 178, 204),
            new Color(76, 76, 76),
            new Color(153, 153, 153),
            new Color(76, 153, 178),
            new Color(178, 102, 229),
            new Color(51, 102, 204),
            new Color(127, 102, 76),
            new Color(87, 166, 78),
            new Color(204, 76, 76),
            new Color(17, 17, 17)
    };


    public CCContentStrategy(HighsubConfig config, StatsDClient statsDClient, FlatSubtitleRepository flatSubtitleRepository, EpisodeRepository episodeRepository, SubtitleService subtitleService, KafkaTemplate<String, ContentHitV4> kafkaTemplate, PngContentStrategy pngContentStrategy) {
        super(config, statsDClient, flatSubtitleRepository, episodeRepository, subtitleService, kafkaTemplate, pngContentStrategy);
    }

    @Override
    AsciiArtCharacterSet getAsciiArtCharacterSet(ContentRequest contentRequest) {
        return new AsciiArtCharacterSet(" .:-=+*#%@".toCharArray(), "Minecraftia", 12, true);
    }

    @Override
    public String getExtension() {
        return "cc";
    }

    @Override
    public MediaType getMediaType() {
        return MediaType.TEXT_PLAIN;
    }

    @Override
    String getEncodedCharacter(char c, Color color) {
        int index = getClosestColorIndex(color);
        return c + Integer.toString(index, 16);
    }

    private static int getClosestColorIndex(Color color) {
        double minDistance = Double.MAX_VALUE;
        int closestIndex = 0;

        for (int i = 0; i < PALETTE.length; i++) {
            double distance = colorDistance(color, PALETTE[i]);
            if (distance < minDistance) {
                minDistance = distance;
                closestIndex = i;
            }
        }
        return closestIndex;
    }

    private static double colorDistance(Color c1, Color c2) {
        int redDiff = c1.getRed() - c2.getRed();
        int greenDiff = c1.getGreen() - c2.getGreen();
        int blueDiff = c1.getBlue() - c2.getBlue();
        return Math.sqrt(redDiff * redDiff + greenDiff * greenDiff + blueDiff * blueDiff);
    }

}
