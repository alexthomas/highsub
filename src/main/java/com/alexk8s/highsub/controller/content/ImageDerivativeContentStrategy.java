package com.alexk8s.highsub.controller.content;

import com.alexk8s.highsub.configuration.HighsubConfig;
import com.alexk8s.highsub.kafka.model.ContentHitV4;
import com.alexk8s.highsub.repository.EpisodeRepository;
import com.alexk8s.highsub.repository.FlatSubtitleRepository;
import com.alexk8s.highsub.service.SubtitleService;
import com.alexk8s.highsub.util.AsciiArtCharacterSet;
import com.timgroup.statsd.StatsDClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.List;

@Slf4j
@Component
public abstract class ImageDerivativeContentStrategy extends ContentStrategy {
    private final PngContentStrategy pngContentStrategy;

    public ImageDerivativeContentStrategy(HighsubConfig config, StatsDClient statsDClient, FlatSubtitleRepository flatSubtitleRepository, EpisodeRepository episodeRepository, SubtitleService subtitleService, KafkaTemplate<String, ContentHitV4> kafkaTemplate, PngContentStrategy pngContentStrategy) {
        super(config, statsDClient, flatSubtitleRepository, episodeRepository, subtitleService, kafkaTemplate);
        this.pngContentStrategy = pngContentStrategy;
    }

    List<String> getKeyParts(ContentRequest contentRequest) {
        List<String> keyParts = super.getKeyParts(contentRequest);
        keyParts.add("characterSet" + contentRequest.getCharacterSet().hashCode());
        keyParts.add("fontSize" + contentRequest.getFontSize());
        keyParts.add("font" + contentRequest.getFont());
        return keyParts;
    }

    AsciiArtCharacterSet getAsciiArtCharacterSet(ContentRequest contentRequest) {
        char[] rawChars = contentRequest.getCharacterSet().toCharArray();
        char[] cleanChars = new char[rawChars.length];
        int cleanIndex = 0;
        char[] blacklistChars = getBlacklistCharacters().toCharArray();
        for (char rawChar : rawChars) {
            boolean blacklisted = false;
            for (char bc : blacklistChars) {
                if (rawChar == bc) {
                    blacklisted = true;
                    break;
                }
            }
            if (!blacklisted)
                cleanChars[cleanIndex++] = rawChar;
        }
        return new AsciiArtCharacterSet(Arrays.copyOf(cleanChars, cleanIndex), contentRequest.getFont(), contentRequest.getFontSize(), true);
    }

    String getBlacklistCharacters() {
        return "";
    }


    @Override
    Mono<ContentMetadata> resolveContentMetadata(ContentRequest contentRequest) {
        return pngContentStrategy.handleContent(contentRequest)
                .map(p -> new ContentMetadata(p.toString(), contentRequest.getHeight(), null, false));
    }

}
