package com.alexk8s.highsub.controller.content;

import com.alexk8s.highsub.configuration.HighsubConfig;
import com.alexk8s.highsub.ffmpeg.FfmpegOutput;
import com.alexk8s.highsub.ffmpeg.FfmpegTools;
import com.alexk8s.highsub.ffmpeg.Stream;
import com.alexk8s.highsub.ffmpeg.operation.AddTextToGif;
import com.alexk8s.highsub.ffmpeg.operation.ExtractGif;
import com.alexk8s.highsub.ffmpeg.operation.GetMetadata;
import com.alexk8s.highsub.kafka.model.ContentHitV4;
import com.alexk8s.highsub.repository.EpisodeRepository;
import com.alexk8s.highsub.repository.FlatSubtitleRepository;
import com.alexk8s.highsub.service.SubtitleService;
import com.timgroup.statsd.StatsDClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class GifContentStrategy extends ContentStrategy {

    private final FfmpegTools ffmpegTools;

    @Autowired
    public GifContentStrategy(HighsubConfig config, StatsDClient statsDClient, FlatSubtitleRepository flatSubtitleRepository, EpisodeRepository episodeRepository, SubtitleService subtitleService, KafkaTemplate<String, ContentHitV4> kafkaTemplate, FfmpegTools ffmpegTools) {
        super(config, statsDClient, flatSubtitleRepository, episodeRepository, subtitleService, kafkaTemplate);
        this.ffmpegTools = ffmpegTools;
    }

    @Override
    public String getExtension() {
        return "gif";
    }

    @Override
    public MediaType getMediaType() {
        return MediaType.IMAGE_GIF;
    }

    @Override
    GenerationResult generateContent(ContentMetadata contentMetadata, Long startTime, Long endTime, ContentRequest contentRequest) {
        String videoPath = contentMetadata.videoPath();
        String subtitlePath = contentMetadata.subtitlePath();
        int height = contentMetadata.height();
        String text = contentRequest.getText();
        if (height < 10 && height != -1)
            throw new IllegalArgumentException("Height must be greater than 10");
        FfmpegOutput videoMetadata = new GetMetadata(videoPath, "video").execute(ffmpegTools);
        Stream videoStream = ffmpegTools.getVideoStream(videoMetadata);
        ExtractGif extractGif = new ExtractGif(videoPath, videoStream.getIndex(), subtitlePath, startTime, endTime, height);
        String gifPath = extractGif.execute(ffmpegTools);
        if (text != null){

            AddTextToGif addTextToGif = new AddTextToGif(gifPath, text, videoStream.getWidth());
            return new GenerationResult(addTextToGif.execute(ffmpegTools), List.of(extractGif,addTextToGif));
        }
        return new GenerationResult(gifPath, List.of(extractGif));
    }
}
