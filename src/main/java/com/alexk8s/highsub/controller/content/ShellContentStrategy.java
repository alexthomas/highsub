package com.alexk8s.highsub.controller.content;

import com.alexk8s.highsub.configuration.HighsubConfig;
import com.alexk8s.highsub.kafka.model.ContentHitV4;
import com.alexk8s.highsub.repository.EpisodeRepository;
import com.alexk8s.highsub.repository.FlatSubtitleRepository;
import com.alexk8s.highsub.service.SubtitleService;
import com.alexk8s.highsub.util.AsciiArtCharacterSet;
import com.timgroup.statsd.StatsDClient;
import lombok.SneakyThrows;
import org.springframework.http.MediaType;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.GZIPOutputStream;

@Component
public class ShellContentStrategy extends EncodedCharacterContentStrategy {

    private static final String SHELL_SCRIPT_TEMPLATE = """
            #!/bin/bash
            function decode_and_print {
              width=$1
              color_string=$(echo "$2" | base64 -d | gunzip)  # Decode and gunzip color string
              pair_data=$(echo "$3" | base64 -d | gunzip)  # Decode and gunzip pair data
                       
              IFS=',' read -r -a pairArray <<< "$pair_data"
                       
              count=0
              for pair in "${pairArray[@]}"; do
                if [ ${#pair} -eq 1 ]; then
                  char=" "  # Set char to space for missing character
                  colorIndex=$(( ${pair:0} * 6 ))  # Multiply index by 6 for substring offset
                else
                  char=${pair:0:1}
                  colorIndex=$(( ${pair:1} * 6 ))  # Multiply index by 6 for substring offset
                fi
                hex_str=${color_string:$colorIndex:6}
                red=$((16#${hex_str:$i:2}))
                green=$((16#${hex_str:$((i + 2)):2}))
                blue=$((16#${hex_str:$((i + 4)):2}))
                
                printf "\\033[38;2;${red};${green};${blue}m%s" "$char"
                       
                count=$((count + 1))
                if [ $count -eq $width ]; then
                  printf "\\n"
                  count=0
                fi
              done
            }

            
            """;
    public ShellContentStrategy(HighsubConfig config, StatsDClient statsDClient, FlatSubtitleRepository flatSubtitleRepository, EpisodeRepository episodeRepository, SubtitleService subtitleService, KafkaTemplate<String, ContentHitV4> kafkaTemplate, PngContentStrategy pngContentStrategy) {
        super(config, statsDClient, flatSubtitleRepository, episodeRepository, subtitleService, kafkaTemplate, pngContentStrategy);
    }

    @Override
    protected String getEncodedCharacter(char c, Color color) {
        return c + "" + color.getRGB() + ",";
    }

    @Override
    @SneakyThrows
    protected String encodeArt(String asciiArt, BufferedImage sourceImage, AsciiArtCharacterSet asciiArtCharacterSet) {
        StringBuilder colorHex = new StringBuilder();
        Map<String, Integer> colorsMap = new HashMap<>();
        StringBuilder pairs = new StringBuilder();
        int width = sourceImage.getWidth()/asciiArtCharacterSet.getChunkWidth();
        String encodedCharacters = super.encodeArt(asciiArt, sourceImage, asciiArtCharacterSet);
        String[] splitChars = encodedCharacters.split(",");

        for (String encodedChar : splitChars) {
            if (encodedChar.isEmpty()) continue;

            char c = encodedChar.charAt(0);
            String colorStr = encodedChar.substring(1);

            if (!colorsMap.containsKey(colorStr)) {
                colorsMap.put(colorStr, colorHex.length() / 6);
                colorHex.append(toHex(Integer.parseInt(colorStr)));
            }

            int colorIndex = colorsMap.get(colorStr);
            pairs.append(c).append(colorIndex).append(",");
        }

        String pairStr = pairs.toString();

        byte[] compressedColorHex = compress(colorHex.toString().getBytes(StandardCharsets.UTF_8));
        byte[] compressedPairStr = compress(pairStr.getBytes(StandardCharsets.UTF_8));

        String encodedColorHex = Base64.getEncoder().encodeToString(compressedColorHex);
        String encodedPairStr = Base64.getEncoder().encodeToString(compressedPairStr);
        return getShellScript(width, encodedColorHex, encodedPairStr);

    }

    String getLineBreak(){
        return "";
    }

    private String toHex(int color) {
        Color c = new Color(color);
        return String.format("%02x%02x%02x", c.getRed(), c.getGreen(), c.getBlue());
    }

    private byte[] compress(byte[] data) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream(data.length);
        GZIPOutputStream gzip = new GZIPOutputStream(bos);
        gzip.write(data);
        gzip.close();
        return bos.toByteArray();
    }


    private String getShellScript(int width, String compressedColorList, String compressedPairList) {
        return SHELL_SCRIPT_TEMPLATE + "decode_and_print " + width + " " + compressedColorList + " " + compressedPairList;

    }

    @Override
    public String getExtension() {
        return "sh";
    }

    @Override
    public MediaType getMediaType() {
        return MediaType.TEXT_PLAIN;
    }

    @Override
    String getBlacklistCharacters(){
        return ",`'\"$";
    }
}
