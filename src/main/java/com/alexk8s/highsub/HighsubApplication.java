package com.alexk8s.highsub;

import com.alexk8s.highsub.configuration.HighsubConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties(HighsubConfig.class)
public class HighsubApplication {

    public static void main(String[] args) {
        SpringApplication.run(HighsubApplication.class, args);
    }

}
