package com.alexk8s.highsub.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.Frozen;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import java.util.List;

@Data
@Table("vector_cache")
@AllArgsConstructor
@NoArgsConstructor
public class CachedVector {
    @PrimaryKey
    private String version;
    private String input;
    private List<Double> vector;
    @Column("cache_time_utc")
    @Frozen
    private long cacheTimeUtc;
}
