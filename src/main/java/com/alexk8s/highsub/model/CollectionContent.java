package com.alexk8s.highsub.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.Frozen;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import java.util.List;
import java.util.UUID;

@Data
@Table("collection_content")
@NoArgsConstructor
@AllArgsConstructor
public class CollectionContent {
    private UUID id;
    @Frozen
    @PrimaryKey
    private List<String> subtitles;
    @Column("created_utc")
    private Long createdUtc;

}
