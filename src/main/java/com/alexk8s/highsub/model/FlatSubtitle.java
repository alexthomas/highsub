package com.alexk8s.highsub.model;

import com.alexk8s.highsub.kafka.model.SubtitleType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.annotation.Transient;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.List;

@Data
@Table("flat_subtitle")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FlatSubtitle implements Comparable<FlatSubtitle> {
    @PrimaryKeyColumn(type = PrimaryKeyType.PARTITIONED, ordinal = 1)
    private String show;
    @PrimaryKeyColumn(type = PrimaryKeyType.PARTITIONED, ordinal = 2)
    private String season;
    @PrimaryKeyColumn(type = PrimaryKeyType.PARTITIONED, ordinal = 3)
    private String episode;
    @PrimaryKeyColumn(name = "subtitle_number", type = PrimaryKeyType.CLUSTERED, ordinal = 40)
    private Integer subtitleNumber;

    @Column("start_time")
    private Long startTime;
    @Column("end_time")
    private Long endTime;
    private String style;
    private String name;
    private String text;
    @Column("created_utc")
    private Long createdUtc;
    @Transient
    private String domain;
    private String kanji;
    private String romaji;
    @Column("japanese_start_time")
    private Long japaneseStartTime;
    @Column("japanese_end_time")
    private Long japaneseEndTime;
    @Column("japanese_subtitle_number")
    private Integer japaneseSubtitleNumber;
    @Column("match_rate")
    private String matchRate;
    private List<String> tags;
    @Column("parent_tags")
    private List<String> parentTags;
    private List<String> context;
    @Column("raw_subtitle")
    private List<String> rawSubtitle;
    @Transient
    @JsonIgnore
    private boolean encodeId;
    @Transient
    @JsonIgnore
    @Builder.Default
    private String apiVersion = "v1";
    private Chapter chapter;
    @Column("should_index")
    private Boolean shouldIndex;
    private SubtitleType type;
    @Column("episode_width")
    private Integer episodeWidth;
    @Column("episode_height")
    private Integer episodeHeight;
    @Column("episode_slug")
    private String episodeSlug;

    @Transient
    public String getId() {
        String id = String.join("-", show, season, episode, String.valueOf(subtitleNumber));
        if (encodeId)//url encode the id
            return encode(id);
        return id;
    }

    @Transient
    public String getVideoUrl() {
        return domain + "/v1/content/subtitle/" + getId() + ".mp4";
    }

    @Transient
    public String getGifUrl() {
        return domain + "/v1/content/subtitle/" + getId() + ".gif";
    }

    @Transient
    public String getPngUrl() {
        return domain + "/v1/content/subtitle/" + getId() + ".png";
    }

    @Transient
    public String getWebpUrl() {
        return domain + "/v1/content/subtitle/" + getId() + ".webp";
    }

    @Transient
    public String getTxtUrl() {
        return domain + "/v1/content/subtitle/" + getId() + ".txt";
    }

    @Transient
    public String getHtmlUrl() {
        return domain + "/v1/content/subtitle/" + getId() + ".html";
    }

    @Transient
    public String getShellUrl() {
        return domain + "/v1/content/subtitle/" + getId() + ".sh";
    }


    @Transient
    public String getNextSubtitleUrl() {
        return domain + "/" + apiVersion + "/subtitle/" + getId() + "/next";
    }

    @Transient
    public String getPreviousSubtitleUrl() {
        return domain + "/" + apiVersion + "/subtitle/" + getId() + "/previous";
    }

    @Transient
    public String getMoreFromEpisodeUrl() {
        return domain + "/" + apiVersion + "/subtitle/" + getId() + "/more-from/episode";
    }

    @Transient
    public String getEpisodeUrl() {
        String episodeId = String.join("-", show, season, episode);
        if (encodeId)
            episodeId = encode(episodeId);
        return domain + "/v1/episode/" + episodeId;
    }

    private String encode(String id) {
        return URLEncoder.encode(id, Charset.defaultCharset()).replaceAll("\\+", "%20");
    }

    private String formatNumber(String inputNumber) {
        String number = inputNumber.toLowerCase().startsWith("season") ? inputNumber.substring(6).trim() : inputNumber;
        return "0".repeat(Math.max(0, 5 - number.length())) + number;
    }

    @Override
    public int compareTo(@NotNull FlatSubtitle o) {
        int comparison = show.compareTo(o.show);
        if(comparison!=0)
            return comparison;
        comparison = formatNumber(season).compareTo(formatNumber(o.season));
        if(comparison!=0)
            return comparison;
        comparison = formatNumber(episode).compareTo(formatNumber(o.episode));
        if(comparison!=0)
            return comparison;
        return subtitleNumber.compareTo(o.subtitleNumber);
    }
}
