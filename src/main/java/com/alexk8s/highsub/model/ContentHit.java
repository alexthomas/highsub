package com.alexk8s.highsub.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Table("content_hit")
public class ContentHit {
    @PrimaryKeyColumn(type = PrimaryKeyType.PARTITIONED)
    private String show;
    @PrimaryKeyColumn(type = PrimaryKeyType.PARTITIONED)
    private String season;
    @PrimaryKeyColumn(type = PrimaryKeyType.PARTITIONED)
    private String episode;
    @PrimaryKeyColumn(type = PrimaryKeyType.PARTITIONED)
    private String subtitle;
    @PrimaryKeyColumn(type = PrimaryKeyType.CLUSTERED)
    private String type;
    @PrimaryKeyColumn(name = "hit_utc",type = PrimaryKeyType.CLUSTERED)
    private Long hitUtc;
    @Column("content_height")
    private Integer contentHeight;
    @Column("content_start_time")
    private Long contentStartTime;
    @Column("content_end_time")
    private Long contentEndTime;
    @Column("hit_type")
    private String hitType;
    private Boolean focused;
    private String user;
    @Column("user_agent")
    private String userAgent;
    private String ip;
    @Column("content_key")
    private String contentKey;
    @Column("content_path")
    private String contentPath;
    private String referer;
    @Column("generation_commands")
    private List<String> generationCommands;
    @Column("generation_time")
    private Long generationTime;
    @Column("trace_id")
    private String traceId;
    private Long size;

}
