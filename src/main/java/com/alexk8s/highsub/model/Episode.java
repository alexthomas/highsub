package com.alexk8s.highsub.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.With;
import org.springframework.data.annotation.Transient;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Table("episode")
@AllArgsConstructor
@NoArgsConstructor
@Data
@With
@Builder
public class Episode {

    @PrimaryKeyColumn(type = PrimaryKeyType.PARTITIONED, ordinal = 1)
    private String show;
    @PrimaryKeyColumn(type = PrimaryKeyType.CLUSTERED, ordinal = 2)
    private String season;
    @PrimaryKeyColumn(type = PrimaryKeyType.CLUSTERED, ordinal = 3)
    private String episode;

    @Column("english_subtitles")
    private String englishSubtitles;
    @Column("japanese_subtitles")
    private String japaneseSubtitles;
    @Column("video_file")
    private String videoFile;
    @Column("subtitle_count")
    private Long subtitleCount;
    private String summary;
    private String studio;
    @Column("opening_start")
    private Long openingStart;
    @Column("opening_end")
    private Long openingEnd;
    @Column("ending_start")
    private Long endingStart;
    @Column("ending_end")
    private Long endingEnd;

    @Column("prologue_start")
    private Long prologueStart;
    @Column("prologue_end")
    private Long prologueEnd;
    @Column("preview_start")
    private Long previewStart;
    @Column("preview_end")
    private Long previewEnd;
    @Column("originally_available_at")
    private String originallyAvailableAt;
    @Column(value = "tmdb_episode_id")
    private Integer tmdbEpisodeId;
    @Column(value = "tmdb_season")
    private Integer tmdbSeason;
    @Column(value = "tmdb_episode")
    private Integer tmdbEpisode;
    private String title;
    @Column("source_video")
    private String sourceVideo;
    @Column("source_video_bytes")
    private Long sourceVideoBytes;


    @Column("scaled_480p_video")
    private String scaled480pVideo;
    @Column("scaled_150p_video")
    private String scaled150pVideo;
    @Column("scaled_480p_video_bytes")
    private Long scaled480pVideoBytes;
    @Column("scaled_150p_video_bytes")
    private Long scaled150pVideoBytes;

    private List<String> tags;
    @Column("parent_tags")
    private List<String> parentTags;
    private Map<String, String> hashes;
    private Integer width;
    private Integer height;
    private Long duration;
    @Column("season_name")
    private String seasonName;
    private String slug;
    @Column("preview_subtitle_id")
    private String previewSubtitleId;
    @Column("indexed_subtitle_count")
    private Long indexedSubtitleCount;


    @JsonIgnore
    @Transient
    private String domain;

    @Transient
    public String getPosterUrl() {
        return domain + "/v1/content/poster/" + URLEncoder.encode(show, Charset.defaultCharset()).replaceAll("\\+","%20") + ".png";
    }

    @Transient
    public String getSourceHash() {
        if (hashes == null)
            return null;
        return hashes.get("source");
    }

    public void setSourceHash(String sourceHash) {
        if (hashes == null)
            hashes = new HashMap<>();
        if (sourceHash != null)
            hashes.put("source", sourceHash);
    }

    @Transient
    public String getScaled480pHash() {
        if (hashes == null)
            return null;
        return hashes.get("scaled_480p");
    }

    public void setScaled480pHash(String scaled480pHash) {
        if (hashes == null)
            hashes = new HashMap<>();
        if (scaled480pHash != null)
            hashes.put("scaled_480p", scaled480pHash);
    }

    @Transient
    public String getScaled150pHash() {
        if (hashes == null)
            return null;
        return hashes.get("scaled_150p");
    }

    public void setScaled150pHash(String scaled150pHash) {
        if (hashes == null)
            hashes = new HashMap<>();
        if (scaled150pHash != null)
            hashes.put("scaled_150p", scaled150pHash);
    }

}
