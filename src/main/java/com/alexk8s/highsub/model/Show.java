package com.alexk8s.highsub.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.With;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import java.util.List;
import java.util.Map;

@Data
@Table("show")
@With
@AllArgsConstructor
@NoArgsConstructor
public class Show {
    @PrimaryKey
    private String show;
    @Column("is_anime")
    private Boolean anime;
    @Column("kitsu_anime_id")
    private String kitsuAnimeId;
    @Column("show_year")
    private Integer year;
    @Column("tmdb_episode_group")
    private String tmdbEpisodeGroup;
    @Column("tmdb_show_id")
    private Integer tmdbShowId;
    @Column("plex_key")
    private String plexKey;
    private String summary;
    private List<String> tags;
    @Column("season_name_map")
    private Map<String,String> seasonNameMap;

}
