package com.alexk8s.highsub.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Getter
@AllArgsConstructor
public enum VideoFormat {
    MKV("mkv"),
    MP4("mp4");

    private final String extension;

    public static List<String> getExtensions() {
        return Arrays.stream(values()).map(VideoFormat::getExtension).toList();
    }

    public static VideoFormat getFormat(final String path) {
        if (path == null) {
            throw new IllegalArgumentException("Path cannot be null");
        }

        for (VideoFormat value : VideoFormat.values()) {
            if (path.toLowerCase(Locale.ROOT).endsWith(value.extension)) {
                return value;
            }
        }

        String supported = Arrays.stream(VideoFormat.values())
                .map(VideoFormat::getExtension)
                .collect(Collectors.joining(", "));
        throw new IllegalStateException("Video format could not be determined for file " + path + ". Supported file types are " + supported);
    }

}
