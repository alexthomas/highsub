package com.alexk8s.highsub.model;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Data;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

@Table("plex_openings")
@Data
public class PlexOpening {
    @PrimaryKeyColumn(type = PrimaryKeyType.PARTITIONED)
    private String show;
    @PrimaryKeyColumn(type = PrimaryKeyType.PARTITIONED)
    private String season;
    @PrimaryKeyColumn(type = PrimaryKeyType.PARTITIONED)
    private String episode;
    @PrimaryKeyColumn(type = PrimaryKeyType.CLUSTERED,name = "marker_type")
    @JsonAlias("marker_type")
    private String markerType;
    @PrimaryKeyColumn(type = PrimaryKeyType.CLUSTERED)
    private Integer id;
    private Long end;
    private Long start;
    @Column("extra_data")
    @JsonAlias("extra_data")
    private String extraData;
}
