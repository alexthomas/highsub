package com.alexk8s.highsub.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.Table;

@Table("saved_event")
@Data
public class SavedEvent {
    @Id
    private String domain;
    private String id;
    @Column("event_utc")
    private long eventUtc;
    @Column("event_json")
    private String eventJson;
}
