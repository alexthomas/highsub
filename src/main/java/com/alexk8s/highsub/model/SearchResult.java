package com.alexk8s.highsub.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.Table;

import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Table("search_result")
@Data
public class SearchResult extends FlatSubtitle{
    @Column("search_id")
    private UUID searchId;
    private int rank;

}
