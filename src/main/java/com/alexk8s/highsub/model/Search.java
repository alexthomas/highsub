package com.alexk8s.highsub.model;

import lombok.Data;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import java.util.List;
import java.util.UUID;

@Table("search")
@Data
public class Search {
    @PrimaryKey
    private UUID id;
    private String query;
    private int page;
    private int size;
    @Column("safe_search")
    private String safeSearch;
    private String type;
    @Column("search_criteria")
    private String searchCriteria;
    @Column("user_agent")
    private String userAgent;
    private String ip;
    private String user;
    private String context;
    private String show;
    @Column("blacklist_shows")
    private List<String> blacklistShows;
    private Long latency;
    private Long timestamp;

}
