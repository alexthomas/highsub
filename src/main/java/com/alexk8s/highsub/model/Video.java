package com.alexk8s.highsub.model;

import lombok.Data;

import java.util.Set;

@Data
public class Video {
    private String path;
    private Long duration;
    private Long size;
    private VideoFormat format;
    private Set<SubtitleFile> subtitleFiles;
    private String show;
    private String movie;
    private Integer season;
    private Integer episode;
    private boolean analyzed;

    public String getFilename() {
        return path.substring(path.lastIndexOf('/') + 1);
    }

}
