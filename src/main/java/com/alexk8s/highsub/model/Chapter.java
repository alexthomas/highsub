package com.alexk8s.highsub.model;

public enum Chapter {
    PROLOGUE,OPENING,ENDING,PREVIEW,OTHER
}
