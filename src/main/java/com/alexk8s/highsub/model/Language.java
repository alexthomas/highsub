package com.alexk8s.highsub.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

@AllArgsConstructor
@Getter
public enum Language {
    ENGLISH("English", List.of("en", "eng")),
    JAPANESE("Japanese", List.of("ja", "jpn")),
    FRENCH("French", List.of("fr", "fre")),
    RUSSIAN("Russian", List.of("ru", "rus")),
    ARABIC("Arabic", List.of("ara")),
    GERMAN("German", List.of("de", "ger")),
    SPANISH("Spanish", List.of("es", "spa")),
    PORTUGUESE("Portuguese", List.of("pt", "por")),
    ITALIAN("Italian", List.of("it", "ita")),
    PERSIAN("Persian", List.of("fa", "per")),
    UNKNOWN("UNKNOWN", List.of("unknown"));

    private final String name;
    private final List<String> codes;

    public static Language findByCode(final String code) {
        if (code == null) return UNKNOWN;
        return Arrays.stream(values())
                .filter(l -> l.getCodes().contains(code.toLowerCase(Locale.ROOT)))
                .findFirst()
                .orElse(UNKNOWN);
    }

    public String getSubtitleCode() {
        return codes.get(0);
    }

}
