package com.alexk8s.highsub.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.Set;

@Data
@Builder
@AllArgsConstructor
public class SubtitleFile {
    private String name;
    private Language language;
    private SubtitleFormat format;
    private Video video;
    private String path;
    private Long size;
    private String hash;
    private Integer embeddedIndex;
    private Set<Subtitle> subtitles;

    public boolean isEmbedded() {
        return embeddedIndex != null;
    }

}
