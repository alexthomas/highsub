package com.alexk8s.highsub.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.function.Predicate;

@AllArgsConstructor
@Getter
public enum SubtitleFormat {
    SRT("srt", "subrip"),
    ASS("ass", "ass"),
    VTT(".vtt","vtt"),
    UNKNOWN(null, null);

    private final String extension;
    private final String codec;

    public static SubtitleFormat getFormat(final String path) {
        if (path == null) {
            throw new IllegalArgumentException("Path cannot be null");
        }
        return findBy(f -> path.endsWith(f.extension));
    }

    public static SubtitleFormat findByCodec(final String codec) {
        if (codec == null) {
            throw new IllegalArgumentException("Codec cannot be null");
        }
        return findBy(f -> codec.equalsIgnoreCase(f.codec));
    }

    private static SubtitleFormat findBy(Predicate<SubtitleFormat> predicate) {
        for (SubtitleFormat format : SubtitleFormat.values()) {
            if (predicate.test(format)) {
                return format;
            }
        }
        return UNKNOWN;
    }
}
