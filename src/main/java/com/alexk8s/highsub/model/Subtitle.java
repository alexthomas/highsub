package com.alexk8s.highsub.model;

import com.alexk8s.highsub.kafka.model.SubtitleType;
import com.alexk8s.highsub.subtitle.reader.ReadSubtitle;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Subtitle {
    private Long startTime;
    private Long endTime;
    private String text;
    private Integer number;
    private String style;
    private String name;
    private List<String> context;
    private List<String> rawSubtitle;
    private Chapter chapter;
    private SubtitleType type;


    public static Subtitle fromReadSubtitle(ReadSubtitle readSubtitle) {
        return new Subtitle(
                parseOffsetToMs(readSubtitle.start()),
                parseOffsetToMs(readSubtitle.end()),
                Normalizer.normalize(readSubtitle.text(), Normalizer.Form.NFKC),
                readSubtitle.number(),
                Objects.requireNonNullElse(readSubtitle.style(),"Dialog").toUpperCase(),
                readSubtitle.name(),
                new ArrayList<>(),
                readSubtitle.rawSubtitle(),
                Chapter.OTHER,
                readSubtitle.type()
        );
    }

    private static long parseOffsetToMs(String offset) {
        Pattern timePattern = Pattern.compile("(?<hours>\\d{1,2}):(?<minutes>\\d{2}):(?<seconds>\\d{1,2})[.,](?<fractionSeconds>\\d{1,3})");
        Matcher matcher = timePattern.matcher(offset);
        if(!matcher.find())
            throw new RuntimeException("Error parsing offset: "+offset);

        int hours = Integer.parseInt(matcher.group("hours"));
        int minutes = Integer.parseInt(matcher.group("minutes"));
        int seconds = Integer.parseInt(matcher.group("seconds"));
        String fractionSeconds = matcher.group("fractionSeconds");
        int fractionMultiplier = (int) (1000/Math.pow(10,fractionSeconds.length()));
        int milliseconds = Integer.parseInt(fractionSeconds) * fractionMultiplier;
        return milliseconds +
                seconds * 1000L +
                minutes * 60_000L +
                hours * 60_000L * 60;
    }
}
