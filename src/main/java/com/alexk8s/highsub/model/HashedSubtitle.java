package com.alexk8s.highsub.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.Table;

@Data
@Table("hashed_subtitles")
@AllArgsConstructor
public class HashedSubtitle {
    @Id
    private String show;
    private String hash;
    private String season;
    private String episode;
    @Column("subtitle_number")
    private int subtitleNumber;

}
