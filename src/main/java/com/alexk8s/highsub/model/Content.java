package com.alexk8s.highsub.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Table("content")
public class Content {
    @PrimaryKey("content_path")
    private String contentPath;
    private String show;
    private String season;
    private String episode;
    @Column("subtitle_number")
    private String subtitleNumber;
    @Column("content_type")
    private String contentType;
    @Column("content_height")
    private Integer contentHeight;
    @Column("content_start_time")
    private Long contentStartTime;
    @Column("content_end_time")
    private Long contentEndTime;
    @Column("content_key")
    private String contentKey;
    @Column("generated_utc")
    private Long generatedUtc;
    @Column("last_hit_utc")
    private Long lastHitUtc;
    private Long size;
    @Column("generation_commands")
    private List<String> generationCommands;
    @Column("generation_time")
    private Long generationTime;
    private String hash;
    @Column("trace_id")
    private String generatedTrace;

}
