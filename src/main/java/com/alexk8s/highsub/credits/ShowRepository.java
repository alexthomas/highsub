package com.alexk8s.highsub.credits;

import org.springframework.data.neo4j.repository.ReactiveNeo4jRepository;
import org.springframework.stereotype.Repository;

@Repository("Neo4jShowRepository")
public interface ShowRepository extends ReactiveNeo4jRepository<Neo4jShow,String> {
}
