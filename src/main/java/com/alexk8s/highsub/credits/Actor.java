package com.alexk8s.highsub.credits;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.Version;
import org.springframework.data.neo4j.core.schema.Id;
import org.springframework.data.neo4j.core.schema.Node;

@Node("ACTOR")
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Data
public class Actor {
    @Id
    private String name;
    @Version
    private Long version;
}
