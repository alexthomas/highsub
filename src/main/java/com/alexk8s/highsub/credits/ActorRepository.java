package com.alexk8s.highsub.credits;

import org.springframework.data.neo4j.repository.ReactiveNeo4jRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ActorRepository extends ReactiveNeo4jRepository<Actor,String> {

}
