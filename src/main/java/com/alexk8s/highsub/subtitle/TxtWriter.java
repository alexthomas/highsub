package com.alexk8s.highsub.subtitle;

import com.alexk8s.highsub.model.FlatSubtitle;
import reactor.core.publisher.Flux;

public class TxtWriter {

    private static final String[] BLACKLIST_STYLE_WORDS = {"title", "stuff", "next_time", "preview", "disclaimer", "lyrics", "next ep", "logo", "next time", "made-up language"};
    private static final String[] WHITELIST_STYLE_WORDS = {"default", "subtitle", "dialog", "main"};

    private final Flux<FlatSubtitle> subtitleFlux;


    public TxtWriter(Flux<FlatSubtitle> subtitleFlux) {
        this.subtitleFlux = subtitleFlux;
    }

    public Flux<String> getText(){
        return subtitleFlux
                .filter(this::isValidSubtitle)
                .map(this::formatSubtitle);
    }

    private boolean isValidSubtitle(FlatSubtitle flatSubtitle){
        if(!Boolean.TRUE.equals(flatSubtitle.getShouldIndex()))
            return false;
        String style = flatSubtitle.getStyle().toLowerCase();
        for (String whitelistStyleWord : WHITELIST_STYLE_WORDS) {
            if(style.contains(whitelistStyleWord))
                return true;
        }
        for (String blacklistStyleWord : BLACKLIST_STYLE_WORDS) {
            if(style.contains(blacklistStyleWord))
                return false;
        }
        return true;
    }

    private String formatSubtitle(FlatSubtitle subtitle){
        StringBuilder builder = new StringBuilder();
        if (subtitle.getName() != null && !subtitle.getName().equalsIgnoreCase("ntp")) {
            builder.append(subtitle.getName());
        }
        if (subtitle.getStyle() != null && subtitle.getStyle().toLowerCase().contains("flashback"))
            builder.append(" (Flashback)");
        if (!builder.isEmpty())
            builder.append(": ");
        builder.append(subtitle.getText());
        return builder.toString();
    }
}
