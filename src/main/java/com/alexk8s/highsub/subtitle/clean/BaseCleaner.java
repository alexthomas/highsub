package com.alexk8s.highsub.subtitle.clean;

abstract class BaseCleaner implements SubtitleCleaner{
    public String clean(String text) {
        return text
                .replace("\n", " ")
                .replace("  ", " ")
                .replaceAll("\\p{C}", "")
                .replaceAll("\\(.*[^\\w\\s].*\\)", "")
                .replaceAll("\\{\\\\[aA]n\\s?8}","");

    }
}
