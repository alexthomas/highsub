package com.alexk8s.highsub.subtitle.clean;

public interface SubtitleCleaner {
        String clean(String text);
}
