package com.alexk8s.highsub.subtitle.clean;

import org.jsoup.Jsoup;
import org.jsoup.safety.Safelist;

public class SrtCleaner extends BaseCleaner{
    @Override
    public String clean(String text) {
        String cleaned = super.clean(text).trim();
        return Jsoup.clean(cleaned, Safelist.none());
    }

}
