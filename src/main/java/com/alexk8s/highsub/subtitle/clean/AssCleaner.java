package com.alexk8s.highsub.subtitle.clean;

import java.util.stream.IntStream;

public class AssCleaner extends BaseCleaner{
    @Override
    public String clean(String text) {
        String clean = super.clean(text)
                .replace("\\N", " ")
                .replaceAll("\\{.*?}", "")
                .trim();
        // if the text is longer than 200 something sketchy is going on.
        // it's likely a "map" line.
        if (clean.length() > 200) return "";
        if (IntStream.range(0, 10).anyMatch(i -> clean.startsWith("m " + i))) return "";
        return clean;
    }

}
