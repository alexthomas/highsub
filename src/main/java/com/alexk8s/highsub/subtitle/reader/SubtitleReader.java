package com.alexk8s.highsub.subtitle.reader;

import datadog.trace.api.Trace;

import java.util.List;

public interface SubtitleReader {
    @Trace
    List<ReadSubtitle> readSubtitles();
}
