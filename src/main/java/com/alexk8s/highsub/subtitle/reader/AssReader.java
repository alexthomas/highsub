package com.alexk8s.highsub.subtitle.reader;

import com.alexk8s.highsub.kafka.model.SubtitleType;
import com.alexk8s.highsub.subtitle.clean.AssCleaner;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;

@Slf4j
public class AssReader implements SubtitleReader {

    private static final Pattern OPENING_ENDING_STYLE = Pattern.compile("((?<!(styl|m|t))(op|(ed(?!en))|ending|opening|song subtitles))", Pattern.CASE_INSENSITIVE);
    private static final List<String> OPENING_ENDING_TITLES = List.of("munou", "kisetsu");
    private final Iterator<String> lineIterator;
    private final AssCleaner assCleaner = new AssCleaner();
    private final AtomicInteger subtitleCounter = new AtomicInteger(1);
    private final boolean excludeKanji;

    @SneakyThrows
    public AssReader(String filePath, boolean excludeKanji) {
        lineIterator = Files.lines(Path.of(filePath)).iterator();
        this.excludeKanji = excludeKanji;
    }

    @SneakyThrows
    public List<ReadSubtitle> readSubtitles() {
        while (lineIterator.hasNext() && !"[Events]".equals(lineIterator.next())) {//skip to the events
        }
        if (!lineIterator.hasNext()) {
            log.warn("File did not have an events line");
            return Collections.emptyList();
        }
        Map<String, Integer> formatKeys = parseFormatKeys();
        if (formatKeys == null) {
            log.warn("file did not have any format keys");
            return Collections.emptyList();
        }
        List<ReadSubtitle> subtitles = new ArrayList<>();
        while (lineIterator.hasNext()) {
            String line = lineIterator.next();
            if (line.startsWith("Dialogue: ")) {
                ReadSubtitle readSubtitle = parseSubtitle(line.substring(9), formatKeys, line);
                subtitles.add(readSubtitle);
            }
        }
        return subtitles;
    }


    private SubtitleType resolveSubtitleType(String style) {
        if (style == null)
            return SubtitleType.DIALOGUE;
        String lowerStyle = style.toLowerCase();
        if (lowerStyle.contains("sign"))
            return SubtitleType.SIGN;
        if (lowerStyle.equals("episode title"))
            return SubtitleType.TITLE;
        if ("vietsub".equals(lowerStyle))
            return SubtitleType.OTHER;
        if (lowerStyle.contains("kanji"))
            return SubtitleType.KANJI;
        if (lowerStyle.contains("romaji"))
            return SubtitleType.ROMAJI;
        if (OPENING_ENDING_TITLES.stream().anyMatch(lowerStyle::contains))
            return SubtitleType.LYRICS;
        if (OPENING_ENDING_STYLE.matcher(style).find()) // try to exclude opening end ending subtitles
            return SubtitleType.LYRICS;
        return SubtitleType.DIALOGUE;
    }

    private Map<String, Integer> parseFormatKeys() {
        while (lineIterator.hasNext()) {
            String line = lineIterator.next();
            if (line.startsWith("Format: ")) {
                String deliminator = line.contains(", ") ? ", " : ",";
                String[] keys = line.substring(8).split(deliminator);
                Map<String, Integer> formatMap = new HashMap<>();
                for (int i = 0; i < keys.length; i++) {
                    formatMap.put(keys[i], i);
                }
                return formatMap;
            }
        }
        return null;
    }

    private ReadSubtitle parseSubtitle(String line, Map<String, Integer> formatKeys, String rawLine) {
        String[] values = new String[formatKeys.size()];
        int lastIndex = 0;
        for (int i = 0; i < values.length; i++) {
            int nextIndex = line.indexOf(',', lastIndex);
            String value;
            if (nextIndex == -1 || i + 1 == values.length)
                value = line.substring(lastIndex);
            else
                value = line.substring(lastIndex, nextIndex);
            values[i] = value;
            lastIndex = nextIndex + 1;
        }
        String style = resolveValue("Style", formatKeys, values);
        SubtitleType type = resolveSubtitleType(style);
        return new ReadSubtitle(
                resolveValue("Layer", formatKeys, values),
                resolveValue("Start", formatKeys, values),
                resolveValue("End", formatKeys, values),
                style,
                resolveValue("Name", formatKeys, values),
                resolveValue("MarginL", formatKeys, values),
                resolveValue("MarginR", formatKeys, values),
                resolveValue("MarginV", formatKeys, values),
                resolveValue("Effect", formatKeys, values),
                assCleaner.clean(resolveValue("Text", formatKeys, values)),
                subtitleCounter.getAndIncrement(),
                Collections.singletonList(rawLine),
                type
        );
    }

    private String resolveValue(String key, Map<String, Integer> fieldMap, String[] values) {
        if (fieldMap.containsKey(key))
            return values[fieldMap.get(key)];
        return null;
    }
}
