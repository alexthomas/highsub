package com.alexk8s.highsub.subtitle.reader;

import com.alexk8s.highsub.kafka.model.SubtitleType;
import com.alexk8s.highsub.subtitle.clean.SrtCleaner;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

@Slf4j
public class VttReader implements SubtitleReader {
    private static final Pattern TIME_PATTERN = Pattern.compile("^([0-9]{2}:){2}[0-9]{2}.[0-9]{3}.*");

    private final Iterator<String> lineIterator;
    private final SrtCleaner srtCleaner = new SrtCleaner();

    @SneakyThrows
    public VttReader(String filePath) {
        lineIterator = Files.lines(Path.of(filePath)).iterator();
    }


    public List<ReadSubtitle> readSubtitles() {
        List<ReadSubtitle> subtitles = new ArrayList<>();
        int counter = 0;
        while (lineIterator.hasNext())
            readNextSubtitle(counter++).ifPresent(subtitles::add);
        return subtitles;
    }


    private Optional<ReadSubtitle> readNextSubtitle(int number) {

        String[] timesStringParts = null;
        List<String> rawLines = new ArrayList<>();
        while (lineIterator.hasNext()) {
            String line = lineIterator.next();
            rawLines.add(line);
            if (TIME_PATTERN.matcher(line).matches()) {
                timesStringParts = line.split(" --> ");
                break;
            }
        }
        if (timesStringParts == null || timesStringParts.length < 2)
            return Optional.empty();
        List<String> textLines = new ArrayList<>(2);
        String line;
        while (lineIterator.hasNext() && StringUtils.hasText(line = lineIterator.next())) {
            rawLines.add(line);
            textLines.add(line);
        }
        if (textLines.isEmpty()) {
            if (lineIterator.hasNext())
                rawLines.add(lineIterator.next());//subtitle was blank, iterator needs to be fed so the next subtitle will be on the correct line
            else
                return Optional.empty();
        }
        String text = srtCleaner.clean(String.join("\n", textLines));
        return Optional.of(new ReadSubtitle(null, timesStringParts[0], timesStringParts[1], null, null, null, null, null, null,
                text, number, rawLines, SubtitleType.DIALOGUE));

    }

}
