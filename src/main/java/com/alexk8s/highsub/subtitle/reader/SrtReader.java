package com.alexk8s.highsub.subtitle.reader;

import com.alexk8s.highsub.kafka.model.SubtitleType;
import com.alexk8s.highsub.subtitle.clean.SrtCleaner;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

@Slf4j
public class SrtReader implements SubtitleReader {
    private final Iterator<String> lineIterator;
    private final SrtCleaner srtCleaner = new SrtCleaner();
    private final String filePath;

    @SneakyThrows
    public SrtReader(String filePath) {
        lineIterator = Files.lines(Path.of(filePath)).iterator();
        this.filePath = filePath;
    }


    public List<ReadSubtitle> readSubtitles() {
        List<ReadSubtitle> subtitles = new ArrayList<>();
        while (lineIterator.hasNext())
            readNextSubtitle().ifPresent(subtitles::add);
        return subtitles;
    }

    private Optional<ReadSubtitle> readNextSubtitle() {
        int number;
        List<String> rawLines = new ArrayList<>();
        String lineNumber = lineIterator.next();
        try {
            number = Integer.parseInt(lineNumber.replaceAll("\uFEFF",""));
        } catch (NumberFormatException e) {
            log.warn("Error parsing file {}", filePath, e);
            return Optional.empty();
        }
        rawLines.add(lineNumber);


        String timingData = lineIterator.next();
        rawLines.add(timingData);
        String[] timesStringParts = timingData.split(" --> ");
        if(timesStringParts.length<2)
            return Optional.empty();
        List<String> textLines = new ArrayList<>(2);
        String line;
        while (lineIterator.hasNext() && StringUtils.hasText(line = lineIterator.next())) {
            rawLines.add(line);
            textLines.add(line);
        }
        if (textLines.isEmpty()) {
            if (lineIterator.hasNext())
                rawLines.add(lineIterator.next());//subtitle was blank, iterator needs to be fed so the next subtitle will be on the correct line
            else
                return Optional.empty();
        }
        String text = srtCleaner.clean(String.join("\n", textLines));
        return Optional.of(new ReadSubtitle(null, timesStringParts[0], timesStringParts[1], null, null, null, null, null, null,
                text, number,rawLines, SubtitleType.DIALOGUE));

    }

}
