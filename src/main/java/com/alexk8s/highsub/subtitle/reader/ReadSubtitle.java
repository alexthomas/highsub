package com.alexk8s.highsub.subtitle.reader;


import com.alexk8s.highsub.kafka.model.SubtitleType;

import java.util.List;

public record ReadSubtitle(String layer, String start, String end, String style, String name,
                           String marginL, String marginR, String marginV, String effect, String text, int number,
                           List<String> rawSubtitle, SubtitleType type) {
}
