package com.alexk8s.highsub.kitsu;

import java.util.Map;

public record KitsuImage(String tiny,String large,String small,String medium,String original, KitsuImageMeta meta) {

    public record KitsuImageMeta(Map<String, KitsuImageDimensions> dimensions){}
    public record KitsuImageDimensions(int width, int height){}
}
