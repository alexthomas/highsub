package com.alexk8s.highsub.kitsu;

import com.alexk8s.highsub.model.Show;
import com.alexk8s.highsub.scrape.ActorRole;
import com.alexk8s.highsub.scrape.ActorRoleRepository;
import com.alexk8s.highsub.service.ActorService;
import com.alexk8s.highsub.service.S3Service;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.data.cassandra.core.ReactiveCassandraTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.DefaultUriBuilderFactory;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.alexk8s.highsub.util.PathUtils.getTempFile;
import static com.alexk8s.highsub.util.ThumbnailUtility.getActorKey;
import static com.alexk8s.highsub.util.ThumbnailUtility.getCharacterKey;

@RequiredArgsConstructor
@Service
@Slf4j
public class KitsuService {
    private final WebClient webClient = WebClient.builder().codecs(c -> c.defaultCodecs().maxInMemorySize(1024 * 1024 * 10)).build();
    private final ReactiveCassandraTemplate cassandraTemplate;
    private final S3Service s3Service;
    private final ActorRoleRepository actorRoleRepository;
    private final ActorService actorService;




    public Mono<Void> updateShow(Show show) {

        return withKitsuAnimeId(show)
                .flatMapMany(this::getActorRoles)
                .flatMap(this::downloadAndSaveImages, 10)
                .flatMap(actorRoleRepository::save)
                .then(actorRoleRepository.findByShow(show.getShow()).collectList())
                .flatMap(l -> actorService.saveActorRoles(show.getShow(), l))
                ;
    }

    public Mono<Show> withKitsuAnimeId(Show show) {
        if (show.getKitsuAnimeId() != null)
            return Mono.just(show);
        return getAnime(show.getShow())
                .doOnNext(kitsuAnime -> {
                    log.info("For {} found kitsu  {}", show.getShow(), kitsuAnime);
                })
                .map(k -> show.withKitsuAnimeId(k.getId()))
                .flatMap(cassandraTemplate::insert);
    }


    private Mono<ActorRole> downloadAndSaveImages(ActorRole actorRole) {
        String actorKey = getActorKey(actorRole.getActor());
        String characterKey = getCharacterKey(actorRole.getShow(), actorRole.getCharacter());
        List<Mono<Void>> imageTasks = new ArrayList<>();
        if (actorRole.getActorImage() != null) {
            imageTasks.add(downloadAndSaveImage(actorRole.getActorImage(), actorKey));
        }

        if (actorRole.getCharacterImage() != null) {
            imageTasks.add(downloadAndSaveImage(actorRole.getCharacterImage(), characterKey));
        }
        return Flux.concat(imageTasks)
                .then(Mono.just(actorRole))
                .doOnNext(a -> {
                    a.setActorImage(actorKey);
                    a.setCharacterImage(characterKey);
                });
    }

    private Mono<Void> downloadAndSaveImage(String url, String key) {
        String tempFile = getTempFile("jpg");
        Flux<DataBuffer> dataBuffer = webClient.get().uri(url)
                .exchangeToFlux(r->{
                    if(r.statusCode().is2xxSuccessful())
                        return r.bodyToFlux(DataBuffer.class);
                    return Flux.empty();
                });
        return DataBufferUtils.write(dataBuffer, Path.of(tempFile), StandardOpenOption.CREATE)
                .share()
                .then(Mono.defer(() -> s3Service.saveScrapedImage(tempFile, key)))
                .then();
    }

    public Flux<ActorRole> getActorRoles(Show show) {
        return getMediaCharacters(show.getKitsuAnimeId()).checkpoint("Get Anime characters for " + show.getShow())
                .flatMap(cm -> Flux.combineLatest(getActors(cm), getCharacter(cm), createActorRole(show.getShow())), 10);
    }

    private BiFunction<KitsuPerson, KitsuCharacter, ActorRole> createActorRole(String show) {
        return (p, c) -> {
            String actor = p.getAttributes().name();
            String character = c.getAttributes().name();
            String actorImage = p.getAttributes().image() != null ? p.getAttributes().image().original() : null;
            String characterImage = c.getAttributes().image() != null ? c.getAttributes().image().original() : null;
            return new ActorRole(show, actor, character, actorImage, characterImage, c.getId(), p.getId(), c.getRole(),"kitsu");
        };
    }

    public Mono<KitsuAnime> getAnime(String title) {
        return webClient.get().uri("https://kitsu.io/api/edge/anime?filter[text]=" + title)
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<KitsuResponse<List<KitsuAnime>, ?>>() {
                })
                .doOnSuccess(r -> log.info("Found {} anime for {}", r.data().size(), title))
                .filter(r -> !r.data().isEmpty())
                .map(r -> r.data().get(0));
    }

    public Flux<KitsuMediaCharacter> getMediaCharacters(String animeId) {
        URI uri = new DefaultUriBuilderFactory("https://kitsu.io/api/edge/anime/{animeId}?include=characters").builder()
                .build(Map.of("animeId", animeId));
        log.info(uri.toASCIIString());
        return webClient.get().uri(uri).retrieve()
                .bodyToMono(new ParameterizedTypeReference<KitsuResponse<KitsuAnime, KitsuMediaCharacter>>() {
                })
                .filter(r ->  r.included() != null)
                .doOnSuccess(r->log.info("Loaded {} characters for anime {}", (r==null ||r.included()==null)?0:r.included().size(), animeId))
                .flatMapIterable(KitsuResponse::included);
    }

    public Flux<KitsuPerson> getActors(KitsuMediaCharacter mediaCharacter) {
        return webClient.get().uri(mediaCharacter.getRelationships().voices().links().related() + "?include=person&page[limit]=20")
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<KitsuResponse<List<KitsuCharacterVoice>, KitsuPerson>>() {
                })
                .filter(r -> r.included() != null)
                .flatMapIterable(this::getJapaneseActors).checkpoint("Get Actors")
                ;
    }

    private List<KitsuPerson> getJapaneseActors(KitsuResponse<List<KitsuCharacterVoice>, KitsuPerson> response) {
        List<KitsuCharacterVoice> japaneseVoices = response.data().stream().filter(k -> "ja_jp".equals(k.getAttributes().locale())).toList();
        Map<String, KitsuPerson> actors = response.included().stream().collect(Collectors.toMap(KitsuObject::getId, Function.identity()));
        return japaneseVoices.stream().map(cv -> actors.get(cv.getRelationships().person().data().id())).toList();
    }

    public Mono<KitsuCharacter> getCharacter(KitsuMediaCharacter mediaCharacter) {
        return webClient.get().uri(mediaCharacter.getRelationships().character().links().related())
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<KitsuResponse<KitsuCharacter, ?>>() {
                })
                .mapNotNull(KitsuResponse::data)
                .doOnNext(k -> k.setRole(mediaCharacter.getAttributes().role())).checkpoint("Get Character")
                ;
    }


}
