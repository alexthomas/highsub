package com.alexk8s.highsub.kitsu;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class KitsuCharacterVoice extends KitsuObject<KitsuCharacterVoice.Attributes, KitsuCharacterVoice.Links, KitsuCharacterVoice.Relationships>{
    public record Attributes(String createdAt,String updatedAt,String locale){}
    public record Links(String self){}
    public record Relationships(KitsuRelationshipSingle mediaCharacter,KitsuRelationshipSingle person,KitsuRelationshipSingle licensor){}
}
