package com.alexk8s.highsub.kitsu;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;
import java.util.Map;

@EqualsAndHashCode(callSuper = true)
@Data
public class KitsuCharacter extends KitsuObject<KitsuCharacter.Attributes, KitsuCharacter.Links, KitsuCharacter.Relationships> {

    @JsonIgnore
    private String role; //this is to reduce the number of object I have to pass around. The role is found on the MediaCharacter
    public record Attributes(String createdAt, String updatedAt, String slug,
                             Map<String, String> names,
                             String canonicalName, List<String> otherNames, String name, int malId,
                             String description,
                             KitsuImage image) {
    }

    public record Links(String self) {
    }

    public record Relationships(KitsuRelationship primaryMedia, KitsuRelationship castings,
                                KitsuRelationship mediaCharacters, KitsuRelationship quotes) {
    }
}
