package com.alexk8s.highsub.kitsu;

import java.util.List;

public record KitsuRelationship(KitsuRelationshipLinks links, List<KitsuRelationshipDatum> data) {
    public record KitsuRelationshipLinks(String self,String related){}
    public record KitsuRelationshipDatum(String type,String id){}
}
