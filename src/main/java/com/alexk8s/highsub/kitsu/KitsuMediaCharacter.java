package com.alexk8s.highsub.kitsu;


import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class KitsuMediaCharacter extends KitsuObject<KitsuMediaCharacter.Attributes, KitsuMediaCharacter.Links, KitsuMediaCharacter.Relationships>{
    public record Links(String self){}
    public record Attributes(String createdAt,String updatedAt,String role){}
    public record Relationships(KitsuRelationship media,KitsuRelationship character,KitsuRelationship voices){}
}
