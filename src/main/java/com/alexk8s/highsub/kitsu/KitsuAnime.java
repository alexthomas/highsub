package com.alexk8s.highsub.kitsu;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;
import java.util.Map;

@EqualsAndHashCode(callSuper = true)
@Data
public class KitsuAnime extends KitsuObject<KitsuAnime.Attributes, KitsuAnime.Links, KitsuAnime.Relationships> {
    public record Links(String self){}

    public record Attributes(String createdAt, String updatedAt, String slug, String synopsis, String descriptions, int coverImageTopOffset,
                             KitsuAnimeTitles titles, String canonicalTitle, List<String>abbreviatedTitles, String averageRating,
                             Map<String,String>ratingFrequencies, int userCount, int favoritesCount, String startDate, String endDate, String nextRelease,
                             int popularityRank, int ratingRank, String ageRating, String ageRatingGuide, String subtitle, String status,
                             String tba, KitsuImage posterImage, KitsuImage coverImage, int episodeCount,
                             int episodeLength, int totalLength, String youtubeVideoId, String showType, boolean nsfw){}
    public record KitsuAnimeTitles(String en, @JsonAlias("en_jp")String enJp, @JsonAlias("en_us")String enUs, @JsonAlias("ja_jp")String jaJp){}



    public record Relationships(Links genres, Links categories, Links castings,
                                Links installments, Links mappings, Links reviews,
                                Links mediaRelationships, Links characters, Links staff,
                                Links productions, Links quotes, Links episodes,
                                Links streamingLinks, Links animeProductions, Links animeCharacters,
                                Links animeStaff){}
}
