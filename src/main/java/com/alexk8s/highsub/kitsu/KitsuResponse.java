package com.alexk8s.highsub.kitsu;

import java.util.List;

public record KitsuResponse<D, I>(D data, List<I> included,
                                  KitsuMeta meta,
                                  KitsuPageLinks links) {
    public record KitsuMeta(int count) {
    }

    public record KitsuPageLinks(String first, String next, String last) {
    }
}

