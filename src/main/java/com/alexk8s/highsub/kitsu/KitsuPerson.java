package com.alexk8s.highsub.kitsu;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class KitsuPerson extends KitsuObject<KitsuPerson.Attributes, KitsuPerson.Links, KitsuPerson.Relationships>{
    public record Attributes(String createdAt,String updatedAt,String name,String malId,String description,
                             KitsuImage image){}
    public record Links(String self){ }
    public record Relationships(KitsuRelationship castings,KitsuRelationship staff,KitsuRelationship voices){}
}
