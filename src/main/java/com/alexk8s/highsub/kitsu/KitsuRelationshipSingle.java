package com.alexk8s.highsub.kitsu;

public record KitsuRelationshipSingle(KitsuRelationshipLinks links, KitsuRelationshipDatum data) {
    public record KitsuRelationshipLinks(String self,String related){}
    public record KitsuRelationshipDatum(String type,String id){}
}
