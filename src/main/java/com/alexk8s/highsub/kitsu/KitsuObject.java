package com.alexk8s.highsub.kitsu;

import lombok.Data;

@Data
public abstract class KitsuObject<A, L,R> {
    private String id;
    private String type;
    private A attributes;
    private L links;
    private R relationships;

}
