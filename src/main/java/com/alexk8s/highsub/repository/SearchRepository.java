package com.alexk8s.highsub.repository;

import com.alexk8s.highsub.model.Search;
import org.springframework.data.cassandra.repository.ReactiveCassandraRepository;

import java.util.UUID;

public interface SearchRepository extends ReactiveCassandraRepository<Search, UUID> {
}
