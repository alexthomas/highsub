package com.alexk8s.highsub.repository;

import com.alexk8s.highsub.model.CollectionContent;
import org.springframework.data.cassandra.repository.ReactiveCassandraRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CollectionContentRepository extends ReactiveCassandraRepository<CollectionContent, List<String>> {
}
