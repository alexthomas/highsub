package com.alexk8s.highsub.repository;

import com.alexk8s.highsub.model.PlexOpening;
import org.springframework.data.cassandra.repository.ReactiveCassandraRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface PlexOpeningRepository extends ReactiveCassandraRepository<PlexOpening, String> {
    Flux<PlexOpening> findByShowAndSeasonAndEpisode(String show,String season,String episode);
}
