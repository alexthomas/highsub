package com.alexk8s.highsub.repository;

import com.alexk8s.highsub.model.SavedEvent;
import org.springframework.data.cassandra.repository.ReactiveCassandraRepository;
import reactor.core.publisher.Flux;

public interface SavedEventRepository extends ReactiveCassandraRepository<SavedEvent, String> {
    Flux<SavedEvent> findByDomainAndIdOrderByEventUtcDesc(String domain, String id);
}
