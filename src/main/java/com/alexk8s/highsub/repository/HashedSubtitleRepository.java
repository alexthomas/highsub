package com.alexk8s.highsub.repository;

import com.alexk8s.highsub.model.HashedSubtitle;
import org.springframework.data.cassandra.repository.Query;
import org.springframework.data.cassandra.repository.ReactiveCassandraRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface HashedSubtitleRepository extends ReactiveCassandraRepository<HashedSubtitle,String> {
    @Query("SELECT * FROM hashed_subtitles WHERE show=?0 AND hash>?1 LIMIT ?2")
    Flux<HashedSubtitle> findByShowAndHashGreaterThanOrderByHashDescLimit(String show, String hash, int limit);
    Mono<Void> deleteByShowAndHash(String show,String hash);
}
