package com.alexk8s.highsub.repository;

import com.alexk8s.highsub.model.CachedVector;
import org.springframework.data.cassandra.repository.ReactiveCassandraRepository;
import reactor.core.publisher.Mono;

public interface CachedVectorRepository extends ReactiveCassandraRepository<CachedVector,String> {
    Mono<CachedVector> findByVersionAndInput(String version, String input);
}
