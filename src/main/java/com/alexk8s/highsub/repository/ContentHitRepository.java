package com.alexk8s.highsub.repository;

import com.alexk8s.highsub.model.ContentHit;
import org.springframework.data.cassandra.repository.ReactiveCassandraRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContentHitRepository extends ReactiveCassandraRepository<ContentHit,String> {
}
