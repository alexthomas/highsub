package com.alexk8s.highsub.repository;

import com.alexk8s.highsub.model.FlatSubtitle;
import org.springframework.data.cassandra.repository.Query;
import org.springframework.data.cassandra.repository.ReactiveCassandraRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.stream.Stream;

public interface FlatSubtitleRepository extends ReactiveCassandraRepository<FlatSubtitle,String> {
    Mono<FlatSubtitle> findByShowAndSeasonAndEpisodeAndSubtitleNumber(String show,String season,String episode,int subtitleNumber);
    Flux<FlatSubtitle> findByShowAndSeasonAndEpisode(String show,String season,String episode);
    @Query("select * from flat_subtitle where show = :show AND season = :season AND episode = :episode order by subtitle_number asc limit :limit")
    Flux<FlatSubtitle> findByShowAndSeasonAndEpisode(String show,String season,String episode,int limit);
    @Query("select * from flat_subtitle where show = :show AND season = :season AND episode = :episode AND subtitle_number > :subtitleNumber order by subtitle_number asc limit :limit")
    Flux<FlatSubtitle> findNext(String show, String season, String episode, int subtitleNumber,int limit);
    @Query("select * from flat_subtitle where show = :show AND season = :season AND episode = :episode AND subtitle_number < :subtitleNumber order by subtitle_number desc limit :limit")
    Flux<FlatSubtitle> findPrevious(String show, String season, String episode, int subtitleNumber,int limit);

    @Query("select * from flat_subtitle where TOKEN(show,season,episode)>= :lower AND TOKEN(show,season,episode)<= :higher")
    Flux<FlatSubtitle> findByTokenRange(Long lower, Long higher);

    @Override
    default Flux<FlatSubtitle> findAll(){
        long interval = Long.MAX_VALUE / 100;
        return Flux.fromStream(Stream.iterate(Long.MIN_VALUE, n -> n + interval).limit(200))
                .skip(1)//to start with the min at min value
                .flatMap(t -> this.findByTokenRange(t - interval, t), 10);
    }
}
