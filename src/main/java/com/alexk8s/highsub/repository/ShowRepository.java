package com.alexk8s.highsub.repository;

import com.alexk8s.highsub.model.Show;
import org.springframework.data.cassandra.repository.ReactiveCassandraRepository;

public interface ShowRepository extends ReactiveCassandraRepository<Show,String> {
}
