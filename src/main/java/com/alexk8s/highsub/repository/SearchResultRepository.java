package com.alexk8s.highsub.repository;

import com.alexk8s.highsub.model.SearchResult;
import org.springframework.data.cassandra.repository.ReactiveCassandraRepository;

import java.util.UUID;

public interface SearchResultRepository extends ReactiveCassandraRepository<SearchResult, UUID> {
}
