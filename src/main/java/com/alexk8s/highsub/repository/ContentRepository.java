package com.alexk8s.highsub.repository;

import com.alexk8s.highsub.model.Content;
import org.springframework.data.cassandra.repository.ReactiveCassandraRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContentRepository extends ReactiveCassandraRepository<Content,String> {
}
