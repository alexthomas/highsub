package com.alexk8s.highsub.repository;


import com.alexk8s.highsub.model.Episode;
import org.springframework.data.cassandra.repository.ReactiveCassandraRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface EpisodeRepository extends ReactiveCassandraRepository<Episode, String> {
    Flux<Episode> findByShow(String show);

    Mono<Episode> findByShowAndSeasonAndEpisode(String show, String season, String episode);

    Mono<Void> deleteByShowAndSeasonAndEpisode(String show, String season, String episode);

}
