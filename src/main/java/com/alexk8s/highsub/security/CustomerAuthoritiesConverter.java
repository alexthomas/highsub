package com.alexk8s.highsub.security;

import org.springframework.core.convert.converter.Converter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtGrantedAuthoritiesConverter;

import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CustomerAuthoritiesConverter implements Converter<Jwt, Collection<GrantedAuthority>> {
    private final Converter<Jwt, Collection<GrantedAuthority>> jwtGrantedAuthoritiesConverter = new JwtGrantedAuthoritiesConverter();

    @Override
    public Collection<GrantedAuthority> convert(Jwt source) {
        Collection<GrantedAuthority> jwtAuthorities = jwtGrantedAuthoritiesConverter.convert(source);
        if (Boolean.TRUE.equals(source.getClaimAsBoolean("admin"))) {
            return Stream.concat(
                    jwtAuthorities.stream(),
                    Stream.of(new SimpleGrantedAuthority("ROLE_ADMIN"))
            ).collect(Collectors.toSet());
        }
        return jwtAuthorities;
    }
}
