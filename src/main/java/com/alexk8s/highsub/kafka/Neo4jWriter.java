package com.alexk8s.highsub.kafka;

import com.alexk8s.highsub.kafka.model.ActorRoleEventType;
import com.alexk8s.highsub.kafka.model.ActorRoleV1;
import com.alexk8s.highsub.scrape.ActorRoleRepository;
import com.alexk8s.highsub.service.ActorService;
import com.alexk8s.highsub.util.KafkaUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@Component
@AllArgsConstructor
@Slf4j
public class Neo4jWriter {

    private final ActorService actorService;
    private final ActorRoleRepository actorRoleRepository;

    @Bean
    public Consumer<Flux<Message<ActorRoleV1>>> actorRoleNeo4jWriter() {
        return flux -> flux.filter(m -> m.getPayload().getType$1() == ActorRoleEventType.CREATE)
                .window(Duration.ofSeconds(5))
                .flatMap(Flux::collectList)
                .flatMap(this::updateRolesForShows, 1)
                .subscribe();
    }

    private Mono<Void> updateRolesForShows(List<Message<ActorRoleV1>> messages) {
        Map<String, List<Message<ActorRoleV1>>> messagesByShow = messages.stream()
                .collect(Collectors.groupingBy(m -> m.getPayload().getShow()));
        return Flux.fromIterable(messagesByShow.keySet())
                .flatMap(s -> updateRolesForShow(s, messagesByShow.get(s)), 1)
                .then();
    }

    private Mono<Void> updateRolesForShow(String show, List<Message<ActorRoleV1>> messages) {
        return actorRoleRepository.findByShow(show).collectList()
                .flatMap(l -> actorService.saveActorRoles(show, l))
                .doOnSuccess(v -> messages.forEach(KafkaUtils::ackMessage));
    }

}
