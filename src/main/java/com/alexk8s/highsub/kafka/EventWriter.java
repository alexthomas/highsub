package com.alexk8s.highsub.kafka;

import com.alexk8s.highsub.kafka.model.EpisodeV2;
import com.alexk8s.highsub.model.SavedEvent;
import com.alexk8s.highsub.util.KafkaUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.avro.Schema;
import org.springframework.context.annotation.Bean;
import org.springframework.data.cassandra.core.ReactiveCassandraTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

@Component
@Slf4j
@AllArgsConstructor
public class EventWriter {
    private final ReactiveCassandraTemplate cassandraTemplate;
    private final ObjectMapper objectMapper;

    @Bean
    public Consumer<Flux<Message<EpisodeV2>>> episodeEventWriter(){
        return flux -> flux.flatMap(this::writeEvent, 10)
                .subscribe();
    }

    private Mono<Void> writeEvent(Message<EpisodeV2> message){
        return cassandraTemplate.insert(toSavedEvent(message))
                .doOnSuccess(v -> KafkaUtils.ackMessage(message))
                .then();
    }

    @SneakyThrows
    private SavedEvent toSavedEvent(Message<EpisodeV2> message){
        SavedEvent savedEvent = new SavedEvent();
        savedEvent.setDomain("EPISODE");
        EpisodeV2 episode = message.getPayload();
        savedEvent.setId(String.join("-",episode.getShow(), episode.getSeason(), episode.getEpisode()));
        Long receivedTimestamp = message.getHeaders().get(KafkaHeaders.RECEIVED_TIMESTAMP, Long.class);
        savedEvent.setEventUtc(receivedTimestamp);
        Map<String, Object> jsonBody = new HashMap<>();
        for (Schema.Field field : episode.getSchema().getFields()) {
            jsonBody.put(field.name(),episode.get(field.name()));
        }
        savedEvent.setEventJson(objectMapper.writeValueAsString(jsonBody));
        return savedEvent;

    }


}
