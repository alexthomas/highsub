package com.alexk8s.highsub.kafka;

import com.alexk8s.highsub.kafka.model.EpisodeEventType;
import com.alexk8s.highsub.kafka.model.EpisodeV2;
import com.alexk8s.highsub.kafka.model.SubtitleV4;
import com.alexk8s.highsub.model.Episode;
import com.alexk8s.highsub.repository.EpisodeRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.ReactiveRedisOperations;
import org.springframework.data.redis.core.ReactiveSetOperations;
import org.springframework.messaging.Message;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.Collections;
import java.util.HashSet;
import java.util.function.Function;

import static com.alexk8s.highsub.util.KafkaUtils.ackMessage;

@Configuration
@Slf4j
@AllArgsConstructor
public class SubtitleAggregator {

    private final EpisodeRepository episodeRepository;
    private final ReactiveRedisOperations<String, String> redisOperations;

    @Bean
    public Function<Flux<Message<SubtitleV4>>, Flux<EpisodeV2>> updateEpisodeSubtitleCounts() {
        return subtitleV4Flux -> subtitleV4Flux
                .flatMap(this::consumeMessage)
                .bufferTimeout(100, Duration.ofSeconds(1), HashSet::new)
                .flatMapIterable(e -> e)
                .flatMapSequential(this::getEpisodeUpdate)
                .retry();
    }

    private record EpisodeKey(String show, String season, String episode) {
        private String getRedisKey() {
            return String.format("subtitle-ids:%s:%s:%s", show(), season(), episode());
        }

        private String getRedisKeyIndexed() {
            return String.format("subtitle-ids-indexed:%s:%s:%s", show(), season(), episode());
        }
    }

    private record SubtitleKey(String show, String season, String episode, int subtitleNumber) {
        private String getRedisKey() {
            return String.join("-", show, season, episode, Integer.toString(subtitleNumber));
        }
    }

    private Mono<EpisodeKey> consumeMessage(Message<SubtitleV4> message) {
        SubtitleV4 subtitle = message.getPayload();
        EpisodeKey episodeKey = new EpisodeKey(subtitle.getShow(), subtitle.getSeason(), subtitle.getEpisode());
        SubtitleKey subtitleKey = new SubtitleKey(subtitle.getShow(), subtitle.getSeason(), subtitle.getEpisode(), subtitle.getSubtitleNumber());
        ReactiveSetOperations<String, String> ops = redisOperations.opsForSet();
        Mono<?> redisUpdateMono = switch (subtitle.getType$1()) {
            case CREATE -> ops.add(episodeKey.getRedisKey(), subtitleKey.getRedisKey());
            case UPDATE_TAGS -> Mono.empty();
            case DELETE -> ops.remove(episodeKey.getRedisKey(), subtitleKey.getRedisKey());
        };
        Mono<?> redisUpdateIndexedMono = switch (subtitle.getType$1()) {
            case CREATE ->
                    subtitle.getShouldIndex() ? ops.add(episodeKey.getRedisKeyIndexed(), subtitleKey.getRedisKey()) : ops.remove(episodeKey.getRedisKeyIndexed(), subtitleKey.getRedisKey());
            case UPDATE_TAGS -> Mono.empty();
            case DELETE -> ops.remove(episodeKey.getRedisKeyIndexed(), subtitleKey.getRedisKey());
        };
        return Flux.concat(redisUpdateMono, redisUpdateIndexedMono).then().doOnSuccess(c -> ackMessage(message)).thenReturn(episodeKey);
    }

    private Mono<EpisodeV2> getEpisodeUpdate(EpisodeKey episodeKey) {
        Mono<Long> countMono = redisOperations.opsForSet().size(episodeKey.getRedisKey());
        Mono<Long> indexedCountMono = redisOperations.opsForSet().size(episodeKey.getRedisKeyIndexed());
        Mono<Episode> episodeMono = episodeRepository.findByShowAndSeasonAndEpisode(episodeKey.show(), episodeKey.season(), episodeKey.episode());
        return Mono.zip(episodeMono, countMono, indexedCountMono)
                .filter(t -> !t.getT1().getSubtitleCount().equals(t.getT2()) || !t.getT1().getIndexedSubtitleCount().equals(t.getT3()))
                .map(tuple -> {
                    EpisodeV2 episodeV2 = new EpisodeV2();
                    BeanUtils.copyProperties(tuple.getT1(), episodeV2);
                    episodeV2.setSubtitleCount(tuple.getT2());
                    episodeV2.setIndexedSubtitleCount(tuple.getT3().intValue());
                    episodeV2.setType$1(EpisodeEventType.UPDATE_SUBTITLE_COUNT);
                    episodeV2.setTags(Collections.emptyList());
                    episodeV2.setParentTags(Collections.emptyList());
                    return episodeV2;
                });
    }

}
