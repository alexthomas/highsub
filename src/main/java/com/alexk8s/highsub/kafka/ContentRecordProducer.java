package com.alexk8s.highsub.kafka;

import com.alexk8s.highsub.kafka.model.ContentEventType;
import com.alexk8s.highsub.kafka.model.ContentHitV4;
import com.alexk8s.highsub.kafka.model.ContentV2;
import com.alexk8s.highsub.model.Content;
import com.alexk8s.highsub.repository.ContentRepository;
import com.alexk8s.highsub.util.HashUtil;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.nio.file.Path;
import java.util.function.Consumer;
import java.util.function.Function;

import static com.alexk8s.highsub.util.KafkaUtils.ackMessage;

@Component("contentRecordProducerConfig")
@AllArgsConstructor
@Slf4j
public class ContentRecordProducer {
    private final ContentRepository contentRepository;

    @Bean
    public Function<Flux<Message<ContentHitV4>>, Flux<ContentV2>> contentRecordProducer() {
        return flux -> flux.map(this::recordContent);
    }

    @Bean
    public Consumer<Flux<Message<ContentV2>>> contentRecordSaver() {
        return flux -> flux.flatMapSequential(this::saveContent, 1)
                .doOnError(e -> log.error("Error saving content", e))
                .subscribe();
    }

    private ContentV2 recordContent(Message<ContentHitV4> message) {
        ContentHitV4 payload = message.getPayload();
        ContentV2 contentV2 = switch (payload.getHitType()) {
            case GENERATE -> {
                ContentV2 c = new ContentV2();
                BeanUtils.copyProperties(payload, c);
                c.setHash(HashUtil.hashFile(Path.of(payload.getContentPath())));
                c.setGeneratedTrace(payload.getTraceId());
                c.setGeneratedUtc(payload.getHitUtc());
                c.setLastHitUtc(payload.getHitUtc());
                c.setType$1(ContentEventType.CREATE);
                yield c;
            }
            case CACHE -> {
                ContentV2 c = new ContentV2();
                BeanUtils.copyProperties(payload, c);
                c.setLastHitUtc(payload.getHitUtc());
                c.setType$1(ContentEventType.UPDATE_LAST_HIT);
                yield c;
            }
        };
        ackMessage(message);
        return contentV2;
    }

    private Mono<?> saveContent(Message<ContentV2> message) {
        ContentV2 payload = message.getPayload();
        Mono<Content> saveMono = switch (payload.getType$1()) {
            case CREATE -> {
                Content content = new Content();
                BeanUtils.copyProperties(payload, content);
                yield contentRepository.save(content);
            }
            case UPDATE_LAST_HIT -> contentRepository.findById(payload.getContentPath())
                    .map(c -> {
                        c.setLastHitUtc(payload.getLastHitUtc());
                        return c;
                    })
                    .flatMap(contentRepository::save);
            case UPDATE_HASH -> contentRepository.findById(payload.getContentPath())
                    .map(c -> {
                        c.setHash(payload.getHash());
                        return c;
                    })
                    .flatMap(contentRepository::save);
        };
        return saveMono.doOnSuccess(c -> ackMessage(message));
    }
}
