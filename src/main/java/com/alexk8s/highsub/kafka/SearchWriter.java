package com.alexk8s.highsub.kafka;

import com.alexk8s.highsub.elasticsearch.SearchCriteria;
import com.alexk8s.highsub.kafka.model.SearchEventType;
import com.alexk8s.highsub.kafka.model.SearchV3;
import com.alexk8s.highsub.kafka.model.SubtitleV4;
import com.alexk8s.highsub.model.Chapter;
import com.alexk8s.highsub.model.Search;
import com.alexk8s.highsub.model.SearchResult;
import com.alexk8s.highsub.repository.SearchRepository;
import com.alexk8s.highsub.repository.SearchResultRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Publisher;
import org.springframework.beans.BeanUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.function.Consumer;

import static com.alexk8s.highsub.util.KafkaUtils.ackMessage;

@Component("searchWriterComponent")
@RequiredArgsConstructor
@Slf4j
public class SearchWriter {

    private final SearchRepository searchRepository;
    private final SearchResultRepository searchResultRepository;
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Bean
    public Consumer<Flux<Message<SearchV3>>> searchWriter() {
        return flux -> flux.flatMap(this::writeSearch,1)
                .doOnError(e->log.error("Error writing search",e))
                .subscribe();
    }

    private Publisher<Void> writeSearch(Message<SearchV3> message) {
        SearchV3 search = message.getPayload();
        if(search.getType$1() == SearchEventType.REQUEST){
            return searchRepository.save(mapSearch(search)).then()
                    .doOnSuccess((a)->ackMessage(message));
        } else if(search.getType$1() == SearchEventType.RESPONSE){
            UUID id = UUID.fromString(search.getId());
            Publisher<Void> updateLatency = searchRepository
                    .findById(id)
                    .map(s->{
                        s.setLatency(search.getResponse().getDurationMs());
                        return s;
                    }).flatMap(searchRepository::save).then();
            Publisher<Void> saveResults = searchResultRepository.saveAll(mapResults(id,search.getResponse().getResults())).then();
            return Flux.concat(updateLatency,saveResults).then().doOnSuccess((a)->ackMessage(message));
        } else {
            throw new RuntimeException("Invalid search event type");
        }
    }

    @SneakyThrows(IOException.class)
    private Search mapSearch(SearchV3 searchV2){
        Search search = new Search();
        BeanUtils.copyProperties(searchV2.getRequest(),search);
        search.setId(UUID.fromString(searchV2.getId()));
        SearchCriteria searchCriteria = new SearchCriteria();
        BeanUtils.copyProperties(searchV2.getRequest().getSearchCriteria(),searchCriteria);
        search.setSearchCriteria(objectMapper.writeValueAsString(searchCriteria));
        search.setTimestamp(search.getTimestamp());
        return search;
    }

    private List<SearchResult> mapResults(UUID id, List<SubtitleV4> resultsV2){
        List<SearchResult> results = new ArrayList<>(resultsV2.size());
        for (int i = 0; i < resultsV2.size(); i++) {
            SearchResult result = new SearchResult();
            SubtitleV4 subtitle = resultsV2.get(i);
            BeanUtils.copyProperties(subtitle, result);
            result.setChapter(Chapter.valueOf(subtitle.getChapter()));
            result.setKanji(subtitle.getJapaneseText());
            result.setSearchId(id);
            result.setRank(i);
            results.add(result);
        }
        return results;
    }
}
