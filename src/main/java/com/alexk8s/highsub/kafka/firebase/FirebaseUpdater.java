package com.alexk8s.highsub.kafka.firebase;

import com.alexk8s.highsub.kafka.model.EpisodeV2;
import com.alexk8s.highsub.kafka.model.IndexingStatusV1;
import com.alexk8s.highsub.kafka.model.ShowV2;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

import java.util.function.Consumer;

@Component
@AllArgsConstructor
public class FirebaseUpdater {
    private final ShowProcessor showProcessor;
    private final EpisodeProcessor episodeProcessor;
    private final IndexingStatusProcessor indexingStatusProcessor;

    @Bean
    public Consumer<Flux<Message<ShowV2>>> updateFirebaseShows() {
        return showProcessor;
    }

    @Bean
    public Consumer<Flux<Message<EpisodeV2>>> updateFirebaseEpisodes(){
        return episodeProcessor;
    }

    @Bean
    public Consumer<Flux<Message<IndexingStatusV1>>> updateFirebaseIndexingStatus(){
        return indexingStatusProcessor;
    }
}
