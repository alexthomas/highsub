package com.alexk8s.highsub.kafka.firebase;

import com.alexk8s.highsub.kafka.model.EpisodeV2;
import com.alexk8s.highsub.model.Episode;
import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.CollectionReference;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.WriteResult;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import static com.alexk8s.highsub.util.FirebaseUtils.encodeKey;
import static com.alexk8s.highsub.util.FirebaseUtils.toMono;

@AllArgsConstructor
@Component
@Slf4j
public class EpisodeProcessor implements MessageConsumer<EpisodeV2> {
    private static final String COLLECTION = "episodes";
    private final Firestore firestore;

    @Override
    public Mono<Void> consume(EpisodeV2 episodeV2) {
        Episode episode = new Episode();
        BeanUtils.copyProperties(episodeV2, episode);
        return switch (episodeV2.getType$1()) {
            case CREATE -> createEpisode(episode);
            case SCALE -> updateScale(episode);
            case UPDATE_SUBTITLE_COUNT -> updateSubtitleCount(episode);
            case UPDATE_JAPANESE_SUBTITLES ->   updateJapaneseSubtitles(episode);
            case UPDATE_TAGS -> updateTags(episode);
            case DELETE -> deleteEpisode(episode);
            case UPDATE_CHAPTERS -> updateChapters(episode);
            case UPDATE_SLUG -> updateSlug(episode);
            case UPDATE_PREVIEW_SUBTITLE_ID -> updatePreviewSubtitleId(episode);
            case REINDEX, RESCALE -> Mono.empty();
        };
    }

    private CollectionReference getCollection(Episode episode) {
        return firestore.collection(ShowProcessor.COLLECTION).document(encodeKey(episode.getShow())).collection(COLLECTION);
    }

    private DocumentReference getEpisodeDocument(Episode episode) {
        return getCollection(episode).document(encodeKey(getEpisodeId(episode)));
    }

    private String getEpisodeId(Episode episode) {
        return String.format("%s-%s-%s", episode.getShow(), episode.getSeason(), episode.getEpisode());
    }

    private Mono<Void> logStatus(Mono<WriteResult> resultMono,Episode episode){
        return resultMono.doOnSuccess((x) -> log.info("Episode {} updated", getEpisodeId(episode)))
                .doOnError((x) -> log.error("Episode {} error", getEpisodeId(episode),x))
                .then();
    }

    private Mono<Void> createEpisode(Episode episode) {
        ApiFuture<WriteResult> future = getEpisodeDocument(episode).set(episode);
        return toMono(future).transform(x->logStatus(x,episode));
    }

    private Mono<Void> updateScale(Episode episode) {
        ApiFuture<WriteResult> future = getEpisodeDocument(episode).update(
                "scaled480pVideo", episode.getScaled480pVideo(),
                "scaled480pVideoBytes", episode.getScaled480pVideoBytes(),
                "scaled150pVideo", episode.getScaled150pVideo(),
                "scaled150pVideoBytes", episode.getScaled150pVideoBytes());
        return toMono(future).doOnSuccess((x) -> log.info("Episode {} updated with scale", getEpisodeId(episode))).then();
    }

    private Mono<Void> updateSubtitleCount(Episode episode) {
        ApiFuture<WriteResult> future = getEpisodeDocument(episode).update("subtitleCount", episode.getSubtitleCount(),"indexedSubtitleCount",episode.getIndexedSubtitleCount());
        return toMono(future).transform(x->logStatus(x,episode));
    }

    private Mono<Void> updateJapaneseSubtitles(Episode episode) {
        ApiFuture<WriteResult> future = getEpisodeDocument(episode).update("japaneseSubtitles", episode.getJapaneseSubtitles());
        return toMono(future).transform(x->logStatus(x,episode));
    }

    private Mono<Void> updateTags(Episode episode) {
        ApiFuture<WriteResult> future = getEpisodeDocument(episode).update("tags", episode.getTags(),"parentTags",episode.getParentTags());
        return toMono(future).transform(x->logStatus(x,episode));
    }

    private Mono<Void> updateChapters(Episode episode){
        ApiFuture<WriteResult> future = getEpisodeDocument(episode).update(
                "openingStart", episode.getOpeningStart(),
                "openingEnd", episode.getOpeningEnd(),
                "endingStart", episode.getEndingStart(),
                "endingEnd", episode.getEndingEnd());
        return toMono(future).transform(x->logStatus(x,episode));
    }

    private Mono<Void> deleteEpisode(Episode episode) {
        ApiFuture<WriteResult> future = getEpisodeDocument(episode).delete();
        return toMono(future).transform(x->logStatus(x,episode));
    }

    private Mono<Void> updateSlug(Episode episode) {
        ApiFuture<WriteResult> future = getEpisodeDocument(episode).update("slug", episode.getSlug(),"seasonName",episode.getSeasonName());
        return toMono(future).transform(x->logStatus(x,episode));
    }

    private Mono<Void> updatePreviewSubtitleId(Episode episode) {
        ApiFuture<WriteResult> future = getEpisodeDocument(episode).update("previewSubtitleId", episode.getPreviewSubtitleId());
        return toMono(future).transform(x->logStatus(x,episode));
    }

}
