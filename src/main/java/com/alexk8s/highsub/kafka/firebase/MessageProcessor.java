package com.alexk8s.highsub.kafka.firebase;

import org.springframework.messaging.Message;
import reactor.core.publisher.Flux;

import java.util.function.Function;

public interface MessageProcessor<I, O> extends Function<Flux<Message<I>>, Flux<O>> {
}
