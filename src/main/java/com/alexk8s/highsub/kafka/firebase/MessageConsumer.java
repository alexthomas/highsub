package com.alexk8s.highsub.kafka.firebase;

import com.alexk8s.highsub.util.KafkaUtils;
import org.springframework.messaging.Message;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.retry.Retry;

import java.util.function.Consumer;

public interface MessageConsumer<T> extends Consumer<Flux<Message<T>>> {

    default void accept(Flux<Message<T>> messageFlux) {
        messageFlux.flatMapSequential(this::process, getConcurrency()).subscribe();
    }

    default int getConcurrency() {
        return 1;
    }

    default Mono<Void> process(Message<T> message) {
        return consume(message.getPayload()).doOnSuccess((x) -> KafkaUtils.ackMessage(message))
                .retryWhen(Retry.backoff(3, java.time.Duration.ofSeconds(1)));
    }

    Mono<Void> consume(T t);
}
