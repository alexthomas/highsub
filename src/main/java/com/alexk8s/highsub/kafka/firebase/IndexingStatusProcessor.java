package com.alexk8s.highsub.kafka.firebase;

import com.alexk8s.highsub.kafka.model.IndexingStatusType;
import com.alexk8s.highsub.kafka.model.IndexingStatusV1;
import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.WriteResult;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import static com.alexk8s.highsub.util.FirebaseUtils.encodeKey;
import static com.alexk8s.highsub.util.FirebaseUtils.toMono;

@Component
@AllArgsConstructor
public class IndexingStatusProcessor implements MessageConsumer<IndexingStatusV1>{
    private static final String COLLECTION = "indexing-status";
    private final Firestore firestore;

    @Data
    private static final class IndexingStatus {
        private  String show;
        private  String season;
        private  String episode;
        private  String message;
        private  Long totalTaskQuantity;
        private  Long taskQuantity;
        private   IndexingStatusType status;
        private  Long timestamp;


    }

    @Override
    public Mono<Void> consume(IndexingStatusV1 indexingStatusV1) {
        IndexingStatus indexingStatus = new IndexingStatus();
        BeanUtils.copyProperties(indexingStatusV1, indexingStatus);
        String id = String.format("%s-%s-%s", encodeKey(indexingStatus.getShow()), indexingStatus.getSeason(), indexingStatus.getEpisode());
        ApiFuture<WriteResult> future = firestore.collection(COLLECTION).document(id).set(indexingStatus);
        return toMono(future).then();
    }
}
