package com.alexk8s.highsub.kafka.firebase;

import com.alexk8s.highsub.kafka.model.ShowV2;
import com.alexk8s.highsub.model.Show;
import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.WriteResult;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import static com.alexk8s.highsub.util.FirebaseUtils.encodeKey;
import static com.alexk8s.highsub.util.FirebaseUtils.toMono;

@Component
@AllArgsConstructor
@Slf4j
public class ShowProcessor implements MessageConsumer<ShowV2> {

    private final Firestore firestore;

    public static final String COLLECTION = "shows";

    @Override
    public Mono<Void> consume(ShowV2 showV2) {
        Show show = new Show();
        BeanUtils.copyProperties(showV2, show);
        return switch (showV2.getType$1()) {
            case CREATE -> createShow(show);
            case UPDATE_KITSU_ID -> updateKitsuId(show);
            case UPDATE_ANIME -> updateAnime(show);
            case UPDATE_SUMMARY -> updateSummary(show);
            case UPDATE_TAGS -> updateTags(show);
            case UPDATE_SEASON_NAME_MAP -> updateSeasonNameMap(show);
            case DELETE -> deleteShow(show);
        };
    }

    private Mono<Void> logStatus(Mono<WriteResult> resultMono, Show show){
        return resultMono.doOnSuccess((x) -> log.info("Show {} updated", show.getShow()))
                .doOnError((x) -> log.error("Show {} error", show.getShow(),x))
                .then();
    }

    private Mono<Void> createShow(Show show) {
        ApiFuture<WriteResult> future = firestore.collection(COLLECTION).document(encodeKey(show.getShow())).set(show);
        return toMono(future).transform(x->logStatus(x,show));
    }

    private Mono<Void> updateKitsuId(Show show){
        ApiFuture<WriteResult> future = firestore.collection(COLLECTION).document(encodeKey(show.getShow())).update("kitsuAnimeId", show.getKitsuAnimeId());
        return toMono(future).transform(x->logStatus(x,show));
    }

    private Mono<Void> updateAnime(Show show){
        ApiFuture<WriteResult> future = firestore.collection(COLLECTION).document(encodeKey(show.getShow())).update("anime", show.getAnime());
        return toMono(future).transform(x->logStatus(x,show));
    }

    private Mono<Void> updateSummary(Show show){
        ApiFuture<WriteResult> future = firestore.collection(COLLECTION).document(encodeKey(show.getShow())).update("summary", show.getSummary());
        return toMono(future).transform(x->logStatus(x,show));
    }

    private Mono<Void> updateTags(Show show){
        ApiFuture<WriteResult> future = firestore.collection(COLLECTION).document(encodeKey(show.getShow())).update("tags", show.getTags());
        return toMono(future).transform(x->logStatus(x,show));
    }

    private Mono<Void> deleteShow(Show show){
        ApiFuture<WriteResult> future = firestore.collection(COLLECTION).document(encodeKey(show.getShow())).delete();
        return toMono(future).transform(x->logStatus(x,show));
    }

    private Mono<Void> updateSeasonNameMap(Show show){
        ApiFuture<WriteResult> future = firestore.collection(COLLECTION).document(encodeKey(show.getShow())).update("seasonNameMap", show.getSeasonNameMap());
        return toMono(future).transform(x->logStatus(x,show));
    }
}
