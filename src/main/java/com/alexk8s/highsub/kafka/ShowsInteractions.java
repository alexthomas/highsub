package com.alexk8s.highsub.kafka;

import com.alexk8s.highsub.kafka.model.ShowEventType;
import com.alexk8s.highsub.kafka.model.ShowV2;
import com.alexk8s.highsub.kitsu.KitsuService;
import com.alexk8s.highsub.model.Show;
import com.alexk8s.highsub.repository.ShowRepository;
import com.alexk8s.highsub.service.PlexService;
import com.alexk8s.highsub.util.KafkaUtils;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;
import reactor.core.Disposable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.annotation.PreDestroy;
import java.time.Duration;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
@Slf4j
public class ShowsInteractions {

    private static final Set<ShowEventType> KITSU_UPDATE_EVENTS = Set.of(ShowEventType.CREATE, ShowEventType.UPDATE_ANIME);

    private final ShowRepository repository;
    private final KitsuService kitsuService;
    private final PlexService plexService;
    private final KafkaTemplate<String,ShowV2> kafkaTemplate;
    private Disposable updateSubscription;


    @PreDestroy
    public void shutdown(){
        if(updateSubscription!=null && !updateSubscription.isDisposed())
            updateSubscription.dispose();
    }

    @Bean
    public Consumer<Flux<Message<ShowV2>>> showInserter() {
        return showV2Flux -> showV2Flux
                .flatMap(this::insertShow)
                .subscribe();

    }

    private Mono<Void> insertShow(Message<ShowV2> message) {
        ShowV2 kafkaShow = message.getPayload();
        Show show = new Show();
        BeanUtils.copyProperties(kafkaShow, show);
        Mono<?> publisher = switch (kafkaShow.getType$1()){
            case CREATE -> repository.save(show);
            case UPDATE_KITSU_ID -> repository.findById(kafkaShow.getShow())
                    .map(s -> s.withKitsuAnimeId(kafkaShow.getKitsuAnimeId()))
                    .flatMap(repository::save);
            case UPDATE_TAGS -> repository.findById(kafkaShow.getShow())
                    .map(s -> s.withTags(kafkaShow.getTags()))
                    .flatMap(repository::save);
            case UPDATE_SUMMARY -> repository.findById(kafkaShow.getShow())
                    .map(s -> s.withSummary(kafkaShow.getSummary()))
                    .flatMap(repository::save);
            case UPDATE_ANIME -> repository.findById(kafkaShow.getShow())
                    .map(s -> s.withAnime(kafkaShow.getAnime()))
                    .flatMap(repository::save);
            case UPDATE_SEASON_NAME_MAP -> repository.findById(kafkaShow.getShow())
                    .map(s -> s.withSeasonNameMap(kafkaShow.getSeasonNameMap()))
                    .flatMap(repository::save);
            case DELETE -> repository.deleteById(kafkaShow.getShow());
        };

        return publisher.doOnSuccess((a) -> KafkaUtils.ackMessage(message)).then();
    }


    @ConditionalOnProperty(name = "highsub.scrape-enabled", havingValue = "true")
    @PostConstruct
    public void showProducer() {
        updateSubscription = Flux.interval(Duration.ofSeconds(60), Duration.ofHours(1))
                .flatMap(i -> plexService.getShows().collectList())
                .flatMap(this::getUpdates)
                .subscribe(s -> kafkaTemplate.send("shows-v2", s.getShow(), s));
    }

    private Flux<ShowV2> getUpdates(List<PlexService.PlexShow> plexShows){
        log.info("Looking for show updates");
        Flux<ShowV2> newShows = getNewShows(plexShows).map(mapShow(ShowEventType.CREATE));
        Flux<ShowV2> updatedShows = getSummaryUpdates(plexShows).map(mapShow(ShowEventType.UPDATE_SUMMARY));
        Flux<ShowV2> deletedShows = getDeletedShows(plexShows).map(mapShow(ShowEventType.DELETE));
        return Flux.merge(newShows, updatedShows,deletedShows);
    }

    private Flux<Show> getSummaryUpdates(List<PlexService.PlexShow> plexShows) {
        return Flux.fromIterable(plexShows)
                .filterWhen(s -> repository.findById(s.title()).map(b -> !Objects.equals(s.summary(),b.getSummary())).defaultIfEmpty(Boolean.TRUE))
                .flatMap(p -> repository.findById(p.title()).map(s -> s.withSummary(p.summary())))
                .doOnNext(s -> log.info("Found updated show {}", s));
    }

    private Flux<Show> getNewShows(List<PlexService.PlexShow> plexShows) {
        log.info("Looking for new shows");
        return Flux.fromIterable(plexShows)
                .filterWhen(s -> repository.findById(s.title()).map(b -> !s.key().equals(b.getPlexKey())).defaultIfEmpty(Boolean.TRUE))
                .flatMap(p -> {
                    Show show = new Show();
                    show.setShow(p.title());
                    show.setYear(p.year());
                    show.setPlexKey(p.key());
                    show.setSummary(p.summary());
                    show.setTags(Collections.emptyList());
                    show.setSeasonNameMap(Collections.emptyMap());
                    return repository.findById(p.title()).map(s -> show.withAnime(s.getAnime()).withKitsuAnimeId(s.getKitsuAnimeId()).withTags(s.getTags()).withTmdbShowId(s.getTmdbShowId()).withTmdbEpisodeGroup(s.getTmdbEpisodeGroup()))
                            .defaultIfEmpty(show);
                }).doOnNext(s -> log.info("Found new show {}", s));
    }

    private Flux<Show> getDeletedShows(List<PlexService.PlexShow> plexShowFlux){
        Set<String> showNames = plexShowFlux.stream().map(PlexService.PlexShow::title).collect(Collectors.toSet());
        return repository.findAll()
                .filter(s -> !showNames.contains(s.getShow()))
                .doOnNext(s -> log.info("Found deleted show {}", s));

    }

    public Mono<SendResult<String, ShowV2>> updateAnime(Show show) {
        log.debug("Updating anime for show {}", show);
        ShowV2 showV2 = mapShow(ShowEventType.UPDATE_ANIME).apply(show);
        return Mono.fromFuture(kafkaTemplate.send("shows-v2", showV2.getShow(), showV2));
    }

    public Mono<SendResult<String, ShowV2>> updateTags(Show show){
        log.debug("Updating tags for show {}", show);
        ShowV2 showV2 = mapShow(ShowEventType.UPDATE_TAGS).apply(show);
        return Mono.fromFuture(kafkaTemplate.send("shows-v2", showV2.getShow(), showV2));
    }

    public Mono<SendResult<String, ShowV2>> deleteShow(Show show){
        log.debug("Deleting show {}", show);
        ShowV2 showV2 = mapShow(ShowEventType.DELETE).apply(show);
        return Mono.fromFuture(kafkaTemplate.send("shows-v2", showV2.getShow(), showV2));
    }

    private Function<Show, ShowV2> mapShow(ShowEventType eventType) {
        return show -> {
            ShowV2 kafkaShow = new ShowV2();
            BeanUtils.copyProperties(show, kafkaShow);
            kafkaShow.setType$1(eventType);
            if(kafkaShow.getTags()==null)
                kafkaShow.setTags(Collections.emptyList());
            if(kafkaShow.getSeasonNameMap()==null)
                kafkaShow.setSeasonNameMap(Collections.emptyMap());
            return kafkaShow;
        };
    }

    @Bean
    public Function<Flux<Message<ShowV2>>, Flux<ShowV2>> applyKitsuId() {
        return showFlux -> showFlux.flatMapSequential(this::processKitsuUpdate, 1);
    }

    private Mono<ShowV2> processKitsuUpdate(Message<ShowV2> message) {
        ShowV2 payload = message.getPayload();
        if (!KITSU_UPDATE_EVENTS.contains(payload.getType$1())) {
            KafkaUtils.ackMessage(message);
            return Mono.empty();
        }
        return repository.findById(payload.getShow())
                .filter(s -> Boolean.TRUE.equals(payload.getAnime()) && s.getKitsuAnimeId() == null)
                .flatMap(this::withKitsuId)
                .map(mapShow(ShowEventType.UPDATE_KITSU_ID))
                .doOnSuccess(s -> KafkaUtils.ackMessage(message));

    }

    private Mono<Show> withKitsuId(Show show) {
        return kitsuService.getAnime(show.getShow()).map(a -> {
            log.info("Found matching anime for show {} {}", show.getShow(), a.getId());
            return show.withKitsuAnimeId(a.getId());
        }).doOnSuccess(s -> {
            if (s == null || s.getKitsuAnimeId() == null) log.warn("Unable to find kitsu anime for {}", show.getShow());
        });
    }
}
