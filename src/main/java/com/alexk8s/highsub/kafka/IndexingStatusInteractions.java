package com.alexk8s.highsub.kafka;

import com.alexk8s.highsub.kafka.model.IndexingStatusV1;
import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.FluxSink;

import java.util.function.Supplier;

@Component
@Slf4j
public class IndexingStatusInteractions {

    private Flux<IndexingStatusV1> indexingStatusFlux;
    private FluxSink<IndexingStatusV1> indexingStatusFluxSink;

    @PostConstruct
    public void configureSink() {
        indexingStatusFlux = Flux.<IndexingStatusV1>create(sink -> indexingStatusFluxSink = sink).share();
    }

    @Bean
    public Supplier<Flux<IndexingStatusV1>> indexingStatusProducer(){
        return () -> indexingStatusFlux;
    }

    public void sendIndexingStatus(IndexingStatusV1 indexingStatusV1){
        if(indexingStatusFluxSink!= null)
            indexingStatusFluxSink.next(indexingStatusV1);
        else
            log.info(indexingStatusV1.toString());
    }
}
