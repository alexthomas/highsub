package com.alexk8s.highsub.kafka;

import com.alexk8s.highsub.elasticsearch.ElasticsearchService;
import com.alexk8s.highsub.elasticsearch.IndexedSubtitle;
import com.alexk8s.highsub.kafka.model.*;
import com.alexk8s.highsub.model.*;
import com.alexk8s.highsub.repository.EpisodeRepository;
import com.alexk8s.highsub.repository.FlatSubtitleRepository;
import com.alexk8s.highsub.repository.HashedSubtitleRepository;
import com.alexk8s.highsub.service.EpisodeResolverService;
import com.alexk8s.highsub.service.IndexerService;
import com.alexk8s.highsub.service.PlexService;
import com.alexk8s.highsub.service.SubtitleService;
import com.alexk8s.highsub.util.HashUtil;
import com.alexk8s.highsub.util.KafkaUtils;
import fr.free.nrw.jakaroma.Jakaroma;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.reactivestreams.Publisher;
import org.springframework.beans.BeanUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.core.ReactiveHashOperations;
import org.springframework.data.redis.core.ReactiveRedisOperations;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;
import reactor.core.Exceptions;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;
import reactor.util.function.Tuples;
import reactor.util.retry.Retry;
import reactor.util.retry.RetrySpec;

import java.io.IOException;
import java.time.Duration;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.alexk8s.highsub.util.KafkaUtils.ackMessage;

@Component
@AllArgsConstructor
@Slf4j
public class SubtitlesProducer {
    private static final double HIGH_PERCENTAGE_THRESHOLD = .8;
    private static final Pattern NAME_REGEX = Pattern.compile("^([-\\s]*)?\\[([^]]+)]");
    private static final Pattern SFX_PATTERN = Pattern.compile("\\(.*\\)");

    private final FlatSubtitleRepository repository;
    private final SubtitleService subtitleService;
    private final ReactiveRedisOperations<String, String> redisOperations;
    private final IndexerService indexerService;
    private final ElasticsearchService elasticsearchService;
    private final HashedSubtitleRepository hashedSubtitleRepository;
    private final PlexService plexService;
    private final EpisodeResolverService episodeResolverService;
    private final EpisodeRepository episodeRepository;
    private final KafkaTemplate<String, EpisodeV2> kafkaTemplate;
    private final Jakaroma jakaroma = new Jakaroma();

    private final Scheduler scheduler = Schedulers.newBoundedElastic(10, 10_000, "subtitle-processor");

    @Bean
    public Function<Flux<Message<EpisodeV2>>, Flux<SubtitleV4>> subtitleProducer() {
        return episodeV2Flux -> episodeV2Flux
                .flatMapSequential(m -> switch (m.getPayload().getType$1()) {
                    case CREATE, UPDATE_JAPANESE_SUBTITLES, REINDEX ->
                            episodeExists(m.getPayload()).filter(e -> e).flatMapMany((x) -> loadSubtitles(m));
                    case UPDATE_SUBTITLE_COUNT, SCALE, RESCALE, UPDATE_PREVIEW_SUBTITLE_ID -> Mono.empty();
                    case UPDATE_TAGS -> episodeExists(m.getPayload()).filter(e -> e).flatMapMany((x) -> updateTags(m));
                    case UPDATE_CHAPTERS -> handleChapterUpdates(m);
                    case UPDATE_SLUG -> handleSlugUpdate(m);
                    case DELETE -> deleteSubtitles(m);
                }, 1);
    }

    @Bean
    public Consumer<Flux<Message<SubtitleV4>>> subtitleSaver() {
        return subtitleV4Flux -> subtitleV4Flux
                .flatMap(this::saveSubtitle, 3)
                .subscribe();
    }

    @Bean
    public Consumer<Flux<Message<SubtitleV4>>> subtitleHasher() {
        return subtitleV4Flux -> subtitleV4Flux
                .flatMap(this::hashSubtitle, 3)
                .subscribe();
    }

    @Bean
    //indexes subtitles in app search
    public Consumer<Flux<Message<SubtitleV4>>> subtitleIndexer() {
        return subtitleV4Flux -> subtitleV4Flux
                .bufferTimeout(100, Duration.ofSeconds(3))
                .flatMap(this::indexSubtitles, 3)
                .retryWhen(RetrySpec.backoff(3, Duration.ofSeconds(3)))
                .subscribe();
    }

    @Bean
    //generates embeddings for subtitles before they go in elasticsearch
    //note all subtitles with shouldIndex = false are filtered out
    public Function<Flux<Message<SubtitleV4>>, Flux<EmbeddedSubtitlesV1>> subtitleVectorProcessor() {
        return subtitleV4Flux -> subtitleV4Flux
                .filter(m -> m.getPayload().getShouldIndex() && m.getPayload().getText().length() < 500) //todo: remove length condition
                .groupBy(m -> m.getPayload().getType$1())
                .flatMap(g -> switch (g.key()) {
                    // it's most efficient to update the subtitles in bulk,
                    // do this we buffer the stream however buffering the stream breaks backpressure.
                    // If there are a lot of subtitles to index at once (50,000+) then a backpressure overflow can occur.
                    // we're getting around this by buffering them and putting the buffer back on a different topic, that way kafka is effectively handling the backpressure
                    case CREATE, UPDATE_TAGS -> handleUpdates(g);
                    case DELETE -> g
                            .flatMapSequential(this::deleteSubtitle, 1)
                            .doOnNext(KafkaUtils::ackMessage)
                            .then(Mono.empty());
                });
    }

    private Mono<Message<SubtitleV4>> deleteSubtitle(Message<SubtitleV4> message) {
        return elasticsearchService.deleteSubtitle(message.getPayload())
                .doOnError(t -> log.error("Error deleting subtitle {}", message.getPayload(), t))
                .retryWhen(Retry.backoff(5, Duration.ofMillis(100)))
                .thenReturn(message).switchIfEmpty(Mono.just(message));
    }

    @Bean
    //saves the embeddings in elasticsearch
    public Consumer<Flux<Message<EmbeddedSubtitlesV1>>> subtitleVectorSaver() {
        return subtitleV4Flux -> subtitleV4Flux
                .flatMapSequential(this::indexSubtitleVectors, 1)
                .subscribe();
    }

    @Bean
    //deletes subtitles from elasticsearch that shouldn't be indexed
    public Consumer<Flux<Message<SubtitleV4>>> deindexSubtitle() {
        return subtitleV4Flux -> subtitleV4Flux
                .flatMap(m -> m.getPayload().getShouldIndex() ? Mono.just(m) : deleteSubtitle(m), 1)
                .doOnNext(KafkaUtils::ackMessage)
                .subscribe();
    }

    private Flux<EmbeddedSubtitlesV1> handleUpdates(Flux<Message<SubtitleV4>> g) {
        Scheduler scheduler1 = Schedulers.newBoundedElastic(10, 100_000, "subtitleProducer-handleUpdates");
        return g.publishOn(scheduler1).flatMapSequential(m -> elasticsearchService.mapSubtitle(m.getPayload()).map(x -> Tuples.of(m, x)), 100)
                .publishOn(scheduler1, 100)
                .map(t -> {
                    EmbeddedSubtitleV1 embeddedSubtitleV1 = map(t.getT2());
                    ackMessage(t.getT1());
                    return embeddedSubtitleV1;
                })
                .bufferTimeout(20, Duration.ofSeconds(3))
                .map(EmbeddedSubtitlesV1::new);

    }

    private EmbeddedSubtitleV1 map(IndexedSubtitle subtitle) {
        EmbeddedSubtitleV1 embeddedSubtitleV1 = new EmbeddedSubtitleV1();
        BeanUtils.copyProperties(subtitle, embeddedSubtitleV1);
        embeddedSubtitleV1.setEmbedding(Arrays.asList(subtitle.getEmbedding()));
        embeddedSubtitleV1.setChapter(subtitle.getChapter().toString());
        return embeddedSubtitleV1;
    }

    private IndexedSubtitle map(EmbeddedSubtitleV1 embeddedSubtitleV1) {
        IndexedSubtitle indexedSubtitle = new IndexedSubtitle();
        BeanUtils.copyProperties(embeddedSubtitleV1, indexedSubtitle);
        indexedSubtitle.setEmbedding(embeddedSubtitleV1.getEmbedding().toArray(new Double[0]));
        indexedSubtitle.setChapter(Chapter.valueOf(embeddedSubtitleV1.getChapter()));
        return indexedSubtitle;
    }

    private Mono<Void> indexSubtitles(List<Message<SubtitleV4>> messages) {
        Map<SubtitleEventType, List<SubtitleV4>> subtitleMap = messages.stream()
                .map(Message::getPayload)
                .filter(SubtitleV4::getShouldIndex)
                .collect(Collectors.groupingBy(SubtitleV4::getType$1));
        List<Publisher<?>> saveActions = new ArrayList<>();
        if (subtitleMap.containsKey(SubtitleEventType.CREATE)) {
            List<SubtitleV4> createSubtitles = subtitleMap.get(SubtitleEventType.CREATE);
            saveActions.add(indexerService.indexSubtitles(createSubtitles));
        }
        if (subtitleMap.containsKey(SubtitleEventType.DELETE)) {
            List<String> subtitleIds = subtitleMap.get(SubtitleEventType.DELETE).stream()
                    .map(SubtitleV4::getIndexedId)
                    .toList();
            saveActions.add(indexerService.deleteSubtitles(subtitleIds));
        }
        return Flux.merge(saveActions).then().doOnSuccess(v -> messages.forEach(KafkaUtils::ackMessage));

    }

    private Mono<Void> indexSubtitleVectors(Message<EmbeddedSubtitlesV1> message) {
        List<IndexedSubtitle> subtitles = message.getPayload().getSubtitles().stream().map(this::map).toList();
        return elasticsearchService.indexSubtitles(subtitles).then().doOnSuccess(v -> ackMessage(message));
    }

    private Mono<Void> saveSubtitle(Message<SubtitleV4> message) {
        SubtitleV4 subtitle = message.getPayload();
        Mono<Void> saveAction = switch (subtitle.getType$1()) {
            case CREATE ->
                    repository.save(mapSubtitle(subtitle)).doOnSuccess(s -> log.trace("saved subtitle {}", s)).then();
            case UPDATE_TAGS ->
                    repository.findByShowAndSeasonAndEpisodeAndSubtitleNumber(subtitle.getShow(), subtitle.getSeason(), subtitle.getEpisode(), subtitle.getSubtitleNumber())
                            .flatMap(s -> {
                                s.setTags(subtitle.getTags());
                                s.setParentTags(subtitle.getParentTags());
                                return repository.save(s);
                            }).doOnSuccess(s -> log.trace("updated subtitle tags {}", s)).then();
            case DELETE ->
                    repository.findByShowAndSeasonAndEpisodeAndSubtitleNumber(subtitle.getShow(), subtitle.getSeason(), subtitle.getEpisode(), subtitle.getSubtitleNumber())
                            .flatMap(repository::delete).doOnSuccess(s -> log.trace("deleted subtitle {}", subtitle));
        };
        return saveAction.doOnSuccess(v -> ackMessage(message));
    }

    private Mono<Void> hashSubtitle(Message<SubtitleV4> message) {
        SubtitleV4 subtitleV4 = message.getPayload();
        HashedSubtitle hashedSubtitle = mapHashedSubtitle(subtitleV4);
        Mono<Void> action = switch (subtitleV4.getType$1()) {
            case CREATE -> hashedSubtitleRepository.save(hashedSubtitle).then();
            case UPDATE_TAGS -> Mono.empty();
            case DELETE ->
                    hashedSubtitleRepository.deleteByShowAndHash(hashedSubtitle.getShow(), hashedSubtitle.getHash());
        };
        return action.doOnSuccess(v -> ackMessage(message));
    }

    private Flux<SubtitleV4> updateTags(Message<EpisodeV2> message) {
        EpisodeV2 episode = message.getPayload();
        List<String> parentTags = new ArrayList<>();
        if (episode.getTags() != null)
            parentTags.addAll(episode.getTags());
        if (episode.getParentTags() != null)
            parentTags.addAll(episode.getParentTags());
        return repository.findByShowAndSeasonAndEpisode(episode.getShow(), episode.getSeason(), episode.getEpisode())
                .filter(s -> !parentTags.equals(s.getParentTags()))
                .map(subtitle -> {
                    subtitle.setParentTags(parentTags);
                    return subtitle;
                })
                .map(mapSubtitle(SubtitleEventType.UPDATE_TAGS))
                .doOnComplete(() -> ackMessage(message));
    }

    public static Function<FlatSubtitle, SubtitleV4> mapSubtitle(SubtitleEventType eventType) {
        return subtitle -> {
            if (subtitle.getContext() == null || subtitle.getRawSubtitle() == null)
                log.warn("subtitle {} has null context or raw subtitle", subtitle);
            if (subtitle.getShouldIndex() == null)
                subtitle.setShouldIndex(true);
            SubtitleV4 kafkaSubtitle = new SubtitleV4();
            BeanUtils.copyProperties(subtitle, kafkaSubtitle);
            kafkaSubtitle.setType$1(eventType);
            kafkaSubtitle.setIndexedId(subtitle.getId());
            if (subtitle.getChapter() != null)
                kafkaSubtitle.setChapter(subtitle.getChapter().name());
            else
                kafkaSubtitle.setChapter(Chapter.OTHER.name());
            kafkaSubtitle.setJapaneseText(subtitle.getKanji());
            if (kafkaSubtitle.getVectors() == null)
                kafkaSubtitle.setVectors(Collections.emptyMap());
            if (kafkaSubtitle.getTags() == null)
                kafkaSubtitle.setTags(Collections.emptyList());
            if (kafkaSubtitle.getParentTags() == null)
                kafkaSubtitle.setParentTags(Collections.emptyList());
            if (kafkaSubtitle.getContext() == null)
                kafkaSubtitle.setContext(Collections.emptyList());
            return kafkaSubtitle;
        };
    }

    private FlatSubtitle mapSubtitle(SubtitleV4 subtitle) {
        FlatSubtitle flatSubtitle = new FlatSubtitle();
        BeanUtils.copyProperties(subtitle, flatSubtitle);
        flatSubtitle.setChapter(Chapter.valueOf(subtitle.getChapter()));
        flatSubtitle.setKanji(subtitle.getJapaneseText());
        return flatSubtitle;
    }

    private HashedSubtitle mapHashedSubtitle(SubtitleV4 subtitleV4) {
        String hash = HashUtil.hashStrings(subtitleV4.getShow(), subtitleV4.getSeason(), " ", subtitleV4.getEpisode(), " " + subtitleV4.getSubtitleNumber(), subtitleV4.getText());
        return new HashedSubtitle(subtitleV4.getShow(), hash, subtitleV4.getSeason(), subtitleV4.getEpisode(), subtitleV4.getSubtitleNumber());
    }

    private Flux<SubtitleV4> loadSubtitles(Message<EpisodeV2> message) {
        EpisodeV2 episode = message.getPayload();
        List<String> subtitleIds = new ArrayList<>();
        Flux<FlatSubtitle> newSubtitles = readSubtitles(episode)
                .publishOn(Schedulers.boundedElastic())
                .flatMapMany(this::getSubtitles)
                .doOnNext(f -> {
                    if (f.getShouldIndex()) subtitleIds.add(f.getId());
                })
                .replay().autoConnect();
        Flux<SubtitleV4> deleteSubtitles = newSubtitles.collectList().flatMapMany(l -> getDeleteSubtitles(episode, l));
        return Flux.merge(deleteSubtitles, newSubtitles.map(mapSubtitle(SubtitleEventType.CREATE)))
                .doOnComplete(() -> {
                    ackMessage(message);
                    sendPreviewUpdateEvent(episode, subtitleIds);
                });
    }

    private void sendPreviewUpdateEvent(EpisodeV2 episode, List<String> subtitleIds) {
        String randomId = subtitleIds.get((int) Math.floor(Math.random() * subtitleIds.size()));
        EpisodeV2 build = EpisodeV2.newBuilder(episode).setPreviewSubtitleId(randomId).setType$1(EpisodeEventType.UPDATE_PREVIEW_SUBTITLE_ID).build();
        kafkaTemplate.send("episodes-v2", build);
    }

    private static class MissingEpisodeException extends RuntimeException {
        public MissingEpisodeException() {
            super("Missing Episode in database");
        }
    }

    private Mono<Boolean> episodeExists(EpisodeV2 episode) {
        log.info("Checking if episode exists {}-{}-{}", episode.getShow(), episode.getSeason(), episode.getEpisode());
        return episodeRepository.findByShowAndSeasonAndEpisode(episode.getShow(), episode.getSeason(), episode.getEpisode())
                .switchIfEmpty(Mono.error(new MissingEpisodeException()))
                .retryWhen(Retry.backoff(5, Duration.ofMillis(200)))
                .map(e -> true)
                .onErrorResume(Exceptions::isRetryExhausted, e -> Mono.just(false));
    }

    private Flux<SubtitleV4> getDeleteSubtitles(EpisodeV2 episode, List<FlatSubtitle> newSubtitles) {
        Set<String> subtitleIds = newSubtitles.stream().map(FlatSubtitle::getId).collect(Collectors.toSet());
        return repository.findByShowAndSeasonAndEpisode(episode.getShow(), episode.getSeason(), episode.getEpisode())
                .filter(s -> !subtitleIds.contains(s.getId()))
                .doOnNext(f -> {
                    f.setContext(Collections.emptyList());
                    f.setRawSubtitle(Collections.emptyList());
                    if (f.getType() == null)
                        f.setType(SubtitleType.OTHER);
                })
                .map(mapSubtitle(SubtitleEventType.DELETE));

    }

    private Flux<SubtitleV4> deleteSubtitles(Message<EpisodeV2> message) {
        EpisodeV2 episode = message.getPayload();
        return repository.findByShowAndSeasonAndEpisode(episode.getShow(), episode.getSeason(), episode.getEpisode())
                .map(mapSubtitle(SubtitleEventType.DELETE))
                .doOnComplete(() -> ackMessage(message));
    }

    private Mono<EpisodeSubtitles> readSubtitles(EpisodeV2 e) {
        Mono<List<Subtitle>> englishSubtitles = subtitleService.getCacheFile(e.getEnglishSubtitles()).checkpoint()
                .onErrorResume(IOException.class, t -> resolveEpisode(e).flatMap(e1 -> subtitleService.getCacheFile(e1.getEnglishSubtitles())))
                .doOnError(t -> log.error("Error katakana subtitle file {}", e.getEnglishSubtitles(), t))
                .onErrorComplete()
                .publishOn(Schedulers.boundedElastic())
                .map(f -> subtitleService.readSubtitles(SubtitleFormat.getFormat(e.getEnglishSubtitles()), f, Language.ENGLISH))
                .doOnNext(l -> applyChapters(e, l))
                .doOnNext(l -> e.setSubtitleCount((long) l.size()));
        if (e.getJapaneseSubtitles() != null) {
            Mono<List<Subtitle>> japaneseSubtitles = subtitleService.getCacheFile(e.getJapaneseSubtitles())
                    .onErrorResume(IOException.class, t -> resolveEpisode(e).flatMap(e1 -> subtitleService.getCacheFile(e1.getJapaneseSubtitles())))
                    .publishOn(Schedulers.boundedElastic())
                    .map(f -> subtitleService.readSubtitles(SubtitleFormat.getFormat(e.getJapaneseSubtitles()), f, Language.ENGLISH));
            return Mono.zip(englishSubtitles, japaneseSubtitles).map(t -> new EpisodeSubtitles(e, t.getT1(), t.getT2())).onErrorResume((ex) -> {
                log.warn("Encountered error katakana both japanese and english subtitles for {}-{}-{}, trying with just english subtitles instead", e.getShow(), e.getSeason(), e.getEpisode(), ex);
                return englishSubtitles.map(s -> new EpisodeSubtitles(e, s, null));
            });
        }
        return englishSubtitles.map(s -> new EpisodeSubtitles(e, s, null));
    }

    private Mono<Episode> resolveEpisode(EpisodeV2 episodeV2) {
        log.info("Resolving plex episode {}-{}-{}", episodeV2.getShow(), episodeV2.getSeason(), episodeV2.getEpisode());
        Mono<PlexService.PlexEpisode> plexEpisodeMono = plexService.getShows()
                .filter(p -> p.title().equals(episodeV2.getShow()))
                .take(1).singleOrEmpty()
                .flatMapMany(p -> plexService.getEpisodes(p.key()))
                .filter(p -> p.episode().equals(episodeV2.getEpisode()) && p.season().equals(episodeV2.getSeason()))
                .take(1).singleOrEmpty()
                .switchIfEmpty(Mono.error(new RuntimeException("Could not find in plex episode")))
                .doOnSuccess(p -> log.info("Found plex episode {}-{}-{}", episodeV2.getShow(), episodeV2.getSeason(), episodeV2.getEpisode()));
        return plexEpisodeMono.flatMap(episodeResolverService::resolveEpisode)
                .doOnError(t -> log.error("Error resolving episode {}-{}-{}", episodeV2.getShow(), episodeV2.getSeason(), episodeV2.getEpisode(), t))
                .doOnSuccess(e -> log.info("Resolved episode {}-{}-{} to {}", episodeV2.getShow(), episodeV2.getSeason(), episodeV2.getEpisode(), e));

    }

    private void applyChapters(EpisodeV2 episodeV2, List<Subtitle> subtitles) {
        Map<Chapter, Predicate<Subtitle>> chapterPredicates = new HashMap<>();
        if (episodeV2.getPrologueStart() != null)
            chapterPredicates.put(Chapter.PROLOGUE, isIntersecting(episodeV2.getPrologueStart(), episodeV2.getPrologueEnd()));
        if (episodeV2.getOpeningStart() != null)
            chapterPredicates.put(Chapter.OPENING, isIntersecting(episodeV2.getOpeningStart(), episodeV2.getOpeningEnd()));
        if (episodeV2.getEndingStart() != null)
            chapterPredicates.put(Chapter.ENDING, isIntersecting(episodeV2.getEndingStart(), episodeV2.getEndingEnd()));
        if (episodeV2.getPreviewStart() != null)
            chapterPredicates.put(Chapter.PREVIEW, isIntersecting(episodeV2.getPreviewStart(), episodeV2.getPreviewEnd()));
        for (Subtitle subtitle : subtitles) {
            for (Chapter chapter : chapterPredicates.keySet()) {
                if (chapterPredicates.get(chapter).test(subtitle))
                    subtitle.setChapter(chapter);
            }
        }
    }

    private Predicate<Subtitle> isIntersecting(Long start, Long end) {
        return f -> f.getStartTime() > start && f.getStartTime() < end;
    }

    private record EpisodeSubtitles(EpisodeV2 episode, List<Subtitle> englishSubtitles,
                                    List<Subtitle> japaneseSubtitles) {
    }

    private Flux<FlatSubtitle> getSubtitles(EpisodeSubtitles episodeSubtitles) {
        EpisodeV2 episode = episodeSubtitles.episode;
        List<String> parentTags = new ArrayList<>();
        if (episode.getTags() != null)
            parentTags.addAll(episode.getTags());
        if (episode.getParentTags() != null)
            parentTags.addAll(episode.getParentTags());
        return Flux.fromIterable(episodeSubtitles.englishSubtitles)
                .filter(s -> StringUtils.isNotBlank(s.getText()))
                .publishOn(scheduler)
                .flatMap(s -> processSubtitle(episode.getShow(), episode.getSeason(), episode.getEpisode(), episode.getWidth(), episode.getHeight(), episode.getSlug(), s, episodeSubtitles.japaneseSubtitles, parentTags), 5);

    }

    private Mono<FlatSubtitle> processSubtitle(String show, String season, String episode, int episodeWidth, int episodeHeight, String episodeSlug, Subtitle subtitle, List<Subtitle> japaneseSubtitles, List<String> parentTags) {
        String text = getText(subtitle);
        Optional<String> name = getName(subtitle);
        FlatSubtitle.FlatSubtitleBuilder builder = FlatSubtitle.builder()
                .show(show)
                .season(season)
                .episode(episode)
                .startTime(subtitle.getStartTime())
                .endTime(subtitle.getEndTime())
                .subtitleNumber(subtitle.getNumber())
                .style(subtitle.getStyle())
                .text(text)
                .context(subtitle.getContext())
                .episodeWidth(episodeWidth)
                .episodeHeight(episodeHeight)
                .episodeSlug(episodeSlug)
                .rawSubtitle(subtitle.getRawSubtitle())
                .chapter(subtitle.getChapter())
                .shouldIndex(shouldIndex(subtitle.getChapter(), subtitle.getType(), subtitle.getText()))
                .type(subtitle.getType())
                .parentTags(parentTags);
        name.ifPresent(builder::name);
        if (subtitle.getType() == SubtitleType.DIALOGUE && SFX_PATTERN.matcher(subtitle.getText()).matches())
            builder.type(SubtitleType.SFX).shouldIndex(false);
        else if (text.isBlank() && name.isPresent()) {
            builder.shouldIndex(false).text(name.get()).name(null)
                    .type(SubtitleType.SFX).shouldIndex(false);
        } else if (text.startsWith("♪") && text.endsWith("♪"))
            builder.shouldIndex(false).type(SubtitleType.LYRICS);
        if (japaneseSubtitles != null) {
            MatchedSubtitle matchedSubtitle = findClosestSubtitle(subtitle, japaneseSubtitles);
            ReactiveHashOperations<String, String, String> hashOperations = redisOperations.opsForHash();
            if (matchedSubtitle != null) {
                String kanji = matchedSubtitle.getText();
                builder.kanji(kanji)
                        .matchRate(matchedSubtitle.getMatchRate())
                        .japaneseStartTime(matchedSubtitle.getStartTime())
                        .japaneseEndTime(matchedSubtitle.getEndTime())
                        .japaneseSubtitleNumber(matchedSubtitle.getNumber());

                return hashOperations.get("kanji-romaji", kanji)
                        .publishOn(scheduler)
                        .switchIfEmpty(Mono.defer(() -> {
                            String romaji = jakaroma.convert(kanji, true, true);
                            return hashOperations.put("kanji-romaji", kanji, romaji).thenReturn(romaji);
                        })).map(romaji -> builder.romaji(romaji).build());

            }
        }
        return Mono.just(builder.build());
    }


    private String getText(Subtitle subtitle) {
        String text = subtitle.getText().trim();
        if (subtitle.getText().startsWith("-"))
            text = text.substring(1).trim();
        Matcher matcher = NAME_REGEX.matcher(text);
        if (matcher.find())
            text = text.substring(matcher.end()).trim();
        return text;
    }

    private Optional<String> getName(Subtitle subtitle) {
        if (subtitle.getName() != null && !subtitle.getName().isBlank())
            return Optional.of(subtitle.getName());
        Matcher matcher = NAME_REGEX.matcher(subtitle.getText());
        if (matcher.find())
            return Optional.of(matcher.group(2));
        return Optional.empty();
    }

    private boolean shouldIndex(Chapter chapter, SubtitleType subtitleType, String text) {
        return chapter == Chapter.OTHER && subtitleType == SubtitleType.DIALOGUE && !text.isBlank();
    }

    @Data()
    @EqualsAndHashCode(callSuper = true)
    private static class MatchedSubtitle extends Subtitle {
        private String matchRate;
    }

    private MatchedSubtitle findClosestSubtitle(Subtitle needle, Collection<Subtitle> haystack) {
        double intersectingPercent = 0.0;
        Subtitle closestSubtitle = null;
        List<Subtitle> highIntersections = new ArrayList<>();
        for (Subtitle subtitle : haystack.stream().sorted(Comparator.comparing(Subtitle::getNumber)).toList()) {
            double iPercent = calculateIntersectionPercentage(needle, subtitle);
            if (iPercent >= HIGH_PERCENTAGE_THRESHOLD) {
                highIntersections.add(subtitle);
            }
            if (iPercent > intersectingPercent) {
                closestSubtitle = subtitle;
                intersectingPercent = iPercent;
            }
        }
        if (closestSubtitle == null)
            return null;
        if (highIntersections.size() > 1) {
            MatchedSubtitle subtitle = new MatchedSubtitle();
            BeanUtils.copyProperties(highIntersections.get(0), subtitle);
            subtitle.setEndTime(highIntersections.get(highIntersections.size() - 1).getEndTime());
            String text = highIntersections.stream().map(Subtitle::getText).collect(Collectors.joining(" "));
            subtitle.setText(text);
            List<String> percentages = highIntersections.stream().map(s -> calculateIntersectionPercentage(needle, s)).map(this::formatPercentage).toList();
            subtitle.setMatchRate(percentages.toString());
            return subtitle;
        }
        MatchedSubtitle matchedSubtitle = new MatchedSubtitle();
        BeanUtils.copyProperties(closestSubtitle, matchedSubtitle);
        matchedSubtitle.setMatchRate(formatPercentage(intersectingPercent));
        return matchedSubtitle;
    }

    private String formatPercentage(double percent) {
        String s = Double.toString(percent * 100);
        return s.substring(0, Math.min(4, s.length())) + "%";
    }

    private double calculateIntersectionPercentage(Subtitle needle, Subtitle subtitle) {

        long nonintersectionDuration = Math.max(0, needle.getStartTime() - subtitle.getStartTime()) + Math.max(subtitle.getEndTime() - needle.getEndTime(), 0);
        double duration = subtitle.getEndTime() - subtitle.getStartTime();
        return (duration - nonintersectionDuration) / duration;
    }

    private Flux<SubtitleV4> handleChapterUpdates(Message<EpisodeV2> message) {
        EpisodeV2 episode = message.getPayload();
        return repository.findByShowAndSeasonAndEpisode(episode.getShow(), episode.getSeason(), episode.getEpisode())
                .collectList().flatMapIterable(subtitles -> updateChapters(subtitles, episode))
                .map(mapSubtitle(SubtitleEventType.CREATE))
                .doOnComplete(() -> ackMessage(message));
    }

    private Flux<SubtitleV4> handleSlugUpdate(Message<EpisodeV2> message) {
        EpisodeV2 episode = message.getPayload();
        return repository.findByShowAndSeasonAndEpisode(episode.getShow(), episode.getSeason(), episode.getEpisode())
                .map(s -> {
                    s.setEpisodeSlug(episode.getSlug());
                    //todo: remove this after migration is complete
                    s.setEpisodeHeight(episode.getHeight());
                    s.setEpisodeWidth(episode.getWidth());
                    return s;
                })
                .map(mapSubtitle(SubtitleEventType.CREATE))
                .doOnComplete(() -> ackMessage(message));
    }

    private List<FlatSubtitle> updateChapters(List<FlatSubtitle> subtitles, EpisodeV2 episodeV2) {
        return subtitles.stream()
                .map(flatSubtitle -> updateChapter(flatSubtitle, episodeV2))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .toList();
    }

    /**
     * Updates the chapter and should index based off of the chapter for a subtitle
     *
     * @param flatSubtitle the subtitle to perform the update on
     * @param episode      the episode with the chapter information
     * @return the updated flat subtitle if an updated was performed, empty if no update was performed
     */
    private Optional<FlatSubtitle> updateChapter(FlatSubtitle flatSubtitle, EpisodeV2 episode) {
        Chapter chapter = Chapter.OTHER;
        if (episode.getOpeningStart() != null && flatSubtitle.getStartTime() > episode.getOpeningStart() && flatSubtitle.getEndTime() < episode.getOpeningEnd())
            chapter = Chapter.OPENING;
        else if (episode.getEndingStart() != null && flatSubtitle.getStartTime() > episode.getEndingStart() && flatSubtitle.getEndTime() < episode.getEndingEnd())
            chapter = Chapter.ENDING;
        else if (episode.getPrologueStart() != null && flatSubtitle.getStartTime() > episode.getPrologueStart() && flatSubtitle.getEndTime() < episode.getPrologueEnd())
            chapter = Chapter.PROLOGUE;
        else if (episode.getPreviewStart() != null && flatSubtitle.getStartTime() > episode.getPreviewStart() && flatSubtitle.getEndTime() < episode.getPreviewEnd())
            chapter = Chapter.PREVIEW;
        boolean shouldIndex = shouldIndex(chapter, flatSubtitle.getType(), flatSubtitle.getText());
        if (chapter != flatSubtitle.getChapter() || shouldIndex != flatSubtitle.getShouldIndex()) {
            flatSubtitle.setChapter(chapter);
            flatSubtitle.setShouldIndex(shouldIndex);
            return Optional.of(flatSubtitle);
        }
        return Optional.empty();
    }

}
