package com.alexk8s.highsub.kafka;

import com.alexk8s.highsub.kafka.model.ActorRoleEventType;
import com.alexk8s.highsub.kafka.model.ActorRoleV1;
import com.alexk8s.highsub.kafka.model.ShowEventType;
import com.alexk8s.highsub.kafka.model.ShowV2;
import com.alexk8s.highsub.kitsu.KitsuService;
import com.alexk8s.highsub.model.Show;
import com.alexk8s.highsub.scrape.ActorRole;
import com.alexk8s.highsub.scrape.ActorRoleRepository;
import com.alexk8s.highsub.util.KafkaUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;

@Component
@AllArgsConstructor
@Slf4j
public class ActorRoleProducer {

    private static final Set<ShowEventType> UPDATE_EVENTS = Set.of(ShowEventType.CREATE, ShowEventType.UPDATE_KITSU_ID);
    private final ActorRoleRepository actorRoleRepository;
    private final KitsuService kitsuService;


    @Bean
    public Function<Flux<Message<ShowV2>>, Flux<ActorRoleV1>> updateActorRoles() {
        return showV2Flux -> showV2Flux
                .flatMapSequential(this::handleShowMessage, 1);
    }

    private Flux<ActorRoleV1> handleShowMessage(Message<ShowV2> message){
        ShowV2 showV2 = message.getPayload();
        if(showV2.getKitsuAnimeId() == null){
            return Flux.empty();
        }
        return switch (showV2.getType$1()) {
            case CREATE, UPDATE_KITSU_ID -> getActorRoles(message).map(this::mapActorRole);
            case  UPDATE_ANIME, UPDATE_SUMMARY,UPDATE_TAGS,DELETE,UPDATE_SEASON_NAME_MAP -> Flux.empty();

        };
    }

    @Bean
    public Consumer<Flux<Message<ActorRoleV1>>> actorRoleSaver() {
        return actorRoleV1Flux -> actorRoleV1Flux
                .flatMap(m -> actorRoleRepository.save(mapActorRole(m.getPayload())).doOnSuccess(s -> KafkaUtils.ackMessage(m)))
                .subscribe(s -> log.info("Saved actor role {}", s));
    }

    private ActorRole mapActorRole(ActorRoleV1 payload) {
        ActorRole actorRole = new ActorRole();
        BeanUtils.copyProperties(payload, actorRole);
        return actorRole;
    }

    private Flux<ActorRole> getActorRoles(Message<ShowV2> message) {
        Show show = new Show();
        BeanUtils.copyProperties(message.getPayload(), show);
        log.debug("Getting actor roles for show {}", show);
        return kitsuService.getActorRoles(show).doOnComplete(()-> KafkaUtils.ackMessage(message));
    }


    private ActorRoleV1 mapActorRole(ActorRole actorRole) {
        ActorRoleV1 kafkaActorRole = new ActorRoleV1();
        BeanUtils.copyProperties(actorRole, kafkaActorRole);
        kafkaActorRole.setType$1(ActorRoleEventType.CREATE);
        return kafkaActorRole;
    }

}
