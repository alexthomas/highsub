package com.alexk8s.highsub.kafka;

import com.alexk8s.highsub.kafka.model.ShowV2;
import com.alexk8s.highsub.plex.LibraryMediaContainer;
import com.alexk8s.highsub.plex.LibraryMetadata;
import com.alexk8s.highsub.service.PlexService;
import com.alexk8s.highsub.service.ThumbnailService;
import com.alexk8s.highsub.util.KafkaUtils;
import com.alexk8s.highsub.util.ThumbnailUtility;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@Component
@Slf4j
@AllArgsConstructor
public class ImageUpdater {

    private final PlexService plexService;
    private final ThumbnailService thumbnailService;

    @Bean
    public Consumer<Flux<Message<ShowV2>>> showImageUpdater() {
        return flux -> flux.window(Duration.ofSeconds(5))
                .flatMap(Flux::collectList)
                .flatMap(this::ensureThumbnails, 1)
                .subscribe();
    }

    private Mono<Void> ensureThumbnails(List<Message<ShowV2>> messages) {
        Map<String, List<Message<ShowV2>>> messageByShow = messages.stream()
                .collect(Collectors.groupingBy(m -> m.getPayload().getShow()));
        return Flux.zip(Flux.fromIterable(messageByShow.keySet()), getPlexShows().repeat())
                .flatMap(t -> ensureThumbnail(t.getT1(), t.getT2()), 1)
                .then()
                .doOnSuccess(v -> messages.forEach(KafkaUtils::ackMessage));
    }

    private Mono<Void> ensureThumbnail(String show, Map<String, LibraryMetadata> libraryMetadataMap) {
        log.info("Ensuring thumbnail for show {}", show);
        if(!libraryMetadataMap.containsKey(show)) {
            log.warn("No library metadata found for show {}", show);
            return Mono.empty();
        }
        return thumbnailService.ensureThumbnail(ThumbnailUtility.getKey(show), libraryMetadataMap.get(show).thumb())
                .then();
    }

    private Mono<Map<String, LibraryMetadata>> getPlexShows() {
        return plexService.getLibraries()
                .filter(d -> "show".equals(d.type()))
                .flatMap(plexService::getLibraryMetadata)
                .flatMapIterable(LibraryMediaContainer::metadata)
                .collectMap(LibraryMetadata::title)
                ;
    }
}
