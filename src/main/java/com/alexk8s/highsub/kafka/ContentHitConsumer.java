package com.alexk8s.highsub.kafka;

import com.alexk8s.highsub.elasticsearch.IndexedContentHit;
import com.alexk8s.highsub.kafka.model.ContentHitV4;
import com.alexk8s.highsub.model.ContentHit;
import com.alexk8s.highsub.repository.ContentHitRepository;
import com.alexk8s.highsub.util.KafkaUtils;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.data.elasticsearch.client.elc.ReactiveElasticsearchTemplate;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.function.Consumer;

@Component
@AllArgsConstructor
public class ContentHitConsumer {
    private final ContentHitRepository repository;
    private final ReactiveElasticsearchTemplate reactiveElasticsearchTemplate;

    @Bean
    public Consumer<Flux<Message<ContentHitV4>>> contentHitSaver(){
        return flux -> flux.flatMap(this::saveContentHit).subscribe();
    }

    @Bean
    public Consumer<Flux<Message<ContentHitV4>>> contentHitIndexer(){
        return flux -> flux.flatMap(this::indexContentHit).subscribe();
    }

    private Mono<Void> saveContentHit(Message<ContentHitV4> message){
        ContentHit contentHit = mapContentHit(message.getPayload());
        return repository.save(contentHit).doOnSuccess(c-> KafkaUtils.ackMessage(message)).then();
    }

    private Mono<Void> indexContentHit(Message<ContentHitV4> message){
        ContentHitV4 payload = message.getPayload();
        if(payload.getFocused()){

        IndexedContentHit contentHit = new IndexedContentHit();
        BeanUtils.copyProperties(payload,contentHit);
        contentHit.setSubtitle(payload.getSubtitleNumber());
        contentHit.setType(payload.getContentType());
        contentHit.setHitTime(ZonedDateTime.ofInstant(Instant.ofEpochMilli(payload.getHitUtc()), ZoneId.of("UTC")));
        contentHit.setEpisodeId(IndexedContentHit.getEpisodeId(contentHit.getShow(), contentHit.getSeason(), contentHit.getEpisode()));
        contentHit.setSubtitleId(IndexedContentHit.getSubtitleId(contentHit.getShow(), contentHit.getSeason(), contentHit.getEpisode(), contentHit.getSubtitle()));
        return reactiveElasticsearchTemplate.save(contentHit).doOnSuccess(c-> KafkaUtils.ackMessage(message)).then();
        }
        KafkaUtils.ackMessage(message);
        return Mono.empty();
    }

    private ContentHit mapContentHit(ContentHitV4 ContentHitV4){
        ContentHit contentHit = new ContentHit();
        BeanUtils.copyProperties(ContentHitV4,contentHit);
        contentHit.setSubtitle(ContentHitV4.getSubtitleNumber());
        contentHit.setType(ContentHitV4.getContentType());
        contentHit.setHitType(ContentHitV4.getHitType().toString());
        return contentHit;
    }
}
