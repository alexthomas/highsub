package com.alexk8s.highsub.kafka;

import com.alexk8s.highsub.kafka.model.EpisodeEventType;
import com.alexk8s.highsub.kafka.model.EpisodeV2;
import com.alexk8s.highsub.kafka.model.PlexOpeningV1;
import com.alexk8s.highsub.kafka.model.ShowV2;
import com.alexk8s.highsub.model.Episode;
import com.alexk8s.highsub.repository.EpisodeRepository;
import com.alexk8s.highsub.service.EpisodeService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;

import static com.alexk8s.highsub.kafka.model.EpisodeEventType.CREATE;
import static com.alexk8s.highsub.kafka.model.EpisodeEventType.DELETE;
import static com.alexk8s.highsub.kafka.model.EpisodeEventType.REINDEX;
import static com.alexk8s.highsub.kafka.model.EpisodeEventType.RESCALE;
import static com.alexk8s.highsub.kafka.model.EpisodeEventType.UPDATE_JAPANESE_SUBTITLES;
import static com.alexk8s.highsub.kafka.model.EpisodeEventType.UPDATE_TAGS;
import static com.alexk8s.highsub.util.EpisodeUtils.getSlug;
import static com.alexk8s.highsub.util.KafkaUtils.ackMessage;

@Component
@RequiredArgsConstructor
@Slf4j
public class EpisodeProducer {

    private final EpisodeRepository repository;
    private final EpisodeService episodeService;
    private final KafkaTemplate<String, EpisodeV2> kafkaTemplate;

    @Bean
    public Consumer<Flux<Message<EpisodeV2>>> episodeSaver() {
        return episodeV2Flux -> episodeV2Flux
                .flatMap(this::saveEpisode, 1)
                .doOnError(e -> log.error("Error saving episode", e))
                .doOnNext((e) -> log.info("Episodes saved {}", e))
                .subscribe();
    }

    @Bean
    public Function<Flux<Message<ShowV2>>, Flux<EpisodeV2>> episodeParentUpdates() {
        return messageFlux -> messageFlux
                .flatMap(this::updateEpisodes, 1);
    }


    @Bean
    public Function<Flux<Message<EpisodeV2>>, Flux<EpisodeV2>> episodeScaler() {
        Set<String> currentlyScaling = new HashSet<>();
        return messageFlux -> messageFlux
                .filterWhen(this::shouldScale)
                .filter(m -> m.getPayload().getScaled480pVideo() == null || m.getPayload().getScaled150pVideo() == null)
                .doOnEach(x -> log.debug("Currently Scaling {}", currentlyScaling))
                .filter(m -> !currentlyScaling.contains(getScalingId(m)))
                .doOnNext(m -> currentlyScaling.add(getScalingId(m)))
                .flatMap(m -> scaleEpisode(m).doFinally((x) -> currentlyScaling.remove(getScalingId(m))), 1)
                .map(mapEpisode(EpisodeEventType.SCALE));
    }

    @Bean
    public Function<Flux<Message<PlexOpeningV1>>, Flux<EpisodeV2>> episodeChapterUpdates() {
        return messageFlux -> messageFlux
                .flatMap(this::handleOpeningMessage, 1)
                .map(mapEpisode(EpisodeEventType.UPDATE_CHAPTERS));
    }

    private String getScalingId(Message<EpisodeV2> message) {
        return message.getPayload().getShow() + "-" + message.getPayload().getSeason() + "-" + message.getPayload().getEpisode();
    }

    private Flux<EpisodeV2> updateEpisodes(Message<ShowV2> message) {
        ShowV2 show = message.getPayload();
        return (switch (show.getType$1()) {
            case UPDATE_TAGS -> repository.findByShow(show.getShow())
                    .map(e -> e.withParentTags(show.getTags()).withTags(Objects.requireNonNullElse(e.getTags(), Collections.emptyList())))
                    .map(mapEpisode(UPDATE_TAGS));
            case DELETE -> repository.findByShow(show.getShow())
                    .map(e -> e.withParentTags(Collections.emptyList()).withTags(Collections.emptyList()))
                    .map(mapEpisode(DELETE));
            case UPDATE_SEASON_NAME_MAP -> repository.findByShow(show.getShow())
                    .filter(e -> {
                        String newSeasonName = show.getSeasonNameMap().getOrDefault(e.getSeason(), e.getSeason());
                        String newSlug = getSlug(show.getShow(), show.getSeasonNameMap(), e.getSeason(), e.getEpisode());
                        return !Objects.equals(e.getSeasonName(), newSeasonName) || !Objects.equals(e.getSlug(), newSlug);
                    })
                    .map(e -> e.withSeasonName(show.getSeasonNameMap().getOrDefault(e.getSeason(), e.getSeason()))
                            .withSlug(getSlug(show.getShow(), show.getSeasonNameMap(), e.getSeason(), e.getEpisode())))
                    .map(mapEpisode(EpisodeEventType.UPDATE_SLUG));
            case UPDATE_KITSU_ID, UPDATE_ANIME, CREATE, UPDATE_SUMMARY -> {
                ackMessage(message);
                yield Flux.<EpisodeV2>empty();
            }
        }).doOnComplete(() -> ackMessage(message));
    }

    public Mono<Boolean> shouldScale(Message<EpisodeV2> message) {
        EpisodeV2 episode = message.getPayload();
        if (RESCALE == episode.getType$1()) {
            return Mono.just(true);
        }
        if (CREATE != episode.getType$1()) {
            log.info("Skipping scale for {} event", episode.getType$1());
            return Mono.just(false);
        }
        return repository.findByShowAndSeasonAndEpisode(episode.getShow(), episode.getSeason(), episode.getEpisode())
                .map(e -> {
                    if (e.getScaled480pVideo() == null || !Files.exists(Path.of(e.getScaled480pVideo()))) {
                        log.info("480p scaled video not found for episode {} {} {}", e.getShow(), e.getSeason(), e.getEpisode());
                        return true;
                    }
                    if (e.getScaled150pVideo() == null || !Files.exists(Path.of(e.getScaled150pVideo()))) {
                        log.info("150p scaled video not found for episode {} {} {}", e.getShow(), e.getSeason(), e.getEpisode());
                        return true;
                    }
                    ackMessage(message);
                    return false;
                });
    }

    private Mono<Episode> scaleEpisode(Message<EpisodeV2> message) {
        Episode episode = mapEpisode(message.getPayload());
        return episodeService.createSizes(episode)
                .doOnSuccess(e -> ackMessage(message))
                .doOnError(e -> log.error("Error scaling episode", e));
    }

    private Mono<Episode> saveEpisode(Message<EpisodeV2> message) {
        Episode episode = mapEpisode(message.getPayload());
        final Mono<Episode> episodeMono = repository.findByShowAndSeasonAndEpisode(episode.getShow(), episode.getSeason(), episode.getEpisode());
        Mono<Episode> publisher = switch (message.getPayload().getType$1()) {
            case CREATE -> repository.save(episode);
            case SCALE -> episodeMono
                    .map(e -> mapScaledChanges(episode, e))
                    .flatMap(repository::save);
            case UPDATE_SUBTITLE_COUNT -> episodeMono
                    .map(e -> e.withSubtitleCount(episode.getSubtitleCount()).withIndexedSubtitleCount(episode.getIndexedSubtitleCount()))
                    .flatMap(repository::save);
            case UPDATE_JAPANESE_SUBTITLES -> episodeMono
                    .map(e -> e.withJapaneseSubtitles(episode.getJapaneseSubtitles()))
                    .flatMap(repository::save);
            case UPDATE_TAGS -> episodeMono
                    .map(e -> e.withTags(episode.getTags()).withParentTags(episode.getParentTags()))
                    .flatMap(repository::save);
            case UPDATE_CHAPTERS -> episodeMono
                    .map(e -> mapChapters(episode, e))
                    .flatMap(repository::save);
            case DELETE -> repository.delete(episode).then(Mono.empty());
            case UPDATE_SLUG -> episodeMono
                    .map(e -> e.withSlug(episode.getSlug()).withSeasonName(episode.getSeasonName()))
                    .flatMap(repository::save);
            case UPDATE_PREVIEW_SUBTITLE_ID -> episodeMono
                    .map(e -> e.withPreviewSubtitleId(episode.getPreviewSubtitleId()))
                    .flatMap(repository::save);
            case REINDEX, RESCALE -> Mono.empty();
        };
        return publisher.doOnSuccess(e -> ackMessage(message));
    }


    private Episode mapChapters(Episode source, Episode target) {
        if (source.getOpeningStart() != null) {
            target.setOpeningStart(source.getOpeningStart());
            target.setOpeningEnd(source.getOpeningEnd());
        }
        if (source.getEndingStart() != null) {
            target.setEndingStart(source.getEndingStart());
            target.setEndingEnd(source.getEndingEnd());
        }
        return target;

    }

    private Episode mapScaledChanges(Episode source, Episode target) {
        Episode updated = target.withScaled480pVideo(source.getScaled480pVideo())
                .withScaled480pVideoBytes(source.getScaled480pVideoBytes())
                .withScaled150pVideo(source.getScaled150pVideo())
                .withScaled150pVideoBytes(source.getScaled150pVideoBytes());
        updated.setScaled480pHash(source.getScaled480pHash());
        updated.setScaled150pHash(source.getScaled150pHash());
        return updated;
    }

    private Mono<Episode> handleOpeningMessage(Message<PlexOpeningV1> message) {
        PlexOpeningV1 opening = message.getPayload();
        Mono<Episode> mono = switch (opening.getType$1()) {
            case CREATE -> updateChapters(opening);
        };
        return mono.doOnSuccess((a) -> ackMessage(message));
    }

    private Mono<Episode> updateChapters(PlexOpeningV1 opening) {
        return repository.findByShowAndSeasonAndEpisode(opening.getShow(), opening.getSeason(), opening.getEpisode())
                .mapNotNull(episode -> {
                    if ("intro".equals(opening.getMarkerType()))
                        return episode.withOpeningStart(opening.getStart()).withOpeningEnd(opening.getEnd())
                                .withEndingStart(null).withEndingEnd(null);
                    else if ("credits".equals(opening.getMarkerType()))
                        return episode.withEndingStart(opening.getStart()).withEndingEnd(opening.getEnd())
                                .withOpeningStart(null).withOpeningEnd(null);
                    log.error("Unknown marker type {}", opening.getMarkerType());
                    return null;
                });
    }

    public Mono<SendResult<String, EpisodeV2>> createEpisode(Episode episode) {
        EpisodeV2 episodeV2 = mapEpisode(CREATE).apply(episode);
        return Mono.fromFuture(kafkaTemplate.send("episodes-v2", episodeV2.getShow(), episodeV2));
    }

    public Mono<SendResult<String, EpisodeV2>> deleteEpisode(Episode episode) {
        EpisodeV2 episodeV2 = mapEpisode(DELETE).apply(episode);
        return Mono.fromFuture(kafkaTemplate.send("episodes-v2", episodeV2.getShow(), episodeV2));
    }

    public Mono<SendResult<String, EpisodeV2>> updateEpisodeTags(Episode episode) {
        EpisodeV2 episodeV2 = mapEpisode(UPDATE_TAGS).apply(episode);
        return Mono.fromFuture(kafkaTemplate.send("episodes-v2", episodeV2.getShow(), episodeV2));
    }

    public Mono<SendResult<String, EpisodeV2>> updateJapaneseSubtitles(Episode episode) {
        EpisodeV2 episodeV2 = mapEpisode(UPDATE_JAPANESE_SUBTITLES).apply(episode);
        return Mono.fromFuture(kafkaTemplate.send("episodes-v2", episodeV2.getShow(), episodeV2));
    }

    public Mono<SendResult<String, EpisodeV2>> reindexEpisode(Episode episode) {
        EpisodeV2 episodeV2 = mapEpisode(REINDEX).apply(episode);
        return Mono.fromFuture(kafkaTemplate.send("episodes-v2", episodeV2.getShow(), episodeV2));
    }

    public Mono<SendResult<String, EpisodeV2>> rescaleEpisode(Episode episode) {
        EpisodeV2 episodeV2 = mapEpisode(RESCALE).apply(episode);
        return Mono.fromFuture(kafkaTemplate.send("episodes-v2", episodeV2.getShow(), episodeV2));
    }

    private Function<Episode, EpisodeV2> mapEpisode(EpisodeEventType episodeEventType) {
        return episode -> {
            if (episode.getTags() == null)
                episode.setTags(Collections.emptyList());
            if (episode.getParentTags() == null)
                episode.setParentTags(Collections.emptyList());
            EpisodeV2 kafkaEpisode = new EpisodeV2();
            BeanUtils.copyProperties(episode, kafkaEpisode);
            kafkaEpisode.setType$1(episodeEventType);
            return kafkaEpisode;
        };
    }

    private Episode mapEpisode(EpisodeV2 episodeV2) {
        Episode episode = new Episode();
        BeanUtils.copyProperties(episodeV2, episode);
        return episode;
    }
}
