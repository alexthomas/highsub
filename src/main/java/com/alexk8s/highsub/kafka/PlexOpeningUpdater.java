package com.alexk8s.highsub.kafka;

import com.alexk8s.highsub.kafka.model.PlexOpeningV1;
import com.alexk8s.highsub.model.PlexOpening;
import com.alexk8s.highsub.repository.PlexOpeningRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.function.Consumer;

import static com.alexk8s.highsub.util.KafkaUtils.ackMessage;

@Component
@AllArgsConstructor
public class PlexOpeningUpdater {

    private final PlexOpeningRepository plexOpeningRepository;

    @Bean
    public Consumer<Flux<Message<PlexOpeningV1>>> plexOpeningInserter(){
        return openingFlux -> openingFlux
                .flatMap(this::handleMessage,1)
                .subscribe();
    }

    public Mono<Void> handleMessage(Message<PlexOpeningV1> message){
        PlexOpeningV1 opening = message.getPayload();
        Mono<Void> mono = switch (opening.getType$1()){
            case CREATE ->  createOpening(opening);
        };
        return mono.doOnSuccess(x->ackMessage(message));
    }

    private Mono<Void> createOpening(PlexOpeningV1 opening){
        PlexOpening plexOpening = new PlexOpening();
        BeanUtils.copyProperties(opening,plexOpening);
        return plexOpeningRepository.save(plexOpening).then();
    }
}
