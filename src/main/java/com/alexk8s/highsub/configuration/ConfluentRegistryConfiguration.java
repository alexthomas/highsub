package com.alexk8s.highsub.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.stream.schema.registry.client.ConfluentSchemaRegistryClient;
import org.springframework.cloud.stream.schema.registry.client.SchemaRegistryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class ConfluentRegistryConfiguration {

    @Primary
    @Bean
    public SchemaRegistryClient schemaRegistryClient(@Value("${SPRING_CLOUD_STREAM_SCHEMA_REGISTRY_CLIENT_ENDPOINT:http://localhost:8081}") String endpoint){
        ConfluentSchemaRegistryClient client = new ConfluentSchemaRegistryClient();
        client.setEndpoint(endpoint);
        return client;
    }
}
