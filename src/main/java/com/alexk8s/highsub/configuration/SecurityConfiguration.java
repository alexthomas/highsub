package com.alexk8s.highsub.configuration;

import com.alexk8s.highsub.security.CustomerAuthoritiesConverter;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableReactiveMethodSecurity;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.oauth2.server.resource.authentication.ReactiveJwtAuthenticationConverter;
import org.springframework.security.oauth2.server.resource.authentication.ReactiveJwtGrantedAuthoritiesConverterAdapter;
import org.springframework.security.web.server.SecurityWebFilterChain;
import org.springframework.session.data.redis.config.annotation.web.server.EnableRedisWebSession;

@Configuration
@EnableRedisWebSession
@EnableWebFluxSecurity
@EnableReactiveMethodSecurity
@Slf4j
@AllArgsConstructor
@Profile("server")
public class SecurityConfiguration {
    private final HighsubConfig highsubConfig;

    @Bean
    public SecurityWebFilterChain securityWebFilterChain(ServerHttpSecurity http) {
        // Disable default security.
        http.httpBasic().disable();
        http.formLogin().disable();
        http.csrf().disable();
        http.logout().disable();

        return http
                .headers().cache().disable().and()
                .authorizeExchange()
                .pathMatchers(HttpMethod.OPTIONS, "/v1/**","/v2/**").permitAll()
                .pathMatchers(HttpMethod.GET, "/v1/**", "/v2/**","/actuator/health/*").permitAll()
                .anyExchange().authenticated().and()

                .oauth2ResourceServer(oauth2 -> oauth2
                        .jwt().jwtAuthenticationConverter(jwtAuthenticationConverter())
                )
                .build();
    }

    public ReactiveJwtAuthenticationConverter jwtAuthenticationConverter() {
        ReactiveJwtAuthenticationConverter jwtAuthenticationConverter = new ReactiveJwtAuthenticationConverter();
        CustomerAuthoritiesConverter customerAuthoritiesConverter = new CustomerAuthoritiesConverter();
        ReactiveJwtGrantedAuthoritiesConverterAdapter reactiveAuthoritiesAdapter = new ReactiveJwtGrantedAuthoritiesConverterAdapter(customerAuthoritiesConverter);
        jwtAuthenticationConverter.setJwtGrantedAuthoritiesConverter(reactiveAuthoritiesAdapter);
        return jwtAuthenticationConverter;
    }
}
