package com.alexk8s.highsub.configuration;

import com.timgroup.statsd.NoOpStatsDClient;
import com.timgroup.statsd.NonBlockingStatsDClientBuilder;
import com.timgroup.statsd.StatsDClient;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class StatsDConfiguration {

    @Bean
    public StatsDClient statsDClient(@Value("${DD_AGENT_HOST:}") String host) {
        if (StringUtils.isBlank(host))
            return new NoOpStatsDClient();
        return new NonBlockingStatsDClientBuilder()
                .prefix("highsub")
                .hostname(host)
                .port(8125)
                .build();

    }
}
