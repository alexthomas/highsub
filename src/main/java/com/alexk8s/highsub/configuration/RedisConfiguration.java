package com.alexk8s.highsub.configuration;

import com.alexk8s.highsub.ffmpeg.FfmpegOutput;
import info.movito.themoviedbapi.model.Credits;
import info.movito.themoviedbapi.model.tv.TvEpisode;
import info.movito.themoviedbapi.model.tv.TvSeries;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializationContext;
import org.springframework.data.redis.serializer.StringRedisSerializer;

@Configuration
public class RedisConfiguration {
    @Bean
    @Primary
    public ReactiveRedisConnectionFactory reactiveRedisConnectionFactory(@Value("${REDIS_HOST:localhost}") String redisHost) {
        return new LettuceConnectionFactory(redisHost, 6379);
    }

    @Bean
    public ReactiveRedisTemplate<String, FfmpegOutput> stringFfmpegOutputReactiveRedisTemplate(ReactiveRedisConnectionFactory factory) {
        return stringJsonRedisTemplate(factory,FfmpegOutput.class);
    }
    @Bean
    public ReactiveRedisTemplate<String, TvEpisode> stringTvEpisodeReactiveRedisTemplate(ReactiveRedisConnectionFactory factory) {
        return stringJsonRedisTemplate(factory,TvEpisode.class);
    }

    @Bean
    public ReactiveRedisTemplate<String, TvSeries> tvSeriesTemplate(ReactiveRedisConnectionFactory factory) {
        return stringJsonRedisTemplate(factory, TvSeries.class);
    }
    @Bean
    public ReactiveRedisTemplate<String, Credits> creditsTemplate(ReactiveRedisConnectionFactory factory) {
        return stringJsonRedisTemplate(factory, Credits.class);
    }

    private <T> ReactiveRedisTemplate<String,T>  stringJsonRedisTemplate(ReactiveRedisConnectionFactory factory,Class<T> clazz){
        Jackson2JsonRedisSerializer<T> valueSerializer = new Jackson2JsonRedisSerializer<>(clazz);
        StringRedisSerializer keySerializer = new StringRedisSerializer();

        RedisSerializationContext<String, T> context = RedisSerializationContext.<String, T>
                        newSerializationContext(new JdkSerializationRedisSerializer())
                .key(keySerializer).value(valueSerializer).build();
        return new ReactiveRedisTemplate<>(factory, context);
    }
    @Bean
    public ReactiveRedisTemplate<String, Integer> stringIntegerReactiveRedisTemplate(ReactiveRedisConnectionFactory factory) {

        StringRedisSerializer keySerializer = new StringRedisSerializer();

        RedisSerializationContext<String, Integer> context = RedisSerializationContext.<String, Integer>
                        newSerializationContext(new JdkSerializationRedisSerializer())
                .key(keySerializer).build();
        return new ReactiveRedisTemplate<>(factory, context);
    }

    @Bean
    public ReactiveRedisTemplate<String,Double[]> stringDoubleArrayReactiveRedisTemplate(ReactiveRedisConnectionFactory factory) {
        Jackson2JsonRedisSerializer<Double[]> valueSerializer = new Jackson2JsonRedisSerializer<>(Double[].class);
        StringRedisSerializer keySerializer = new StringRedisSerializer();

        RedisSerializationContext<String, Double[]> context = RedisSerializationContext.<String, Double[]>
                        newSerializationContext(new JdkSerializationRedisSerializer())
                .key(keySerializer).value(valueSerializer).build();
        return new ReactiveRedisTemplate<>(factory, context);
    }
}
