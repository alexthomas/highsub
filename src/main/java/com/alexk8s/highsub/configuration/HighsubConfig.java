package com.alexk8s.highsub.configuration;

import com.alexk8s.highsub.model.Language;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

@Data
@ConfigurationProperties(prefix = "highsub")
public class HighsubConfig {
    private String configRoot;
    private String cacheRoot;
    private String subtitleCacheRoot;
    private String serverUrl;
    private Appwrite appwrite;
    private Images images;
    private Video video;
    private Gif gif;
    private Tools tools;
    private Index index;
    private Minio minio;
    private Tmdb tmdb;
    private Embedding embedding;
    private List<String> blacklistTags;

    private Language nativeLanguage;
    private Language targetLanguage;


    @Data
    public static class Images {
        private boolean enabled;
        private String quality;
        private String format;
        private Integer maxHeight;
    }


    @Data
    public static class Video {
        private Integer maxHeight;
        private String format;
    }

    @Data
    public static class Gif {
        private Integer maxHeight;
        private String format;
    }


    @Data
    public static class Tools {
        private String mkvmerge;
        private String mkvextract;
        private String ffmpeg;
        private String ffprobe;
        private String alass;
    }

    @Data
    public static class Index {
        private String indexUrl;
        private String clickAnalyticsUrl;
        private String token;
    }

    @Data
    public static class Appwrite {
        private String baseUrl;
        private String apiKey;
        private String project;

        private String userDatabase;
        private String collectionsCollection;
        private String collectionClipsCollection;

        private String contentDatabase;
        private String showsCollection;
        private String episodesCollection;
        private String indexingStatusCollection;

    }

    @Data
    public static class Minio {
        private String user;
        private String password;
        private String subtitleBucket;
        private String thumbnailBucket;
        private String serverUrl;
    }

    @Data
    public static class Tmdb {
        private String apiKey;
        private String url;
    }

    @Data
    public static class Embedding {
        private String url;
        private String apiKey;
    }


}
