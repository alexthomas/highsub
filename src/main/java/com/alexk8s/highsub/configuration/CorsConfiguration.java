package com.alexk8s.highsub.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.config.CorsRegistry;
import org.springframework.web.reactive.config.WebFluxConfigurer;

@Configuration
public class CorsConfiguration implements WebFluxConfigurer {
    @Override
    public void addCorsMappings(CorsRegistry corsRegistry) {
        corsRegistry.addMapping("/v*/**")
                .allowedOriginPatterns("https://*.vercel.app","https://*.aniclips.alexk8s.com","https://*.alexk8s.com")
                .allowedOrigins("http://localhost:5173","http://localhost:4173","http://192.168.5.36:5173","http://localhost:8080")
                .allowedMethods("PUT", "GET", "POST", "OPTIONS","DELETE")
                .allowCredentials(true)
                .maxAge(3600);
    }
}
