package com.alexk8s.highsub.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.http.async.SdkAsyncHttpClient;
import software.amazon.awssdk.http.nio.netty.NettyNioAsyncHttpClient;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3AsyncClient;

import java.net.URI;
import java.time.Duration;

@Configuration
public class S3Configuration {

    @Bean
    public S3AsyncClient s3Client(HighsubConfig highsubConfig){
        AwsBasicCredentials awsBasicCredentials = AwsBasicCredentials.create(highsubConfig.getMinio().getUser(), highsubConfig.getMinio().getPassword());
        SdkAsyncHttpClient httpClient = NettyNioAsyncHttpClient.builder()
                .writeTimeout(Duration.ZERO)
                .maxConcurrency(64)
                .build();
        software.amazon.awssdk.services.s3.S3Configuration configuration = software.amazon.awssdk.services.s3.S3Configuration.builder()
                .build();
        return S3AsyncClient.builder()
                .httpClient(httpClient)
                .region(Region.US_EAST_1)
                .credentialsProvider(()->awsBasicCredentials)
                .serviceConfiguration(configuration)
                .forcePathStyle(true)
                .endpointOverride(URI.create(highsubConfig.getMinio().getServerUrl()))
                .build();
    }
}
