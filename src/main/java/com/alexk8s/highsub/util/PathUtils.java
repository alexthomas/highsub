package com.alexk8s.highsub.util;

import java.util.UUID;

public class PathUtils {

    public static final String TEMP_DIRECTORY;
    private static final String OUTPUT_FORMAT = "%s/%s.%s";

    static {
        final String tmpDir = System.getProperty("java.io.tmpdir");
        if (tmpDir.endsWith("/")) {
            TEMP_DIRECTORY = tmpDir.substring(0, tmpDir.length() - 1);
        } else {
            TEMP_DIRECTORY = tmpDir;
        }
    }
    public static String cleanPath(String path){
        return path.replaceAll("[';\\\\/]", "");
    }
    public static String getTempFile(String extension) {
        return String.format(OUTPUT_FORMAT, TEMP_DIRECTORY, UUID.randomUUID(), extension);
    }

    public static String getExtension(String path) {
        return path.substring(path.lastIndexOf(".") + 1);
    }
}
