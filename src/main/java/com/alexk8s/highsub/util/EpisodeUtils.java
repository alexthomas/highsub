package com.alexk8s.highsub.util;

import org.apache.commons.lang3.StringUtils;

import java.util.Map;
import java.util.regex.Pattern;

public class EpisodeUtils {
    public static String getSlug(String show, Map<String, String> seasonNameMap, String season, String episode) {
        if (show.equals("Movies"))
            return season;
        String seasonName = seasonNameMap == null ? null : seasonNameMap.get(season);
        String episodeNumber = StringUtils.leftPad(episode, 2, "0");
        if (seasonName != null && !seasonName.isEmpty())
            return seasonName + " E" + episodeNumber;
        if (Pattern.compile("\\S+\\s\\d+").matcher(season).matches()) {
            String seasonNumber = season.split(" ")[1];
            return "S" + seasonNumber + " E" + episodeNumber;
        }
        return season + " E" + episodeNumber;
    }
}
