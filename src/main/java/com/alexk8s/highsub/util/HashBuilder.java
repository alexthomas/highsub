package com.alexk8s.highsub.util;

import java.math.BigInteger;

public class HashBuilder {
    private static final byte[] MASK = { 1, 1 << 1, 1 << 2, 1 << 3, 1 << 4, 1 << 5, 1 << 6, (byte) (1 << 7) };

    private byte[] bytes;
    private int arrayIndex = 0;
    // The current bit offset used to calculate the byte packing [0-7]
    private int bitIndex = 0;
    protected int length;

    /**
     * Create a hashbuilder.
     *
     * @param bits the number of bits the hash will have [8 - Integer.MAX_VALUE]. If
     *             the builder requires more space than specified copy operations
     *             will take place to grow the builder automatically.
     *             <p>
     *             Allocating to much space also results in the correct hash value
     *             being created but might also result in a slight performance
     *             penalty
     */
    public HashBuilder(int bits) {
        bytes = new byte[(int) Math.ceil(bits / 8d)];
        arrayIndex = bytes.length - 1;
    }

    /**
     * Add a zero bit to the hash
     */
    public void prependZero() {
        decrementArrayIndex();
        bitIndex++;
        length++;
    }

    /**
     * Add a one bit to the hash
     */
    public void prependOne() {
        decrementArrayIndex();
        bytes[arrayIndex] |= MASK[bitIndex++];
        length++;
    }

    private void decrementArrayIndex() {
        if (bitIndex == 8) {
            bitIndex = 0;
            arrayIndex--;
            if (arrayIndex == -1) {
                byte[] temp = new byte[bytes.length + 1];
                System.arraycopy(bytes, 0, temp, 1, bytes.length);
                bytes = temp;
                arrayIndex = 0;
            }
        }
    }

    /**
     * Convert the internal state of the hashbuilder to a big integer object
     *
     * @return a big integer object
     */
    public BigInteger toBigInteger() {
        return new BigInteger(1, bytes);
    }
}
