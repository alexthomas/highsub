package com.alexk8s.highsub.util;

import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;

public class ServerUtils {

    public static String getRequestIp(ServerHttpRequest request){
        HttpHeaders headers = request.getHeaders();
        if(headers.containsKey("X-Original-Forwarded-For")){
            return headers.getFirst("X-Original-Forwarded-For");
        }
        if(headers.containsKey("X-Forwarded-For")){
            return headers.getFirst("X-Forwarded-For");
        }
        return request.getRemoteAddress().getAddress().getHostAddress();
    }
}
