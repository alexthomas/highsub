package com.alexk8s.highsub.util;

import lombok.AllArgsConstructor;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.util.function.Function;

public class AsciiArtUtils {
    private static final int DIFF_BUFFER = 2;

    public static void main(String[] args) {
        AsciiArtCharacterSet asciiArtCharacterSet = new AsciiArtCharacterSet(" .,:;ox%#@".toCharArray(), "Courier", 12, true);
        try {
            BufferedImage sourceImage = ImageIO.read(new URL("https://highsub.alexk8s.com/v1/content/subtitle/Solo%20Leveling-Season%201-5-219.png"));
            String asciiArt = generateGreyArt(sourceImage, asciiArtCharacterSet);
            System.out.println(asciiArt);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String generateBestArt(BufferedImage sourceImage, AsciiArtCharacterSet asciiArtCharacterSet) {
        return generateArt(sourceImage, asciiArtCharacterSet, asciiArtCharacterSet::findBestMatch);
    }

    public static String generateGreyArt(BufferedImage sourceImage, AsciiArtCharacterSet asciiArtCharacterSet) {
        return generateArt(sourceImage, asciiArtCharacterSet, asciiArtCharacterSet::findGreyMatch);
    }

    private static String generateArt(BufferedImage sourceImage, AsciiArtCharacterSet asciiArtCharacterSet, Function<BufferedImage, Character> converter) {
        StringBuilder stringBuilder = new StringBuilder();
        int width = sourceImage.getWidth();
        int height = sourceImage.getHeight();
        int ch = asciiArtCharacterSet.getChunkHeight();
        int cw = asciiArtCharacterSet.getChunkWidth();
        for (int y = 0; y + ch < height; y += ch) {
            for (int x = 0; x + cw < width; x += cw) {
                BufferedImage chunk = sourceImage.getSubimage(x, y, cw, ch);
                stringBuilder.append(converter.apply(chunk));

            }
            stringBuilder.append("\n");
        }
        return stringBuilder.toString();
    }

    // Calculate luminosity for a pixel
    private static int calculateLuminosity(int color) {
        return (int) (0.3 * ((color >> 16) & 0xFF) + 0.59 * ((color >> 8) & 0xFF) + 0.11 * (color & 0xFF));
    }

    // Generate a mask for an image chunk
    public static long generateMask(BufferedImage chunk) {
        int width = chunk.getWidth();
        int height = chunk.getHeight();
        int subChunkSize = (int) Math.ceil((double) (width * height) / 64);
        int[][] luminosities = new int[width / subChunkSize + 1][height / subChunkSize + 1];
        return generateSubChunkMask(chunk, new BorderFillStrategy(luminosities), subChunkSize);
    }

    public static long generateMask(BufferedImage chunk, int threshold) {
        int width = chunk.getWidth();
        int height = chunk.getHeight();
        int subChunkSize = (int) Math.ceil((double) (width * height) / 64);
        return generateSubChunkMask(chunk, new ThresholdFillStrategy(threshold), subChunkSize);
    }

    interface FillStrategy {
        boolean isFill(int x, int y, int luminosity);
    }

    @AllArgsConstructor
    private static class ThresholdFillStrategy implements FillStrategy {
        private final int threshold;

        @Override
        public boolean isFill(int x, int y, int luminosity) {
            return luminosity < threshold;
        }
    }

    @AllArgsConstructor
    private static class BorderFillStrategy implements FillStrategy {
        private final int[][] luminosities;

        @Override
        public boolean isFill(int x, int y, int luminosity) {
            luminosities[x][y] = luminosity;
            return (y > 0 && luminosity > luminosities[x][y - 1] + DIFF_BUFFER) ||
                    (x > 0 && luminosity > luminosities[x - 1][y] + DIFF_BUFFER);
        }
    }


    // Generate a mask for an image chunk using sub-chunks
    private static long generateSubChunkMask(BufferedImage chunk, FillStrategy fillStrategy, int subChunkSize) {
        int width = chunk.getWidth();
        int height = chunk.getHeight();
        if (width * height / subChunkSize > 64)
            throw new IllegalArgumentException("The image is too large to be processed");
        long mask = 0L;
        int bitCount = 0;
        int[][] luminosities = new int[width / subChunkSize + 1][height / subChunkSize + 1];
        for (int y = 0; y < height; y += subChunkSize) {
            for (int x = 0; x < width; x += subChunkSize) {
                int avgLuminosity = calculateAverageLuminosity(chunk, x, y, subChunkSize);
                luminosities[x / subChunkSize][y / subChunkSize] = avgLuminosity;
                boolean isDark = fillStrategy.isFill(x / subChunkSize, y / subChunkSize, avgLuminosity);
                if (isDark) {
                    mask |= (1L << bitCount);
                }
                bitCount++;
            }
        }
        return mask;
    }

    public static int calculateAverageLuminosity(BufferedImage image) {
        return calculateAverageLuminosity(image, 0, 0, image.getWidth() * image.getHeight());
    }

    // Calculate average luminosity for a sub-chunk
    private static int calculateAverageLuminosity(BufferedImage chunk, int startX, int startY, int subChunkSize) {
        if (subChunkSize == 1)
            return calculateLuminosity(chunk.getRGB(startX, startY));
        int width = chunk.getWidth();
        int height = chunk.getHeight();
        long sumLuminosity = 0;
        int count = 0;

        for (int y = startY; y < startY + subChunkSize && y < height; y++) {
            for (int x = startX; x < startX + subChunkSize && x < width; x++) {
                sumLuminosity += calculateLuminosity(chunk.getRGB(x, y));
                count++;
            }
        }

        return (int) (sumLuminosity / count);
    }

    // Calculate the Hamming distance between two masks
    public static int hammingDistance(long mask1, long mask2) {
        return Long.bitCount(mask1 ^ mask2);
    }
}
