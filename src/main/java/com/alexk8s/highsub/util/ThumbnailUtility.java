package com.alexk8s.highsub.util;

import static com.alexk8s.highsub.util.PathUtils.cleanPath;

public class ThumbnailUtility {
    public static String getKey(String show) {
        return cleanPath(show) + ".png";
    }

    public static String getKey(String show, int height) {
        return cleanPath(show) + "_" + height + "px.png";
    }

    public static String getKey(String show, String season, String episode) {
        return String.join("/", show, season, episode) + ".png";
    }

    public static String getActorKey(String actor) {
        return "actor/" + actor.replaceAll("/", "");
    }

    public static String getCharacterKey(String show, String character) {
        return "show/" + show + "/" + character.replaceAll("/", "");
    }
}
