package com.alexk8s.highsub.util;

import com.google.api.core.ApiFuture;
import com.google.api.core.ApiFutureCallback;
import com.google.api.core.ApiFutures;
import com.google.common.util.concurrent.MoreExecutors;
import reactor.core.publisher.Mono;

public class FirebaseUtils {
    public static <T> Mono<T> toMono(ApiFuture<T> apiFuture) {
        return Mono.create(sink -> ApiFutures.addCallback(apiFuture,
                new ApiFutureCallback<T>() {
                    @Override
                    public void onFailure(Throwable t) {
                        sink.error(t);
                    }

                    @Override
                    public void onSuccess(T result) {
                        sink.success(result);
                    }
                },
                MoreExecutors.directExecutor()));
    }

    public static String encodeKey(String key) {
        return key.replaceAll("\\.","%2E")
                .replaceAll("\\$","%24")
                .replaceAll("\\[","%5B")
                .replaceAll("]","%5D")
                .replaceAll("#","%23")
                .replaceAll("/","%2F");
    }
}
