package com.alexk8s.highsub.util;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public class AsciiArtCharacterSet {
    private static final int THRESHOLD = 128;
    private final char[] asciiChars;
    private final String fontName;
    private final int fontSize;
    private final boolean invert;

    @Getter
    private int chunkWidth;
    @Getter
    private int chunkHeight;
    private Map<Long, Character> charMasks;
    private char[] charsByGrey;

    public AsciiArtCharacterSet(char[] asciiChars, String fontName, int fontSize,boolean invert) {
        this.asciiChars = asciiChars;
        this.fontName = fontName;
        this.fontSize = fontSize;
        this.invert=invert;
        calculateChunkSize();
        generateCharacterMasks();
    }

    private void calculateChunkSize() {
        Font font = new Font(fontName, Font.PLAIN, fontSize);
        BufferedImage tempImage = new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = tempImage.createGraphics();
        g2d.setFont(font);

        FontMetrics fontMetrics = g2d.getFontMetrics();
        int maxWidth = 0;
        int maxHeight = fontMetrics.getHeight();

        for (char c : asciiChars) {
            int charWidth = fontMetrics.charWidth(c);
            if (charWidth > maxWidth) {
                maxWidth = charWidth;
            }
        }

        this.chunkWidth = maxWidth;
        this.chunkHeight = maxHeight;

        g2d.dispose();
    }


    private void generateCharacterMasks() {
        charMasks = new HashMap<>();
        Font font = new Font(fontName, Font.PLAIN, fontSize);
        Map<Character, Integer> charGreys = new HashMap<>();
        for (char c : asciiChars) {
            BufferedImage charImage = new BufferedImage(chunkWidth, chunkHeight, BufferedImage.TYPE_INT_ARGB);
            Graphics2D g2d = charImage.createGraphics();
            g2d.setFont(font);
            g2d.setColor(Color.WHITE);
            g2d.fillRect(0, 0, chunkWidth, chunkHeight);
            g2d.setColor(Color.BLACK);
            g2d.drawString(String.valueOf(c), 0, g2d.getFontMetrics().getAscent());
            g2d.dispose();

            long mask = AsciiArtUtils.generateMask(charImage, THRESHOLD);
            charMasks.put(mask, c);
            int grey = AsciiArtUtils.calculateAverageLuminosity(charImage);
            charGreys.put(c, grey);
        }
        charsByGrey = new char[asciiChars.length];
        List<Character> charList = charGreys.entrySet().stream()
                .sorted(Map.Entry.comparingByValue())
                .map(Map.Entry::getKey)
                .toList();
        for (int i = 0; i < charList.size(); i++) {
            charsByGrey[i] = charList.get(i);
        }

        log.info("Generated {} character masks\n{}", charMasks.size(), charMasks.values());
    }

    public char findBestMatch(BufferedImage chunk) {
        long chunkMask = AsciiArtUtils.generateMask(chunk);
        if(invert)
            chunkMask=~chunkMask;
        if (charMasks.containsKey(chunkMask)) {
            return charMasks.get(chunkMask);
        }

        char bestChar = ' ';
        int bestDistance = Integer.MAX_VALUE;

        for (Map.Entry<Long, Character> entry : charMasks.entrySet()) {
            int distance = AsciiArtUtils.hammingDistance(chunkMask, entry.getKey());
            if (distance < bestDistance) {
                bestDistance = distance;
                bestChar = entry.getValue();
            }
        }

        charMasks.put(chunkMask, bestChar);
        return bestChar;
    }

    public char findGreyMatch(BufferedImage chunk) {
        int chunkGrey = AsciiArtUtils.calculateAverageLuminosity(chunk);
        if(invert)
            chunkGrey=255-chunkGrey;
        int index = (int) (chunkGrey / 256d * charsByGrey.length);
        return charsByGrey[index];
    }

}
