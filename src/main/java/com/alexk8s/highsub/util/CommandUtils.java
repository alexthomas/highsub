package com.alexk8s.highsub.util;

import datadog.trace.api.Trace;
import io.opentracing.Span;
import io.opentracing.util.GlobalTracer;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


@Slf4j
@Component
public class CommandUtils {
    @Trace
    public String executeCommand(List<String> commandList) {
        String commandString = buildCommandString(commandList);
        try {
            log.debug("Running command: {}", commandString);
            final ProcessBuilder pb = new ProcessBuilder(commandList).redirectErrorStream(true);
            Process process = pb.start();
            BufferedReader stdOut = new BufferedReader(new InputStreamReader(process.getInputStream()), 64_000);
            StringBuilder output = new StringBuilder();
            while (!process.waitFor(100, TimeUnit.MILLISECONDS)){
                output.append(getOutput(stdOut));
            }
            output.append(getOutput(stdOut));
            if (process.exitValue() != 0) {
                log.error("An error occurred trying to execute command. Exit code: {}", process.exitValue());
                throw new CommandExecutionException("An error occurred executing command:\n\n: " + output);
            }
            return output.toString();
        } catch (IOException | InterruptedException e) {
            log.error("An error occurred attempting to execute command: {}", String.join(commandString), e);
            throw new CommandExecutionException("Couldn't execute command", e);
        }
    }

    @Trace
    public void executeCommand(List<String> commandList, Consumer<String> outputLineHandler) {
        String commandString = buildCommandString(commandList);
        try {
            log.debug("Running command: {}", commandString);
            final ProcessBuilder pb = new ProcessBuilder(commandList).redirectErrorStream(true);
            Process process = pb.start();
            BufferedReader stdOut = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line;
            StringBuilder stdout = new StringBuilder();
            while ((line = stdOut.readLine()) != null) {
                stdout.append(line);
                outputLineHandler.accept(line);
            }
            process.waitFor(20, TimeUnit.MINUTES);
            if (process.exitValue() != 0) {
                log.error("An error occurred trying to execute command. Exit code: {}", process.exitValue());
                throw new CommandExecutionException("An error occurred executing command:\n\n: " + stdout);
            }
        } catch (IOException | InterruptedException e) {
            log.error("An error occurred attempting to execute command: {}", String.join(commandString), e);
            throw new CommandExecutionException("Couldn't execute command", e);
        }
    }

    private String buildCommandString(List<String> commandList) {
        String commandString = commandList.stream()
                .map(s -> s.contains(" ") ? String.format("\"%s\"", s) : s)
                .collect(Collectors.joining(" "));
        Span span = GlobalTracer.get().activeSpan();
        if (span != null) {
            span.setTag("command", commandString);
        }
        return commandString;
    }

    public String executeCommand(String... command) {
        List<String> commandList = Arrays.stream(command)
                .filter(Objects::nonNull)
                .filter(StringUtils::isNotBlank)
                .toList();
        return executeCommand(commandList);
    }

    public void executeCommand(Consumer<String> outputLineHandler, String... command) {
        List<String> commandList = Arrays.stream(command)
                .filter(Objects::nonNull)
                .filter(StringUtils::isNotBlank)
                .toList();
        executeCommand(commandList, outputLineHandler);
    }


    private String getOutput(BufferedReader reader) throws IOException {
        StringBuilder sb = new StringBuilder();
        String s;
        Pattern glyphNotFoundPattern = Pattern.compile("Glyph 0x\\w+ not found, selecting one more font for");
        while ((s = reader.readLine()) != null) {
            if (!glyphNotFoundPattern.matcher(s).find()) {
                log.trace(s);
                sb.append(s);
                sb.append("\n");
            }
        }
        return sb.toString();
    }

}
