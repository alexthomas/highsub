package com.alexk8s.highsub.util;

import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Component
@Slf4j
public class KanaMap {
    private static final char LITTLE_TSU_KATAKANA = 'ッ';
    private static final char LITTLE_TSU_HIRAGANA = 'っ';
    private static final char KANA_N = 'ン';
    private static final Set<Character> KANA_VOWELS = Set.of('ア', 'イ', 'ウ', 'エ', 'オ', 'ン', 'ァ', 'ィ', 'ゥ', 'ェ', 'ォ',
            'あ', 'い', 'う', 'え', 'お', 'ん', 'ぁ', 'ぃ', 'ぅ', 'ぇ', 'ぉ');
    private static final Set<Character> SMALL_VOWELS = Set.of('ァ', 'ィ', 'ゥ', 'ェ', 'ォ', 'ぁ', 'ぃ', 'ぅ', 'ぇ', 'ぉ');
    private static final Set<Character> Y_KANA = Set.of('ヤ', 'ユ', 'ヨ', 'ャ', 'ュ', 'ョ', 'や', 'ゆ', 'よ', 'ゃ', 'ゅ', 'ょ');
    private final Map<String, String> hiraganaToKatana = new HashMap<>();
    private final Map<String, String> katakanaToHiragana = new HashMap<>();
    private final Map<String, String> katakanaToRomaji = new HashMap<>();

    @PostConstruct
    public void loadKana() {
        try {
            Resource kanaFile = new ClassPathResource("highsub/kana.csv");
            BufferedReader reader = new BufferedReader(new InputStreamReader(kanaFile.getInputStream()));
            List<String> lines = reader.lines().toList();
            List<String> header = List.of(lines.get(0).split(","));
            final int hiraganaIndex = header.indexOf("hiragana");
            final int katakanaIndex = header.indexOf("katakana");
            final int romajiIndex = header.indexOf("romaji");
            for (int i = 1; i < lines.size(); i++) {
                String[] split = lines.get(i).split(",");
                String hiragana = split[hiraganaIndex];
                String katakana = split[katakanaIndex];
                String romaji = split[romajiIndex];
                hiraganaToKatana.put(hiragana, katakana);
                katakanaToHiragana.put(katakana, hiragana);
                katakanaToRomaji.put(katakana, romaji);
            }
        } catch (IOException e) {
            log.error("Error katakana kana.csv", e);
            throw new RuntimeException(e);
        }
    }

    public String getHiragana(String katakana) {
        return katakana.chars().mapToObj(Character::toString)
                .map(k -> katakanaToHiragana.getOrDefault(k, k))
                .collect(Collectors.joining());
    }

    public String getRomaji(String katakana) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < katakana.length(); i++) {
            String romaji = getRomaji(katakana, i);
            result.append(romaji);
            if (i < katakana.length() - 1 && isCombinationKana(katakana.substring(i, i + 2))) {
                i++;
            }
        }
        return result.toString();
    }
    String safeGetRomaji(String katakana){
        if(katakanaToRomaji.containsKey(katakana))
            return katakanaToRomaji.get(katakana);
        if(katakana.isBlank())
            return katakana;
        log.warn("No romaji for {}", katakana);
        return "*".repeat(katakana.length());
    }

    public String getRomaji(String katakana, int cursor) {
        if (cursor < katakana.length() - 1) {
            if (isLittleTsu(katakana.charAt(cursor)))
                return safeGetRomaji(katakana.substring(cursor + 1, cursor + 2)).substring(0, 1);
            if (isDoubleNCase(katakana.substring(cursor, cursor + 2)))
                return "n'";
            if (isCombinationKana(katakana.substring(cursor, cursor + 2)))
                return safeGetRomaji(katakana.substring(cursor, cursor + 2));
            if (isLittleVowel(katakana.charAt(cursor + 1))) {
                String romaji = safeGetRomaji(katakana.substring(cursor, cursor + 1));
                return romaji.substring(0, romaji.length() - 1);
            }
        }
        if (katakanaToRomaji.containsKey(katakana.substring(cursor, cursor + 1)))
            return katakanaToRomaji.get(katakana.substring(cursor, cursor + 1));
        log.warn("No romaji for {}", katakana.charAt(cursor));
        return katakana.substring(cursor, cursor + 1);
    }

    public boolean isLittleVowel(char c) {
        return SMALL_VOWELS.contains(c);
    }

    public boolean isDoubleNCase(String katakana) {
        return katakana.charAt(0) == KANA_N &&
                (KANA_VOWELS.contains(katakana.charAt(1)) || Y_KANA.contains(katakana.charAt(1))
                );
    }

    public boolean isCombinationKana(String katakana) {
        if (katakana.length() != 2)
            return false;
        return katakanaToRomaji.containsKey(katakana);
    }

    public boolean isLittleTsu(char c) {
        return c == LITTLE_TSU_HIRAGANA || c == LITTLE_TSU_KATAKANA;
    }
}
