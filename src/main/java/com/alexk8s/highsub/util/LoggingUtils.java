package com.alexk8s.highsub.util;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.avro.specific.SpecificRecordBase;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

@Slf4j
public class LoggingUtils {
    private static final ObjectMapper objectMapper = new ObjectMapper().addMixIn(SpecificRecordBase.class, SpecificRecordBaseMixIn.class);

    public abstract static class SpecificRecordBaseMixIn {
        @JsonIgnore
        abstract void getSchema();

        @JsonIgnore
        abstract void getSpecificData();
    }


    private record LogObject(@JsonProperty("@timestamp") String timestamp, String level,
                             @JsonProperty("logger_name") String loggerName,
                             @JsonUnwrapped Object object) {
    }

    public static void logJson(String logger, Object toLog) {
        LogObject searchLog = new LogObject(DateTimeFormatter.ISO_INSTANT.format(ZonedDateTime.now()), "INFO", logger, toLog);
        try {
            System.out.println(objectMapper.writeValueAsString(searchLog));
        } catch (IOException e) {
            log.error("Error writing object to json", e);
        }
    }
}
