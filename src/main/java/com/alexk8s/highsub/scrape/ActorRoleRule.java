package com.alexk8s.highsub.scrape;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

@Data
@Table("actor_role_rule")
@AllArgsConstructor
@NoArgsConstructor
public class ActorRoleRule {
    public enum Type {IS_DUPLICATE,RENAME_CHARACTER,SET_CHARACTER_ROLE,NONE}

    public static final ActorRoleRule NONE_RULE = new ActorRoleRule(null,null,null,Type.NONE,null);

    @PrimaryKeyColumn(type = PrimaryKeyType.PARTITIONED)
   private String actor;
    @PrimaryKeyColumn(type = PrimaryKeyType.CLUSTERED)
    private String character;
    @PrimaryKeyColumn(type = PrimaryKeyType.CLUSTERED)
    private String show;

    private Type type;
    private String option;


}
