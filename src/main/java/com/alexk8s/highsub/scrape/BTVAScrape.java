package com.alexk8s.highsub.scrape;

import com.alexk8s.highsub.service.ActorService;
import com.alexk8s.highsub.service.S3Service;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.springframework.data.cassandra.core.ReactiveCassandraTemplate;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.FluxSink;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;
import reactor.util.function.Tuples;

import java.io.File;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import static com.alexk8s.highsub.util.ThumbnailUtility.getActorKey;
import static com.alexk8s.highsub.util.ThumbnailUtility.getCharacterKey;

@Component
@Slf4j
@RequiredArgsConstructor
public class BTVAScrape {

    private final ReactiveCassandraTemplate cassandraTemplate;
    private final S3Service s3Service;
    private final ActorService actorService;
    private final ActorRoleRepository actorRoleRepository;

    @PostConstruct
    public void init() {
        System.setProperty("webdriver.chrome.driver", "chromedriver/chromedriver");
        System.setProperty("webdriver.http.factory", "jdk-http-client");
    }

//    @EventListener(ApplicationReadyEvent.class)
    public void scrapeShows() {
         cassandraTemplate.query(ForScrape.class).all()
                .publishOn(Schedulers.boundedElastic())
                .filter(f -> !f.getLoaded())
                .flatMap(f -> scrapeShow(f).then(Mono.just(f)), 3)
                .flatMap(f -> {
                    f.setLoaded(true);
                    return cassandraTemplate.insert(f);
                }).map(ForScrape::getShow).distinct().collectList()
                 .flatMapIterable(Function.identity())
                 .flatMap(s->actorRoleRepository.findByShow(s).collectList().map(l-> Tuples.of(s,l)))
                 .flatMap(t->actorService.saveActorRoles(t.getT1(),t.getT2()).thenReturn(t),1)
                 .subscribe(t -> log.info("Saved actor roles for {}", t.getT1()));
    }

    private Flux<ActorRole> scrapeShow(ForScrape forScrape) {
        Flux<ActorRole> actorRoles = Flux.create(sink -> {
            scrapeShow(forScrape.getShow(), forScrape.getUrl(), sink);
            sink.complete();
        });
        return actorRoles.flatMap(this::saveImages)
                .flatMap(cassandraTemplate::insert)
                ;
    }

    private WebDriver getWebDriver() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remove-allow-origins=*");
        // with a headless window the size can't be changed and with the default size the images won't be captured correctly
//        options.addArguments("--headless=new");
//        options.addArguments("--no-sandbox");
//        options.addArguments("--disable-dev-shm-usage");
        return new ChromeDriver(options);
    }

    @SneakyThrows
    private void scrapeShow(String show, String url, FluxSink<ActorRole> sink) {
        WebDriver driver = getWebDriver();
        driver.get(url);
        Thread.sleep(1000);
        ((JavascriptExecutor) driver).executeScript("return document.querySelectorAll('.flag, .play_sound_clip, .confirmed_credit').forEach(e=>e.remove());");
        ((JavascriptExecutor) driver).executeScript("return document.querySelectorAll('img').forEach(e=>e.removeAttribute('loading'));");
        Thread.sleep(2000);
        List<WebElement> elements = driver.findElements(By.xpath("//*[@id=\"medium_credits_container\"]/div/div"));
        for (WebElement element : elements) {
            ActorRole actorRole = new ActorRole();
            actorRole.setShow(show);
            for (WebElement link : element.findElements(By.xpath("a"))) {
                Optional<TitleImage> titleImage = processLink(link);
                if (titleImage.isPresent()) {
                    if (actorRole.getCharacter() == null) {
                        actorRole.setCharacterImage(titleImage.get().image.getAbsolutePath());
                        actorRole.setCharacter(titleImage.get().title);
                    } else {
                        actorRole.setActorImage(titleImage.get().image.getAbsolutePath());
                        actorRole.setActor(titleImage.get().title);
                    }
                }
            }
            if (actorRole.getCharacter() != null && actorRole.getActor() != null)
                sink.next(actorRole);
        }
        driver.close();
    }

    private record TitleImage(String title, File image) {
    }

    @SneakyThrows
    private Optional<TitleImage> processLink(WebElement link) {
        for (WebElement image : link.findElements(By.xpath("img"))) {
            String src = image.getAttribute("src");
            if (!src.contains("_img/languages")) {
                File img = image.getScreenshotAs(OutputType.FILE);
                return Optional.of(new TitleImage(link.getAttribute("title"), img));
            }
        }
        return Optional.empty();
    }

    private Mono<ActorRole> saveImages(ActorRole actorRole) {
        String actorKey = getActorKey(actorRole.getActor());
        String characterKey = getCharacterKey(actorRole.getShow(), actorRole.getCharacter());
        return Flux.concat(s3Service.saveScrapedImage(actorRole.getActorImage(), actorKey), s3Service.saveScrapedImage(actorRole.getCharacterImage(), characterKey))
                .then(Mono.just(actorRole))
                .doOnNext(a -> {
                    a.setActorImage(actorKey);
                    a.setCharacterImage(characterKey);
                });
    }


}
