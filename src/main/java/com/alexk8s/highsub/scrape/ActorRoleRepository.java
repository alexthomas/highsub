package com.alexk8s.highsub.scrape;

import org.springframework.data.cassandra.repository.ReactiveCassandraRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface ActorRoleRepository extends ReactiveCassandraRepository<ActorRole,String> {

    Flux<ActorRole> findByShow(String show);
    Flux<ActorRole> findByActor(String actor);
    Mono<ActorRole> findByShowAndActorAndCharacter(String show, String actor, String character);
}
