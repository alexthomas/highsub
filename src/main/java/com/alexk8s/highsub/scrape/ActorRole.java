package com.alexk8s.highsub.scrape;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.With;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

@Table("actor_role")
@Data
@With
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ActorRole {
    @PrimaryKeyColumn(type = PrimaryKeyType.PARTITIONED)
    private String show;
    @PrimaryKeyColumn(type = PrimaryKeyType.CLUSTERED)
    private String actor;
    @PrimaryKeyColumn(type = PrimaryKeyType.CLUSTERED)
    private String character;

    @Column("actor_image")
    private String actorImage;
    @Column("character_image")
    private String characterImage;
    @Column("kitsu_character_id")
    private String kitsuCharacterId;
    @Column("kitsu_person_id")
    private String kitsuPersonId;
    @Column("character_role")
    private String characterRole;
    private String source;

}
