package com.alexk8s.highsub.scrape;

import lombok.Data;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

@Table("for_scrape")
@Data
public class ForScrape {
    @PrimaryKey
    private String url;
    private String show;
    private Boolean loaded;
}
