package com.alexk8s.highsub.scrape;

import org.springframework.data.cassandra.repository.ReactiveCassandraRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface ActorRoleRuleRepository extends ReactiveCassandraRepository<ActorRoleRule,String> {
    Mono<ActorRoleRule> findByShowAndActorAndCharacter(String show, String actor, String character);
}
