package com.alexk8s.highsub.filter;

import io.opentracing.Span;
import io.opentracing.util.GlobalTracer;
import org.jetbrains.annotations.NotNull;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;
import reactor.core.publisher.Mono;

@Component
public class RequestIdFilter implements WebFilter {
    private static final String REQUEST_ID_HEADER = "x-request-id";

    @NotNull
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, @NotNull WebFilterChain chain) {
        HttpHeaders headers = exchange.getRequest().getHeaders();
        String requestId = headers.getFirst(REQUEST_ID_HEADER);
        Span span = GlobalTracer.get().activeSpan();
        headers.forEach((k, v) -> {
            String kl = k.toLowerCase();
            if (kl.startsWith("x-forwarded-") || kl.startsWith("x-real-ip") || kl.startsWith("x-original-")) {
                span.setTag("forwardedHeaders." + k, v.get(0));
            }
        });
        if (requestId != null) {
            span.setTag("requestId", requestId);

        }

        return chain.filter(exchange);
    }
}
