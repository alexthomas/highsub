grammar SearchQuery;


query: (query_part WS?)+;

query_part
    :required=STRING_LITERAL
    |PLUS required=WORD
    |tag=TAG
    |filter
    |word=WORD
    ;

filter: filterField=(NAME|SHOW|SEASON|EPISODE|ROMAJI|JAPANESE)  filterValue=(WORD|STRING_LITERAL);

//facets
NAME: 'name:' ;
SHOW: ('show'|'series') ':';
SEASON: 'season:';
EPISODE: 'episode:';
ROMAJI: 'romaji:';
JAPANESE: ('japanese'|'kanji') ':';

PLUS: '+';
STRING_LITERAL: STRING_LITERAL_BOUNDARY ( STRING_LITERAL_CHARACTER )* STRING_LITERAL_BOUNDARY;
TAG: '#' TAG_CHARACTER+;
WORD: WORD_FIRST_CHARACTER WORD_CHARACTER*;
WS: [ \t\n\r]+ ;

fragment WORD_CHARACTER: ~[ \t\n\r:];
fragment WORD_FIRST_CHARACTER: ~[ \t\n\r+#:];
fragment TAG_CHARACTER: [a-zA-Z0-9_\-];
fragment STRING_LITERAL_BOUNDARY: [\n\r"\u201D] | EOF;
fragment STRING_LITERAL_CHARACTER: ~[\n\r"\u201D];
