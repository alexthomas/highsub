FROM openjdk:17-slim-bullseye
EXPOSE 8080
RUN  apt update && apt install -y ffmpeg vim
COPY Minecraftia-Regular.ttf /usr/local/share/fonts/Minecraftia-Regular.ttf
RUN fc-cache -f -v
#RUN  apt-get update && \
#    apt-get install -y gnupg wget curl unzip --no-install-recommends && \
#    wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - && \
#    echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list && \
#    apt-get update -y && \
#    apt-get install -y google-chrome-stable && \
#    CHROMEVER=$(google-chrome --product-version | grep -o "[^\.]*\.[^\.]*\.[^\.]*") && \
#    DRIVERVER=$(curl -s "https://chromedriver.storage.googleapis.com/LATEST_RELEASE_$CHROMEVER") && \
#    wget -q --continue -P /chromedriver "http://chromedriver.storage.googleapis.com/$DRIVERVER/chromedriver_linux64.zip" && \
#    unzip /chromedriver/chromedriver* -d /chromedriver
ADD https://github.com/kaegi/alass/releases/download/v2.0.0/alass-linux64 ./
RUN chmod +x alass-linux64
ADD clear-cache.sh clear-cache.sh
RUN chmod +x clear-cache.sh
COPY build/libs/highsub.jar highsub.jar

ENTRYPOINT java -jar highsub.jar -Xmx256m