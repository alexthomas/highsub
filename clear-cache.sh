#!/bin/bash

TARGET_DIR="/content/cache/"
SHOW=""
BYTE_COUNT=0
FILE_COUNT=0

# Function to convert bytes to human readable format
to_human_readable() {
    local bytes=$1
    if [ "$bytes" -lt 1024 ]; then
        echo "${bytes}B"
    elif [ "$bytes" -lt 1048576 ]; then
        echo $((bytes/1024))"KB"
    elif [ "$bytes" -lt 1073741824 ]; then
        echo $((bytes/1048576))"MB"
    else
        echo $((bytes/1073741824))"GB"
    fi
}

# Parse the command line arguments
while [[ "$#" -gt 0 ]]; do
    case $1 in
        -show) SHOW="$2"; shift ;;
        *) echo "Unknown parameter passed: $1"; exit 1 ;;
    esac
    shift
done

if [ ! -z "$SHOW" ]; then
    TARGET_DIR="/content/cache/$SHOW/"
fi

# Ensure the directory exists
if [ ! -d "$TARGET_DIR" ]; then
    echo "Directory $TARGET_DIR does not exist!"
    exit 1
fi

# Find the files and delete them, keeping track of count and bytes
find "$TARGET_DIR" -type f -name "*.mp4" -print0 | while IFS= read -r -d '' file; do
    BYTES=$(du -b "$file" | cut -f1)
    BYTE_COUNT=$((BYTE_COUNT + BYTES))
    rm -f "$file"
    FILE_COUNT=$((FILE_COUNT + 1))
done

# Convert total bytes to human-readable format
HR_BYTE_COUNT=$(to_human_readable $BYTE_COUNT)

# Output the results
echo "Deleted $FILE_COUNT files totaling $HR_BYTE_COUNT."

