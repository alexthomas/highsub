SELECT
    shows.title AS show,
    ('Season ' || seasons."index") AS season,
    CAST(episodes."index" AS TEXT) AS episode,
       taggings.text AS marker_type,
       taggings.id AS id,
       taggings.time_offset AS start,
       taggings.end_time_offset AS end,
       taggings.extra_data
FROM taggings
         INNER JOIN metadata_items episodes ON taggings.metadata_item_id = episodes.id
         INNER JOIN metadata_items seasons ON episodes.parent_id = seasons.id
         INNER JOIN metadata_items shows ON seasons.parent_id = shows.id
WHERE tag_id = (SELECT id FROM tags WHERE tag_type=12)
